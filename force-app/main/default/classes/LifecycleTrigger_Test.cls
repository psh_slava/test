@isTest
public class LifecycleTrigger_Test
{
   public static testmethod void leadmethod()
   {
     Lead ld = new Lead(lastname = 'ldTest1', company ='abcd') ;
               
        insert ld ;
 
        lifecycle__c lyf1 = new lifecycle__c(Related_Lead__c =ld.id, Lifecycle_Status__c = 'Active', Lifecycle_Stage__c = 'AQL',Start_Date__c=system.today(),AQL_Entry_Date__c=system.today()) ;
       
        insert lyf1;
        
        lyf1.AQL_Entry_Date__c=system.today()+1;
        
        Update lyf1;
        
        ld.AQL_Date__c = lyf1.AQL_Entry_Date__c;
     
         Update ld;
        
   }
   
   public static testmethod void Contactmethod()
   {
      Account acc = new Account (Name ='testAc1');
        insert acc;
        
       Contact con = new Contact(lastname = 'conTest1', AccountID = acc.id) ;
        
        insert con ;
                
        lifecycle__c lyf1 = new lifecycle__c( Related_Contact__c = con.id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today(),AQL_Entry_Date__c=system.today()) ;
       
        insert lyf1 ;
        
         lyf1.AQL_Entry_Date__c=system.today()+1;
        
        Update lyf1;
        
        con.AQL_Date__c = lyf1.AQL_Entry_Date__c;
     
         Update con;
   }

}