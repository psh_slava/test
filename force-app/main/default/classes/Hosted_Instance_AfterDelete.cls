/** 
* Hosted_Instance_AfterDelete  Trigger Handler
*
* @author Naomi Harmon 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Hosted_Instance_AfterDelete  extends TriggerHandlerBase {

    public override void mainEntry(TriggerParameters tp) {
       	if (TriggerHelper.DoExecute('Hosted_Instance__c.CentralSync')) {
            CentralSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.oldList);
        }
    }
}