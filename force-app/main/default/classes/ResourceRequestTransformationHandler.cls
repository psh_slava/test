/**
* @author CRMCulture
* @version 1.00
* @description This Class 
*/
public class ResourceRequestTransformationHandler implements ICSMCustomTransformationHandler{

	public static List<String> contractFieldList = new List<String>{
		'Contract1',
		'Contract2',
		'Contract3',
		'Contract4',
		'Contract5',
		'Contract6',
		'Contract7',
		'Contract8',
		'Contract9',
		'Contract10',
		'Contract11',
		'Contract12'

	};

	public CSMBusinessObjectRecordWrapper doAdditionalTransformFromSFtoCSM(SObject obj, CSMBusinessObjectRecordWrapper borw) {
    	// put transform logic here
 		System.debug('DEBUG: ResourceRequestTransformationHandler obj' + obj);
 		System.debug('DEBUG: ResourceRequestTransformationHandler borw' + borw);
 		Resource_Request__c r = (Resource_Request__c)obj;
 		if(String.isNotEmpty(r.Legal_Contracts__c)){
 			String [] contracts = r.Legal_Contracts__c.split(';');
 			List<CSMBusinessObjectRecordWrapper.FieldWrapper> fwList = borw.fields;
 			for(Integer i = 0; i < contracts.size(); i++){
 				for(CSMBusinessObjectRecordWrapper.FieldWrapper fw : fwList){
 					if(contractFieldList[i] == fw.name){
 						fw.value = contracts[i];
 						//r.Contract1__c = contracts[i];
 					}
 				}
 			}
 		}
    	return borw;
 	}
 
 	public SObject doAdditionalTransformFromCSMtoSF(CSMBusinessObjectRecordWrapper borw, SObject obj) {
   		// put transform logic here
   		System.debug('DEBUG: ResourceRequestTransformationHandler obj' + obj);
 		System.debug('DEBUG: ResourceRequestTransformationHandler borw' + borw);
 		List<CSMBusinessObjectRecordWrapper.FieldWrapper> fwList = borw.fields;
 		Resource_Request__c r = (Resource_Request__c)obj;
 		List<String> contracts = new List<String>();
 		for(CSMBusinessObjectRecordWrapper.FieldWrapper fw : fwList){
 			for(String s : contractFieldList){
 				if(fw.Name == s && String.isNotBlank(fw.value)){
 					contracts.add(fw.value);
 				}
 			}
 		}
 		if(contracts.size()>0){
 			String contractString = String.join(contracts, ';');
 			r.Legal_Contracts__c = contractString;
 		}
   		return obj;
 	}
}