@isTest

public class ManageHostedInstancesTest { 
    
    public static testmethod void contractHostedInstance(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Total_Contract_Value__c = 100000;
        o.PSO_Total_Amount__c = 0;
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discover';
        o.Forecast_Category__c = 'Omit';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        //q.SBQQ__Opportunity2__c = o.Id;
        q.Billing_Contact__c = c.Id;
        q.License_Key_Contact__c = c.Id;
        q.PO_Required__c = 'No';
        q.Tax_Exempt__c = 'No';
        q.Term__c = '3';
        q.SBQQ__BillingCity__c = 'Colorado Springs';
        q.SBQQ__BillingCountry__c = 'United States';
        q.SBQQ__BillingStreet__c = '123 Test Street';
        q.SBQQ__BillingPostalCode__c = '12345';
        q.SBQQ__BillingState__c = 'Colorado';
        q.SBQQ__BillingName__c = 'Test Account';
        q.Regional_Quote_Terms__c = 'North America';
        q.Preferred_Prod_Server_DNS_Name_for_CAM__c = 'testcam-prod.cherwellondemand.com';
        q.Preferred_Prod_Server_DNS_Name_for_CSM__c = 'testcsm-prod.cherwellondemand.com';
        q.Preferred_Test_Server_DNS_Name_for_CSM__c = 'testcsm-test.cherwellondemand.com';
        q.Time_Zone__c = '(UTC-07:00) Mountain Time (US & Canada)';
        q.Server_Location__c = 'Canada';
        q.Server_Language__c = 'English';
        insert q;
        
        Product2 csm = new Product2();
        csm.Name = 'CSM Subscription License (Legacy)';
        csm.Quote_Group_Name__c = 'Licensed Software';
        csm.Family = 'CSM';
        insert csm;
        
        Product2 csmH = new Product2();
        csmH.Name = 'CSM Hosting Service';
        csmH.Family = 'Hosting Services';
        insert csmH;
        
        Product2 cam = new Product2();
        cam.Name = 'CAM Subscription License';
        cam.Quote_Group_Name__c = 'Licensed Software';
        cam.Family = 'CAM';
        insert cam;
        
        Product2 camH = new Product2();
        camH.Name = 'CAM Hosting Service';
        camH.Family = 'Hosting Services';
        insert camH;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        con.SBQQ__Quote__c = q.Id;
        con.SBQQ__Opportunity__c = o.Id;
        con.StartDate = Date.today();
        con.ContractTerm = 12;
        con.Hosting_Model__c = 'Cherwell Hosted';
        insert con;
        
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = csm.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = con.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        subs.add(s); 
        
        SBQQ__Subscription__c s2 = new SBQQ__Subscription__c();
        s2.SBQQ__Quantity__c = 1;
        s2.SBQQ__Product__c = csmH.Id;
        s2.SBQQ__Account__c = a.Id;
        s2.SBQQ__Contract__c = con.Id;
        s2.Instance_Number__c = 'Instance 1';
        s2.Instance_Name__c = 'TestInst';
        subs.add(s2); 
        
        SBQQ__Subscription__c s3 = new SBQQ__Subscription__c();
        s3.SBQQ__Quantity__c = 1000;
        s3.SBQQ__Product__c = cam.Id;
        s3.SBQQ__Account__c = a.Id;
        s3.SBQQ__Contract__c = con.Id;
        s3.Instance_Number__c = 'Instance 1';
        s3.Instance_Name__c = 'TestInst';
        subs.add(s3); 
        
        SBQQ__Subscription__c s4 = new SBQQ__Subscription__c();
        s4.SBQQ__Quantity__c = 1;
        s4.SBQQ__Product__c = camH.Id;
        s4.SBQQ__Account__c = a.Id;
        s4.SBQQ__Contract__c = con.Id;
        s4.Instance_Number__c = 'Instance 1';
        s4.Instance_Name__c = 'TestInst';
        subs.add(s4);
        
        insert subs;
        
        con.Process_SaaS__c = true;
        update con;
        
        List<Hosted_Instance__c> hi = [Select Id From Hosted_Instance__c Where Contract__c = :con.Id];
        System.assertEquals(3, hi.size());
    
    }
    
    public static testmethod void contractOnPremInstance(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Total_Contract_Value__c = 100000;
        o.PSO_Total_Amount__c = 0;
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discover';
        o.Forecast_Category__c = 'Omit';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.Billing_Contact__c = c.Id;
        q.License_Key_Contact__c = c.Id;
        q.PO_Required__c = 'No';
        q.Tax_Exempt__c = 'No';
        q.Term__c = '3';
        q.SBQQ__BillingCity__c = 'Colorado Springs';
        q.SBQQ__BillingCountry__c = 'United States';
        q.SBQQ__BillingStreet__c = '123 Test Street';
        q.SBQQ__BillingPostalCode__c = '12345';
        q.SBQQ__BillingState__c = 'Colorado';
        q.SBQQ__BillingName__c = 'Test Account';
        q.Regional_Quote_Terms__c = 'North America';
        insert q;
        
        Product2 csm = new Product2();
        csm.Name = 'CSM Subscription License (Legacy)';
        csm.Quote_Group_Name__c = 'Licensed Software';
        csm.Family = 'CSM';
        insert csm;
        
        Product2 cam = new Product2();
        cam.Name = 'CAM Subscription License (Legacy)';
        cam.Quote_Group_Name__c = 'Licensed Software';
        cam.Family = 'CAM';
        insert cam;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        con.SBQQ__Quote__c = q.Id;
        con.SBQQ__Opportunity__c = o.Id;
        con.StartDate = Date.today();
        con.ContractTerm = 12;
        con.Hosting_Model__c = 'On Premise';
        insert con;
        
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = csm.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = con.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        subs.add(s); 
            
        SBQQ__Subscription__c s3 = new SBQQ__Subscription__c();
        s3.SBQQ__Quantity__c = 1000;
        s3.SBQQ__Product__c = cam.Id;
        s3.SBQQ__Account__c = a.Id;
        s3.SBQQ__Contract__c = con.Id;
        s3.Instance_Number__c = 'Instance 1';
        s3.Instance_Name__c = 'TestInst';
        subs.add(s3); 
        
        insert subs;
        
        con.Process_SaaS__c = true;
        update con;
        
        List<Hosted_Instance__c> hi = [Select Id From Hosted_Instance__c Where Contract__c = :con.Id];
        System.assertEquals(2, hi.size());
        
    }    

}