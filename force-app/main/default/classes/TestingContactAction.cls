@isTest
Global class TestingContactAction
{
    @TestSetup
    public static void setup()
    {
        list<ContactLifeCycleMapping__c> lyfCycleMapping =new list<ContactLifeCycleMapping__c> ();
        
        ContactLifeCycleMapping__c filedMap4=new ContactLifeCycleMapping__c(Name='Lifecycle_Stage__c', isDefault__c=True, Default_Value__c='Inquiry');
        ContactLifeCycleMapping__c filedMap5=new ContactLifeCycleMapping__c(Name='Lifecycle_Status__c',  isDefault__c=True, Default_Value__c='Inactive');
        ContactLifeCycleMapping__c filedMap6=new ContactLifeCycleMapping__c(Name='OwnerId', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='OwnerId');
        ContactLifeCycleMapping__c filedMap7=new ContactLifeCycleMapping__c(Name='Person_Status__c', isDefault__c=False,OnCreate__c=True, Contact_Api_Name__c='Contact_Status__c');
        ContactLifeCycleMapping__c filedMap8=new ContactLifeCycleMapping__c(Name='Priority__c', isDefault__c=False,  OnCreate__c=True, Contact_Api_Name__c='Priority__c');
        ContactLifeCycleMapping__c filedMap9=new ContactLifeCycleMapping__c(Name='Related_Account__c', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='AccountId');
        ContactLifeCycleMapping__c filedMap10=new ContactLifeCycleMapping__c(Name='Related_Contact__c', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='Id');
        
        lyfCycleMapping.add(filedMap4); 
        lyfCycleMapping.add(filedMap5); 
        lyfCycleMapping.add(filedMap6); 
        lyfCycleMapping.add(filedMap7); 
        lyfCycleMapping.add(filedMap8); 
        lyfCycleMapping.add(filedMap9); 
        lyfCycleMapping.add(filedMap10);
        insert lyfCycleMapping;
        
        string tstLabel = Label.BDR_Profile_IDs; 
    }
    
    @isTest
    public static void TestingNewLsOnLeadInsert(){
        
        List<Profile> p =new List<Profile>();
        
        p = [SELECT Id FROM Profile WHERE Name Like '%Inside Sales%' limit 1]; 
        
        String orgId = userInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));       
        String uniqueName = orgId + dateString + randomInt;
        String email_str = uniqueName + '@test' + orgId + '.org';
        
        User u = new User(Alias = uniqueName.substring(18, 23), Email= email_str , 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p[0].Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName= email_str );
        
        insert u; 
        
        account acc=new account(name='Test Account 1');
        insert acc;
        
        list<contact> conList=new list<contact>();
        
        contact cont1=new contact(lastname='Test Cont 1',phone='1234567890',Community_Groups__c='CSIG: Education;CSIG: Healthcare',mkto71_Lead_Score__c =100, Contact_Status__c = 'New', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont1);
        contact cont2=new contact(lastname='Test Cont 2',phone='1234567890',Contact_Status__c = 'Working', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont2);
        contact cont3=new contact(lastname='Test Cont 3',phone='1234567890',Contact_Status__c = 'Engaged', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id,mkto71_Lead_Score__c=20);
        conList.add(cont3);
        contact cont4=new contact(lastname='Test Cont 4',phone='1234567890',Contact_Status__c ='Invalid Request', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont4);
        contact cont5=new contact(lastname='Test Cont 5',phone='1234567890',Contact_Status__c = 'Back To Nurture', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont5);
        contact cont6=new contact(lastname='Test Cont 6',phone='1234567890',mkto71_Lead_Score__c =110, Contact_Status__c = 'New', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont6);
        
        contact cont8=new contact(lastname='Test Cont 7',phone='1234567890',mkto71_Lead_Score__c =30, Behavior_Score__c = 12 ,Contact_Status__c = 'New', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont8);
        contact cont9 =new contact(lastname='Test Cont 7',phone='1234567890',mkto71_Lead_Score__c =100, Behavior_Score__c = 100 ,Contact_Status__c = 'New', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id);
        conList.add(cont9);
        insert conList;
        
        cont1.Community_Member__c = True;
        cont1.Community_Groups__c = 'Forums';
        Update cont1;
        
        cont2.Community_Member__c = True;
        cont2.Community_Groups__c = 'Forums';
        Update cont2;
        
        conList[1].Contact_Status__c ='Engaged';
        conList[2].Contact_Status__c ='Working';
     
        conList[0].Contact_Status__c ='Invalid Request';
        conList[5].Contact_Status__c ='Back To Nurture';
        conList[5].Priority__c = 'High Priority';
        
        conList[3].Contact_Status__c ='Marketing Qualified';
        conList[3].mkto71_Lead_Score__c = 10;
        conList[3].IsLead_generated__c = false;
        conList[2].OwnerId = u.Id;
        conList[7].mkto71_Lead_Score__c =30;
        conList[7].Behavior_Score__c = 12;
        update conList; 

        delete conList[1];
        
        contact cont7=new contact(lastname='Test Cont 7',phone='1234567890',mkto71_Lead_Score__c =100, Contact_Status__c = 'Working', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', IsLead_Generated__c=False);
        insert cont7;
        cont7.Contact_Status__c = 'New';
        update cont7;
        
    }
    
    
}