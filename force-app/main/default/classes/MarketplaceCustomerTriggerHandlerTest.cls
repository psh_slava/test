@isTest
public class MarketplaceCustomerTriggerHandlerTest {
    
    public static testmethod void testNewMarketplaceCustomers(){
        
        Account a = new Account(name = 'TestCompany123');
        insert a;
        
        Contact c = new Contact(lastname = 'Tester1', accountId = a.Id, email = 'tester1@test.test');
        insert c;
        
        List<Marketplace_Customer__c> customers = new List<Marketplace_Customer__c>();
        
        Marketplace_Customer__c c1 = new Marketplace_Customer__c();
        c1.Company_Name__c = 'TestCompany123';
        c1.Email__c = 'tester1@test.test';
        customers.add(c1);
        
        Marketplace_Customer__c c2 = new Marketplace_Customer__c();
        c2.Company_Name__c = 'TestCompany123';
        c2.Email__c = 'tester2@test.test';
        c2.First_Name__c = 'TestMan';
        c2.Last_Name__c = 'Tester2';
        c2.Title__c = 'Test Pro';
        c2.Phone__c = '1111111111';
        customers.add(c2);
        
        Marketplace_Customer__c c3 = new Marketplace_Customer__c();
        c3.Company_Name__c = 'TestCompany456';
        c3.Email__c = 'tester4@test.test';
        c3.First_Name__c = 'TestMan';
        c3.Last_Name__c = 'Tester4';
        c3.Title__c = 'Test Pro';
        c3.Phone__c = '1111111111';
        c3.Street_Address__c = '111 Street';
        c3.City__c = 'Testville';
        c3.State_or_Province__c = 'CO';
        c3.Country__c = 'USA';
        c3.Postal_code__c = '11111';
        customers.add(c3);
        
        insert customers;
        
    }

}