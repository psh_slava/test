@isTest
public class PartnerInvolvementTriggerTest {
    
    public static testmethod void testDelete(){
        Account a = new Account(Name='Test Account');
        insert a;
        
        Id partnerRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        
        Account a2 = new Account(Name='Test Partner', Account_Status__c = 'Active', Partner_Types__c = 'Reseller Partner; Support Partner;',
                                RecordTypeId = partnerRTId);
        insert a2;
        
        Contact c = new Contact(LastName='Tester',Phone='1111111111', AccountId = a.Id);
        insert c;
        
        Opportunity o = new Opportunity(CloseDate = Date.today(), Primary_Contact__c = c.Id, AccountId = a.Id, Name='Test Opp',
                                        StageName = 'Discover', Forecast_Category__c = 'Omit');
        insert o;
        
        Partner_Involvement__c pi = new Partner_Involvement__c(Partner__c = a2.id, Opportunity__c = o.Id, Partner_Program__c = 'Authorized Partner (Channel)',
                                                              Partner_Role__c = 'Reseller');
        insert pi;
        
        Partner_Involvement__c pi2 = new Partner_Involvement__c(Partner__c = a2.id, Opportunity__c = o.Id, Partner_Program__c = 'Authorized Partner (Channel)',
                                                              Partner_Role__c = 'Support');
        insert pi2;
        
        delete pi;
        
        delete pi2;
    }

}