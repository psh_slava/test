public class LicenseKeyTriggerHandler {
    
    public static void burstKeyEffect(Id csmLKId, String instanceNumber, String instanceType, Id accountId ) {
        CSM_License_Record__c customerKey;
        System.debug('Parameters passed in === ' + csmLKId + instanceNumber + instanceType + accountId);
        try{
            customerKey = [Select Id 
                           From CSM_License_Record__c
                           Where Account__c = :accountId
                           And Current_Key__c = 'Yes'
                           And Id != :csmLKId
                           And Maintenance__c = :instanceNumber
                           And License_Type__c = :instanceType
                           ORDER BY CreatedDate DESC
                           Limit 1];
        } catch (Exception e){
            System.debug('Exception caught----'+e.getMessage());
        }
        if(customerKey != null){
            customerKey.Previous_Key__c = true;
            customerKey.Current_Key__c = 'No';
            customerKey.Overriding_Key_Id__c = csmLKId;
            update customerKey;
        }
    }
    
    public static void makeFormerKey(Id formerKeyId){
        
        CSM_License_Record__c formerKey = [Select Id, Current_Key__c
                                           From CSM_License_record__c
                                           Where Id = :formerKeyId];
        formerKey.Current_Key__c = 'No';
        update formerKey;
    }
    
    public static void issueRenewal(Id csmLKId, Id formerKeyId){
        CSM_License_Record__c formerKey = [Select Id, Current_Key__c
                                           From CSM_License_Record__c
                                           Where Id = :formerKeyId];
        formerKey.Renewal_Key__c = csmLKId;
        System.debug('csmLKId'+csmLKId);
         System.debug('formerKey'+formerKey);
        update formerKey;
        
    }   
    
    public static void getFormerKey(CSM_License_Record__c key){
        try{
            List<CSM_License_Record__c> keysToUpdate = new List<CSM_License_Record__c>();
            CSM_License_Record__c formerKey = [Select Id, Current_Key__c
                                                 From CSM_License_Record__c
                                                 Where Account__c = :key.Account__c
                                                 //And License_Type__c = :key.License_Type__c
                                                 And Current_Key__c = 'Yes'
                                                 And Maintenance__c = :key.Maintenance__c
                                                 Limit 1];
            key.Former_Key__c = formerKey.Id;
            keysToUpdate.add(key);
            update keysToUpdate;
        } catch(Exception e){
            System.debug('Exception caught while looking for Former Key ---' + e.getMessage());
        }
        
    }
}