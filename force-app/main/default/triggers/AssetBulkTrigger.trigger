trigger AssetBulkTrigger on Asset (after insert, after update) {

    //After Insert Handling
    if(Trigger.isAfter && Trigger.isInsert) {
        //AssetHandler.newTrainingPackage(Trigger.new);
        //AssetHandler.newTrainingPurchase(Trigger.new);
        List<Id> triggerIds = new List<Id>();
        for(Asset a : trigger.new){
            triggerIds.add(a.Id);
        }
       GnosisAPIController.getPurchaseDetails(triggerIds);
    }
    
    //After Update Handling
    if(Trigger.isAfter && Trigger.isUpdate && !System.isFuture()) {
        List<Id> triggerIds = new List<Id>();
        for(Asset a : trigger.new){
            if(trigger.oldMap.get(a.Id).PurchaseDate != trigger.newMap.get(a.Id).PurchaseDate ||
               trigger.oldMap.get(a.Id).Expiry_Date__c != trigger.newMap.get(a.Id).Expiry_Date__c ||
               trigger.oldMap.get(a.Id).Number_of_Credits__c != trigger.newMap.get(a.Id).Number_of_Credits__c ||
               trigger.oldMap.get(a.Id).Price_USD__c != trigger.newMap.get(a.Id).Price_USD__c ||
               trigger.oldMap.get(a.Id).StockKeepingUnit != trigger.newMap.get(a.Id).StockKeepingUnit
              ){
                 triggerIds.add(a.Id); 
              }
        }
       GnosisAPIController.getPurchaseDetails(triggerIds);
    }
}