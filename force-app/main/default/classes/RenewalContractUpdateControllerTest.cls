@isTest
public class RenewalContractUpdateControllerTest {
     testmethod static void testController(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        a.BillingStreet = '123 Street';
        a.BillingCity = 'Testville';
        a.BillingCountry = 'United States';
        a.BillingPostalCode = '12345';
        a.BillingState = 'Texas';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
         
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.Status = 'Activated';
        con.StartDate = Date.today().addDays(-5);
        con.EndDate = Date.today().addDays(360);
        insert con;
         
        Opportunity opp = new Opportunity();
         opp.Name = 'Test';
         opp.CloseDate = Date.today().addDays(130);
         opp.StageName = 'Propose';
         opp.Forecast_Category__c = 'Commit';
         opp.AccountId = a.Id;
         opp.ContactId = c.Id;
         opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
         insert opp;
        
        con.Status = 'Activated';
        con.SBQQ__RenewalForecast__c = true;
        con.SBQQ__RenewalOpportunity__c = opp.Id;
        update con;
        
        PageReference myVfPage = Page.Renewalcontractupdate;
        Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('id', con.Id);
        ApexPages.currentPage().getParameters().put('conName', 'Tester');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        Renewalcontractupdatecontroller nOpp = new Renewalcontractupdatecontroller(sc);
        nOpp.OverallAction();
        //nOpp.redirectrnewal();
        nOpp.existigOpportunity();
    }
    
        testmethod static void testControllerAgain(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        a.BillingStreet = '123 Street';
        a.BillingCity = 'Testville';
        a.BillingCountry = 'United States';
        a.BillingPostalCode = '12345';
        a.BillingState = 'Texas';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
         
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.Status = 'Activated';
        con.StartDate = Date.today().addDays(-5);
        con.EndDate = Date.today().addDays(360);
        insert con;
         
        Opportunity opp = new Opportunity();
         opp.Name = 'Test';
         opp.CloseDate = Date.today().addDays(130);
         opp.StageName = 'Propose';
         opp.Forecast_Category__c = 'Commit';
         opp.AccountId = a.Id;
         opp.ContactId = c.Id;
         opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
         insert opp;
        
        con.Status = 'Activated';
        //con.SBQQ__RenewalForecast__c = true;
        con.SBQQ__RenewalOpportunity__c = opp.Id;
        update con;
        
        PageReference myVfPage = Page.Renewalcontractupdate;
        Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('id', con.Id);
        ApexPages.currentPage().getParameters().put('conName', 'Tester');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        Renewalcontractupdatecontroller nOpp = new Renewalcontractupdatecontroller(sc);
        nOpp.OverallAction();
        nOpp.redirectrnewal();
        nOpp.existigOpportunity();
    }
}