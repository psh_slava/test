global class CleanLifecycleDuplicates_LeadBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    global String query = '';
    global String obj_str = '';
    
    global CleanLifecycleDuplicates_LeadBatch (String st /*, String obj_name*/){
        
        //obj_str = obj_name;
        
        if(st != null){
            query = st;
        }else{
            // QUERY ON UNCOVERTED LEADS
           // if(obj_str=='Lead'){
                query = 'SELECT Id, Name, isconverted, (SELECT Id, name, Related_Lead__c, Lifecycle_Status__c, Lifecycle_Stage__c, Start_Date__c, CreatedDate FROM Lifecycles__r WHERE Lifecycle_Status__c = \'Active\' or Lifecycle_Status__c = \'Inactive\' ORDER BY CreatedDate desc) FROM Lead where isconverted = false';
            //}
            // QUERY ON CONTACTS
            /*else if(obj_str == 'Contact'){
                query = 'SELECT Id, Name, (SELECT Id, name, Related_Contact__c, Lifecycle_Status__c, Lifecycle_Stage__c, Start_Date__c, CreatedDate FROM Lifecycle__c WHERE Lifecycle_Status__c = \'Active\' or Lifecycle_Status__c \'Inactive\') FROM Contact';
            }
            */
            System.debug('_____obj_str____'+obj_str);
            System.debug('_____query____'+query);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext dbc){
        System.debug('______queryyy____'+query);
        return Database.getQueryLocator(query);
    }  
    
    global void execute(Database.BatchableContext dbc,List<sObject> scope){  
        
        system.debug('_____scope_____'+scope);
        
        Map<Id,List<Lifecycle__c>> leadWithLifeCycles = new Map<Id,List<Lifecycle__c>>();
        Map<Id,List<Lifecycle__c>> contactWithLifeCycles = new Map<Id,List<Lifecycle__c>>();
        Map<Id,List<LifeCycle__c>> ledWithInActiveLFCs = new Map<Id,List<Lifecycle__c>>();
        Map<Id,List<Lifecycle__c>> ledWithActiveLFCs = new Map<Id,List<LifeCycle__c>>();
        Map<Id,Integer> led_activeLFC_count = new Map<Id,Integer>();
        Map<Id,Integer> led_InactiveLFC_count = new Map<Id,Integer>();
        
        Map<Id,LifeCycle__c> ledId_highestActiveLFC = new Map<Id,LifeCycle__c>();
        Map<Id,LifeCycle__c> lfcToUpdate = new Map<Id,LifeCycle__c>();
        
        Map<String,Integer> LFC_stageWithPriority = new Map<String,Integer>();
        LFC_stageWithPriority.put('SQL',5);
        LFC_stageWithPriority.put('SAL',4);
        LFC_stageWithPriority.put('TQL',3);
        LFC_stageWithPriority.put('TAL',2);
        LFC_stageWithPriority.put('AQL',1);
        
        system.debug('___LFC_stageWithPriority___'+LFC_stageWithPriority);
        
        for(sObject obj : scope){
        
            Lead ldObj;  Contact cntObj;
            
            // IF RECORDS FETCHED ARE - LEADS
            if( /*obj_str=='Lead' &&*/ String.valueOf(obj.id).startsWith('00Q')){
                
                ldObj = (Lead)obj;
                
                System.debug('____Lead___object__'+ldObj);
                
                // IF LIFECYCLES (LFC) ARE ASSOCIATED TO UNCONVERTED LEADS FETHED ABOVE
                if(ldObj.Lifecycles__r!=null && ldObj.Lifecycles__r.size()>0){
                    
                    // PREPARES A MAP OF LEAD -> LIFECYCLES (IF A LEAD HAS MORE THAN ONE LFC ASSOCIATED)
                    if(ldObj.Lifecycles__r.size()>1)
                        leadWithLifeCycles.put(ldObj.Id , ldObj.Lifecycles__r);
                    
                    Integer count_Active = 0;
                    Integer count_Inactive = 0;
                    
                    // COUNTS NO. OF ACTIVE AND INACTIVE LIFECYCLES (LFC) ASSOCIATED TO LEAD
                    for(Lifecycle__c lfc : ldObj.Lifecycles__r){
                                                
                        if(lfc.Lifecycle_Status__c == 'Active'){
                            
                            count_Active++;
                            
                            // PREPARES A MAP OF LEAD ==> ASSOCIATED ACTIVE LFCS
                            if(ledWithActiveLFCs!=null && ledWithActiveLFCs.size()>0 && ledWithActiveLFCs.containsKey(ldObj.Id))
                                ledWithActiveLFCs.get(ldObj.Id).add(lfc);
                            else{
                                ledWithActiveLFCs.put(ldObj.Id , new List<LifeCycle__c>());
                                ledWithActiveLFCs.get(ldObj.Id).add(lfc);
                            }
                        }
                        if(lfc.Lifecycle_Status__c == 'Inactive'){
                            
                            count_Inactive++;
                            
                            // PREPARES A MAP OF LEAD ==> ASSOCIATED INACTIVE LFCS
                            if(ledWithInActiveLFCs!=null && ledWithInActiveLFCs.size()>0 && ledWithInActiveLFCs.containsKey(ldObj.Id))
                                ledWithInActiveLFCs.get(ldObj.Id).add(lfc);
                            else{
                                ledWithInActiveLFCs.put(ldObj.Id , new List<LifeCycle__c>());
                                ledWithInActiveLFCs.get(ldObj.Id).add(lfc);
                            }
                        }
                    }
                    
                    // MAP OF LEAD WITH COUNT OF ACTIVE LFC ASSOCIATED TO IT
                    if(count_Active>0)
                        led_activeLFC_count.put(ldObj.Id,count_Active);
                                        
                    // MAP OF LEAD WITH COUNT OF INACTIVE LFC ASSOCIATED TO IT
                    if(count_Inactive>0)
                        led_InactiveLFC_count.put(ldObj.Id,count_Inactive);
                                                            
                }
                
                    
            }
            // ELSE IF RECORDS FETCHED ARE - CONTACTS
            else if(obj_str=='Contact' && String.valueOf(obj.id).startsWith('003')){
                System.debug('____Contact___object__');
                cntObj = (Contact)obj;
                
                if(cntObj.Lifecycles__r!=null && cntObj.Lifecycles__r.size()>1)
                    contactWithLifeCycles.put(cntObj.Id , cntObj.Lifecycles__r);
                
                /*****
                
                    CODE IN PROGRESS FOR CONTACTS
                
                ******/
            }
        }
           
        system.debug('___led_activeLFC_count___'+led_activeLFC_count);
        system.debug('___led_inactiveLFC_count___'+led_InactiveLFC_count);
        system.debug('____leadWithLifeCycles____'+leadWithLifeCycles);
                            
        // IF MORE THAN 1 LIFECYCLE IS ASSOCIATED TO LEAD
        if(leadWithLifeCycles!=null && leadWithLifeCycles.size()>0){
            
            for(Id led_id : leadWithLifeCycles.KeySet()){
                
                system.debug('____lead__being___processed__'+led_id);
                
                system.debug('______active___lifecycle___associated___'+led_activeLFC_count.containsKey(led_id));
                
                // IF LEAD HAS - INACTIVE LFCS > 0 ; ACTIVE LFC = 0
                if(!led_activeLFC_count.containsKey(led_id) && led_InactiveLFC_count!=null && led_InactiveLFC_count.size()>0 && led_InactiveLFC_count.containsKey(led_id)){
                    
                    system.debug('____Zero___Active__lfc___block___');
                    system.debug('____no__of___inactive___lfc____'+led_InactiveLFC_count.get(led_id));
                    
                    // IF LEAD HAS - INACTIVE LFC > 1
                    if(led_InactiveLFC_count.get(led_id)>1){
                        List<LifeCycle__c> lfc_list = new List<LifeCycle__c>();
                        lfc_list.addAll(ledWithInActiveLFCs.get(led_id));
                        
                        system.debug('__inactive___lfcs___'+lfc_list);
                        
                        // MARK ALL LFCS WITH STATUS = RECORD MERGED EXCEPT THE ONE WITH OLDEST CREATED DATE (AS FETCHED IN QUERY - ORDER BY CREATED DATE DESC)
                        for(Integer i=1 ; i<lfc_list.size(); i++){
                            Integer index_var = i-1;
                            lfcToUpdate.put(lfc_list[index_var].Id , new Lifecycle__c(Id = lfc_list[index_var].Id , Lifecycle_Status__c='Record Merged'));
                        }
                    }
                    
                    system.debug('_____lfcToUpdate___if__zero__Active__lfc__'+lfcToUpdate);
                }
                // ELSE IF LEAD HAS - ACTIVE LFC > 0
                else if(led_activeLFC_count!=null && led_activeLFC_count.size()>0 && led_activeLFC_count.containsKey(led_id)){
                    
                    // IF ACTIVE LFC = 1 ; INACTIVE LFC > 0
                    if(led_activeLFC_count.get(led_id)==1 && led_InactiveLFC_count!=null && led_InactiveLFC_count.size()>0 && led_InactiveLFC_count.containsKey(led_id)){
                        
                        system.debug('____one___Active__lfc___block___');
                        
                        for(LifeCycle__c lf : ledWithInActiveLFCs.get(led_id)){
                            lfcToUpdate.put(lf.Id , new Lifecycle__c(Id = lf.Id , Lifecycle_Status__c='Record Merged'));
                        }
                        
                        system.debug('__if__one___Active__lfcToUpdate__'+lfcToUpdate);
                    }
                    // IF ACTIVE LFC > 1 
                    else if(led_activeLFC_count.get(led_id)>1){
                        
                        system.debug('____more__than___one__active__lfc__block');
                        
                        Id highestLFC_ID ;
                        
                        system.debug('___is__highestLFC_ID__null___'+highestLFC_ID);
                        
                        Set<Lifecycle__c> toBeSort_basedOnStartDate = new Set<Lifecycle__c>();
                        Set<Lifecycle__c> toBeSort_basedOnCreatedDate = new Set<Lifecycle__c>();
                        Map<String,Set<LifeCycle__c>> stageWithLFC = new Map<String,Set<LifeCycle__c>>();
                        Map<Date,Set<LifeCycle__c>> startDateWithLFC = new Map<Date,Set<LifeCycle__c>>();
                        Map<Datetime,Set<LifeCycle__c>> createdDateWithLFC = new Map<Datetime,Set<LifeCycle__c>>();
                        
                        String highestStage = 'AQL';
                        
                        // FILTERS THE LIFECYCLE BASED ON HIGH PRIORITY - STAGE
                        for(LifeCycle__c lc : ledWithActiveLFCs.get(led_id)){
                            
                            system.debug('____lfc__id___'+lc.id+'____Lifecycle_Stage__c____'+lc.Lifecycle_Stage__c);
                            
                            if(LFC_stageWithPriority.containsKey(lc.Lifecycle_Stage__c)){
                                
                                system.debug('___lfc_stage___priority__'+LFC_stageWithPriority.get(lc.Lifecycle_Stage__c));
                                system.debug('___highest__stage_and__priority____'+highestStage+'___'+LFC_stageWithPriority.get(highestStage));
                                
                                if( LFC_stageWithPriority.get(lc.Lifecycle_Stage__c) >= LFC_stageWithPriority.get(highestStage)){
                                    
                                    highestStage = lc.Lifecycle_Stage__c;
                                    
                                    // PREPARES A MAP OF LFC CORRESPONDING TO HIGH PRIORITY STAGE
                                    if(stageWithLFC!=null && stageWithLFC.size()>0 && stageWithLFC.containsKey(highestStage))
                                        stageWithLFC.get(highestStage).add(lc);
                                    else{
                                        stageWithLFC.put(highestStage, new Set<LifeCycle__c>());
                                        stageWithLFC.get(highestStage).add(lc);
                                    }
                                }
                                                                        
                            }
                            
                        }
                        
                        system.debug('__highestStage___'+highestStage);
                        system.debug('__stageWithLFC___'+stageWithLFC);
                        
                        if(stageWithLFC!=null && stageWithLFC.size()>0 && stageWithLFC.containsKey(highestStage)){
                            
                            List<LifeCycle__c> lfc_highestStage_set = new List<LifeCycle__c>();
                            lfc_highestStage_set.addAll(stageWithLFC.get(highestStage));
                            
                            // IF ONLY 1 RECORD IS FOUND BASED ON HIGH PRIORITY - STAGE, THEN LEAVE THAT LFC ACTIVE AND MARK OTHER LFCS AS 'RECORD MERGED'
                            if(lfc_highestStage_set.size()==1){
                                //highestActiveLFC.put(led_id, lfc_highestStage_set[0]);
                                highestLFC_ID = lfc_highestStage_set[0].Id;
                            }
                            // IF MORE THAN 1 ACTIVE LFC IS FOUND BASED ON HIGH PRIORITY STAGE
                            else if(lfc_highestStage_set.size()>1){                                 
                                toBeSort_basedOnStartDate.addAll(lfc_highestStage_set);
                            }
                            
                            system.debug('_____highestLFC_ID_based__on___stage___'+highestLFC_ID);
                            
                        }
                        // IF NO LFC IS FOUND BASED ON HIGH PRIORITY - STAGE
                        else{
                            toBeSort_basedOnStartDate.addAll(ledWithActiveLFCs.get(led_id));
                        }
                        
                        system.debug('____toBeSort_basedOnStartDate____'+toBeSort_basedOnStartDate);
                        
                        // FILTERS THE ACTIVE LFC BASED ON OLDEST START DATE
                        if(toBeSort_basedOnStartDate!=null && toBeSort_basedOnStartDate.size()>0 && highestLFC_ID==null){
                                
                            Date oldest_startDate = Date.newInstance(2030, 01, 15);
                                
                            for(LifeCycle__c lf_stg : toBeSort_basedOnStartDate){
                                
                                if(lf_stg.Start_Date__c!=null && lf_stg.Start_Date__c <= oldest_startDate){
                                        
                                    oldest_startDate = lf_stg.Start_Date__c;
                                    
                                    // PREPATES A MAP OF ACTIVE LFC CORRESPONDING TO OLDEST START DATE
                                    if(startDateWithLFC!=null && startDateWithLFC.size()>0 && startDateWithLFC.containsKey(oldest_startDate))
                                        startDateWithLFC.get(oldest_startDate).add(lf_stg);
                                    else{
                                        startDateWithLFC.put(oldest_startDate, new Set<LifeCycle__c>());
                                        startDateWithLFC.get(oldest_startDate).add(lf_stg);
                                    }
                                }   
                                                                    
                            }
                            
                            system.debug('____oldest_startDate___'+oldest_startDate);
                            system.debug('____startDateWithLFC___'+startDateWithLFC);
                            
                            if(startDateWithLFC!=null && startDateWithLFC.size()>0 && startDateWithLFC.containsKey(oldest_startDate)){
                                
                                List<Lifecycle__c> lfc_oldestStatDate_set = new List<LifeCycle__c>();
                                lfc_oldestStatDate_set.addAll(startDateWithLFC.get(oldest_startDate));
                                
                                // IF ONLY 1 RECORD IS FOUND BASED ON OLDEST START DATE, THEN LEAVE THAT LFC ACTIVE AND MARK OTHER LFCS AS 'RECORD MERGED'
                                if(lfc_oldestStatDate_set.size()==1){
                                    
                                    highestLFC_ID = lfc_oldestStatDate_set[0].Id; 
                                }
                                // IF MORE THAN 1 ACTIVE LFC IS FOUND BASED ON OLDEST START DATE
                                else if(lfc_oldestStatDate_set.size()>1){
                                    toBeSort_basedOnCreatedDate.addAll(lfc_oldestStatDate_set);
                                }
                                
                                system.debug('____highestLFC_ID___based_on__startdate___'+highestLFC_ID);
                            }
                            // IF NO LFC IS FOUND BASED ON OLDEST START DATE
                            else{
                                toBeSort_basedOnCreatedDate.addAll(toBeSort_basedOnStartDate);
                            }
                            
                            system.debug('____toBeSort_basedOnCreatedDate___'+toBeSort_basedOnCreatedDate);
                            
                            // FILTERS THE ACTIVE LFC BASED ON OLDEST CREATED DATE
                            if(toBeSort_basedOnCreatedDate!=null && toBeSort_basedOnCreatedDate.size()>0 && highestLFC_ID==null){
                                
                                DateTime oldest_createdDate = DateTime.newInstance(2030, 01, 15, 0, 0, 0);
                                
                                for(LifeCycle__c lf_sd : toBeSort_basedOnCreatedDate){
                                    
                                    if(lf_sd.CreatedDate <= oldest_createdDate){
                                            
                                        oldest_createdDate = lf_sd.CreatedDate;
                                        
                                        // PREPATES A MAP OF ACTIVE LFC CORRESPONDING TO OLDEST CREATED DATE
                                        if(createdDateWithLFC!=null && createdDateWithLFC.size()>0 && createdDateWithLFC.containsKey(oldest_createdDate))
                                            createdDateWithLFC.get(oldest_createdDate).add(lf_sd);
                                        else{
                                            createdDateWithLFC.put(oldest_createdDate, new Set<LifeCycle__c>());
                                            createdDateWithLFC.get(oldest_createdDate).add(lf_sd);
                                        }
                                    }   
                                                                        
                                }
                                
                                system.debug('___oldest_createdDate___'+oldest_createdDate);
                                system.debug('____createdDateWithLFC___'+createdDateWithLFC);
                                
                                // SETS HIGHEST LFC BASED ON OLDES CREATED DATE, TO MARK REST OF ACTIVE LFCS AS 'RECORD MERGED'
                                if(createdDateWithLFC!=null && createdDateWithLFC.size()>0 && createdDateWithLFC.containsKey(oldest_createdDate)){
                                    
                                    List<LifeCycle__c> lfc_oldestCreatedDate_set = new List<LifeCycle__c>();
                                    lfc_oldestCreatedDate_set.addAll(createdDateWithLFC.get(oldest_createdDate));
                                    
                                    highestLFC_ID = lfc_oldestCreatedDate_set[0].Id;
                                }
                                
                                system.debug('____highestLFC_ID___based_on__createddate___'+highestLFC_ID);
                            }
                            // BLOCK ENDS : FILTERS THE ACTIVE LFC BASED ON OLDEST CREATED DATE
                        }
                        // BLOCK ENDS : FILTERS THE ACTIVE LFC BASED ON OLDEST START DATE
                        
                        if(highestLFC_ID!=null){
                            
                            for(LifeCycle__c all_lf : leadWithLifeCycles.get(led_id)){
                                
                                if(all_lf.Id!=highestLFC_ID){
                                    
                                    LifeCycle__c lfc_update = new LifeCycle__c();
                                    lfc_update.Id = all_lf.Id;
                                    lfc_update.Lifecycle_Status__c= 'Record Merged';
                                    
                                    if(all_lf.Lifecycle_Status__c == 'Active')
                                        lfc_update.Completed_Date__c = Date.Today();
                                    
                                    if(all_lf.Lifecycle_Stage__c == 'SQL')
                                        lfc_update.SQL_Exit_Date__c = Date.Today();
                                    else if(all_lf.Lifecycle_Stage__c == 'SAL')
                                        lfc_update.SAL_Exit_Date__c = Date.Today();
                                    else if(all_lf.Lifecycle_Stage__c == 'TQL')
                                        lfc_update.TQL_Exit_Date__c = Date.Today();
                                    else if(all_lf.Lifecycle_Stage__c == 'TAL')
                                        lfc_update.TAL_Exit_Date__c = Date.Today();
                                    else if(all_lf.Lifecycle_Stage__c == 'AQL')
                                        lfc_update.AQL_Exit_Date__c = Date.Today();

                                    
                                    lfcToUpdate.put(all_lf.Id , lfc_update);
                                }
                            }
                            
                            system.debug('__if__more__than__one___active___lfc__lfcToUpdate___'+lfcToUpdate);
                        }
                    }
                    // BLOCK ENDS : IF ACTIVE LFC > 1
                }
                // BLOCK ENDS : LEAD HAS - ACTIVE LFC > 0
            }
            // FOR LOOP ON LEADS ENDS
        }
            
        if(lfcToUpdate!=null && lfcToUpdate.size()>0){
            update lfcToUpdate.values();
        }
        
    
    }
    
    global void finish(Database.BatchableContext dbc){ }
    
}