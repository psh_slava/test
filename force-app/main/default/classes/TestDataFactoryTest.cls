@isTest
Public Class TestDataFactoryTest{

    public static testMethod void TestDataFactory(){
        
        //CREATE ACCOUNT
        Account acc = TestDataFactory.createAccount(True,'TestDataFactory');
        
        //CREATE CONTACT
        Contact con = TestDataFactory.createContact(True,'firstname','lastname',acc);
        
        //CREATE OPPORTUNITY
        Opportunity opp = TestDataFactory.createOpportunity(True,'oppname',acc);
        
    }
}