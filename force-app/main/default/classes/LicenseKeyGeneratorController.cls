public class LicenseKeyGeneratorController {
    
    // Property for Sorting 
    public String orderBy {get; set;}
    public String sortOrder {get; set;}
    public String previousOrderBy {get; set;}  
    
    //private final Contract c;
    public CSM_License_Record__c csmLKR {get;set;}
    public List<KeyWrapper> keyWrapperList {get;set;}
    public List<CSM_License_Record__c> currentKeyList {get;set;}
    public CSM_License_Record__c record {get; set;}
    public List<CSM_License_Record__c> oldKeyList {get;set;}
    public Integer counter {get;set;}
    public String newLicenseKey {get;set;}
    public String accountName {get;set;}
    public String success {get;set;}
    public String u {get;set;}
    
    public LicenseKeyGeneratorController(){
        String contractId = ApexPages.currentPage().getParameters().get('id');
        String accountId = ApexPages.currentPage().getParameters().get('accountid');
        u = UserInfo.getUserId();
        List<PermissionSetAssignment> pset = [SELECT Id,Assignee.Name,PermissionSet.Name, PermissionSet.Label 
                                              FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() ];
        
        if(contractId == null){
            try{
                contractId = [Select Id From Contract Where AccountId = :accountId And Status = 'Activated' And SBQQ__ExpirationDate__c > TODAY Order by ActivatedDate DESC].Id;
            } catch (Exception e){
                System.debug('Exception caught-----' + e.getMessage());
            }
        }
        System.debug('Contract Id === ' + contractId);
        
        accountName = ApexPages.currentPage().getParameters().get('accountName');
        String success = ApexPages.currentPage().getParameters().get('success');
        
        if(success == 'true'){
            System.debug('Successful save:   ' + success);
            gosuccess();
        }
        
        counter = 0;
        keyWrapperList = new List<KeyWrapper>();
        List<CSM_License_Record__c> autoGenKeys;
        if(contractId != null){ 
            autoGenKeys = LicenseKeyAutoGenerator.generateKeyRows(contractId);
            for(CSM_License_Record__c clr : autoGenKeys){
                System.debug(clr);
                KeyWrapper clrWrap = new KeyWrapper(clr);
                counter++;
                clrWrap.countWrap = counter;
                keyWrapperList.add(clrWrap);
            }
        }     
        
        currentKeyList = new List<CSM_License_Record__c>();
        List<CSM_License_Record__c> currentKeys = LicenseKeyAutoGenerator.getCurrentKeys(accountId);
        for(CSM_License_Record__c ck : currentKeys){
            System.debug(ck);
            record = ck;
            currentKeyList.add(ck);
        }
        
        oldKeyList = new List<CSM_License_Record__c>();
        List<CSM_License_Record__c> oldKeys = LicenseKeyAutoGenerator.getOldKeys(accountId);
        for(CSM_License_Record__c ck : oldKeys){
            System.debug(ck);
            oldKeyList.add(ck);
        }
    }
    
    public PageReference addLicenseKey() {
        CSM_License_Record__c lkr = new CSM_License_Record__c();
        KeyWrapper kWrap = new KeyWrapper(lkr);
        counter++;
        kWrap.countWrap = counter;
        keyWrapperList.add(kWrap);
        return null;      
    }
    
    public PageReference saveKeys() {
        String contractId = ApexPages.currentPage().getParameters().get('id');
        String accountId = ApexPages.currentPage().getParameters().get('accountid');
        //accountName = ApexPages.currentPage().getParameters().get('accountName');
        
        List<CSM_License_Record__c> keyList = new List<CSM_License_Record__c>();
        if(!keyWrapperList.isEmpty()){
            for(KeyWrapper wrapper : keyWrapperList){
                System.debug('Callout parameters ==== '+ wrapper.lk.License_Name__c + ' | ' + wrapper.lk.Expires__c + ' | ' + wrapper.lk.Licensed_Items__c + ' | ' + wrapper.lk.Hosted__c);
                newLicenseKey = String.valueOf(LicenseKeyGenCallout.makeLKGCallout(wrapper.lk.License_Name__c, wrapper.lk.Expires__c, wrapper.lk.Licensed_Items__c, wrapper.lk.Hosted__c));
                if(wrapper.lk.Purchase_Scenario2__c == 'Renewal'){ wrapper.lk.Current_Key__c = 'No';  wrapper.lk.Future_Key__c = true; }
                if(wrapper.lk.Purchase_Scenario2__c != 'Renewal'){ wrapper.lk.Current_Key__c = 'Yes'; }
                wrapper.lk.License_Key__c = newLicenseKey;
                wrapper.lk.Generated_in_Salesforce__c = true;
                keyList.add(wrapper.lk);
            }
        }
        if(!keyList.isEmpty()){
            try{ 
                //insert keyList;
                upsert keyList;
            } catch (DMLException e){
                System.debug('DML Exception caught----'+e.getMessage());
                ApexPages.addMessages(e);
                return null;         
            }
        }
        PageReference tempPage = Page.LicenseKeyGenerator;
        tempPage.getParameters().put('Id',contractId);
        tempPage.getParameters().put('accountId',accountId);
        tempPage.getParameters().put('accountName',accountName);
        if(!keyList.isEmpty()){ tempPage.getParameters().put('success','true'); }
        tempPage.setRedirect(true);       
        return tempPage;
    }
    
    public PageReference deleteRow()
    {
        Integer addedRow = Integer.valueOf(Apexpages.currentPage().getParameters().get('index'));
        
        for(Integer i=0; i<keyWrapperList.size(); i++){
            if(keyWrapperList[i].countWrap == addedRow){
                keyWrapperList.remove(i);
            }
        }
        counter--;
        return null;    
    }
    
    public class KeyWrapper {
        String accountId = ApexPages.currentPage().getParameters().get('accountid');
        String oppId = ApexPages.currentPage().getParameters().get('oppId');
        public CSM_License_Record__c lk {get;set;}
        public Integer countWrap {get;set;}
        Id csa;
        Id lkc;
        String oppRT;
        
        public KeyWrapper(CSM_License_Record__c key){
            if(accountId == null && oppId != null){ accountId = [Select AccountId From Opportunity Where Id = :oppId].AccountId; }
            csa = [Select Customer_Success_Advocate__c From Account Where Id = :accountId].Customer_Success_Advocate__c;
            if(oppId != null){ oppRT = [Select RecordType.Name From Opportunity Where Id =:oppId].RecordType.Name; }
            if(oppId != null){ key.Opportunity__c = oppId; }
            try{ lkc = [Select ContactId From AccountContactRole Where AccountId = :accountId And Role = 'License Key Contact' Limit 1].ContactId; }
            catch(Exception e){ System.debug('Error caught +++' + e.getMessage()); }
            key.Account__c = accountId;
            if(key.Primary_Contact__c == null) { key.Primary_Contact__c = lkc; }
            key.License_Type__c = 'CSM';
            key.CSA__c = csa;
            this.lk = key;
        }
    }
     
    
    public PageReference goSuccess() {
        //Display success message
        apexpages.Message msg = new Apexpages.Message(Apexpages.Severity.INFO, 'A License Key has been generated and email notifications will be sent shortly.');
        apexpages.addMessage(msg);
        return null;
    } 
    
}