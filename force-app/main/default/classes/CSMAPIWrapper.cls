/**
    Client side stub for the Cherwell Service Management (CSM) API.
*/
public class CSMAPIWrapper
{
    public class CSMAPIWrapperException extends Exception {}

    private static final Integer requestTimeout = 120000;
    private static CSM_API_Settings__c apiSettings;
    private static Boolean newAccessToken = false;

    public static List<String> requiredPermSetsForRetry {
        get {
            if (String.isNotBlank(apiSettings.Required_Permission_Sets_for_Retry__c)) {
                Set<String> ps = new Set<String>();
                for(String s : apiSettings.Required_Permission_Sets_for_Retry__c.split(',')) {
                    ps.add(s);
                }

                return new List<String>(ps);
            }

            return null;
        }
    }
    
    public static String csmTimezone {
        get {
            return apiSettings.Timezone__c;
        }
    }

    public static String accessToken {
        get {
            // The access token can be longer than the limit of 255 characters
            // in custom setting.  So we need to concatenate up to 1000 characters.
            String a = apiSettings.Access_Token1__c;
            if (String.isNotBlank(apiSettings.Access_Token2__c)) {
                a += apiSettings.Access_Token2__c;
                if (String.isNotBlank(apiSettings.Access_Token3__c)) {
                    a += apiSettings.Access_Token3__c;
                    if (String.isNotBlank(apiSettings.Access_Token4__c)) {
                        a += apiSettings.Access_Token4__c;
                    }
                }
            }

            return a;
        }

        set {
            apiSettings.Access_Token1__c = value.left(255);
            if (value.length() > 255) {
                apiSettings.Access_Token2__c = value.mid(255, 255);
                if (value.length() > (255*2)) {
                    apiSettings.Access_Token3__c = value.mid((255*2), 255);
                    if (value.length() > (255*3)) {
                        apiSettings.Access_Token4__c = value.mid((255*3), 255);
                        if (value.length() > (255*4)) {
                            throw new CSMAPIWrapperException('Access token length has more than 1000 characters.');
                        }
                    }
                }
            }
        }
    }

    public static Integer csmMaxAutoRetries {
        get {
            return Integer.valueOf(apiSettings.Max_Sync_Entry_Auto_Retries__c);
        }
    }

    static {
    	// Assumes a separate set of settings for Sandbox and Production environments.  This will protect against
    	// inadvertent access to production from a sandbox environment when the sandbox is refreshed with data from
    	// production. 
    	Boolean runningInASandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    	apiSettings = CSM_API_Settings__c.getInstance(runningInASandbox ? 'Sandbox' : 'Production');
    }

// Start of Private helper methods

    private static void setAccessToken() {
        Boolean usedRefreshToken = false;
        // Make sure we only get a fresh token if it's not valid anymore so that we can
        // minimize the number of logins we make to CSM, as CSM limits it to 10 per second.
    	if (String.isBlank(accessToken) || 
            String.isBlank(apiSettings.Refresh_Token__c) ||
            !CSMAccessTokenWrapper.isTokenValid(accessToken, apiSettings.Access_Token_Expiration__c, requestTimeout)) {
	        
            HttpRequest request = new HttpRequest();
	    	request.setEndpoint(String.format('{0}/token?api_key={1}', 
	    		new String[]{ apiSettings.Endpoint__c, apiSettings.Client_Key__c }));
	    	request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
	        request.setTimeout(requestTimeout);
	    	request.setMethod('POST');
            if (String.isBlank(apiSettings.Refresh_Token__c)) {
	           request.setBody(String.format('grant_type=password&client_id={0}&username={1}&password={2}',
	    	      new String[]{ apiSettings.Client_Key__c, apiSettings.Username__c, apiSettings.Password__c }));
                System.debug('Used password');
            } else {
                request.setBody(String.format('grant_type=refresh_token&client_id={0}&refresh_token={1}',
                    new String[]{ apiSettings.Client_Key__c, apiSettings.Refresh_Token__c }));
                usedRefreshToken = true;
                System.debug('Used refresh token');
            }
            System.debug('DEBUG: CSMAPIWrapper request is ' + request);
	    	Http http = new Http();
	    	HTTPResponse response = http.send(request);
	        String responseBody = response.getBody();
            System.debug('DEBUG: CSMAPIWrapper response ' + responseBody);
	        if (response.getStatusCode() == 200) {
	        	CSMAccessTokenWrapper accessTokenWrapper = CSMAccessTokenWrapper.parseJSON(responseBody);
                accessToken = accessTokenWrapper.access_token;
                apiSettings.Access_Token_Expiration__c = accessTokenWrapper.expires_x;
                apiSettings.Refresh_Token__c = accessTokenWrapper.refresh_token;
                newAccessToken = true;
	        } else {
                // If we used the refresh token and we didn't get a successfule response,
                // the refresh token must already be invalid (maybe the time elapsed from the last
                // API request has been long enough for the refresh token to have expired).  In this case,
                // we try 1 more time using the credentials.  If that one fails, we throw an error.
                if (usedRefreshToken) {
                    apiSettings.Access_Token_Expiration__c = null;
                    apiSettings.Refresh_Token__c = '';
                    accessToken = '';
                    newAccessToken = true;
                    setAccessToken();
                } else {
	        	  throw new CSMAPIWrapperException('Problem getting access token: ' + responseBody);
                }
	        }
	    } 
	    
    }

    /**
        Generic method to make a HTTP callout using a pre-retrieved access token.
    */
    private static String invoke(String operation, String method, String requestBody) {
	   setAccessToken();

        HttpRequest request = new HttpRequest(); 

        request.setEndpoint(String.format('{0}/api/v1/{1}',
        	new String[]{  apiSettings.Endpoint__c, operation }));

        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + accessToken);

        request.setTimeout(requestTimeout);

        request.setMethod(method);

        if (String.isNotBlank(requestBody)) {
        	request.setBody(requestBody);
        }
    	Http http = new Http();
    	HTTPResponse response = http.send(request);
        String responseBody = response.getBody();

        if (response.getStatusCode() == 200) {
        	return responseBody;
        } else {
        	throw new CSMAPIWrapperException('Problem invoking operation (' + operation + '): ' + responseBody);
        }
    }

// End of Private helper methods

// Start of Public methods

    // TODO: Consider storing business object id's in static variables or even custom settings (
    // if they rarely change) to save extra api calls.  When making CRUD calls, CSM always requires
    // passing the business object id (that is the id of the Account table, not just the record id itself).
    // I'm guessing it's because their record id's are not globally unique, that is, they're only unique
    // within the same object type.
    
    //public static CSMBusinessObjectWrapper getBusinessObjectSummaryByName(String name) {
    //	String operation = 'getbusinessobjectsummary/busobname/' + name;
    //	String method = 'GET';
    //	String responseBody = invoke(operation, method, null);

    //	Map<String, CSMBusinessObjectWrapper> bowMap = CSMBusinessObjectWrapper.parseJSON(responseBody);
    //	if (bowMap.size() == 0 || !bowMap.containsKey(name)) {
    //    	throw new CSMAPIWrapperException('Problem invoking operation (' + operation + '): There\'s no business object with this name');
    //	} else if (bowMap.size() > 1) {
    //    	throw new CSMAPIWrapperException('Problem invoking operation (' + operation + '): Found more than 1 business object with this name');
    //	} 

    //	return bowMap.get(name);

    //}

    // Retrieves a single business object record using its record id
    public static CSMBusinessObjectRecordWrapper getBusinessObjectRecordByRecordId(String name, String recordId) {
    	String operation = 'getbusinessobject/busobid/' + CSMFieldMappingHelper.getAllObjectMappingsByCSMObjectName().get(name).CSM_Business_Object_Id__c + 
    		'/busobrecid/' + recordId;
    	String method = 'GET';
    	String responseBody = invoke(operation, method, null);

    	return CSMBusinessObjectRecordWrapper.parseJSON(responseBody);
    }

    // Retrieves a single business object record using its public id
    //public static CSMBusinessObjectRecordWrapper getBusinessObjectRecordByPublicId(String name, String publicId) {
    //	String operation = 'getbusinessobject/busobid/' + getBusinessObjectSummaryByName(name).busObId + 
    //		'/publicid/' + publicId;
    //	String method = 'GET';
    //	String responseBody = invoke(operation, method, null);

    //	return CSMBusinessObjectRecordWrapper.parseJSON(responseBody);
    //}

    // Creates (if record and pulic id's are null) or updates the given business object record
    public static CSMBusinessObjectRecordResponseWrapper saveBusinessObjectRecord(SObject obj) {
    	String operation = 'savebusinessobject';
    	String method = 'POST';
    	String requestBody = CSMBusinessObjectRecordWrapper.getRequestBody(obj);
        System.debug('DEBUG: requestBody ' + requestBody);
    	String responseBody = invoke(operation, method, requestBody);
        System.debug('DEBUG: responseBody ' + responseBody);
    	return CSMBusinessObjectRecordResponseWrapper.parseJSON(responseBody);
    }

    public static String uploadBusinessObjectAttachment(Attachment att) {
        String operation = 'uploadbusinessobjectattachment';
        String method = 'POST';

        Id parentId = att.parentId;
        String objTypeName = String.valueOf(parentId.getSObjectType());
        CSM_Object_Mapping__c objMap = CSMFieldMappingHelper.getAllObjectMappingsBySObjectName().get(objTypeName);
        String busObId = objMap.CSM_Business_Object_Id__c;
        
        String query = 'SELECT ' + objMap.Salesforce_External_Id_Field_Name__c + ' FROM ' + objTypeName + ' WHERE Id = :parentId';
        SObject obj = Database.query(query);
        String busObRecId = String.valueOf(obj.get(objMap.Salesforce_External_Id_Field_Name__c));

        String fileName = att.Name;
        Blob body = att.Body;
        Integer offset = 0;
        Integer totalsize= att.BodyLength;

        // When in test mode, we can't set the body size when inserting the test attachment.
        // So we dummy up the body size when testing so that we can match the resulting
        // endpoint in our mock class.
        if (Test.isRunningTest()) totalSize = TestingUtility.AttachmentBodySize;
        
        filename = EncodingUtil.urlEncode(filename, 'UTF-8').replace('+','%20');

        //uploadbusinessobjectattachment/filename/{filename}/busobid/{busobid}/busobrecid/{busobrecid}/offset/{offset}/totalsize/{totalsize}
        String endpoint = 'uploadbusinessobjectattachment/filename/'+filename+'/busobid/'+busobid+'/busobrecid/'+busObRecId+'/offset/'+offset+'/totalsize/'+totalsize;
        
        setAccessToken();

        HttpRequest request = new HttpRequest(); 

        request.setEndpoint(String.format('{0}/api/v1/{1}',
            new String[]{  apiSettings.Endpoint__c, endpoint }));

        request.setHeader('Content-Type', 'application/octet-stream');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + accessToken);

        request.setTimeout(requestTimeout);

        request.setMethod(method);

        request.setBodyAsBlob(att.Body);

        Http http = new Http();
        HTTPResponse response = http.send(request);
        String responseBody = response.getBody();
        System.debug('DEBUG: response is ' + response);
        System.debug('DEBUG: response body is ' + responseBody);
        if (response.getStatusCode() == 200) {
            return responseBody;
        } else {
            throw new CSMAPIWrapperException('Problem invoking operation (' + operation + '): ' + responseBody);
        }
    }

    // This needs to be called by callers to indicate that all API calls have been made for
    // the current session.  This is because we need to save the access token to the custom
    // settting so it can be reused by the next sessions until it expires.  CSM has a limit
    // of 10 logins per second.  So we want to limit the number of requests for access tokens
    // as much as possible.  However, since saving to the custom setting is a DML call, 
    // we will have to rely on the caller to indicate when we can do the DML because Salesforce
    // does not all callouts to happen after a DML.  So all callouts must be done first before
    // any DMLs.  
    public static void endOfAPICalls() {
        
        if (newAccessToken) {
            CSM_API_Settings__c myAPISettings = 
                [SELECT Id, Access_Token1__c, Access_Token2__c, Access_Token3__c, Access_Token4__c, Access_Token_Expiration__c, Refresh_Token__c
                 FROM CSM_API_Settings__c
                 WHERE Id = :apiSettings.Id];

            myApiSettings.Access_Token1__c = apiSettings.Access_Token1__c;
            myApiSettings.Access_Token2__c = apiSettings.Access_Token2__c;
            myApiSettings.Access_Token3__c = apiSettings.Access_Token3__c;
            myApiSettings.Access_Token4__c = apiSettings.Access_Token4__c;
            myApiSettings.Access_Token_Expiration__c = apiSettings.Access_Token_Expiration__c;
            myApiSettings.Refresh_Token__c = apiSettings.Refresh_Token__c;

            update myApiSettings;

            newAccessToken = false;
        }
    }

    @future(callout=true)
    public static void uploadBusinessObjectAttachmentAsync(Set<Id> ids){
        // If in test mode, this would have have been called from the attachment trigger
        // which would have resulted into the "Uncommitted..." error when the callout to
        // upload is executed.  So we skip this if in test mode.  We'll directly call
        // the upload method in the test class to test that code.
        if (!Test.isRunningTest()) {
            for(Attachment a : [Select Id, ParentId, Body, Name, BodyLength From Attachment Where Id In :ids]){
                uploadBusinessObjectAttachment(a);  
            }
            endOfAPICalls();
        }
    }

// End of Public methods
}