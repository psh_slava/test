public class OpportunityTrigger_Action{
    
    @future
    public static void future_LifecycleRecordUpdate_OnOpportunityInsert(Set<Id> runFutureOpps){
        if(runFutureOpps <> null && runFutureOpps.size() > 0){
            System.debug('runFutureOpps >> ' + runFutureOpps);
            
            Map<Id, Id> primaryContactIds = new Map<Id, Id>();
            for(OpportunityContactRole ocrObj : [Select id, opportunityId, contactid from OpportunityContactRole where opportunityId in :runFutureOpps and isPrimary = true]){
                primaryContactIds.put(ocrObj.opportunityId, ocrObj.contactId);
            }
            String fields = 'id, ';
            for(Schema.SObjectField sfield : Schema.SObjectType.Opportunity.fields.getMap().values()){
                Schema.DescribeFieldResult dfield = sfield.getDescribe();
                if(dfield.isUpdateable()){
                    fields += dfield.getName() + ', ';
                }
                
            }          
            if(!fields.contains('Probability')){  
                fields += 'Probability, ';  
            }  
            fields = fields.removeEnd(', ');
            System.debug('fields >> ' + fields);
            List<Opportunity> newList = Database.query('Select ' + fields +' from Opportunity where id in :runFutureOpps');
            if((primaryContactIds <> null && primaryContactIds.size() > 0) || Test.isRunningTest()){
                OpportunityTrigger_Action.processRecordsAfterInsert(primaryContactIds, newList);
            }
        }     
    }
    public void LifecycleRecordUpdate_OnOpportunityInsert(list<opportunity> newList){
        
        Map<Id, Id> primaryContactIds = new Map<Id, Id>();
        Set<Id> runFutureOpps = new Set<Id>();
        
        for(opportunity op : newList){
            if(op.StageName <> null){
                //primaryContactIds.put(op.id, op.Primary_Contact__c);
                runFutureOpps.add(op.id);
            }
        }
        if(runFutureOpps <> null && runFutureOpps.size() > 0 && System.isBatch() == false && System.isFuture() == false){
            OpportunityTrigger_Action.future_LifecycleRecordUpdate_OnOpportunityInsert(runFutureOpps);
        }
        
    }
    
        
     public void queueUpdatePrimaryCampaignAttribution(list<opportunity> newList){
               
        Set<Id> opportunityids = new Set<Id>();
        
        for(opportunity op : newList){
            if(op.Primary_Contact__c == null){
                 opportunityids.add(op.id);
            }
        }
        if(opportunityids <> null && opportunityids.size() > 0 && System.isBatch() == false && System.isFuture() == false){
            OpportunityTrigger_Action.future_primaryCampaignAttribution(opportunityids);
        }
        
    }
    
    @future
    public static void future_primaryCampaignAttribution(Set<Id> opportunityids)
    {
        if(opportunityids <> null && opportunityids.size() > 0)
        {
           List<OpportunityContactRole>  OCRs = [Select id, opportunityId, contactid from OpportunityContactRole where opportunityId in :opportunityids and isPrimary = true ORDER BY CreatedDate DESC];
            List<Id> contactIds = new List<Id>();
            for(OpportunityContactRole OCR : OCRs)
            {
           contactIds.add(OCR.contactId);
           }
           List<Contact> cons = [Select Id,(Select id,Campaignid from CampaignMembers ORDER BY CreatedDate DESC limit 1) from contact Where ID  IN : contactids];

           List<opportunity> Updateopportunity = New List<Opportunity>();
         
           For(opportunity op:[Select id,Primary_Contact__c,CampaignId from Opportunity where id IN : opportunityids])
           {
             
             For(contact con:cons)
             {
               If(Con.CampaignMembers != Null && con.CampaignMembers.size() > 0)
             { 
             CampaignMember c = Con.CampaignMembers;
              Op.CampaignId = C.Campaignid;
             }
             Op.Primary_Contact__c = con.id;
             Updateopportunity.Add(Op);
             }
           }
           
          IF(Updateopportunity.Size()>0 && Updateopportunity <> Null)
          {
             Update Updateopportunity;
          } 
        }
    
    }
    
    public static void processRecordsAfterInsert(Map<Id, Id> primaryContactIds, list<opportunity> newList){
        if((primaryContactIds <> null && primaryContactIds.size() > 0) || Test.isRunningTest()){
            
            Map<Id, Lifecycle__c> LifecylceUpdate = new Map<Id, Lifecycle__c>();
            //Map<Id, Contact> primaryContacts = new Map<Id, Contact>((List<Contact>)[Select id, mkto71_Lead_Score__c from Contact where id in :primaryContactIds.values()]);
            //System.debug('primaryContacts >> ' + primaryContacts);
            
            //getting the latest lifeCycleRecord 
            Map<id, Lifecycle__c> conIdToLifecycleMap = OpportunityTrigger_Action.getIdToLifecycleMap(new Set<Id>(primaryContactIds.values()), new Set<Id>());
            System.debug('conIdToLifecycleMap >> ' + conIdToLifecycleMap);
            
            if(conIdToLifecycleMap <> null && conIdToLifecycleMap.size() > 0){
                Map<Id, Contact> relatedContactMap = new Map<Id, Contact>([Select id, mkto71_Lead_Score__c from Contact where id in :conIdToLifecycleMap.keySet()]);
                
                if(relatedContactMap <> null && relatedContactMap.size() > 0){
                    for(Opportunity oppObj : newList){
                        
                        Id contactId = primaryContactIds.get(oppObj.id);
                        System.debug('Contactid >> ' + contactId + ' and map contains key >> ' + conIdToLifecycleMap.containsKey(contactId));
                        
                        if(conIdToLifecycleMap.containsKey(contactId)){
                            LifeCycle__c currentLifeRec = conIdToLifecycleMap.get(contactId);
                            System.debug('currentLifeRec ---' + currentLifeRec);
                            
                            if(currentLifeRec.Related_contact__c <> null){
                                //update LifecycleRecord
                                Lifecycle__c lyf = new Lifecycle__c(id = currentLifeRec.id);
                                lyf.Related_Opportunity__c = oppObj.id;
                                lyf.Opportunity_Stage__c = oppObj.StageName;
                                
                                if(currentLifeRec.Lifecycle_Stage__c == 'TQL'){
                                    lyf.TQL_Exit_Date__c = Date.today();
                                }
                                else if(currentLifeRec.Lifecycle_Stage__c == 'TAL'){
                                    lyf.TAL_Exit_Date__c = Date.today();
                                }
                                else if(currentLifeRec.Lifecycle_Stage__c == 'AQL'){
                                    lyf.AQL_Exit_Date__c = Date.today();
                                }
                                
                                if(OppObj.Probability >= 0 && OppObj.Probability < 19){
                                    System.debug('C1 >> OppObj.StageName >>' + OppObj.StageName );
                                    if(currentLifeRec.Lifecycle_Status__c == 'Inactive'){
                                        lyf.Lifecycle_Status__c = 'Active';
                                        lyf.Start_Date__c = Date.today();
                                    }
                                    
                                    lyf.Lifecycle_Stage__c = 'SAL';
                                    lyf.SAL_Entry_Date__c = Date.today();
                                    if(relatedContactMap.containsKey(contactId))
                                        lyf.Score_at_SAL__c = relatedContactMap.get(contactId).mkto71_Lead_Score__c;
                                    
                                }else if(OppObj.Probability >= 19 && OppObj.Probability < 100){
                                             
                                             System.debug('C2 >> OppObj.StageName >>' + OppObj.StageName );
                                             
                                             if(currentLifeRec.Lifecycle_Status__c == 'Inactive'){
                                                 lyf.Lifecycle_Status__c = 'Active';
                                                 lyf.Start_Date__c = Date.today();
                                             }
                                             lyf.Lifecycle_Stage__c = 'SQL';
                                             lyf.SQL_Entry_Date__c = Date.today();
                                             if(relatedContactMap.containsKey(contactId))
                                                 lyf.Score_at_SQL__c = relatedContactMap.get(contactId).mkto71_Lead_Score__c;
                                             
                                         }else if(oppObj.Probability == 100){
                                             
                                             System.debug('C3 >> OppObj.StageName >>' + OppObj.StageName );
                                             lyf.Closed_Reason__c = oppObj.Close_Reason__c;
                                             
                                             if(currentLifeRec.Lifecycle_Status__c == 'Inactive'){
                                                 lyf.Lifecycle_Status__c = 'Completed';
                                                 lyf.Completed_Date__c = Date.today();
                                                 lyf.Start_Date__c = Date.today();
                                             }
                                             lyf.Lifecycle_Stage__c = 'Closed Won';
                                             lyf.Won_Date__c = Date.today();
                                             lyf.SQL_Entry_Date__c = Date.today();
                                             lyf.SQL_Exit_Date__c = DAte.today();
                                             
                                             if(relatedContactMap.containsKey(contactId))
                                                 lyf.Score_at_SQL__c = relatedContactMap.get(contactId).mkto71_Lead_Score__c;
                                         }
                                if(!LifecylceUpdate.containsKey(lyf.id))
                                    LifecylceUpdate.put(lyf.id, lyf);
                            }
                            
                        }
                        
                    }//loop ends
                    
                    if(LifecylceUpdate <> null && LifecylceUpdate.size() > 0){
                        System.debug('LifecylceUpdate >> '  +LifecylceUpdate);
                        update LifecylceUpdate.values();
                    }
                }
            }// main if ends
            
        }
    }
    public static Map<id, Lifecycle__c> getIdToLifecycleMap(Set<Id> primaryContactIds, Set<Id> relatedOppIds){
        System.debug('primaryContactIds  >> ' + primaryContactIds );
        if((primaryContactIds <> null && primaryContactIds.size() > 0) || Test.isRunningTest()){
            Map<id, Lifecycle__c> oppIdToLifecycleMap = new Map<Id, Lifecycle__c>();
            List<Lifecycle__c> relatedRecs = new List<Lifecycle__c>();
            System.debug('relatedOppIds >> ' + relatedOppIds );
            
            //isRelatedOpportunity will match with Related Opportuntiy as well
            if(relatedOppIds == null || relatedOppIds.size() <= 0){
                relatedRecs = [Select id, Related_Contact__c, Lifecycle_Status__c, Lifecycle_Stage__c, Person_Status__c from Lifecycle__c where (Lifecycle_Status__c = 'Active' or Lifecycle_Status__c = 'Inactive') and Related_Contact__c in :primaryContactIds and Related_Opportunity__c = null order by lastModifiedDate];
            }else{
                relatedRecs = [Select id, Related_Contact__c, Lifecycle_Status__c, Related_Opportunity__c, Lifecycle_Stage__c, Person_Status__c from Lifecycle__c where Related_Contact__c != null and Lifecycle_Status__c = 'Active' and Related_Opportunity__c in :relatedOppIds order by lastModifiedDate];  //Related_Contact__c in :primaryContactIds and
                
            }
            System.debug('relatedRecs >> ' + relatedRecs );
            if(relatedRecs <> null && relatedRecs.size() > 0){
                for(Lifecycle__c obj : relatedRecs){
                    if(relatedOppIds == null || relatedOppIds.size() <= 0){
                        if(!oppIdToLifecycleMap.containsKey(obj.Related_Contact__c))
                            oppIdToLifecycleMap.put(obj.Related_Contact__c, obj);
                        else if(oppIdToLifecycleMap.containsKey(obj.Related_Contact__c) && obj.Lifecycle_Status__c == 'Active'){
                            oppIdToLifecycleMap.put(obj.Related_Contact__c, obj);
                            break;
                        }
                    }else{
                        if(!oppIdToLifecycleMap.containsKey(obj.Related_Opportunity__c))
                            oppIdToLifecycleMap.put(obj.Related_Opportunity__c, obj);
                        else if(oppIdToLifecycleMap.containsKey(obj.Related_Opportunity__c) && obj.Lifecycle_Status__c == 'Active'){
                            oppIdToLifecycleMap.put(obj.Related_Opportunity__c, obj);
                            break;
                        }                        
                    }
                    
                }
                
                System.debug('oppIdToLifecycleMap >> ' + oppIdToLifecycleMap);
                if(oppIdToLifecycleMap <> null && oppIdToLifecycleMap.size() > 0)
                    return oppIdToLifecycleMap;
            }            
        }
        return null;
    }
    
    public void LifecycleRecordUpdate_OnOpportunityUpdate(Map<Id,Opportunity> newMap, Map<Id,Opportunity> oldMap){
        
        List<Opportunity> filteredOpps = new LIst<Opportunity>();
        
        for(opportunity op : newMap.values()){ 
            if(op.StageName <> oldMap.get(op.id).stageName){
                filteredOpps.add(op);
            }
        }
        if(filteredOpps <> null && filteredOpps.size() > 0){
            Map<Id, Id> oppIdToContactIdMap = new Map<Id, Id>();
            
            //fetch primary opportunity contact role
            for(Opportunity oppObj : [Select id, (Select Id, ContactId From OpportunityContactRoles where isPrimary = True limit 1) from Opportunity where id in :filteredOpps]){
                if(oppObj.OpportunityContactRoles <> null && oppObj.OpportunityContactRoles.size() > 0)
                    oppIdToContactIdMap.put(oppObj.Id, oppObj.OpportunityContactRoles[0].contactId);
            }
            
            if(oppIdToContactIdMap <> null && oppIdToContactIdMap.size() > 0){
                System.debug('oppIdToContactIdMap ---- ' + oppIdToContactIdMap);
                //Map<Id, Contact> primaryContacts = new Map<Id, Contact>((List<Contact>)[Select id, mkto71_Lead_Score__c from Contact where id in :oppIdToContactIdMap.values()]);
                //getting the latest lifeCycleRecord 
                Map<id, Lifecycle__c> oppIdToLifecycleMap = OpportunityTrigger_Action.getIdToLifecycleMap(new Set<Id>(oppIdToContactIdMap.values()), new Set<Id>(oppIdToContactIdMap.keySet()));
                System.debug('oppIdToLifecycleMap >> '  +oppIdToLifecycleMap);
                
                if(oppIdToLifecycleMap <> null && oppIdToLifecycleMap.size() > 0){
                    
                    //get related contact from lifecycle records
                    Set<Id> relatedCotctIds = new Set<Id>();
                    for(Lifecycle__c lyf : oppIdToLifecycleMap.values()){
                        if(!relatedCotctIds.contains(lyf.Related_Contact__c))
                            relatedCotctIds.add(lyf.Related_Contact__c);
                    }
                    
                    Map<Id, Contact> relatedContacts = new Map<Id, Contact>();
                    if(relatedCotctIds <> null && relatedCotctIds.size() > 0){
                        relatedContacts = new Map<Id, Contact>((List<Contact>)[Select id, mkto71_Lead_Score__c from Contact where id in :relatedCotctIds]);
                    }
                    
                    List<Lifecycle__c> LifecylceUpdate = new list<Lifecycle__c>();
                    List<Contact> relatedContactUpdate = new list<Contact>();
                    Boolean doUpdateContact = false;
                    
                    if(relatedContacts <> null && relatedContacts.size() > 0){
                        for(Opportunity oppObj : filteredOpps){
                            if(oppIdToLifecycleMap.containsKey(oppObj.id))
                            {
                                
                                
                                LifeCycle__c currentLifeRec = oppIdToLifecycleMap.get(oppObj.id);
                                Contact relatedContact = relatedContacts.get(currentLifeRec.Related_Contact__c);
                                System.debug('currentLifeRec ---' + currentLifeRec);
                                
                                //update LifecycleRecord
                                Lifecycle__c lyf = new Lifecycle__c(id = currentLifeRec.id);
                                lyf.Opportunity_Stage__c = oppObj.StageName;
                                lyf.Prior_Opportunity_Stage__c = oldMap.get(oppObj.id).stageName;
                                
                                if((oldMap.get(oppobj.id).Probability >= 0 && oldMap.get(oppobj.id).Probability <= 19)  && oppObj.Probability > 19)
                                {
                                    System.debug('C1 >> Opportunity stage updated to : ' + oppObj.StageName);
                                    lyf.SAL_Exit_Date__c = Date.today();
                                    lyf.Lifecycle_Stage__c = 'SQL'; 
                                    lyf.Score_at_SQL__c = relatedContact.mkto71_Lead_Score__c;
                                    lyf.SQL_Entry_Date__c = Date.today();
                                         
                                }
                                else if(oppObj.StageName <> oldMap.get(oppobj.id).stageName && currentLifeRec.Lifecycle_Status__c == 'Active' && currentLifeRec.Related_Opportunity__c == oppObj.id)
                                {
                                    
                                    if(oldMap.get(oppobj.id).stageName == 'Pre-Qualification' && (oppObj.StageName == 'Closed - Lost' || oppObj.StageName == 'Closed - Won' || oppObj.StageName == 'Closed No Sale')){
                                        System.debug(' C3 >> Opportunity stage updated to : ' + oppObj.StageName);
                                        
                                        lyf.Closed_Reason__c = oppObj.Close_Reason__c;lyf.Lifecycle_Status__c = 'Completed';lyf.Completed_Date__c = Date.today(); lyf.SAL_Exit_Date__c = Date.today();
                                                                                
                                        if(oppObj.StageName == 'Closed - Lost' || oppObj.StageName == 'Closed No Sale'){
                                            lyf.Lifecycle_Stage__c = 'Recycled';lyf.Recycled_Date__c = Date.today();
                                            lyf.Person_Status__c = 'Back To Nurture';lyf.Prior_Person_Status__c = currentLifeRec.Person_Status__c;
                                            
                                            if(oppObj.StageName == 'Closed No Sale'){
                                                lyf.Recycle_Reason__c = 'Opportunity - Closed No Sale';
                                                relatedContactUpdate.add(new Contact(Id = relatedContact.id, Contact_Status__c = 'Back To Nurture', Back_to_Nurture_Reason__c = 'Opportunity - No Sale'));
                                            }else if(oppObj.StageName == 'Closed - Lost'){
                                                lyf.Recycle_Reason__c = 'Opportunity - Closed Lost';
                                                relatedContactUpdate.add(new Contact(Id = relatedContact.id, Contact_Status__c = 'Back To Nurture', Back_to_Nurture_Reason__c = 'Opportunity - Closed Lost'));
                                            }
                                            doUpdateContact = true;
                                            if(!RecursionController.stopContactTriggerAfterUpdate)
                                                RecursionController.stopContactTriggerAfterUpdate = true;
                                        }
                                        else if(oppObj.StageName == 'Closed - Won'){
                                            lyf.Score_at_SQL__c = relatedContact.mkto71_Lead_Score__c;
                                            lyf.SQL_Entry_Date__c = Date.today();
                                            lyf.SQL_Exit_Date__c = Date.today();
                                            lyf.Lifecycle_Stage__c = 'Closed Won';
                                            lyf.Won_Date__c = Date.today();
                                        }
                                    }
                                    else if(oldMap.get(oppobj.id).Probability >= 19 && (oppObj.StageName == 'Closed - Lost' || oppObj.StageName == 'Closed - Won' || oppObj.StageName == 'Closed No Sale'))
                                    {
                                        System.debug(' C4 >> Opportunity stage updated to : ' + oppObj.StageName);
                                        
                                        lyf.Closed_Reason__c = oppObj.Close_Reason__c;
                                        lyf.Lifecycle_Status__c = 'Completed';
                                        lyf.Completed_Date__c = Date.today();
                                        lyf.SQL_Exit_Date__c = Date.today();
                                        
                                        if(oppObj.StageName == 'Closed - Lost' || oppObj.StageName == 'Closed No Sale')
                                        {
                                            lyf.Lifecycle_Stage__c = 'Recycled';
                                            lyf.Recycled_Date__c = Date.today();
                                            lyf.Person_Status__c = 'Back To Nurture';
                                            lyf.Prior_Person_Status__c = currentLifeRec.Person_Status__c;
                                            
                                            if(oppObj.StageName == 'Closed No Sale' && oldMap.get(oppobj.id).Probability >= 19){
                                                
                                                lyf.Recycle_Reason__c = 'Opportunity - Closed No Sale';
                                                relatedContactUpdate.add(new Contact(Id = relatedContact.id, Contact_Status__c = 'Back To Nurture', Back_to_Nurture_Reason__c = 'Opportunity - No Sale'));
                                                
                                            }else if(oppObj.StageName == 'Closed - Lost' && oldMap.get(oppobj.id).Probability > 19){
                                                
                                                lyf.Recycle_Reason__c = 'Opportunity - Closed Lost';
                                                relatedContactUpdate.add(new Contact(Id = relatedContact.id, Contact_Status__c = 'Back To Nurture', Back_to_Nurture_Reason__c = 'Opportunity - Closed Lost'));
                                                
                                            }
                                            doUpdateContact = true;
                                            if(!RecursionController.stopContactTriggerAfterUpdate)
                                                RecursionController.stopContactTriggerAfterUpdate = true;
                                        }
                                        else if(oppObj.Probability == 100 && oldMap.get(oppobj.id).Probability > 19){
                                            lyf.Lifecycle_Stage__c = 'Closed Won';
                                            lyf.Won_Date__c = Date.today();
                                        }
                                    }
                                    
                                }
                                
                                LifecylceUpdate.add(lyf);
                            }
                        }//loop ends
                    }
                    
                    if(LifecylceUpdate <> null && LifecylceUpdate.size() > 0){
                        System.debug('LifecylceUpdate >> '  +LifecylceUpdate);
                        update LifecylceUpdate;
                    }
                    if(doUpdateContact && relatedContactUpdate <> null && relatedContactUpdate.size() > 0){
                        System.debug('relatedContactUpdate >> '  +relatedContactUpdate);
                        update relatedContactUpdate;
                    }
                    
                }
                
            }
        }
    }

}