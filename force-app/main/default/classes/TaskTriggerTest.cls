@isTest
public class TaskTriggerTest {
    
    
    public static Account aNew ;
    public static Contact cNew ;
    
    @testsetup
    public static void testsetupdata(){
        aNew = TestDataFactory.createAccount(true, 'Test Account');
        cNew = TestDataFactory.createContact(true, 'Test', 'Contact', aNew);
    }
    
    @isTest
    public static void NewTaskTest(){ 
        List<Contact> cnt = [Select id, name from Contact where Name like 'Test Contact%' limit 1];   
        List<task> newTask = new List<task>{ new task(
        WhoId = cnt[0].id,
        Type = 'Meeting',
        Subject='Discovery Call',
        Status='Open',
        Priority='Normal',
        Opportunity_Source__c = 'BDR Outbound',
        ActivityDate = System.today()
        )};

        insert newTask;  
        
        //Test parentTaskIDToClose
        List<task> newTask2 = new List<task>{ new task(
        WhoId = cnt[0].id,
        Type = 'Meeting',
        Subject='Discovery Call',
        Status='Open',
        Priority='Normal',
        Opportunity_Source__c = 'BDR Outbound',
        ActivityDate = System.today(),
        Parent_Task_ID__c = newTask[0].id
        )};
        insert newTask2;
        
        Opportunity checkOpp = [Select id, name from Opportunity where name like 'Test Account%'];
        OpportunityContactRole checkOcr = [Select id from OpportunityContactRole where opportunityid = :checkOpp.id];
        Task checkTask = [Select id, whatid, Status from task where id = :newTask[0].id];
        System.assert(checkOpp != null);
        System.assert(checkOcr != null);
        System.assert(checkTask != null);
        System.assert(checkTask.Status == 'Completed');
    }
    
}