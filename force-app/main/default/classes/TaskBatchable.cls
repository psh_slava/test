/**
* @author Conga Services
* @author Dan Sherman
* @version 1.00
* @date 10/27/2016
* @description Create a batchable class to count all countable tasks on Leads and set the lead outreach date to the earliest date
*/
public class TaskBatchable implements Database.Batchable<sObject>{
	public String query;
	
	//Get all leads which have not yet been converted
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}


	public void execute(Database.BatchableContext BC, List<sObject> scope) {

		TaskBatchableHelper bth = new TaskBatchableHelper();
			bth.activityCount(scope);
			bth.firstOutreach(scope);
	}


	public void finish(Database.BatchableContext BC){ }

}