@isTest
public class CleanLifecycleDuplicates_LedConBatchTest{
    @isTest
    public static void LeadBatchTest(){
        
        List<Lead> ld = new List<Lead>();
        ld.add(new Lead(lastname = 'ldTest1', company ='abcd')) ;
        ld.add(new Lead(lastname = 'ldTest2', company ='abcd')) ;
        ld.add(new Lead(lastname = 'ldTest3', company ='abcd')) ;
        ld.add(new Lead(lastname = 'ldTest4', company ='abcd')) ;
        ld.add(new Lead(lastname = 'ldTest5', company ='abcd')) ;
        
        insert ld ;
 
        list<lifecycle__c>lyfLst = new list<lifecycle__c>();
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[0].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[0].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'TQL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c =ld[1].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c ='SQL',Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[1].id, Lifecycle_Status__c ='InActive' , Lifecycle_Stage__c = 'SQL',Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c(Related_Lead__c =ld[2].id, Lifecycle_Status__c = 'Active', Lifecycle_Stage__c = 'SQL',Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c(Related_Lead__c =ld[2].id, Lifecycle_Status__c = 'Active', Lifecycle_Stage__c = 'AQL',Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c(Related_Lead__c = ld[2].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'AQL',Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[3].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[3].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today().adddays(1) )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[3].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[4].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[4].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c( Related_Lead__c = ld[4].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today() )) ;
        
        insert lyfLst ;
        
         Test.startTest();

            CleanLifecycleDuplicates_LeadBatch obj = new CleanLifecycleDuplicates_LeadBatch(null);
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
        
    
    }
    
    @isTest
    public static void ContactBatchTest(){
        Account acc = new Account (Name ='testAc1');
        insert acc;
        
        List<Contact> con = new List<Contact>();
        con.add(new Contact(lastname = 'conTest1', AccountID = acc.id)) ;
        con.add(new Contact(lastname = 'conTest2', AccountID =acc.id)) ;
        con.add(new Contact(lastname = 'conTest3', AccountID =acc.id)) ;
        con.add(new Contact(lastname = 'conTest4', AccountID =acc.id)) ;
        con.add(new Contact(lastname = 'conTest5', AccountID =acc.id)) ;
        insert con ;
        
        
        list<lifecycle__c>lyfLst = new list<lifecycle__c>();
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[0].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[0].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'TQL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[1].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c ='SQL',Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[1].id, Lifecycle_Status__c ='InActive' , Lifecycle_Stage__c = 'SQL',Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c(Related_Contact__c = con[2].id, Lifecycle_Status__c = 'Active', Lifecycle_Stage__c = 'SQL',Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c(Related_Contact__c = con[2].id, Lifecycle_Status__c = 'Active', Lifecycle_Stage__c = 'AQL',Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c(Related_Contact__c = con[2].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'AQL',Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[3].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c(Related_Contact__c = con[3].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today().adddays(1) )) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[3].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[4].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today() )) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[4].id, Lifecycle_Status__c ='Active' , Lifecycle_Stage__c = 'TAL' ,Start_Date__c=system.today())) ;
        lyfLst.add(new lifecycle__c( Related_Contact__c = con[4].id, Lifecycle_Status__c ='Inactive' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today() )) ;
        
        insert lyfLst ;
        
        Test.startTest();

            CleanLifecycleDuplicates_ContactBatch  obj = new CleanLifecycleDuplicates_ContactBatch (null);
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
        
    
    
    }
    
}