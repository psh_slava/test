/*
      Purpose:
            Your purpose here...
            
      Initiative: ...Initiative...
      Author:     Alan Birchenough
      Company:    Icon Cloud Consulting
      Contact:    alan.birchenough@iconatg.com
      Created:    4/12/18
*/

@IsTest
private class dlrs_pse_AssignmentTest {
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_pse_AssignmentTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        pse__Region__c region = PsaTestFactory.createRegion();

        Contact resource = createResource(region);
        pse__Proj__c project = PsaTestFactory.createProject(region);
        project.pse__Project_Manager__c = resource.Id;
        update project;

        pse__Schedule__c schedule = new pse__Schedule__c(
                pse__Start_Date__c = Date.today().toStartOfWeek(),
                pse__End_Date__c = Date.today().toStartOfWeek().addDays(6),
                pse__Monday_Hours__c = 8,
                pse__Tuesday_Hours__c = 8,
                pse__Wednesday_Hours__c = 8,
                pse__Thursday_Hours__c = 8,
                pse__Friday_Hours__c = 8
        );
        insert schedule;

        insert new pse__Permission_Control__c(
                pse__User__c = UserInfo.getUserId(),
                pse__Region__c = region.Id,
                pse__Staffing__c = true
        );

        pse__Assignment__c assignment = new pse__Assignment__c(
                pse__Schedule__c = schedule.Id,
                pse__Bill_Rate__c = 1.0,
                pse__Resource__c = resource.Id,
                pse__Project__c = project.Id,
                pse__Role__c = 'Consulting',
            	pse__Planned_Bill_Rate__c = 20.0
        );
        insert assignment;

        dlrs.RollupService.testHandler(assignment);
    }

    public static Contact createResource(pse__Region__c region) {

        List<Contact> cList = [select Id from Contact where pse__Salesforce_User__c=:UserInfo.getUserId()];
        for (Contact c : cList) {
            c.pse__Salesforce_User__c = null;
        }

        if (!cList.isEmpty()) {
            update cList;
        }

        pse__Work_Calendar__c wc = new pse__Work_Calendar__c(Name='test');
        insert wc;

        Contact resource = new Contact(
                LastName = 'test',
                pse__Resource_Role__c = 'Consultant',
                pse__Salesforce_User__c = UserInfo.getUserId(),
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true,
                MailingCountry = 'United States',
                pse__Work_Calendar__c = wc.Id,
                pse__Region__c = region.Id
        );

        insert resource;
        return resource;
    }
}