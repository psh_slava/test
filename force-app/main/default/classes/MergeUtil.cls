public class MergeUtil {
    public static Map<Id, Lifecycle__c> mergedLifecycles = new Map<Id, Lifecycle__c>();
    
    public void leadMergeProcessBeforeDelete(Map<Id, sObject> oldMap){
        
        List<Id> ids = new List<Id>(oldMap.keyset());
        System.debug('ids >> ' + ids[0]);
        if(String.valueOf(ids[0]).startsWith('00Q')){
            System.debug('Lead Merge!');
            for(Lead ldObj : [Select id, name, (Select id, Name, Lifecycle_Status__c, Related_lead__c, Lifecycle_Stage__c, SQL_Exit_Date__c, SAL_Exit_Date__c, TQL_Exit_Date__c, TAL_Exit_Date__c, AQL_Exit_Date__c from lifecycles__r order by lifecycle_status__c desc) from Lead where id IN :ids]){
                System.debug('Lead Merge Lifecycle >> '  +ldObj.lifecycles__r);
                for(Lifecycle__c lyf : ldObj.lifecycles__r){
                    mergedLifecycles.put(lyf.id, lyf);
                }
            }
            
        }else if(String.valueOf(ids[0]).startsWith('003')){
            System.debug('Contact Merge!');
            for(Contact conObj : [Select id, name, (Select id, Name, Lifecycle_Status__c, Related_Contact__c, Lifecycle_Stage__c, SQL_Exit_Date__c, SAL_Exit_Date__c, TQL_Exit_Date__c, TAL_Exit_Date__c, AQL_Exit_Date__c from lifecycles__r order by lifecycle_status__c desc) from Contact where id IN :ids]){
                System.debug('Contact Merge Lifecycle >> '  +conObj.lifecycles__r);
                for(Lifecycle__c lyf : conObj.lifecycles__r){
                    mergedLifecycles.put(lyf.id, lyf);
                }
            }
        }
        System.debug('mergedLifecycles >>>> '+ mergedLifecycles);
    }
    
    public void leadMergeProcessAfterDelete(Map<Id, sObject> oldMap){
        //dummy error
        //oldMap.values()[0].id.addError('Test error');
        System.debug('mergedLifecycles >>>> '+ mergedLifecycles);
        
        List<lifecycle__c> lifecycleToUpdate = new List<lifecycle__c>();
        List<lifecycle__c> lifecycleToDelete = new List<lifecycle__c>();
        Set<Id> deleteOrphan = new Set<Id>();
        Map<Id, Set<Id>> masterRecordAndMergedLeads = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> masterRecordAndMergedContacts = new Map<Id, Set<Id>>();
        Map<Id, List<Lifecycle__c>> masterLifecycles = new Map<Id, List<Lifecycle__c>>();
        
        //get master lead to merged leads ids in map
        for(sObject obj : oldMap.values()){
            Lead ldObj;  Contact cntObj;
            
            if(String.valueOf(obj.id).startsWith('00Q')){
                System.debug('After Delete : Lead Merge!');
                ldObj = (Lead)obj;
                if(ldObj.masterRecordId <> null){
                    if(!masterRecordAndMergedLeads.containsKey(ldObj.MasterRecordId))
                        masterRecordAndMergedLeads.put(ldObj.masterRecordId, new Set<Id>{ldObj.id});
                    else
                        masterRecordAndMergedLeads.get(ldObj.masterRecordId).add(ldObj.id);
                }else{
                    deleteOrphan.add(ldObj.id);
                }
            }else if(String.valueOf(obj.id).startsWith('003')){
                System.debug('After Delete : Contact Merge!');
                cntObj = (Contact)obj;
                if(cntObj.masterRecordId <> null){
                    if(!masterRecordAndMergedContacts.containsKey(cntObj.MasterRecordId))
                        masterRecordAndMergedContacts.put(cntObj.masterRecordId, new Set<Id>{cntObj.id});
                    else
                        masterRecordAndMergedContacts.get(cntObj.masterRecordId).add(cntObj.id);
                }else{
                    deleteOrphan.add(cntObj.id);
                }
            }            
        }//loop ends
        
        if(deleteOrphan <> null && !deleteOrphan.isEmpty()){

            for(Lifecycle__c lc : [select id, Related_Contact__c, Related_Lead__c from Lifecycle__c where id in :mergedLifecycles.keyset()]){
                // filtering lifecycle record having having Related_Contact__c field== null to be deleted
                if(lc.Related_Contact__c == null && lc.Related_Lead__c == null){
                    system.debug('Entered in if loop lifecycle related contact equals null');
                    lifecycleToDelete.add(lc);
                }
            }
            system.debug('lifecycleToDelete-------------'+ lifecycleToDelete);
            
            if(lifecycleToDelete.size() > 0){
                delete lifecycleToDelete;
            }
        }else{
            System.debug('masterRecordAndMergedLeads >> '  +masterRecordAndMergedLeads);
            System.debug('masterRecordAndMergedContacts >> '  +masterRecordAndMergedContacts);
            
            //get lifecycles of master record as well
            if(masterRecordAndMergedLeads <> null && masterRecordAndMergedLeads.size() > 0){
                for(Lead ldObj : [Select id, (Select id, Name, Lifecycle_Status__c, Related_lead__c, Lifecycle_Stage__c, SQL_Exit_Date__c, SAL_Exit_Date__c, TQL_Exit_Date__c, TAL_Exit_Date__c, AQL_Exit_Date__c from lifecycles__r order by lifecycle_status__c desc) from lead where id in :masterRecordAndMergedLeads.keySet()]){
                    masterLifecycles.put(ldObj.id, new List<Lifecycle__c>(ldObj.lifecycles__r));
                }
                
            }//masterRecordAndMergedLeads if ends
            
            //get lifecycles of master record as well
            if(masterRecordAndMergedContacts <> null && masterRecordAndMergedContacts.size() > 0){
                for(Contact conObj : [Select id, (Select id, Name, Lifecycle_Status__c, Related_Contact__c, Lifecycle_Stage__c, SQL_Exit_Date__c, SAL_Exit_Date__c, TQL_Exit_Date__c, TAL_Exit_Date__c, AQL_Exit_Date__c from lifecycles__r order by lifecycle_status__c desc) from Contact where id in :masterRecordAndMergedContacts.keySet()]){
                    masterLifecycles.put(conObj.id, new List<Lifecycle__c>(conObj.lifecycles__r));
                }
            }
            
            for(Id masterRecord : masterRecordAndMergedLeads.keyset()){
                List<Lifecycle__c> allLyf = masterLifecycles.get(masterRecord);
                System.debug('allLyf >> ' + allLyf);
                lifecycleToUpdate = this.processActiveRecords(allLyf);
            }//loop ends
            
            for(Id masterRecord : masterRecordAndMergedContacts.keyset()){
                List<Lifecycle__c> allLyf = masterLifecycles.get(masterRecord);
                System.debug('allLyf >> ' + allLyf);
                lifecycleToUpdate = this.processActiveRecords(allLyf);
            }//loop ends
            
        }
        
        
    }
    
    public List<lifecycle__c> processActiveRecords(List<Lifecycle__c> allLyf){
        if(allLyf <> null && allLyf.size() > 0){
            Map<Id, lifecycle__c> lifecycleToUpdate = new Map<Id, lifecycle__c>();
            Map<String, List<Lifecycle__c>> sortedLifecyclesOnStatus = new Map<String, List<Lifecycle__c>>();
            Lifecycle__c topActiveStageLifecycles = new Lifecycle__c();
            
            
            sortedLifecyclesOnStatus = this.seperatedByStatuses(allLyf);
            System.debug('sortedLifecyclesOnStatus.get(active) >> '  + sortedLifecyclesOnStatus.get('active'));
            if(sortedLifecyclesOnStatus.get('active').size() >= 0)
                System.debug('sortedLifecyclesOnStatus.get(active) >> '  + sortedLifecyclesOnStatus.get('active').size());
            System.debug('sortedLifecyclesOnStatus.get(inactive) >> '  + sortedLifecyclesOnStatus.get('inactive'));
            
            
            //stage sort
            for(Lifecycle__c lyf : allLyf){
                System.debug('lyf >> ' + lyf);
            }//loop ends
            
            //process starts
            List<Lifecycle__c> activeAndInactive = new List<Lifecycle__c>();
            activeAndInactive.addAll(sortedLifecyclesOnStatus.get('active'));
            activeAndInactive.addAll(sortedLifecyclesOnStatus.get('inactive'));
            
            
            //only one active lifecycle
            if(sortedLifecyclesOnStatus.get('active') <> null && sortedLifecyclesOnStatus.get('active').size() == 1 && !sortedLifecyclesOnStatus.get('active').isEmpty()  && sortedLifecyclesOnStatus.get('inactive') <> null){
                System.debug('Only one active');
                for(Lifecycle__c lyf : sortedLifecyclesOnStatus.get('inactive')){
                    lifecycleToUpdate.put(lyf.id,  new Lifecycle__c(id = lyf.id, Lifecycle_Status__c = 'Record Merged'));
                }
            }
            //if there are no active lifecycle and more than one inactive lifecycles
            else if(sortedLifecyclesOnStatus.get('active').isEmpty() && sortedLifecyclesOnStatus.get('inactive') <> null && !sortedLifecyclesOnStatus.get('inactive').isEmpty()){
                Integer masterInactiveCount = 0;                
                System.debug('No active lifecycle!');
                Id lifecycleSkipId;
                
                for(Lifecycle__c lyf : sortedLifecyclesOnStatus.get('inactive')){
                    List<Lifecycle__c> masterinactive = sortedLifecyclesOnStatus.get('masterInactive');
                    
                    if(masterinactive <> null && !masterinactive.isEmpty() && masterinactive.size() > 0){
                        
                        for(Lifecycle__c inactiveLyf : masterinactive){
                            System.debug(' masterinactive lyf : ' + inactiveLyf);
                            if(masterInactiveCount >= 1 && lifecycleSkipId == null){
                                lifecycleToUpdate.put(lyf.id,  new Lifecycle__c(id = lyf.id, Lifecycle_Status__c = 'Record Merged'));
                            } 
                            else if(lifecycleSkipId == null){
                                lifecycleSkipId = inactiveLyf.id;
                            }
                            masterInactiveCount++;
                        }//loop ends
                        
                        System.debug('lifecycleSkipId >> ' + lifecycleSkipId + ' masterInactiveCount >> '  +masterInactiveCount);
                        
                    }
                    
                    if(lifecycleSkipId <> null && lifecycleSkipId <> lyf.id)
                        lifecycleToUpdate.put(lyf.id,  new Lifecycle__c(id = lyf.id, Lifecycle_Status__c = 'Record Merged'));
                }
            }
            //more than one active lifecycle records, update all other active and inactive
            else if(activeAndInactive <> null && activeAndInactive.size() > 0 && topActiveStageLifecycles <> null){
                
                if(sortedLifecyclesOnStatus.get('active') <> null && !sortedLifecyclesOnStatus.get('active').isEmpty()){
                    topActiveStageLifecycles = this.topActivetLifecycleStage(sortedLifecyclesOnStatus.get('active'), sortedLifecyclesOnStatus.get('masterActive'));
                }
                System.debug('topActiveStageLifecycles >> ' + topActiveStageLifecycles);
                
                
                System.debug('more than one active records!');
                for(Lifecycle__c lyf : sortedLifecyclesOnStatus.get('active')){
                    //exclude the one that should remain unchanged
                    if(topActiveStageLifecycles <> null && topActiveStageLifecycles.id <> lyf.id){
                        Lifecycle__c newLyf = new Lifecycle__c(id = lyf.id, Lifecycle_Status__c = 'Record Merged');
                        newLyf.Completed_Date__c = date.today();
                        switch on lyf.Lifecycle_Stage__c {
                            when 'SQL'{
                                if(lyf.SQL_Exit_Date__c == null)
                                    newLyf.SQL_Exit_Date__c = Date.today();
                            }
                            when 'SAL' {
                                if(lyf.SAL_Exit_Date__c == null)
                                    newLyf.SAL_Exit_Date__c = date.today();
                            }
                            when 'TQL' {
                                if(lyf.TQL_Exit_Date__c == null)
                                    newLyf.TQL_Exit_Date__c = date.today();
                            }
                            when 'TAL' {
                                if(lyf.TAL_Exit_Date__c == null)
                                    newLyf.TAL_Exit_Date__c = date.today();
                            }
                            when 'AQL' {
                                if(lyf.AQL_Exit_Date__c == null)
                                    newLyf.AQL_Exit_Date__c = date.today();
                            }
                        }
                        lifecycleToUpdate.put(lyf.id, newLyf);
                    }
                }
                
                //update all other Lifecycle records found in Lifecycle.Lifecycle_Status__c = ‘Active’ or ‘Inactive’ 
                if(sortedLifecyclesOnStatus.get('inactive') <> null){
                    for(Lifecycle__c lyf : sortedLifecyclesOnStatus.get('inactive')){
                        lifecycleToUpdate.put(lyf.id,  new Lifecycle__c(id = lyf.id, Lifecycle_Status__c = 'Record Merged'));
                    }
                }
                
                
            }   
            
            System.debug('lifecycleToUpdate >> ' + lifecycleToUpdate);
            if(lifecycleToUpdate <> null && lifecycleToUpdate.size() >= 0)
                update lifecycleToUpdate.values();
        }
        
        return null;
    }
    public Lifecycle__c topActivetLifecycleStage(List<Lifecycle__c> allActiveLyf, List<Lifecycle__c> masterActiveLyf){
        System.debug('topActivetLifecycleStage Invoked!');
        
        Set<String> sortArray = new Set<String>{'SQL', 'SAL', 'TQL', 'TAL', 'AQL'};
            List<String> sortArrayList = new List<String>{'SQL', 'SAL', 'TQL', 'TAL', 'AQL'};
                List<Lifecycle__c> sortedActiveStageLifecycles = new List<Lifecycle__c>();
        Set<Id> masterActiveLyfIds = new Set<Id>();
        
        if(masterActiveLyf <> null && !masterActiveLyf.isEmpty()){
            for(Lifecycle__c lyf : masterActiveLyf){
                masterActiveLyfIds.add(lyf.id);
            }
        }
        System.debug('masterActiveLyfIds >> ' + masterActiveLyfIds);
        for(integer i = 0; i < sortArrayList.size(); i++){
            System.debug('sortArrayList[i] >> ' + sortArrayList[i]);
            
            for(Lifecycle__c lyf : allActiveLyf){
                
                System.debug('masterActiveLyfIds.contains(lyf.id) >> ' + masterActiveLyfIds.contains(lyf.id));

                if(lyf.Lifecycle_Stage__c == sortArrayList[i] && sortArray.contains(sortArrayList[i]) && masterActiveLyfIds.contains(lyf.id)){ 
                    System.debug('masterActiveLyf Exists! sortArrayList[i] >> '  +sortArrayList[i]);  
                    System.debug('lyf inside condition to sort lifecycle stage >> ' + lyf); 
                    sortedActiveStageLifecycles.add(lyf);
                    sortArray.remove(sortArrayList[i]);
                } 
            }
            
            if(sortedActiveStageLifecycles == null || sortedActiveStageLifecycles.isEmpty()){
                for(Lifecycle__c lyf : allActiveLyf){
                    
                    System.debug('masterActiveLyfIds.contains(lyf.id) >> ' + masterActiveLyfIds.contains(lyf.id));
                    
                    if(lyf.Lifecycle_Stage__c == sortArrayList[i] && sortArray.contains(sortArrayList[i]) && !masterActiveLyfIds.contains(lyf.id)){
                        System.debug('masterActiveLyf Does not Exists!'); 
                        System.debug('lyf inside condition to sort lifecycle stage >> ' + lyf);
                        sortedActiveStageLifecycles.add(lyf);
                        sortArray.remove(sortArrayList[i]);
                    }
                }
            }
           
            
        }
        
        if(sortedActiveStageLifecycles <> null && sortedActiveStageLifecycles.size() > 0){
            System.debug('sortedActiveStageLifecycles >> ' + sortedActiveStageLifecycles[0]);
            return sortedActiveStageLifecycles[0];
        }
        
        
        return null;
    }
    
    public Map<String, List<Lifecycle__c>> seperatedByStatuses(List<Lifecycle__c> allLyf){
        if(allLyf <> null && allLyf.size() > 0){
            Map<String, List<Lifecycle__c>> sortedLifecycles = new Map<String, List<Lifecycle__c>>();
            sortedLifecycles.put('active', new List<Lifecycle__c>());
            sortedLifecycles.put('inactive', new List<Lifecycle__c>());
            sortedLifecycles.put('masterActive', new List<Lifecycle__c>());
            sortedLifecycles.put('masterInactive', new List<Lifecycle__c>());
            sortedLifecycles.put('mergeActive', new List<Lifecycle__c>());
            sortedLifecycles.put('mergeInactive', new List<Lifecycle__c>());
            
            for(Lifecycle__c lyf : allLyf){
                if(lyf.Lifecycle_Status__c == 'Active'){
                    //add to active map
                    sortedLifecycles.get('active').add(lyf);
                    
                    //add to mater active map
                    if(!mergedLifecycles.containsKey(lyf.id))
                        sortedLifecycles.get('masterActive').add(lyf);
                    
                    //add to merge active map
                    if(mergedLifecycles.containsKey(lyf.id))
                        sortedLifecycles.get('mergeActive').add(lyf);
                    
                }else if(lyf.Lifecycle_Status__c == 'Inactive'){
                    //add to inactive map
                    sortedLifecycles.get('inactive').add(lyf);
                    
                    //add to mater active map
                    if(!mergedLifecycles.containsKey(lyf.id))
                        sortedLifecycles.get('masterInactive').add(lyf);
                    
                    //add to merge active map
                    if(mergedLifecycles.containsKey(lyf.id))
                        sortedLifecycles.get('mergeInactive').add(lyf);
                    
                }
            } 
            
            System.debug('masterActive) >> ' + sortedLifecycles.get('masterActive'));
            System.debug('Active) >> ' + sortedLifecycles.get('active'));
            System.debug('mergeActive) >> ' + sortedLifecycles.get('mergeActive'));
            System.debug('mergeInactive >> ' + sortedLifecycles.get('mergeInactive'));
            System.debug('inactive) >> ' + sortedLifecycles.get('inactive'));
            System.debug('masterInactive) >> ' + sortedLifecycles.get('masterInactive'));
            
            return sortedLifecycles;
        }
        return null;
    }
    
}