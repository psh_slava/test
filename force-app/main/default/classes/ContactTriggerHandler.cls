public class ContactTriggerHandler {  
    ContactTriggerAction action = new ContactTriggerAction();
    MergeUtil util = new MergeUtil();
    
    public void onBeforeUpdate(List<Contact> newList, Map<Id, Contact> oldMap){
        Boolean triggerEnabled = true;
        try{
            triggerEnabled = [Select Enabled__c 
                              From Lifecycle_Trigger_Settings__c
                              Where Name = 'ContactTriggerAction'
                              Limit 1].Enabled__c;
        } catch (Exception e){
            System.debug('No Lifecycle Trigger Setting Found');
        }
        //Contact To New Lifecycle Record
        if(triggerEnabled){ action.contactRecordOnbeforeupdate(newList, oldMap); }
       
        //**********Track history on Community Groups field
        for(Contact c : newList){
            if(c.Community_Groups__c != oldMap.get(c.Id).Community_Groups__c){ 
                Datetime nowMDT = System.now().addHours(-6);
                List<String> newValues = new List<String>();
                List<String> oldValues = new List<String>();
                List<String> addedValues = new List<String>();
                List<String> removedValues = new List<String>();
                String historyString;           
                
                if(c.Community_Groups__c != null){
                    newValues.addAll(c.Community_Groups__c.split(';')); 
                }      
                if(oldmap.get(c.Id).Community_Groups__c != null){
                    oldValues.addAll(oldMap.get(c.Id).Community_Groups__c.split(';'));
                } 
                
                if(newValues.size() > 0){
                    for(String newV : newValues){
                        if(!oldValues.contains(newV)){
                            addedValues.add(newV);
                        }
                    }
                }
                
                if(oldValues.size() > 0){
                    for(String oldV : oldValues){
                        if(!newValues.contains(oldV)){
                            removedValues.add(oldV);
                        }
                    }
                }
                
                
                if(oldmap.get(c.Id).Community_Groups_History__c != null){
                    historyString = 'At ' + nowMDT + ' ' + userinfo.getName();
                    if(addedValues.size() > 0){ historyString  += ' added: ' + addedValues; }
                    if(addedValues.size() > 0 && removedValues.size() > 0){ historyString  += ' and removed: ' + removedValues; }
                    if(addedValues.size() == 0 && removedValues.size() > 0){ historyString  += ' removed: ' + removedValues; }              
                    c.Community_Groups_History__c = historyString + '\n' + oldmap.get(c.Id).Community_Groups_History__c;
                }
                if(oldmap.get(c.Id).Community_Groups_History__c == null){ 
                    historyString = 'At ' + nowMDT + ' ' + userinfo.getName();
                    if(addedValues.size() > 0){ historyString += ' added: ' + addedValues; } 
                    if(addedValues.size() > 0 && removedValues.size() > 0){ historyString  += ' and removed: ' + removedValues; }
                    if(addedValues.size() == 0 && removedValues.size() > 0){ historyString  += ' removed: ' + removedValues; } 
                    c.Community_Groups_History__c = historyString;
                }                     
            }
        }
        //***************
       
    }
    public void onAfterInsert(List<Contact> newList){
        Boolean triggerEnabled = true;
        try{
            triggerEnabled = [Select Enabled__c 
                              From Lifecycle_Trigger_Settings__c
                              Where Name = 'ContactTriggerAction'
                              Limit 1].Enabled__c;
        } catch (Exception e){
            System.debug('No Lifecycle Trigger Setting Found');
        }        
        //Contact To New Lifecycle Record
        if(triggerEnabled){ action.newLifecycleRecordOnAfterInsert(newList); }
    }
    public void onAfterUpdate(List<Contact> newList, Map<Id, Contact> oldMap){
        Boolean triggerEnabled = true;
        try{
            triggerEnabled = [Select Enabled__c 
                              From Lifecycle_Trigger_Settings__c
                              Where Name = 'ContactTriggerAction'
                              Limit 1].Enabled__c;
        } catch (Exception e){
            System.debug('No Lifecycle Trigger Setting Found');
        }        
        System.debug('RecursionController.stopContactTriggerAfterUpdate >> ' + RecursionController.stopContactTriggerAfterUpdate);
        if(triggerEnabled){ 
            if(!RecursionController.stopContactTriggerAfterUpdate)
                action.existingLifecycleRecordOnAfterUpdate(newList, oldMap);
        }
    }
    public void onBeforeDelete(Map<Id, Contact> oldMap){
        System.debug('onBeforeDelete!');
        util.leadMergeProcessBeforeDelete(oldMap);
    }
    public void onAfterDelete(Map<Id, Contact> oldMap){
        System.debug('onAftereDelete!');
        
        //delete orphan record if process is NOT Merge
        //action.deleteOrphanLifecycleRecords(oldMap);
        
        util.leadMergeProcessAfterDelete(oldMap);
    }
}