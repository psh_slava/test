/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Partner_InvolvementTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Partner_InvolvementTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Partner_Involvement__c());
    }
}