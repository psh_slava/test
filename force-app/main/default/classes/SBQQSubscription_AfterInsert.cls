/** 
* SBQQSubscription_AfterInsert  Trigger Handler
*
* @author Naomi Harmon 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class SBQQSubscription_AfterInsert  extends TriggerHandlerBase {

    public override void mainEntry(TriggerParameters tp) {
       	if (TriggerHelper.DoExecute('SBQQ__Subscription__c.CentralSync')) {
            CentralSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.newList);
        }
    }
}