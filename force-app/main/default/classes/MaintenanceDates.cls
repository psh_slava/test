public class MaintenanceDates {
    
    public static void updateSubscriptions(List<Id> contractIds){

        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        
        List<SBQQ__Subscription__c> allSubs = [Select Id, Maintenance_Start_Date__c, Maintenance_End_Date__c, 
                                               SBQQ__Contract__r.Maintenance_Start_Date__c, SBQQ__Contract__r.Maintenance_End_Date__c,
                                               Extended_Grace_Period__c, SBQQ__Contract__r.Extended_Grace_Period__c, Hosting_Model__c, SBQQ__Contract__r.Hosting_Model__c
                                               From SBQQ__Subscription__c
                                               Where SBQQ__Contract__c IN :contractIds];
        
        for(SBQQ__Subscription__c s : allSubs){
            s.Maintenance_Start_Date__c = s.SBQQ__Contract__r.Maintenance_Start_Date__c;
            s.Maintenance_End_Date__c = s.SBQQ__Contract__r.Maintenance_End_Date__c;
            s.Hosting_Model__c = s.SBQQ__Contract__r.Hosting_Model__c;
            s.Extended_Grace_Period__c = s.SBQQ__Contract__r.Extended_Grace_Period__c;
            subsToUpdate.add(s);
        }
        
        if(subsToUpdate.size() > 0){
            update subsToUpdate;
        }
        
    }

}