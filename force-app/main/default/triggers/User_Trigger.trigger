trigger User_Trigger on User (after insert) {
    
    List<User> newPartnerUsers = new List<User>();
    UserTriggerAction action = new UserTriggerAction();
    for(User u : trigger.new){
        if(u.ContactId != null){
            newPartnerUsers.add(u);
        }
    }
    if(newPartnerUsers != null && newPartnerUsers.size()> 0){
        action.addPartnerUserToPublicGroup(newPartnerUsers);
    }

}