public with sharing class LicenseKeyGenCallout {
    
    public static String makeLKGCallout(String instanceName, Date maintenanceEndDate, Decimal quantity, Boolean hostedSaaS){ 
        
        Date d = maintenanceEndDate;
        Datetime ddd;
        String dropDeadDate;
        if(d != null) { 
            ddd = datetime.newInstance(d.year(), d.month(), d.day());
            dropDeadDate = ddd.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX');
        }
        
        Datetime gd = datetime.now();
        String generatedDate = gd.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX');
        
        String apiKey;
        if(Test.isRunningTest()){ 
            apiKey = 'npeeD8V8dUS4iQA+R8/aAg=='; 
        } else { 
            apiKey = [Select API_Key__c from LicenseKeyGeneratorAPI__c].API_Key__c;
        }
        System.debug(apiKey);
        
        //Convert to json body
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('ApiKey', apiKey);
        gen.writeStringField('CompanyName', instanceName);
        if(dropDeadDate != null){ gen.writeStringField('DropDead', dropDeadDate); }
        gen.writeStringField('RequestDateTime', generatedDate);
        gen.writeBooleanField('Eval', false);
        gen.writeBooleanField('HostedSaas', hostedSaaS);
        gen.writeBooleanField('InternalUse', false);
        gen.writeNumberField('LicenseCount', quantity);
        gen.writeBooleanField('MultipleLogins', false);
        gen.writeNullField('ProductCode');
        gen.writeNumberField('RecordCount',0);
        String jsonS = gen.getAsString();
        System.debug('jsonMaterials'+jsonS);
        
        String endpointURL;
        if(Test.isRunningTest()){ 
            endpointURL = 'https://ec2-52-33-76-77.us-west-2.compute.amazonaws.com/api/KeyGen'; 
        } else { 
            endpointURL = [Select API_URL__c from LicenseKeyGeneratorAPI__c].API_URL__c;
        }
        System.debug(endpointURL);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointURL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        
        //Set the body as a JSON object
        request.setBody(jsonS);
        
        HttpResponse response = http.send(request);
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
        
        //return response.getBody().replace('"','');
        Map<String,String> jsonResponse = (Map<String,String>)
            JSON.deserialize(response.getBody(), Map<String,String>.class);
        String key = jsonResponse.get('LicenseKey');
        System.debug('Deserialized key ====' + key);
        return key;
    }
    
}