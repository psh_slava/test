/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
 
@IsTest
private class dlrs_refedge_Reference_Basic_InfoaF6Test
{
    @IsTest
    private static void testTrigger()
    {
        //Force the dlrs_refedge_Reference_Basic_IaF6Trigger to be invoked, fails the test if org config or other Apex code prevents this.
        Account a = new Account(name='test account');
        insert a;
        
        refedge__Reference_Basic_Information__c rbi = new refedge__Reference_Basic_Information__c();
        rbi.refedge__Account__c = a.Id;
        insert rbi;
        
        dlrs.RollupService.testHandler(new refedge__Reference_Basic_Information__c());
    }
}