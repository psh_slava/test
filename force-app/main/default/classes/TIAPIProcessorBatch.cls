global class TIAPIProcessorBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
	
	//Get all contacts needing to be updated with TI License
	global Database.QueryLocator start(Database.BatchableContext BC) {
        Date initiatedDate = Date.newInstance(2019, 5, 21);
		return Database.getQueryLocator( 'Select Id, CreatedDate, AccountId, StockKeepingUnit, ContactId, Product2Id, Product2.TI_Course_Slug__c, ' +
                                         'Product2.TI_Learning_Path_Slug__c, Product2.TI_Bundle_Slug__c, Account.TI_Employee_SKU__c ' +
                                         //'From Asset Where CreatedDate = TODAY ' +
                                         'From Asset Where StockKeepingUnit != null ' +
                                         'And CreatedDate > :initiatedDate ' + 
                                         'And Account_Record_Type__c = \'Customer\' And ProductFamily = \'Learning Services\' ');
	}

	global void execute(Database.BatchableContext BC, List<Asset> scope) {
        
        String clientId = [Select Customer_ClientId__c From TI_API_Settings__c Limit 1].Customer_ClientId__c;
        
        //System.debug('BATCH GET TI users....');
        //List<ResponseResult.TIUser> userList = TIAPIWrapper.getTIUsersInformation();
        System.debug('BATCH GET user packages...');
        //List<ResponseResult.TIUser> userPackagesList = TIAPIWrapper.getUserPackages(userList, scope);
        List<ResponseResult.TIUser> userPackagesList = TIAPIWrapper.getUserPackages(scope);
        System.debug('BATCH PUT user updates....' + userPackagesList);
        TIAPIWrapper.updateTIUsers(null, null, null, userPackagesList);
	}

	global void finish(Database.BatchableContext BC){ } 

}