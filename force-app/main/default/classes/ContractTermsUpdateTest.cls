@isTest
public class ContractTermsUpdateTest {
    
    public static testmethod void contracttermsupdatetest(){
        Account a = new Account(name='Test Account', BillingStreet = '123 Street', BillingCity = 'TestCity', BillingState = 'Colorado', BillingCountry = 'United States', BillingPostalCode = '80920');
        insert a;
        
        Account pa = new Account(name='Test Partner Account', Reseller_Partner__c = true, Partner_Types__c = 'Reseller Partner');
        insert pa;
        
        Account pa2 = new Account(name='Test Partner Account 2', Reseller_Partner__c = true, Partner_Types__c = 'Reseller Partner');
        insert pa2;
        
        List<Contract_Term_Mappings__c> fieldMappings = new List<Contract_Term_Mappings__c>();
        fieldMappings.add(new Contract_Term_Mappings__c(Name='Hosting', Mirror_Field_API_Name__c = 'Hosting_Model__c', From_Object_API_Name__c = 'SBQQ__Opportunity2__r', From_Field_API_Name__c = 'Hosting_Model__c', 
                                                        Update__c=false, Active__c = true));
        fieldMappings.add(new Contract_Term_Mappings__c(Name='Reseller Partner', Mirror_Field_API_Name__c = 'Billing_Thru_Partner__c', From_Object_API_Name__c = 'SBQQ__Opportunity2__r', 
                                                        From_Field_API_Name__c = 'Reseller_Partner__c', Update__c=true, Active__c = true));
        fieldMappings.add(new Contract_Term_Mappings__c(Name = 'Legal Special Addendums', Mirror_Field_API_Name__c = 'Legal_Special_Addendums__c', From_Object_API_Name__c = 'SBQQ__Quote__c',
                                                        From_Field_API_Name__c = 'Legal_Special_Addendums__c', Update__c=true, Active__c = true));
        fieldMappings.add(new Contract_Term_Mappings__c(Name = 'Financial Special', Mirror_Field_API_Name__c = 'Financial_Special_Addendums__c', From_Object_API_Name__c = 'SBQQ__Quote__c',
                                                        From_Field_API_Name__c = 'Financial_Special_Addendums__c', Update__c=true, Active__c = true));
        insert fieldMappings;
        
        Contact con = new Contact(AccountId = a.Id, LastName = 'Tester', Email = 'test@test.test');
        insert con;
        
        Opportunity o = new Opportunity(AccountId = a.Id, StageName = 'Discover', Forecast_Category__c = 'Omit', CloseDate = Date.today().addDays(30), Name = 'Test Opp',
                                       Primary_Contact__c = con.Id, Hosting_Model__c = 'On Premise', Reseller_Partner__c = pa.Id);
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__StartDate__c = Date.today(), Legal_Special_Addendums__c  = 'Test legal terms', Hosting_Model__c = 'On Premise', SBQQ__Opportunity2__c = o.Id, Financial_Special_Addendums__c = 'Test Terms');
        insert q;
        
        Contract c = new Contract(AccountId = a.Id, StartDate = Date.today(), Legal_Special_Addendums__c = 'Test legal terms', SBQQ__Quote__c = q.Id, Billing_Thru_Partner__c = pa2.Id);
        insert c;
        
    }
    
     public static testmethod void carryovertermstest(){
        String renewalRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
         
        Account a = new Account(name='Test Account 2');
        insert a;
         
        Contact con = new Contact(AccountId = a.Id, LastName = 'Tester2', Email = 'test2@test.test');
        insert con;
         
        Contract oc = new Contract(AccountId = a.Id, StartDate = Date.today(), Legal_Special_Addendums__c = 'Test legal terms');
        insert oc;
         
        Opportunity o = new Opportunity(AccountId = a.Id, StageName = 'Discover', Forecast_Category__c = 'Omit', CloseDate = Date.today(), Name = 'Test Opp', RecordTypeId = renewalRTId,
                                       SBQQ__RenewedContract__c = oc.Id, Primary_Contact__c = con.Id, Hosting_Model__c = 'On Premise');
        insert o;  
         
        Contract c = new Contract(AccountId = a.Id, StartDate = Date.today(), Legal_Special_Addendums__c = 'Test legal terms', SBQQ__Opportunity__c = o.Id, Carry_Over_Original_Terms__c = false);
        insert c;
         
        c.Carry_Over_Original_Terms__c = true;
        update c;
     }
        
}