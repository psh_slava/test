trigger ContactTrigger on Contact ( before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    TriggerFactory.createTriggerDispatcher(Contact.sObjectType);
        
    ContactTriggerHAndler handler = new ContactTriggerHAndler();
    
    if(trigger.isBefore){
        if(trigger.isInsert){ 
            ValidateCountryStateNames.runValidationCheck(trigger.new, null, null); 
        }
    }
    if(trigger.isAfter && trigger.isInsert){
            
            //*****
            //When a new Learner Contact is added, send notice to the LMS
            List<Id> newLearners = new List<Id>();
            for(Contact c : trigger.new){
                if(c.Learner__c == true){
                        newLearners.add(c.Id);
                }
            }
            if(newLearners.size()>0){
                GnosisAPIController.passContactUpdates(null, newLearners);
            }
            //*****
        
        }
    if(trigger.isBefore){
        if(trigger.isUpdate){
            List<Contact> contactsToValidate = new List<Contact>();
            for(Contact c : trigger.new){
                if((c.Country_External_Source__c != null && c.Country_External_Source__c != trigger.oldMap.get(c.Id).Country_External_Source__c) ||
                   (c.State_External_Source__c != null && c.State_External_Source__c != trigger.oldMap.get(c.Id).State_External_Source__c)){
                       contactsToValidate.add(c);
                   }
            }
            if(contactsToValidate.size()>0){
                ValidateCountryStateNames.runValidationCheck(contactsToValidate, null, null);
            }
            
            //*****
            //When a Contact's status changes, send a notice to the LMS to update Learner
            List<Id> updatedContacts = new List<Id>(); 
            for(Contact c : trigger.new){
                if(trigger.oldMap.get(c.Id).Status__c  != trigger.newMap.get(c.Id).Status__c){
                    updatedContacts.add(c.Id);
                }
            }
            if(updatedContacts.size()>0){
                GnosisAPIController.passStatusUpdates(null, updatedContacts);
            }
            //*****
            
            //*****
            //When a Contact's relevant details change, send notice to the LMS to update Learner
            List<Id> changedContacts = new List<Id>();
            for(Contact c : trigger.new){
                if(c.Learner__c == true){
                    if(trigger.oldMap.get(c.Id).FirstName != trigger.newMap.get(c.Id).FirstName ||
                       trigger.oldMap.get(c.Id).LastName != trigger.newMap.get(c.Id).LastName ||
                       trigger.oldMap.get(c.Id).Email != trigger.newMap.get(c.Id).Email ||
                       trigger.oldMap.get(c.Id).AccountId != trigger.newMap.get(c.Id).AccountId ||
                       trigger.oldMap.get(c.Id).LastName != trigger.newMap.get(c.Id).LastName ||
                       trigger.oldMap.get(c.Id).Learner__c == false && trigger.newMap.get(c.Id).Learner__c == true
                      )
                        changedContacts.add(c.Id);
                }
            }
            if(changedContacts.size()>0){
                GnosisAPIController.passContactUpdates(null, changedContacts);
            }
            //*****
            
        }
        
        if(trigger.isUpdate){
           
            handler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
        if(trigger.isDelete){
            System.debug('Before Delete triggered!');
            handler.onBeforeDelete(trigger.oldMap);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
            handler.onAfterInsert(trigger.new);
        }
        if(trigger.isUpdate){
            handler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
        if(trigger.isDelete){
            handler.onAfterDelete(trigger.oldMap);
        }
    }
}