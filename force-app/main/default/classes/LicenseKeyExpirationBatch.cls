global class LicenseKeyExpirationBatch implements Schedulable, Database.Batchable<SObject>, Database.Stateful {
    
    //Schedulable
    global void execute( SchedulableContext SC ){
        LicenseKeyExpirationBatch lkeb = new LicenseKeyExpirationBatch();
        Database.executeBatch(lkeb);
    }
    
    //Start
    global Database.QueryLocator start( Database.BatchableContext BC ){
        
        String query = ' Select Id From CSM_License_Record__c Where Expires__c < TODAY ';
        
        return Database.getQueryLocator(query);
        
    }
    
    //Execute
    global void execute( Database.BatchableContext BC, SObject[] scope ){
        
        List<CSM_License_Record__c> lkrsExpired = new List<CSM_License_Record__c>();
        
        for( SObject s : scope ){
            CSM_License_Record__c lkr = (CSM_License_Record__c)s;
            lkr.Current_Key__c = 'No';
            lkrsExpired.add(lkr);
        }
        
        update lkrsExpired;
    }
    
    //Finish
    global void finish( Database.BatchableContext BC ){
        System.debug(' Completed Batch update of expired License Keys. ');
    }

}