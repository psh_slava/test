/** 
* Task_BeforeDelete  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Task_BeforeDelete  extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @date 20140701
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
        //Map<Id, Integer> leadMap = new Map<Id, Integer>();

        //for(Task t : (List<Task>)tp.oldList) {
        //    String tid = t.WhoId;
        //    if(tid.left(3) == '00Q') {
        //        leadMap.put(t.WhoId, 0);
        //    }
        //}
        //System.debug('leadMap size: '+leadMap.size());
        //TaskHelper.countCompletedTasks(leadMap);
    }
    /** 
    * @author CRMCulture
    * @version 1.00, 20140701
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}
}