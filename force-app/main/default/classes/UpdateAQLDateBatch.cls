global class UpdateAQLDateBatch implements Database.Batchable<sObject>{

   global Database.QueryLocator start(Database.BatchableContext BC){
    
      String Query = 'Select id,AQL_Entry_Date__c,Related_Contact__c,Related_Lead__c from Lifecycle__c where AQL_Entry_Date__c != Null';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Lifecycle__c> scope){
     Set<id> Contactids = New Set<id>();
     Set<id> Leadids = New Set<id>();
        For(Lifecycle__c L:scope)
     {
    
       IF(L.Related_Contact__c !=Null && L.AQL_Entry_Date__c != Null )
       {
        Contactids.add(L.Related_Contact__c);
       }
       IF(L.Related_Lead__c!=Null && L.AQL_Entry_Date__c != Null )
       {
        Leadids.add(L.Related_Lead__c);
       }
           
     } 
     If(Contactids.Size()>0 )
     {
     Map<id,Contact> UpdateMapcontacts = New Map<id,Contact>();
     List<contact> lstcontacts = [Select id,AQL_Date__c from Contact where id IN :Contactids]; 
     For(Lifecycle__c L:scope)
     {
        For(contact C :lstcontacts)
        {
          IF(c.id == L.Related_Contact__c)
          {
            c.AQL_Date__c = L.AQL_Entry_Date__c;
            UpdateMapcontacts.Put(C.id,C);
          }
        }
     }
     
      Update UpdateMapcontacts.Values();
     
     }
     If(Leadids.Size()>0 )
     {
     Map<id,Lead> UpdateMapLeads = New Map<id,Lead>();
     List<Lead> LstLeads = [Select id,AQL_Date__c from Lead where id IN :Leadids];
     
     For(Lifecycle__c L:scope)
     {
        For(Lead C :LstLeads)
        {
          IF(c.id == L.Related_Lead__c)
          {
            c.AQL_Date__c = L.AQL_Entry_Date__c;
            UpdateMapLeads.Put(C.id,C);
          }
        }
     }
     
      Update UpdateMapLeads.Values();
     
     }
  }
   global void finish(Database.BatchableContext BC){
   }
}