public class LicenseKeyContacts {
    
    public static void populateLicenseKeyContacts(Id licenseKey, Id accountId){
        
        //Query Contact Roles with Role = License Key Contact related to Account with Id = accountId 
        //Get the Contact email addresses
        //If none, do nothing
        //If out of the list, one already equals the Primary Contact's email, then skip that one
        //Then, for each other, up to 5, fill in email addresses 1 by 1 
        //Populate the License Key Contact Email 2 - 6 email fields on the licenseKey record with Id = licenseKey
        
        List<AccountContactRole> cRoles = new List<AccountContactRole>();
        List<AccountContactRole> pRoles = new List<AccountContactRole>();
       
        CSM_License_Record__c key = [Select Id, Primary_Contact__r.Email, Reseller_Partner_Account__c, Partner_Contact__r.Email
                                     From CSM_License_Record__c
                                     Where Id = :licenseKey];
        Id partnerId = key.Reseller_Partner_Account__c;
        String pcEmail = key.Primary_Contact__r.Email;
        String prtEmail = key.Partner_Contact__r.Email;
        
        try{
            cRoles = [Select ContactId
                      From AccountContactRole
                      Where AccountId = :accountId
                      And Role = 'License Key Contact'];
            System.debug('Contact Roles - ' + cRoles);
            
        } catch (Exception e){
            System.debug('Exception caught while searching for License Key Contact roles on Account = ' + accountId);
        } 
        if(partnerId != null){
            try{
                pRoles = [Select ContactId
                          From AccountContactRole
                          Where AccountId = :partnerId
                          And Role = 'License Key Contact'];
                System.debug('Partner Contact Roles - ' + pRoles);
            } catch (Exception e){
                System.debug('Exception caught while searching for Partner License Key Contact roles on Account = ' + partnerId);
            }       
        }

        List<Id> cIds = new List<Id>();   
        if(cRoles.size() > 0){
            for(AccountContactRole cR : cRoles){
                cIds.add(cR.ContactId);
            }
            
            List<Contact> contacts = [Select Email, FirstName, LastName
                                      From Contact
                                      Where Id IN :cIds
                                      And Email != :pcEmail
                                      Limit 5];
            
            Integer count = 1;
            for(Contact c : contacts){
                count++;
                String fieldName = 'License_Key_Contact_Email_'+ count +'__c';
                key.put(fieldName, c.Email);
            }
        }
        
        List<Id> pIds = new List<Id>();
        if(pRoles.size() > 0){
            for(AccountContactRole pR : pRoles){
                pIds.add(pR.ContactId);
            }
            
            List<Contact> partners = [Select Email, FirstName, LastName
                                      From Contact
                                      Where Id IN :pIds
                                      And Email != :prtEmail
                                      Limit 5];
            
            Integer count = 1;
            for(Contact p : partners){
                count++;
                String fieldName = 'Partner_Contact_Email_'+ count + '__c';
                key.put(fieldName, p.Email);
            }
        }
        update key;        
    }
}