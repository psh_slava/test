/*
* @class name:   BookingsProcessor
* @created:      By Naomi Harmon in Apr 2020
* @test class:   BookingsProcessorTest.apxc
* @initiated by: ContractTriggerHandler.apxc
* @description: 
*    Creates/updates Invoicing Opportunities for multi-year Contracts as well as calculates and sets offset amounts 
*    on Invoicing/Renewal Opportunities for Upsells that were done in the last 365 days
* @modifcation log:
*    Naomi Harmon - Jun 2020 - Adding a recalculation method for when an Invoicing opp that includes offsets has its 
 date manually updated. This should udpate the proration of the offset calculation.   
*    Naomi Harmon - Oct 2020 - Updating the Offset calculation to match the Proration Calculator, and moving calculation method to separate class (BookingsOffsetCalculator)
*    Naomi Harmon - Oct 2020 - Changed default Invoicing Opportunity Stage from 'Win Pending' to 'Propose'
*    Naomi Harmon - Oct 2020 - Changed default Close Date to 'plus one year minus one day'
*/

public class BookingsProcessor {
    
    public static boolean RecursionHandler = True;    
    public static boolean RecursionControl(){        
        if(RecursionHandler){            
            RecursionHandler = False;           
            return true;            
        }        
        else{           
            return RecursionHandler;           
        }    
    }
    
    //For Contracts with a term of more than 2 years,
    //create an Opportunity for each year that serves as a way to book that year's invoice
    
    public static void bookMidTerm(List<Contract> contracts){
        
        //put all Contract Ids in a list for queries
        //and put all Contract start dates in a map to reference later
        List<Id> contractIds = new List<Id>();
        Map<Id, Date> contractStartDateMap = new Map<Id, Date>();
        for(Contract c : Contracts){
            contractIds.add(c.Id);
            contractStartDateMap.put(c.Id, c.StartDate);
        }
        
        //Get Record Type Id for the Opportunities we're going to be creating
        String invoicingRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
        
        
        //For OLI creation, get all pricebook entries
        List<PriceBookEntry> priceBookList = [Select Id, Product2Id, CurrencyIsoCode, Product2.Id, Product2.Name, Product2.SBQQ__SubscriptionTerm__c 
                                              From PriceBookEntry 
                                              Where PriceBook2.isStandard = true
                                              And Product2.IsActive = true];
        Map<String, Id> productPriceMap = new Map<String, Id>();
        Map<Id, Integer> productTermMap = new Map<Id, Integer>();
        for(PriceBookEntry pbe : priceBookList){
            String productIdLong = pbe.Product2.Id;
            String productIdCurrency = productIdLong.substring(0,15) + pbe.CurrencyIsoCode;
            productPriceMap.put(productIdCurrency, pbe.Id);    
            productTermMap.put(pbe.Product2.Id, Integer.valueOf(pbe.Product2.SBQQ__SubscriptionTerm__c));
        }
        System.debug('Product term map ==' + productTermMap);
        System.debug('Product price map == ' + productPriceMap);
        
        //Get all Subscriptions tied to these Contracts
        Map<Id, List<SBQQ__Subscription__c>> contractToSubsMap = new Map<Id, List<SBQQ__Subscription__c>>();
        List<SBQQ__Subscription__c> subs = [Select Id, SBQQ__ProductId__c, SBQQ__RenewalQuantity__c, SBQQ__RenewalPrice__c, SBQQ__ProrateMultiplier__c, CurrencyIsoCode,
                                            SBQQ__Quantity__c, SBQQ__NetPrice__c, SBQQ__SubscriptionStartDate__c, SBQQ__StartDate__c, SBQQ__Contract__c, Net_Unit_Price__c,
                                            SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate
                                            From SBQQ__Subscription__c
                                            Where SBQQ__Contract__c IN :contractIds
                                            And (SBQQ__ListPrice__c > 0 OR
                                                 SBQQ__Bundled__c = true)
                                            And (SBQQ__SubscriptionEndDate__c = null OR
                                                 SBQQ__SubscriptionEndDate__c > TODAY)];
        for(Contract c : contracts){
            List<SBQQ__Subscription__c> thisContractsSubs = new List<SBQQ__Subscription__c>();
            for(SBQQ__Subscription__c sub : subs){
                if(sub.SBQQ__Contract__c == c.Id){
                    thisContractsSubs.add(sub);
                }
            }
            contractToSubsMap.put(c.Id, thisContractsSubs);
        }
        System.debug('Subscriptions map - ' + contractToSubsMap);
        
        //Get any existing Invoicing Opportunities for this Contract
        List<Opportunity> allExistingInvoices = [Select Id, CloseDate, ContractId, Name
                                                 From Opportunity
                                                 Where ContractId IN :contractIds
                                                 And IsClosed = false
                                                 //And CloseDate > Today
                                                 And RecordTypeId = :invoicingRTId];
        List<Id> allExistingInvoiceIds = new List<Id>();
        for(Opportunity opp : allExistingInvoices){
            allExistingInvoiceIds.add(opp.Id);
        }
        Map<Id,List<Opportunity>> contractToExistingOppsMap = new Map<Id,List<Opportunity>>();
        for(Id cId : contractIds){
            List<Opportunity> thisContractsExistingOpps = new List<Opportunity>();
            if(allExistingInvoices.size()>0){
                for(Opportunity o : allExistingInvoices){
                    if(o.ContractId == cId){
                        thisContractsExistingOpps.add(o);
                    }
                }
            } 
            contractToExistingOppsMap.put(cId, thisContractsExistingOpps);
        }
        
        //Get all existing OLIs on these Invoicing Opportunities
        List<OpportunityLineItem> allExistingOLIs = [Select Id, OpportunityId, Original_Subscription_Id__c, Offset_Amount_for_Renewal_Booking__c
                                                     From OpportunityLineItem
                                                     Where OpportunityId IN :allExistingInvoiceIds];
        Map<Id,List<OpportunityLineItem>> existingOppToOLIsMap = new Map<Id,List<OpportunityLineItem>>();
        for(Opportunity eOpp : allExistingInvoices){
            List<OpportunityLineItem> thisOppsOLIs = new List<OpportunityLineItem>();
            if(allExistingOLIs.size()>0){
                for(OpportunityLineItem oli : allExistingOLIs){
                    if(oli.OpportunityId == eOpp.Id){
                        thisOppsOLIs.add(oli);
                    }
                }
            } 
            existingOppToOLIsMap.put(eOpp.Id, thisOppsOLIs);
        }
        
        //Evaluate each Contract to get/create the Bookings ("Invoicing" Opps)
        List<Opportunity> newBookingsOpps = new List<Opportunity>();
        List<Opportunity> allBookingsOpps = new List<Opportunity>();
        for(Contract c : Contracts){
            //Owner of invoicing event should be the creator of the Contract (typically a Finance team member)
            Id ownerId;
            ownerId = c.CreatedById;     
            
            Integer term = c.StartDate.monthsBetween(c.EndDate.addDays(1))/12;
            System.debug('Term for Contract ' + c.Id + ' - ' + term); 
            
            Integer count = 1;
            while(count < term){
                //Date invoiceDate = c.StartDate.addDays(365*count);
                Date invoiceDate = (c.StartDate.addYears(count)).addDays(-1);
                System.debug('Invoice Date for Invoice Opp#' + count + ' - ' + invoiceDate);
                String invoiceName = c.Account_Name__c + ' - Invoicing Opp for ' + c.StartDate.addDays(365*count).year();
                
                Boolean thisInvoiceExists = false;
                if(contractToExistingOppsMap.get(c.Id) != null){
                    for(Opportunity thisOpp : contractToExistingOppsMap.get(c.Id)){
                        if(thisOpp.Name == invoiceName){
                            thisInvoiceExists = true;
                            allBookingsOpps.add(thisOpp);
                        }
                    }
                }
                
                if(thisInvoiceExists == false){
                    Opportunity bookingOpp = new Opportunity();
                    bookingOpp.AccountId = c.AccountId;
                    bookingOpp.CurrencyIsoCode = c.CurrencyIsoCode;
                    bookingOpp.Name = invoiceName;
                    bookingOpp.RecordTypeId = invoicingRTId;
                    bookingOpp.CloseDate = invoiceDate;
                    bookingOpp.StageName = 'Propose';
                    bookingOpp.Forecast_Category__c = 'Commit';
                    bookingOpp.OwnerId = ownerId;
                    bookingOpp.Close_Reason__c = null;
                    bookingOpp.ContractId = c.Id;
                    if(c.Original_Sales_Order_Number__c != null){ bookingOpp.Sales_Order_Number__c = c.Original_Sales_Order_Number__c; }
                    if(c.Hosting_Model__c != null){ bookingOpp.Hosting_Model__c = c.Hosting_Model__c; }
                    if(invoiceDate > Date.today()){ newbookingsOpps.add(bookingOpp); allBookingsOpps.add(bookingOpp); }
                }                
                count++;              
            }
            System.debug('Number of Invoicing Opps to insert - ' + newbookingsOpps.size());
        }
        insert newbookingsOpps;
        
        List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
        Boolean offsetApplied = false;
        Map<OpportunityLineItem, Decimal> oppLineToOffsetMap = new Map<OpportunityLineItem, Decimal>();
        for(Opportunity opp : allbookingsOpps){
            for(SBQQ__Subscription__c sub : contractToSubsMap.get(opp.ContractId)){
                OpportunityLineItem thisLineItem;
                String thisProductIdCurrency = sub.SBQQ__ProductId__c + sub.CurrencyIsoCode;
                Integer productTerm = productTermMap.get(sub.SBQQ__ProductId__c);
                System.debug('Product term for ' + sub.SBQQ__ProductId__c + ' = ' + productTerm);
                String priceTerm;
                if(productTerm == 12){ priceTerm = 'annually'; }
                if(productTerm == 1){ priceTerm = 'monthly'; }
                
                //Run through all existing OLIs and see if one exists already for this sub
                if(contractToExistingOppsMap.get(opp.ContractId).contains(opp)){
                    if(existingOppToOLIsMap.get(opp.Id) != null){
                        for(OpportunityLineItem oli : existingOppToOLIsMap.get(opp.Id)){
                            if(oli.Original_Subscription_Id__c == sub.Id){
                                thisLineItem = oli;
                            }
                        }
                    }
                }
                if(thisLineItem == null){    
                    thisLineItem = new OpportunityLineItem();                 
                    thisLineItem.OpportunityId = opp.Id;
                    thisLineItem.Product2Id = sub.SBQQ__ProductId__c;
                    thisLineItem.PricebookEntryId = productPriceMap.get(thisProductIdCurrency);
                    thisLineItem.Original_Subscription_Id__c = sub.Id;
                    System.debug('This Line Pricebook Entry Id (looking up value ' + thisProductIdCurrency + ' = ' + productPriceMap.get(thisProductIdCurrency));
                }
                
                thisLineItem.Quantity = sub.SBQQ__RenewalQuantity__c;
                Decimal unitPrice;
                Decimal pricePerYear;
                //Was this subscription discounted for a partner?
                if(sub.SBQQ__RenewalPrice__c != null){ unitPrice = sub.SBQQ__RenewalPrice__c; }
                if(sub.SBQQ__RenewalPrice__c == null || sub.SBQQ__RenewalPrice__c != sub.Net_Unit_Price__c && sub.Net_Unit_Price__c != null){ unitPrice = sub.Net_Unit_Price__c; }
                System.debug(priceTerm + ', ' + unitPrice + ', ' + sub.SBQQ__RenewalQuantity__c);
                if(priceTerm == 'monthly') { thisLineItem.First_Year_Value_Text__c = unitPrice * sub.SBQQ__RenewalQuantity__c * 12; 
                                            pricePerYear = unitPrice * sub.SBQQ__RenewalQuantity__c * 12; }
                if(priceTerm == 'annually'){ thisLineItem.First_Year_Value_Text__c = unitPrice * sub.SBQQ__RenewalQuantity__c; 
                                            pricePerYear = unitPrice * sub.SBQQ__RenewalQuantity__c; }
                if(priceTerm == 'monthly') { thisLineItem.UnitPrice = unitPrice * 12; }
                if(priceTerm == 'annually'){ thisLineItem.UnitPrice = unitPrice; }
                
                //Should there be an offset?
                /****** Get Offset Calcuation *****/
                Date subStart = sub.SBQQ__SubscriptionStartDate__c;
                Decimal daysTillInvoice = subStart.daysBetween(opp.CloseDate);
                System.debug('Days till invoice = ' + daysTillInvoice);
                Decimal offsetAmount;
                if(sub.SBQQ__SubscriptionStartDate__c > contractStartDateMap.get(sub.SBQQ__Contract__c) && daysTillInvoice <= 365){
                    offsetAmount = BookingsOffsetCalculator.calculateOffset(subStart, opp.CloseDate, pricePerYear);
                }
                
                //Apply offset when appropriate
                if(offsetAmount != null){ 
                    offsetApplied = true;
                    System.debug('Applying offset...');
                    if(oppLineToOffsetMap.containsKey(thisLineItem)){ 
                        Decimal totalOffset = offsetAmount + oppLineToOffsetMap.get(thisLineItem);
                        oppLineToOffsetMap.remove(thisLineItem);
                        oppLineToOffsetMap.put(thisLineItem, totalOffset);
                    }
                    if(!oppLineToOffsetMap.containsKey(thisLineItem)){
                        oppLineToOffsetMap.put(thisLineItem, offsetAmount);
                    }                   
                }
                if(offsetAmount == null){
                    lineItems.add(thisLineItem);
                }
            }      
        }
        for(OpportunityLineItem ol : oppLineToOffsetMap.keySet()){
            ol.Offset_Amount_for_Renewal_Booking__c = oppLineToOffsetMap.get(ol);
            lineItems.add(ol);
        }
        
        System.debug('Number of Line Items to upsert - ' + lineItems.size());
        System.debug('Line Items to upsert - ' + lineItems);
        upsert lineItems;
    }/*
* End bookMidTerm */       
    
    @future
    public static void offsetRenewal(List<Id> contractIds, List<Id> renewalOpps, Date earliestEndDate){
        
        List<SBQQ__QuoteLine__c> linesToUpdate = new List<SBQQ__QuoteLine__c>();
        
        //Evaluate the contracts' subscriptions
        //If a subscription requires an offset on Renewal, then populate that Quote Line's Offset Amount for Renewal Bookings with the Offset
        //Get the Quote Lines tied to the Renewal's primary Quote
        //Filter those Quote Lines to find the match for each subscription based on that QL's RenewedSubscription Id
        
        
        //Get all subscriptions that are within a year of the earliestEndDate
        Map<Id, List<SBQQ__Subscription__c>> contractToSubsMap = new Map<Id, List<SBQQ__Subscription__c>>();
        List<SBQQ__Subscription__c> subs = [Select Id, SBQQ__ProductId__c, SBQQ__RenewalQuantity__c, SBQQ__RenewalPrice__c, SBQQ__ProrateMultiplier__c, CurrencyIsoCode, SBQQ__RevisedSubscription__c,
                                            SBQQ__Quantity__c, SBQQ__NetPrice__c, SBQQ__SubscriptionStartDate__c, SBQQ__StartDate__c, SBQQ__EndDate__c, SBQQ__Contract__c, SBQQ__Contract__r.StartDate, Net_Unit_Price__c
                                            From SBQQ__Subscription__c
                                            Where SBQQ__Contract__c IN :contractIds
                                            And (SBQQ__ListPrice__c > 0 OR
                                                 SBQQ__Bundled__c = true)
                                            And SBQQ__SubscriptionStartDate__c > : earliestEndDate.addDays(-365)
                                           ];
        List<Id> subIds = new List<Id>();
        for(SBQQ__Subscription__c sub : subs){          
            if(sub.SBQQ__RevisedSubscription__c != null){ subIds.add(sub.SBQQ__RevisedSubscription__c); }
            if(sub.SBQQ__RevisedSubscription__c == null){ subIds.add(sub.Id); }
        }
        
        if(subs != null && subs.size() > 0){
            for(Id cId : contractIds){
                List<SBQQ__Subscription__c> thisContractsSubs = new List<SBQQ__Subscription__c>();
                for(SBQQ__Subscription__c sub : subs){
                    if(sub.SBQQ__Contract__c == cId){
                        thisContractsSubs.add(sub);
                    }
                }
                contractToSubsMap.put(cId, thisContractsSubs);
            }
            System.debug('Subscriptions map - ' + contractToSubsMap);
            
            List<SBQQ__QuoteLine__c> quoteLines = [Select Id, SBQQ__RenewedSubscription__c, Offset_Amount_for_Renewal_Booking__c, SBQQ__Quote__r.SBQQ__StartDate__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate, SBQQ__DefaultSubscriptionTerm__c
                                                   From SBQQ__QuoteLine__c
                                                   Where SBQQ__RenewedSubscription__c IN : subIds
                                                   And SBQQ__Quote__r.SBQQ__Primary__c = true];
            Map<SBQQ__QuoteLine__c, Id> quoteLineToSubMap = new Map<SBQQ__QuoteLine__c, Id>();
            for(SBQQ__QuoteLine__c ql : quoteLines){
                quoteLineToSubMap.put(ql, ql.SBQQ__RenewedSubscription__c);
            }
            
            Map<SBQQ__QuoteLine__c, Decimal> quoteLineToOffsetMap = new Map<SBQQ__QuoteLine__c, Decimal>();
            for(SBQQ__Subscription__c sub : subs){
                String subId;
                if(sub.SBQQ__RevisedSubscription__c != null){ subId = sub.SBQQ__RevisedSubscription__c; }
                if(sub.SBQQ__RevisedSubscription__c == null){ subId = sub.Id; }
                
                for(SBQQ__QuoteLine__c thisLine : quoteLineToSubMap.keySet()){
                    if(quoteLineToSubMap.get(thisLine) == subId){
                        SBQQ__QuoteLine__c thisSubsQuoteLine = thisLine;
                        Decimal unitPrice;
                        Decimal pricePerYear;
                        String priceTerm;
                        if(thisSubsQuoteLine.SBQQ__DefaultSubscriptionTerm__c == 12){ priceTerm = 'annually'; }
                        if(thisSubsQuoteLine.SBQQ__DefaultSubscriptionTerm__c == 1){ priceTerm = 'monthly'; }  
                        //if(sub.SBQQ__RenewalPrice__c != null){ unitPrice = sub.SBQQ__RenewalPrice__c; }
                        //if(sub.SBQQ__RenewalPrice__c == null){ unitPrice = sub.SBQQ__NetPrice__c/sub.SBQQ__ProrateMultiplier__c; }
                        unitPrice = sub.SBQQ__NetPrice__c/sub.SBQQ__ProrateMultiplier__c;
                        unitPrice = unitPrice.setScale(2);
                        if(priceTerm == 'monthly') { pricePerYear = unitPrice * sub.SBQQ__RenewalQuantity__c * 12; }
                        if(priceTerm == 'annually'){ pricePerYear = unitPrice * sub.SBQQ__RenewalQuantity__c; }
                        
                        /****** Get Offset Calcuation *****/
                        Date subStart = sub.SBQQ__SubscriptionStartDate__c;
                        Date endDate = thisLine.SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate;
                        Decimal offsetAmount;
                        Decimal daysTillInvoice = sub.SBQQ__SubscriptionStartDate__c.daysBetween(endDate);
                        if(sub.SBQQ__SubscriptionStartDate__c > sub.SBQQ__Contract__r.StartDate && daysTillInvoice <= 365){
                            offsetAmount = BookingsOffsetCalculator.calculateOffset(subStart, endDate, pricePerYear);
                        }
                        
                        //Apply offset when appropriate
                        if(offsetAmount != null){ 
                            if(quoteLinetoOffsetMap.containsKey(thisSubsQuoteLine)){ 
                                Decimal totalOffset = offsetAmount + quoteLinetoOffsetMap.get(thisSubsQuoteLine);
                                quoteLinetoOffsetMap.remove(thisSubsQuoteLine);
                                quoteLinetoOffsetMap.put(thisSubsQuoteLine, totalOffset);
                            }
                            if(!quoteLinetoOffsetMap.containsKey(thisSubsQuoteLine)){
                                quoteLinetoOffsetMap.put(thisSubsQuoteLine, offsetAmount);
                            }       
                        }                                  
                    }
                }
            }
            for(SBQQ__QuoteLine__c ql : quoteLinetoOffsetMap.keySet()){
                ql.Offset_Amount_for_Renewal_Booking__c = quoteLinetoOffsetMap.get(ql);
                linesToUpdate.add(ql);
            }
        }        
        if(linesToUpdate != null && linesToUpdate.size()>0){
            update linesToUpdate;
        }           
    }/*
* End offsetRenewal */
    
    
    public static void updateOffsetOnInvoice(List<Opportunity> invoiceOpps){
        
        //Get any OLI(s) that are offset on the Invoice
        //Update the offset amount so that it prorates to the Opportunity's new Close Date
        List<Id> invoiceOppIds = new List<Id>();
        for(Opportunity invoiceOpp : invoiceOpps){
            invoiceOppIds.add(invoiceOpp.Id);
        }
        List<OpportunityLineItem> offsetOlis = [Select Id, Offset_Amount_for_Renewal_Booking__c, Original_Subscription_Id__c, Opportunity.CloseDate, UnitPrice, Quantity
                                                From OpportunityLineItem 
                                                Where Offset_Amount_for_Renewal_Booking__c != null
                                                And Original_Subscription_Id__c != null
                                                And OpportunityId IN :invoiceOppIds];
        List<OpportunityLineItem> updateOlis = new List<OpportunityLineItem>();
        List<Id> subIds = new List<Id>();
        
        for(OpportunityLineItem oli : offsetOlis){
            subIds.add(oli.Original_Subscription_Id__c);
        }
        
        Map<Id,Date> subToStartDateMap = new Map<Id,Date>();
        List<SBQQ__Subscription__c> originalSubs = [Select SBQQ__SubscriptionStartDate__c
                                                    From SBQQ__Subscription__c
                                                    Where Id IN :subIds];
        for(SBQQ__Subscription__c sub : originalSubs){
            subToStartDateMap.put(sub.Id, sub.SBQQ__SubscriptionStartDate__c);
        }
        
        for(OpportunityLineItem line : offsetOlis){
            /****** Get Offset Calcuation *****/
            Date subStart = subToStartDateMap.get(line.Original_Subscription_Id__c);
            Decimal pricePerYear = line.UnitPrice * line.Quantity;
            Decimal offsetAmount = BookingsOffsetCalculator.calculateOffset(subStart, line.Opportunity.CloseDate, pricePerYear);
            
            //Apply offset when appropriate   
            line.Offset_Amount_for_Renewal_Booking__c = offsetAmount;
            updateOlis.add(line);
        }         
        update updateOlis;
    }/*
* End updateOffsetOnInvoice */
    
}