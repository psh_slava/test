@isTest

public class SumLikeLinesAcrossInstanceTest {
    
     public static testmethod void testTotalCount() {
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contract c = new Contract();
        c.AccountId = a.Id;
        c.StartDate = Date.today();
        c.ContractTerm = 12;
        c.Status = 'Draft';
        insert c;
        
        Product2 p = new Product2();
        p.Name = 'Test License Product';
        insert p;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = p.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = c.Id;
        s.Instance_Number__c = 'Instance 1';
        s.SBQQ__Bundle__c = true;
        insert s;   
         
        SBQQ__Subscription__c s2 = new SBQQ__Subscription__c();
        s2.SBQQ__Quantity__c = 15;
        s2.SBQQ__Product__c = p.Id;
        s2.SBQQ__Account__c = a.Id;
        s2.SBQQ__Contract__c = c.Id;
        s2.Instance_Number__c = 'Instance 1';
        s2.SBQQ__Bundle__c = true;
        insert s2; 
        
        c.Multiple_Rates__c = true;
        update c;
         
        SBQQ__Subscription__c sub = [Select Id, Total_Count_of_Same_Product__c
                                       From SBQQ__Subscription__c
                                     Where Id = :s2.Id]; 
        System.assertEquals(sub.Total_Count_of_Same_Product__c,25);
    }

}