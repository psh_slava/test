@IsTest (SeeAllData=true)
public with sharing class CustomUserLookupControllerTest {
    
    @IsTest public static void testCustomUserLookupController_multiplePartners() {
        //Find Accounts to be the pretend Partners involved on the Opportunity
        User partnerUser = [Select Id, Contact.AccountId, ContactId From User Where IsActive = true AND Contact.Account.Referral_Partner__c = true LIMIT 1];
        Id partnerAcctId = partnerUser.Contact.AccountId;
        
        User partnerUser2 = [Select Id, Contact.AccountId, ContactId From User Where IsActive = true AND Contact.Account.Sales_Assist_Partner__c = true LIMIT 1];
        Id partnerAcctId2 = partnerUser.Contact.AccountId;
        
        //Create the Opportunity that you're pretending to push the Opportunity Team Edit button on
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test';
        oppty.Amount = 0;
        //oppty.Referral_PartnerId__c = partnerAcctId;
        //oppty.Sales_Assist_PartnerId__c = partnerAcctId2;
        oppty.StageName = 'Pre-Qualification';
        oppty.CloseDate = System.today();
        oppty.Forecast_Category__c = 'Omit';
        insert oppty;
        
        //Create the Partner Involvement record on that Opportunity
        Partner_Involvement__c pi = new Partner_Involvement__c();
        pi.Partner__c = partnerAcctId;
        pi.Opportunity__c = oppty.Id;
        pi.Partner_Program__c = 'Authorized Partner (Channel)';
        pi.Partner_Role__c = 'Delivery';
        insert pi;
        
        //Pretend to push the button by launching the OpportunityTeamEdit page
        PageReference newPage = Page.CustomUserLookup;
        newPage.getParameters().put('lksrch', 'Partner');
        newPage.getParameters().put('type', 'Partner');
        newPage.getParameters().put('oppId', oppty.Id);
        newPage.getParameters().put('frm', 'oppty.Id');
        newPage.getParameters().put('txt', 'txt');
        Test.setCurrentPage(newPage);
        
        //Call the controller
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppty);
        CustomUserLookupController controller = new CustomUserLookupController();       
        
        //Call individual methods in the controller
        controller.search();
        controller.getFormTag();
        controller.getTextBox();
    }   
    
    @IsTest public static void testCustomUserLookupController_ResellerPartner() {  
        //Find Accounts to be the pretend Partners involved on the Opportunity
        User partnerUser = [Select Id, Contact.AccountId, ContactId From User Where IsActive = true AND Contact.Account.Reseller_Partner__c = true LIMIT 1];
        Id partnerAcctId = partnerUser.Contact.AccountId;
        
        //Create the Opportunity that you're pretending to push the Opportunity Team Edit button on
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test';
        oppty.Amount = 0;
        //oppty.Reseller_Partner__c = partnerAcctId;
        oppty.StageName = 'Pre-Qualification';
        oppty.CloseDate = System.today();
        oppty.Forecast_Category__c = 'Omit';
        insert oppty;
        
        //Create the Partner Involvement record on that Opportunity
        Partner_Involvement__c pi = new Partner_Involvement__c();
        pi.Partner__c = partnerAcctId;
        pi.Opportunity__c = oppty.Id;
        pi.Partner_Program__c = 'Authorized Partner (Channel)';
        pi.Partner_Role__c = 'Reseller';
        insert pi;
        
        //Pretend to push the button by launching the OpportunityTeamEdit page
        PageReference newPage = Page.CustomUserLookup;
        newPage.getParameters().put('lksrch', 'Partner');
        newPage.getParameters().put('type', 'Partner');
        newPage.getParameters().put('oppId', oppty.Id);
        newPage.getParameters().put('frm', 'oppty.Id');
        newPage.getParameters().put('txt', 'txt');
        Test.setCurrentPage(newPage);
        
        //Call the controller
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppty);
        CustomUserLookupController controller = new CustomUserLookupController();       
        
        //Call individual methods in the controller
        controller.search();
        controller.getFormTag();
        controller.getTextBox();
    }    
    
    @IsTest public static void testCustomUserLookupController_NoPartner() {
        
        //Create the Opportunity that you're pretending to push the Opportunity Team Edit button on
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test';
        oppty.Amount = 0;
        oppty.StageName = 'Pre-Qualification';
        oppty.CloseDate = System.today();
        oppty.Forecast_Category__c = 'Omit';
        insert oppty;
        
        //Pretend to push the button by launching the OpportunityTeamEdit page
        PageReference newPage = Page.CustomUserLookup;
        newPage.getParameters().put('lksrch', 'User');
        newPage.getParameters().put('type', 'User');
        newPage.getParameters().put('oppId', oppty.Id);
        newPage.getParameters().put('frm', 'oppty.Id');
        newPage.getParameters().put('txt', 'txt');
        newPage.getParameters().put('userType', 'User');
        Test.setCurrentPage(newPage);
        
        //Call the controller
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppty);
        CustomUserLookupController controller = new CustomUserLookupController();       
        
        //Call individual methods in the controller
        controller.search();
        controller.getFormTag();
        controller.getTextBox();
    } 
}