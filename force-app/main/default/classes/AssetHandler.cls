public class AssetHandler {
    
    public static void newTrainingPackage(List<Asset> newPackages) {
        for(Asset a : newPackages){
            if(a.Account_Record_Type__c == 'Customer' && a.ProductFamily == 'Learning Services' && a.StockKeepingUnit != null && a.StockKeepingUnit.contains('package')){
                Id contactId = a.Quote_Primary_Contact_Id__c;
                String productSKU = a.StockKeepingUnit;
                String accountName = a.Account_Name__c;
                String userType = a.Account_Record_Type__c;
                String accountId = a.AccountId;
                System.debug('Initiating TI API Controller......'+contactId+'+'+productSKU+'+'+accountName+'+'+userType+'+'+accountId);
                TIAPIController.TIAPIController(contactId, productSKU, accountName, userType, accountId);
            }
        }
    }
    
    public static void newTrainingPurchase(List<Asset> newPurchases) {    
        
        Map<Id,String> courseMap = new Map<Id,String>();
        Map<Id,String> bundleMap = new Map<Id,String>();
        Map<Id,String> lpathMap = new Map<Id,String>();
        
        for(Asset a : newPurchases){
            if(a.ProductFamily == 'Learning Services' && a.StockKeepingUnit != null && !a.StockKeepingUnit.contains('package')){
                Id contactId;
                //Contact contact = a.Contact;
                if( a.Quote_Primary_Contact_Id__c != null ) { contactId = a.Quote_Primary_Contact_Id__c; }
                else if ( a.ContactId != null ) { contactId = a.ContactId; }
                String productSKU = a.StockKeepingUnit;
                String contentType;
                String slug;
                Product2 purchase = [Select Id, TI_Course_Slug__c, TI_Bundle_Slug__c, TI_Learning_Path_Slug__c
                                     From Product2
                                     Where Id = :a.Product2Id];
                String courseSlug = purchase.TI_Course_Slug__c;
                String bundleSlug = purchase.TI_Bundle_Slug__c;
                String learningPathSlug = purchase.TI_Learning_Path_Slug__c;
                
                if(courseSlug != null){ 
                    contentType = 'course'; 
                    courseMap.put(contactId, courseSlug);
                }
                if(bundleSlug != null){ 
                    contentType = 'bundle'; 
                    bundleMap.put(contactId, bundleSlug);
                }
                if(learningPathSlug != null){ 
                    contentType = 'learningpath'; 
                    lpathMap.put(contactId, learningPathSlug);
                    System.debug('LPathMap >>>>' + lPathMap);
                }
                if(contentType != null){
                    TIAPIController.TICallout(courseMap, bundleMap, lpathMap); 
                }      
            }
        }
    }
}