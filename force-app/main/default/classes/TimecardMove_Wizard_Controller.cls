/*
	Timecard Move Wizard Controller
	J.McCarvill - john.mccarvill@iconatg.com - 10/19/2015

*/
public with sharing class TimecardMove_Wizard_Controller {

	//timecard
	public pse__Timecard_Header__c m_pTimecard {get; set;}
	public pse__Timecard_Header__c m_pTimecardProject {get; set;}
	
	//status
	public string showMessage {get; set;}
	public string showMessageColor {get; set;}
	

	public TimecardMove_Wizard_Controller(ApexPages.StandardController stdController)
	{
		//initialize 
		
		//get current timecard
		string sId = ((pse__Timecard_Header__c)stdController.getRecord()).Id;
		list<pse__Timecard_Header__c> listTimecards = [SELECT Id, pse__Project__c, pse__Project__r.Name, pse__Assignment__c 
										FROM pse__Timecard_Header__c WHERE Id=:sId];
		
		if (listTimecards.size()==1)
		{		
			m_pTimecard = listTimecards[0];
		}
		else
		{
			this.showMessage = 'Problem loading requested timecard: ID=' + sId;
			this.showMessageColor='red';
			
		}
		
		//create dummy timecard to use for wizard lookups
		m_pTimecardProject = new pse__Timecard_Header__c();
		
	}


	public PageReference ProcessTimecardMove()
	{
		//process the requested project move
		
		try
		{

			//required field changes
			m_pTimecard.pse__Admin_Global_Edit__c = true;
			m_pTimecard.pse__Audit_Notes__c = 'Timecard moved from ' + m_pTimecard.pse__Project__r.Name + ' by ' + UserInfo.getName() + ' on ' + DateTime.now();
			
			//attempt to assign the new project to the existing timecard and save
			m_pTimecard.pse__Project__c = m_pTimecardProject.pse__Project__c;
			m_pTimecard.pse__Assignment__c = m_pTimecardProject.pse__Assignment__c;
			
			update(m_pTimecard);


			this.showMessage = 'Timecard moved successfully! Click <a href="/' + m_pTimecard.Id + '">here</a> to return to timecard.';
			this.showMessageColor = 'green';

			//forward back to timecard
			PageReference pResult = new PageReference('/' + m_pTimecard.Id);
			return(pResult);
			
		}
		catch(Exception err)
		{
			this.showMessage = 'Error: ' + err.getMessage() + '<br/><br/>Click <a href="/' + m_pTimecard.Id + '">here</a> to return to timecard.';
			this.showMessageColor = 'red';
		}
		
		
		return(null);
	}

}