/**
 * Resource_Request__c trigger
 *   
 * @author CRMCulture  
 * @version 1.00 
 * @param Platform after insert, after update, before insert, before update
*/ 
trigger Resource_RequestTrigger on Resource_Request__c (
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {
	TriggerFactory.createTriggerDispatcher(Resource_Request__c.sObjectType);
}