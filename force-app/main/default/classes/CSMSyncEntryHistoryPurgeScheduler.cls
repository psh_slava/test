global class CSMSyncEntryHistoryPurgeScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
    	Boolean runningInASandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    	CSM_API_Settings__c apiSettings = CSM_API_Settings__c.getInstance(runningInASandbox ? 'Sandbox' : 'Production');
    	Integer purgeNDays = (Integer)apiSettings.Sync_Entry_History_Purge_N_Days__c;

    	CSMSyncEntryHistoryPurgeBatch b = new CSMSyncEntryHistoryPurgeBatch(purgeNDays);
    	Database.executeBatch(b);
	}
}