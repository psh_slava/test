@isTest

public class HostedInstanceTriggerTest {
    
    public static testmethod void insertHostedInstance(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__StartDate__c = Date.today();
        q.SBQQ__Primary__c = true;
        insert q;
        
        Hosted_Instance__c hi = new Hosted_Instance__c();
        hi.Quote__c = q.Id;
        hi.Account__c = a.Id;
        insert hi;
        
        delete hi;
        
    }

}