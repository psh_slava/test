/*
* @class name:   PartnerInvolvementTriggerAction
* @created:      By Naomi Harmon in Jun 2020
* @test class:   PartnerInvolvementTriggerTest.apxc
* @initiated by: PartnerInvolvementTrigger.apxt
* @description: 
*    Performs multiple automated actions on the Partner Involvement object that are not possibly via declarative tools
* @modifcation log:
*       Aug 2020 -  Naomi Harmon - updated autoCreateInvolvement method to associate Partner Program and Partner Role 
*                   based on the community's user's selection on the "Nature of Partner Involvement Here" field
*
*/

public class PartnerInvolvementTriggerAction {
    
    public void deletionNotification(List<Partner_Involvement__c> deletedPIs){
        
        //For any Channel PI that's deleted, send email alert to Channel VP
        //Grab the email template
        List<Messaging.SingleEmailMessage> mailMsgs = new List<Messaging.SingleEmailMessage>();
        String channelVP = System.Label.Channel_VP_Email;
        List<String> recipients = new List<String>(channelVP.split(','));
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        for(Partner_Involvement__c delPI : deletedPIs){
            if(delPi.Partner_Program__c == 'Authorized Partner (Channel)'){
                recipients.add(delPi.Partner_Account_Owner_Email__c);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setReplyTo('sfdchelp@cherwell.com');
                mail.setSenderDisplayName('Salesforce Administration');
                mail.setSubject(' A Partner Involvement record has been Deleted');

                mail.setHtmlBody('A Partner Involvement record has just been deleted by ' + System.UserInfo.getName() + ' on Opportunity ' +
                                 '<a href='+ baseURL + delPi.Opportunity__c +'>'+delPi.Opportunity_Name__c + '</a>.<p> ' +
                                 'Partner: ' + delPi.Partner_Account_Name__c + '<br>' +
                                 'Role: ' + delPi.Partner_Role__c + '<p>' +
                                 '--- This is an automated email. Please do not respond directly. ---'
                                );
                mail.setToAddresses(recipients);
                mailMsgs.add(mail); 
            }          
        }     
        
        if(mailMsgs.size()>0){
            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(mailMsgs);
        }
        
        //For any TAP PI that's deleted, send email alert to Technology Alliance leadership
        //Grab the email template
        List<Messaging.SingleEmailMessage> mailMsgsTAP = new List<Messaging.SingleEmailMessage>();
        String tapVP = System.Label.Technology_Alliance_Email;
        List<String> recipientsTAP = new List<String>(tapVP.split(','));
        
        for(Partner_Involvement__c delPI : deletedPIs){
            if(delPi.Partner_Program__c == 'Technology Alliance Partner (TAP)'){
                recipientsTAP.add(delPi.Partner_Account_Owner_Email__c);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setReplyTo('sfdchelp@cherwell.com');
                mail.setSenderDisplayName('Salesforce Administration');
                mail.setSubject(' A Partner Involvement record has been Deleted');

                mail.setHtmlBody('A Partner Involvement record has just been deleted by ' + System.UserInfo.getName() + ' on Opportunity ' +
                                 '<a href='+ baseURL + delPi.Opportunity__c +'>'+delPi.Opportunity_Name__c + '</a>.<p> ' +
                                 'Partner: ' + delPi.Partner_Account_Name__c + '<br>' +
                                 'Role: ' + delPi.Partner_Role__c + '<p>' +
                                 '--- This is an automated email. Please do not respond directly. ---'
                                );
                mail.setToAddresses(recipientsTAP);
                mailMsgsTAP.add(mail); 
            }          
        }     
        
        if(mailMsgsTAP.size()>0){
            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(mailMsgsTAP);
        }
        
        //Also remove Channel manager from record if that's the last Partner Involvement record on the Oppty
        List<Id> oppsToUpdate = new LIst<Id>(); 
        for(Partner_Involvement__c delPI : deletedPIs){
                 oppsToUpdate.add(delPI.Opportunity__c);
         }
        if(oppsToUpdate.size()>0){
            List<Opportunity> oppsToRemoveCMs = [Select Id, Partner_Involvements__c From Opportunity Where Id IN :oppsToUpdate];
            if(oppsToRemoveCMS.size()>0){
                for(Opportunity opp : oppsToRemoveCMs){
                    if(opp.Partner_Involvements__c == 1 || opp.Partner_Involvements__c == null){
                        opp.Partner_Manager__c = null;
                    }  
                }
                
                update oppsToRemoveCMs;
            }
        }
        
    }//End deletionNotification
    
    
    public void removeBillingPartner(List<Partner_Involvement__c> deletedPIs){
        
        //If the PI deleted is of Role = Fulfillment or Reseller
        //And the Opportunity has a Fulfillment or Reseller Partner listed
        //Remove the Fulfillment or Reseller Partner
        List<Id> oppIdsToQuery = new List<Id>();
        Map<Id,String> oppToRoleIdMap = new Map<Id,String>();
        for(Partner_Involvement__c delPi : deletedPIs){
            if(delPi.Partner_Role__c == 'Reseller' || delPi.Partner_Role__c == 'Fulfillment'){
                oppIdsToQuery.add(delPi.Opportunity__c);
                oppToRoleIdMap.put(delPi.Opportunity__c, delPi.Partner_Role__c);
            }
        }
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        List<Opportunity> theseOpps = [Select Id, Reseller_Partner__c, Fulfillment_Partner__c
                                       From Opportunity
                                       Where Id IN : oppIdsToQuery];
        for(Opportunity opp : theseOpps){
            if(oppToRoleIdMap.get(opp.Id) == 'Reseller' && opp.Reseller_Partner__c != null){
                opp.Reseller_Partner__c = null;
                oppsToUpdate.add(opp);
            }
            if(oppToRoleIdMap.get(opp.Id) == 'Fulfillment' && opp.Fulfillment_Partner__c != null){
                opp.Fulfillment_Partner__c = null;
                oppsToUpdate.add(opp);
            }
        }
        if(oppsToUpdate.size()>0){
            update oppsToUpdate;
        }            
        
    }//End removeBillingPartner
    
     public void removeSupportPartner(List<Partner_Involvement__c> deletedPIs){
        
        //If the PI deleted is of Role = Support
        //And the Opportunity has a Support Partner listed
        //Remove the Support Partner
        List<Id> oppIdsToQuery = new List<Id>();
        Map<Id,String> oppToRoleIdMap = new Map<Id,String>();
        for(Partner_Involvement__c delPi : deletedPIs){
            if(delPi.Partner_Role__c == 'Support'){
                oppIdsToQuery.add(delPi.Opportunity__c);
                oppToRoleIdMap.put(delPi.Opportunity__c, delPi.Partner_Role__c);
            }
        }
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        List<Opportunity> theseOpps = [Select Id, Support_Partner__c
                                       From Opportunity
                                       Where Id IN : oppIdsToQuery];
        for(Opportunity opp : theseOpps){
            if(oppToRoleIdMap.get(opp.Id) == 'Support'){
                opp.Support_Partner__c = null;
                oppsToUpdate.add(opp);
            }
        }
        if(oppsToUpdate.size()>0){
            update oppsToUpdate;
        }            
        
    }//End removeSupportPartner
    
  
    public static void autoCreateInvolvement(List<Request__c> partnerDealRegs){
        
        List<Partner_Involvement__c> newPIs = new List<Partner_Involvement__c>();
        
        for(Request__c r : partnerDealRegs){
            Partner_Involvement__c thisPI = new Partner_Involvement__c();
            thisPI.Opportunity__c = r.Opportunity__c;
            thisPI.Partner__c = r.Account__c;
            
            //Default the Partner Program on Profile and default Role to Referral
            if(r.CreatedbyProfile__c.contains('TAP')){ thisPI.Partner_Program__c = 'Technology Alliance Partner (TAP)'; }
            if(!r.CreatedbyProfile__c.contains('TAP')){ thisPI.Partner_Program__c = 'Authorized Partner (Channel)'; }
            thisPI.Partner_Role__c = 'Referral';
            
            //Override default where appropriate based on the chosen involvement
            if(r.Nature_of_Partner_Involvement_Here__c.contains('Reseller')){ 
                thisPI.Partner_Program__c = 'Authorized Partner (Channel)'; 
                thisPI.Partner_Role__c = 'Reseller';
            }
            if(r.Nature_of_Partner_Involvement_Here__c.contains('Alliance')){ 
                thisPI.Partner_Program__c = 'Technology Alliance Partner (TAP)'; 
            }
            if(r.Nature_of_Partner_Involvement_Here__c.contains('Delivery')){ 
                thisPI.Partner_Program__c = 'Authorized Partner (Channel)'; 
                thisPI.Partner_Role__c = 'Delivery'; 
            }
            newPIs.add(thisPI);
        }
        
        try{
        insert newPIs; 
        } catch (Exception e){
        System.debug('Exception caught while trying to add Partner Involvement on new Partner Opportunity.');
        }
        
    }//End autoCreateInvolvement

    
}