@isTest

public class AssignUpsellEligibilityonSubsTest {
    
    public static testmethod void testUpsellFlag() {
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contract c = new Contract();
        c.AccountId = a.Id;
        c.StartDate = Date.today();
        c.ContractTerm = 12;
        c.Status = 'Draft';
        insert c;
        
        Product2 p = new Product2();
        p.Name = 'Test Product';
        insert p;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = p.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = c.Id;
        s.Instance_Number__c = 'Instance 1';
        s.SBQQ__Bundle__c = true;
        insert s;   
        
        c.Multiple_Rates__c = true;
        update c;
    }
    
}