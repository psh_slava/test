public with sharing class CSMSyncEntryHistoryRetryController {

	private final CSM_Sync_Entry_History__c syncEntryHist;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CSMSyncEntryHistoryRetryController(ApexPages.StandardController stdController) {
        this.syncEntryHist = (CSM_Sync_Entry_History__c)stdController.getRecord();
        System.debug('Get endpoint system   ' + this.syncEntryHist.Endpoint_System__c);
    }

    public PageReference retry() {
        
        String endpoint = this.syncEntryHist.Endpoint_System__c;
        String errMsg;

        if(endpoint == 'LIVE'){
            errMsg = CSMSyncEntryHelper.enqueueSyncEntry(new List<CSM_Sync_Entry_History__c>{ this.syncEntryHist });}
        if(endpoint == 'Central'){
            errMsg = CentralSyncEntryHelper.enqueueSyncEntry(new List<CSM_Sync_Entry_History__c>{ this.syncEntryHist });}

        if (String.isNotBlank(errMsg)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg));
            return null;
        }

        PageReference nextpage= new PageReference('/' + this.syncEntryHist.Id);
        return nextpage;
    }


}