@isTest
global class LicenseKeyGenCalloutMock implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest request){
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"ApiKey": "3klqCibQeEOJY/p7lU9LKA=="}');
        response.setStatusCode(200);
        return response;
    }

}