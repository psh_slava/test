@isTest
private class QuoteLineTriggerHandlerTest {
    
    @isTest
    private static void testSaleTypeNew(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Logo').getRecordTypeId();
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discovery';
        o.Forecast_Category__c = 'Pipeline';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.Term__c = '3';
        q.SBQQ__Opportunity2__c = o.Id;
        insert q;
        
        Product2 csm = new Product2();
        csm.Name = 'CSM Subscription License';
        csm.License_Model__c = 'Subscription';
        insert csm;
        
        SBQQ__QuoteLine__c ql1 = new SBQQ__QuoteLine__c();
        ql1.SBQQ__Quote__c = q.Id;
        ql1.SBQQ__Quantity__c = 50;
        ql1.SBQQ__Product__c = csm.Id;
        insert ql1;    
        
        //TEST #1
        SBQQ__QuoteLine__c testline1 = [Select Sale_Type__c From SBQQ__QuoteLine__c Where Id = :ql1.Id];
        System.assertEquals('New Logo', testline1.Sale_Type__c);
        //
    }
    
    @isTest
    private static void testSaleTypeUpsell(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discovery';
        o.Forecast_Category__c = 'Pipeline';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.SBQQ__Quote__c = q.Id;
        con.StartDate = Date.today();
        con.ContractTerm = 12;
        con.SBQQ__Opportunity__c = o.Id;
        insert con;
        
        Product2 csm = new Product2();
        csm.Name = 'CSM Subscription License';
        csm.License_Model__c = 'Subscription';
        insert csm;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 50;
        s.SBQQ__Product__c = csm.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = con.Id;
        s.SBQQ__Bundle__c = true;
        insert s;        
        
        Product2 ppm = new Product2();
        ppm.Name = 'PPM (Add-On) Subscription License';
        ppm.License_Model__c = 'Subscription';
        insert ppm;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.Term__c = '3';
        q.SBQQ__Opportunity2__c = o.Id;        
        q.SBQQ__StartDate__c = date.today();
        q.SBQQ__EndDate__c = date.today().addDays(30);
        insert q;
        
        List<SBQQ__QuoteLine__c> qls = new List<SBQQ__QuoteLine__c>();
        //upsell line
        SBQQ__QuoteLine__c ql1u = new SBQQ__QuoteLine__c();
        ql1u.SBQQ__Quote__c = q.Id;
        ql1u.SBQQ__Quantity__c = 50;
        ql1u.SBQQ__Product__c = csm.Id;
        ql1u.SBQQ__UpgradedSubscription__c = s.Id;
        insert ql1u;
        
        //TEST #2
        SBQQ__QuoteLine__c testline1b = [Select Sale_Type__c From SBQQ__QuoteLine__c Where Id = :ql1u.Id];
        System.assertEquals('Upsell', testline1b.Sale_Type__c);
        
    }
    
    @isTest
    private static void testSaleTypeCrossell(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discovery';
        o.Forecast_Category__c = 'Pipeline';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.SBQQ__Quote__c = q.Id;
        con.StartDate = Date.today();
        con.ContractTerm = 12;
        con.SBQQ__Opportunity__c = o.Id;
        insert con;
        
        Product2 csm = new Product2();
        csm.Name = 'CSM Subscription License';
        csm.License_Model__c = 'Subscription';
        insert csm;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 50;
        s.SBQQ__Product__c = csm.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = con.Id;
        s.SBQQ__Bundle__c = true;
        insert s;        
        
        Product2 ppm = new Product2();
        ppm.Name = 'PPM (Add-On) Subscription License';
        ppm.License_Model__c = 'Subscription';
        insert ppm;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.Term__c = '3';
        q.SBQQ__Opportunity2__c = o.Id;        
        q.SBQQ__StartDate__c = date.today();
        q.SBQQ__EndDate__c = date.today().addDays(30);
        insert q;
        
        //cross-sell line
        SBQQ__QuoteLine__c ql2 = new SBQQ__QuoteLine__c();
        ql2.SBQQ__Quote__c = q.Id;
        ql2.SBQQ__Quantity__c = 25;
        ql2.SBQQ__Product__c = ppm.Id;
        insert ql2;
        
        //TEST #3
        SBQQ__QuoteLine__c testline2 = [Select Sale_Type__c From SBQQ__QuoteLine__c Where Id = :ql2.Id];
        System.assertEquals('Cross-sell', testline2.Sale_Type__c);
        
    }
    
    @isTest
    private static void testSaleTypeRenewal(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discovery';
        o.Forecast_Category__c = 'Pipeline';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.SBQQ__Quote__c = q.Id;
        con.StartDate = Date.today();
        con.ContractTerm = 12;
        con.SBQQ__Opportunity__c = o.Id;
        insert con;
        
        Product2 csm = new Product2();
        csm.Name = 'CSM Subscription License';
        csm.License_Model__c = 'Subscription';
        insert csm;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 50;
        s.SBQQ__Product__c = csm.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = con.Id;
        s.SBQQ__Bundle__c = true;
        insert s;        
        
        Product2 ppm = new Product2();
        ppm.Name = 'PPM (Add-On) Subscription License';
        ppm.License_Model__c = 'Subscription';
        insert ppm;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.Term__c = '3';
        q.SBQQ__Opportunity2__c = o.Id;        
        q.SBQQ__StartDate__c = date.today();
        insert q;
        
        //renewal line
        SBQQ__QuoteLine__c ql1u = new SBQQ__QuoteLine__c();
        ql1u.SBQQ__Quote__c = q.Id;
        ql1u.SBQQ__Quantity__c = 50;
        ql1u.SBQQ__Product__c = csm.Id;
        ql1u.SBQQ__RenewedSubscription__c = s.Id;
        insert ql1u;
        
        //TEST #4
        SBQQ__QuoteLine__c testline1c = [Select Sale_Type__c From SBQQ__QuoteLine__c Where Id = :ql1u.Id];
        System.assertEquals('Renewal', testline1c.Sale_Type__c);
        
    }    
}