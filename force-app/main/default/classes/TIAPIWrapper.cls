//** Client side stub for TI (Thought Industries) API.
//** Author: Naomi Harmon
//** Last updated: 5/2019

public class TIAPIWrapper {
    
    public class TIAPIWrapperException extends Exception {}
    
    //*Private helper method
    private static String invoke(String operation, String method, String requestBody){
        
        //Access and authentication values stored in TI API Settings custom setting
        TI_API_Settings__c apiSettings = [Select Base_URL__c, API_Key__c From TI_API_Settings__c];
        String endpointURL = apiSettings.Base_URL__c;
        String apiKey = apiSettings.API_Key__c;
        System.debug('Base URL === '+endpointURL);      
        System.debug('API Key ===' + apiKey);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointURL+operation);
        request.setMethod(method);
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + apiKey);
        if (String.isNotBlank(requestBody)) {
            request.setBody(requestBody);
        }
        
        HTTPResponse response = http.send(request);
        
        if (response.getStatusCode() == 200) {
            String responseBody = response.getBody();
            return responseBody;
        } 
        if (response.getStatusCode() == 429) {
            return '429';        
        }
        else {
            system.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
            throw new TIAPIWrapperException('Problem invoking operation (' + operation + '): ' + response);          
        }
    }
    
    //*Start of public methods
    public class licenseWrapper{
        public String employeeLicenseId {get;set;}
        public String companyLicenseId {get;set;}
        public String companyParentLicenseId {get;set;}
        public String productLicenseId {get;set;}
        public String accountName {get;set;}
    }
    
    //**Start of methods initiated by Trigger (to happen @future immediately)
    
    //***Retrieve current Client License List in TI to see if License under Client, Company & Training Product already exists
    public static licenseWrapper getClientLicense(String clientId, String company, String productSKU){
        
        licenseWrapper lw = new licenseWrapper();
        
        //clientId passed in is TI Id of Panorama (i.e. Customer)
        String operation = '/clients/'+clientId+'/licenses';
        String method = 'GET';
        String responseBody = invoke(operation, method, null);
        System.debug('DEBUG: responseBody ' + responseBody);
        
        Map<String,Object> jsonResponse = (Map<String,Object>)
            JSON.deserializeUntyped(responseBody);
        List<Object> licenseObjs = (List<Object>)jsonResponse.get('licenses');
        String companySkuRoot = (company).replaceAll('\\s+', '');
        String employeeLicenseSKU = 'c-' + companySkuRoot + '-employees';
        String companyLicenseSKU = 'c-' + companySkuRoot;
        String licenseId;
        String parentLicenseId;
        String companyLicenseId;
        String parent2LicenseId;
        String parent2SKU;
        String productLicenseId;
        
        for(Object obj : licenseObjs){
            Map<String, Object> licenseAttributes = (Map<String, Object>)obj;
            for(String attributeName : licenseAttributes.keySet()){
                if(attributeName == 'sku'){
                    if((String)licenseAttributes.get(attributeName)==employeeLicenseSKU){
                        licenseId = (String)licenseAttributes.get('id');
                        parentLicenseId = (String)licenseAttributes.get('parentLicenseId');
                        System.debug('Client License Id for '+(String)licenseAttributes.get(attributeName)+' === '+licenseId);
                    }
                    if((String)licenseAttributes.get(attributeName)==companyLicenseSKU){
                        companyLicenseId = (String)licenseAttributes.get('id');
                        parent2LicenseId = (String)licenseAttributes.get('parentLicenseId');
                        System.debug('Client License Id for '+(String)licenseAttributes.get(attributeName)+' === '+companyLicenseId);
                    }
                    if((String)licenseAttributes.get(attributeName)==productSKU){
                        productLicenseId = (String)licenseAttributes.get('id');
                        System.debug('Client License Id for '+(String)licenseAttributes.get(attributeName)+' === '+productLicenseId);
                    }
                }
                if(attributeName == 'id' && parent2LicenseId != null){
                    if((String)licenseAttributes.get(attributeName)==parent2LicenseId){
                        parent2SKU = (String)licenseAttributes.get('sku');
                    }
                }
            }
        }
        if(licenseId == null){ 
            //No employee-level sublicense found in TI for this company
            //(Matched based on systematically create sku value)
            //Next action will be to create these new TI sublicenses
            System.debug('No Client License found for '+employeeLicenseSKU); 
        }
        if(licenseId != null && parent2LicenseId == productLicenseId){
            //This company's employee-level sublicense already exists under the purchased product
            //Do nothing
            // ??? Or maybe update the access days ???
            System.debug('Client License found for '+employeeLicenseSKU + ' and already associated to ' + productSKU + '. Moving on...');
        }
        if(licenseId != null && parent2LicenseId != productLicenseId){
            //This company's employee-level sublicense already exists, but under a difference product parent-license
            //If it's under the default license for just free access, then re-parent the Company license
            // ??? If it's under a different product, then may need to throw an error and flag a notification ???
            System.debug('Client License found for '+employeeLicenseSKU + ' but is parented to '+  parent2SKU + '.' );
        }
        if(productLicenseId == null){
            //??? If no product-level license is found, then throw an error and flag a notification ???
            System.debug('No Client License SKU found for '+productSKU);
        } 
        //In all cases, return licensewrapper. Controller will pick up on specific id values to trigger actions.
        lw.accountName = company;
        lw.employeeLicenseId = licenseId; 
        lw.companyLicenseId = parentLicenseId;
        lw.companyParentLicenseId = parent2LicenseId;
        lw.productLicenseId = productLicenseId;
        return lw;  
    }
    
    //***If Client Licenses do not exist, create them* (2)
    public static licenseWrapper createSKUs(String clientId, licenseWrapper lwrap){
        
        licenseWrapper newLicensesIds = new licenseWrapper();
        
        String sku = ('c-'+lwrap.accountName).replaceAll('\\s+', '');
        System.debug('SKU generated === '+sku);
        String operation = '/licenses';
        String method = 'POST';
        
        //Create company level SKU first
        String requestBody1 = TIRecordWrapper.getBodyLicenseCreate(clientId, sku, lwrap.productLicenseId, lwrap.accountName, null);
        System.debug('DEBUG: requestBody ' + requestBody1);
        String responseBody1 = invoke(operation, method, requestBody1);
        System.debug('DEBUG: responseBody1 ' + responseBody1);
        Map<String,Object> jsonResponse1 = (Map<String,Object>)
            JSON.deserializeUntyped(responseBody1);
        String newCompanyLicenseId = (String)jsonResponse1.get('id');
        System.debug('Deserialized Company License Id ====' + newCompanyLicenseId);
        
        //Then create employee level SKU and assign access (Content)
        String licenseName = lwrap.accountName + ' Employees';
        sku += '-employees';
        String requestBody2 = TIRecordWrapper.getBodyLicenseCreate(clientId, sku, newCompanyLicenseId, licenseName, lwrap.productLicenseId);
        System.debug('DEBUG: requestBody ' + requestBody2);
        String responseBody2 = invoke(operation, method, requestBody2);
        Map<String,Object> jsonResponse2 = (Map<String,Object>)
            JSON.deserializeUntyped(responseBody2);
        String newEmployeeLicenseId = (String)jsonResponse2.get('id');
        System.debug('Deserialized Employee License Id ====' + newEmployeeLicenseId);
        
        newLicensesIds.companyLicenseId = newCompanyLicenseId;
        newLicensesIds.employeeLicenseId = newEmployeeLicenseId;
        
        return newLicensesIds;
        
    }
    
    //***If company/employee sublicenses already exist but under another product-level license, then re-parent company-level license & update content on employee-level license
    public static licenseWrapper reparentSKUs(String companyName, String productSKU, licenseWrapper lwrap){
        String companyLicenseSKU = ('c-'+lwrap.accountName).replaceAll('\\s+', '');
        licenseWrapper reparentedLicense = new licenseWrapper();
        
        //update company license with parentlicenseid = productlicenseId
        String operation = '/licenses/'+lwrap.companyLicenseId;
        String method = 'PUT';
        String requestBody = TIRecordWrapper.getBodyReparentLicense(companyName, companyLicenseSKU, lwrap.productLicenseId);
        System.debug('DEBUG: requestBody ' + requestBody);
        String responseBody = invoke(operation, method, requestBody);
        Map<String,Object> jsonResponse = (Map<String,Object>)
            JSON.deserializeUntyped(responseBody);
        String newParentLicenseId = (String)jsonResponse.get('parentLicenseId');
        System.debug('Deserialized Parent (Product) License Id ====' + newParentLicenseId);
        
        reparentedLicense.employeeLicenseId = lwrap.employeeLicenseId;
        reparentedLicense.companyLicenseId = lwrap.companyLicenseId;
        reparentedLicense.productLicenseId = newParentLicenseId;
        
        //update employee license with correct Content Ids
        operation = '/licenses/'+lwrap.employeeLicenseId;
        String employeeLicenseName = companyName + ' Employees';
        String employeeLicenseSKU = ('c-'+lwrap.accountName).replaceAll('\\s+', '') + '-employees';
        
        String requestBody2 = TIRecordWrapper.getBodyUpdateLicenseContent(employeeLicenseName, employeeLicenseSKU, lwrap.companyLicenseId, productSKU);
        System.debug('DEBUG: requestBody ' + requestBody2);
        String responseBody2 = invoke(operation, method, requestBody2);
        Map<String,Object> jsonResponse2 = (Map<String,Object>)
            JSON.deserializeUntyped(responseBody2);
        System.debug('DEBUG: responseBody for updated Employee-level license '+jsonResponse2);
        
        return reparentedLicense;
    }
    
    
    //**Start of methods to be used by Batch scheduled job
    
    //***Helper method to manage 429 errors
    /* public static void wait(Integer millisec) {

if(millisec == null || millisec < 0) {
millisec = 0;
}

Long startTime = DateTime.now().getTime();
Long finishTime = DateTime.now().getTime();
while ((finishTime - startTime) < millisec) {
//sleep for parameter x millisecs
finishTime = DateTime.now().getTime();
}
System.debug('>>> Done from ' + startTime + ' to ' + finishTime);
} */
    
    //***Connect TI User Map with training access that each Contact needs in batch query, and update TI Users
    public static List<ResponseResult.TIUser> getUserPackages(List<Asset> scope) {
        
        System.debug('Parameters passed in for updating Training Package Access ===' + scope);
        
        List<ResponseResult.TIUser> usersUpdateList = new List<ResponseResult.TIUser>();
        //For package purchases, get all active Contacts who belong to those Accounts 
        //and need to have their TI access updated
        List<Id> acctIds = new List<Id>();
        Map<Id, Datetime> purchaseMap = new Map<Id,Datetime>();
        
        for(Asset a : scope){
            if(a.StockKeepingUnit.contains('package')){
                acctIds.add(a.AccountId); 
                purchaseMap.put(a.AccountId, a.CreatedDate);
            }
        }
        System.debug('Looking for contacts associated to new Training Packages purchased today.....');
        List<Contact> contacts = [Select Id, Email, TILBP__TIId__c, AccountId, Account.TI_Employee_SKU__c, TI_Access_Last_Updated__c
                                  From Contact
                                  Where AccountId IN :acctIds
                                  And Learner__c = true
                                  And Status__c = 'Active'];
        String tiId;
        Integer index = 0;
        Id jobId;
        for(Contact c : contacts){
            index++;
            System.debug('Looping through Contact '+ index + ' out of contacts.size()');
            Integer contactsLeft = contacts.size() - index;
            System.debug('c.TI_Access--'+c.TI_Access_Last_Updated__c);
            if(c.TI_Access_Last_Updated__c == null || (c.TI_Access_Last_Updated__c < purchaseMap.get(c.AccountId))){
                if(c.TILBP__TIID__c != null){
                    System.debug('TI ID '+c.TILBP__TIID__c);
                    tiId = c.TILBP__TIID__c;
                    ResponseResult.TIUser rr = new ResponseResult.TIUser();
                    rr.id = tiId;
                    rr.sfContactId = c.Id;
                    rr.accessSlug = c.Account.TI_Employee_SKU__c;
                    rr.email = c.email;
                    usersUpdateList.add(rr);
                }
            }
        }
        System.debug('User packages Update List ====' + usersUpdateList);
        return usersUpdateList;
    }
    
    //Once we have all the maps or lists of users to update, make the updates
    public static void updateTIUsers(Map<Id,String> newCourses, Map<Id,String> newBundles, Map<Id,String> newLPaths, List<ResponseResult.TIUser> newPackages){
        
        //List<ResponseResult.TIUser> tiUserList;
        //if(newPackages == null){ tiUserList = TIAPIWrapper.getTIUsersInformation(); }
        List<Id> allContactIds = new List<Id>();
        List<Contact> allContacts = new List<Contact>();
        List<Contact> allUpdatedContacts = new List<Contact>();
        
        String operation;
        String method = 'PUT';
        String requestBody;
        String responsesBody;
        String externalCustomerId;
        String studentAccessSlug;
        String tiId;
        
        if(newPackages != null){
            for(ResponseResult.TIUser user : newPackages){
                if(user.accessSlug != null){
                    operation = '/users/'+ user.id;
                    System.debug('Id pulled in for updateTIUsers packages ====' + user.Id);
                    //externalCustomerId = user.externalCustomerId;
                    externalCustomerId = user.email;
                    studentAccessSlug = user.accessSlug;
                    requestBody = TIRecordWrapper.getBodyUserUpdate(externalCustomerId, studentAccessSlug, 'package');
                    System.debug('DEBUG: requestBody ' + requestBody);
                    String responseBody = invoke(operation, method, requestBody);
                    if(responseBody.length()<10){
                        break;
                        //Integer mili = Integer.valueOf(responseBody) * 1000;
                        //wait(mili);
                        //responseBody = invoke(operation, method, requestBody);
                    }
                    responsesBody += responseBody;
                    System.debug('DEBUG: responseBody ' + responseBody); 
                    allContactIds.add(user.sfContactId); 
                }
            }        
        }
        if(newCourses != null){
            Map<Id,Contact> cMapCourses = new Map<Id,Contact>([Select Id, Email, TILBP__TIID__c From Contact Where Id IN :newcourses.keySet()]);
            for(Id i : newCourses.keySet()){
                Contact con = cMapCourses.get(i);
                //GET
                if(con.TILBP__TIID__c != null){ tiId = con.TILBP__TIID__c; }
                /* if(con.TILBP__TIID__c == null){
operation = '/users/'+con.Email;
method = 'GET';
String responseBody = invoke(operation, method, null);
Map<String,Object> jsonResponse = (Map<String,Object>)
JSON.deserializeUntyped(responseBody);
tiId = (String)jsonResponse.get('id');
System.debug('Deserialized TI User Id ====' + tiId);
} */
                //PUT
                operation = '/users/'+tiId;
                method = 'PUT';
                externalCustomerId = con.email;
                studentAccessSlug = newCourses.get(i);
                requestBody = TIRecordWrapper.getBodyUserUpdate(externalCustomerId, studentAccessSlug, 'course');
                
                System.debug('DEBUG: requestBody ' + requestBody);
                String responseBody = invoke(operation, method, requestBody);
                if(responseBody.length()<10){
                    break;
                    //Integer mili = Integer.valueOf(responseBody) * 1000;
                    //wait(mili);
                    //responseBody = invoke(operation, method, requestBody);
                }
                responsesBody += responseBody;
                System.debug('DEBUG: responseBody ' + responseBody); 
                //allContactIds.add(rr.sfContactId);
                allContacts.add(con);
            }
        }  
        if(newBundles != null){
            Map<Id,Contact> cMapBundles = new Map<Id,Contact>([Select Id, Email, TILBP__TIId__c From Contact Where Id IN :newBundles.keySet()]);
            for(Id i : newBundles.keySet()){
                Contact con = cMapBundles.get(i);
                //GET
                if(con.TILBP__TIID__c != null){ 
                    tiId = con.TILBP__TIID__c;
                    /*  if(con.TILBP__TIID__c == null){
operation = '/users/'+con.Email;
method = 'GET';
String responseBody = invoke(operation, method, null);
Map<String,Object> jsonResponse = (Map<String,Object>)
JSON.deserializeUntyped(responseBody);
tiId = (String)jsonResponse.get('id');
System.debug('Deserialized TI User Id ====' + tiId);
}*/
                    //PUT
                    operation = '/users/'+tiId;
                    method = 'PUT';
                    externalCustomerId = con.email;
                    studentAccessSlug = newBundles.get(i);
                    
                    requestBody = TIRecordWrapper.getBodyUserUpdate(externalCustomerId, studentAccessSlug, 'bundle');
                    System.debug('DEBUG: requestBody ' + requestBody);
                    String responseBody = invoke(operation, method, requestBody);
                    responsesBody += responseBody;
                    System.debug('DEBUG: responseBody ' + responseBody); 
                    allContacts.add(con);
                }
            } 
        }
        if(newLPaths != null){
            
            Map<Id,Contact> cMapPaths = new Map<Id,Contact>([Select Id, Email, TILBP__TIId__c From Contact Where Id IN :newLPaths.keySet()]);
            for(Id i : newLPaths.keySet()){
                Contact con = cMapPaths.get(i);
                //GET
                if(con.TILBP__TIID__c != null){ 
                    tiId = con.TILBP__TIID__c;
                    /*if(con.TILBP__TIID__c == null){
operation = '/users/'+con.Email;
method = 'GET';
String responseBody = invoke(operation, method, null);
Map<String,Object> jsonResponse = (Map<String,Object>)
JSON.deserializeUntyped(responseBody);
tiId = (String)jsonResponse.get('id'); 
System.debug('Deserialized TI User Id ====' + tiId);
}*/
                    //PUT
                    operation = '/users/'+tiId;
                    method = 'PUT';
                    externalCustomerId = con.email;
                    studentAccessSlug = newLPaths.get(i);
                    
                    requestBody = TIRecordWrapper.getBodyUserUpdate(externalCustomerId, studentAccessSlug, 'learningpath');
                    System.debug('DEBUG: requestBody ' + requestBody);
                    String responseBody = invoke(operation, method, requestBody);
                    responsesBody += responseBody;
                    System.debug('DEBUG: responseBody ' + responseBody); 
                    //allContactIds.add(rr3.sfContactId);
                    allContacts.add(con);
                }
            }
        }
        System.debug('All Contact Ids.....' + allContactIds);
        List<Contact> updatedContacts = [Select Id, TI_Access_Last_Updated__c
                                         From Contact
                                         Where Id IN : allContactIds];
        for(Contact ct : updatedContacts){
            allContacts.add(ct);
        }
        for(Contact con : allContacts){
            //con.TI_Access_Last_Updated__c = Date.today();
            con.TI_Access_Last_Updated__c = Datetime.now();
            //allUpdatedContacts.add(con);
        }
        System.debug('Pushing update to these Contacts' + allContacts);
        update allContacts;
        
        //return responsesBody; 
    }
}