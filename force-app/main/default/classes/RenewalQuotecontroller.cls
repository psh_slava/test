public with sharing class RenewalQuotecontroller {
   
    public Opportunity opp{get; set;}
    public Opportunity vopp{get; set;}
    Public Contract con {Get;set;}
    ApexPages.StandardController standardController;

    public RenewalQuotecontroller(ApexPages.StandardController controller) {
    This.opp = (Opportunity)controller.getRecord();
   
    }
    public PageReference doupdate(){
       
     vopp = [Select id,SBQQ__RenewedContract__c, Primary_Contact__c from Opportunity where Id =: Opp.id];
     con = [Select id,SBQQ__RenewalOpportunity__c,SBQQ__RenewalQuoted__c,Primary_Renewal_Contact__c from Contract where Id =:Vopp.SBQQ__RenewedContract__c];
     IF(Con.SBQQ__RenewalQuoted__c == True )
     {
       Con.SBQQ__RenewalQuoted__c = False;
       Update Con;
     }  
     IF(Con.SBQQ__RenewalQuoted__c == False )
     {
       Con.SBQQ__RenewalQuoted__c = True;
       if(con.Primary_Renewal_Contact__c == null){ Con.Primary_Renewal_Contact__c = vopp.Primary_Contact__c; }
       Update Con;
     }
     
      Opportunity pcopp = [Select id,SBQQ__RenewedContract__c,(Select id from SBQQ__Quotes2__r order by createdDate DESC limit 1) From Opportunity Where SBQQ__RenewedContract__c =: Con.id and id =:con.SBQQ__RenewalOpportunity__c];
      // PageReference resPg = new PageReference('/apex/sb?scontrolCaching=1&id='+ pcopp.SBQQ__Quotes2__r[0].id +'#quote/le?qId='+ pcopp.SBQQ__Quotes2__r[0].id);
       PageReference resPg = Page.SBQQ__SB;
        resPg.getParameters().put('scontrolCaching','1');
        resPg.getParameters().put('id',pcopp.SBQQ__Quotes2__r[0].id); 
       resPg.setRedirect(true);
       
       return resPg;
      
    }
}