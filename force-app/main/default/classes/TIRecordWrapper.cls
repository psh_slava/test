public class TIRecordWrapper {
    
    public static String getBodyUserUpdate(String externalCustomerId, String accessId, String contentType) {
                
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        //gen.writeStringField('externalCustomerId', externalCustomerId);
        gen.writeStringField('email', externalCustomerId);
        gen.writeBooleanField('upsert', true);
        gen.writeBooleanField('replaceLicenseAccess',false);
        if(contentType == 'package') { gen.writeFieldName('studentLicenseIds'); }
        if(contentType == 'course') { gen.writeFieldName('courseSlugs'); }
        if(contentType == 'bundle') { gen.writeFieldName('bundleSlugs'); }
        if(contentType == 'learningpath') { gen.writeFieldName('learningPathSlugs'); }
        gen.writeStartArray();
        gen.writeString(accessId);
        gen.writeEndArray();
        gen.writeEndObject();
        
        String jsonStr = gen.getAsString();
        System.debug('jsonMaterials'+jsonStr);
        
        return jsonStr;
    }
    
    public static String getBodyLicenseCreate(String clientId, String sku, String parentLicenseId, String accountName, String productLicenseId) {
        System.debug('Parameters passed in for getBodyLicenseCreate === '+ clientId +' - ' + sku + ' - ' + parentLicenseId + ' - ' + accountName + ' - ' + productLicenseId);
        String query;
        String courseTagIds;
        String courseIds;
        String learningPathIds;
        
        
        
        if(productLicenseId != null){
            query = 'Select courseTagIds__c, courseIds__c, learningPathIds__c From TI_License_Access__c Where productLicenseId__c = \'';
            query += productLicenseId;
            query += '\' Limit 1';
            
            TI_License_Access__c tla = Database.query(query);
            
            courseTagIds = tla.courseTagIds__c;
            courseIds = tla.courseIds__c;
            learningPathIds = tla.learningPathIds__c;
        }
        //Get Content, Learning Paths, and Content Tags from TI API Settings table based on Product sublicense       
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('name', accountName);
        gen.writeStringField('sku', sku);
        gen.writeStringField('clientId', clientId);
        if(!Test.isRunningTest()){ gen.writeStringField('parentLicenseId', parentLicenseId); }
        //add license access (Content) to employee-level sublicenses only
        if(sku.contains('employees')){
            System.debug('updating employee license access....');
            if(courseTagIds != null){
                List<String> courseTagIdsSplit = courseTagIds.split(',');
                System.debug(courseTagIdsSplit.size() + ' acessIds: ' + courseTagIdsSplit);
                
                gen.writeFieldName('courseTagIds');
                gen.writeStartArray();
                for(String s : courseTagIdsSplit){
                    gen.writeString(s); 
                }
                gen.writeEndArray();
            }
            if(courseIds != null){
                List<String> courseIdsSplit = courseIds.split(',');
                System.debug(courseIdsSplit.size() + ' acessIds: ' + courseIdsSplit);
                
                gen.writeFieldName('courseIds');
                gen.writeStartArray();
                for(String s : courseIdsSplit){
                    gen.writeString(s); 
                }
                gen.writeEndArray(); 
            }
            if(learningPathIds != null){
                List<String> learningPathIdsSplit = learningPathIds.split(',');
                System.debug(learningPathIdsSplit.size() + ' acessIds: ' + learningPathIdsSplit);
                
                gen.writeFieldName('learningPathIds');
                gen.writeStartArray();
                 gen.writeStartArray();
                for(String s : learningPathIdsSplit){
                    gen.writeString(s); 
                }
                gen.writeEndArray();
            }
            gen.writeNumberField('accessDays', 365);
        }
        gen.writeEndObject();
        String jsonStr = gen.getAsString();
        System.debug('jsonMaterials '+jsonStr);
        
        return jsonStr;
    }
    
    public static String getBodyReparentLicense(String companyLicenseName, String companyLicenseSKU, String productLicenseId){
        System.debug('Parameters passed in for getBodyLicenseUpdate ==='+ companyLicenseName + ' - ' + companyLicenseSKU + ' - ' + productLicenseId);
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('name', companyLicenseName);
        gen.writeStringField('parentLicenseId', productLicenseId);
        gen.writeStringField('sku', companyLicenseSKU);
        //gen.writeNumberField('accessDays', 365);
        gen.writeEndObject();
        String jsonStr = gen.getAsString();
        System.debug('jsonMaterials '+jsonStr);
        
        return jsonStr;
    }
    
    public static String getBodyUpdateLicenseContent(String licenseName, String licenseSKU, String companyLicenseId, String productLicenseSKU){
        System.debug('Parameters passed in for getBodyLicenseUpdate ==='+ licenseName + ' - ' + licenseSKU + ' - ' + companyLicenseId + ' - ' + productLicenseSKU);
        String query = 'Select courseTagIds__c, courseIds__c, learningPathIds__c From TI_License_Access__c Where productLicenseSKU__c = \'';
        query += productLicenseSKU;
        query += '\' Limit 1';
        
        TI_License_Access__c tla = Database.query(query);
        
        String courseTagIds = tla.courseTagIds__c;
        String courseIds = tla.courseIds__c;
        String learningPathIds = tla.learningPathIds__c;
        //Get Content, Learning Paths, and Content Tags from TI API Settings table based on Product sublicense
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('name', licenseName);
        gen.writeStringField('sku', licenseSKU);
        gen.writeStringField('parentLicenseId', companyLicenseId);
        if(courseTagIds != null){
            gen.writeFieldName('courseTagIds');
            gen.writeStartArray();
            gen.writeString(courseTagIds);
            gen.writeEndArray();
        }
        if(courseIds != null){
            gen.writeFieldName('courseIds');
            gen.writeStartArray();
            gen.writeString(courseIds);
            gen.writeEndArray(); 
        }
        if(learningPathIds != null){
            gen.writeFieldName('learningPathIds');
            gen.writeStartArray();
            gen.writeString(learningPathIds);
            gen.writeEndArray();
        }
        gen.writeNumberField('accessDays', 365);
        gen.writeEndObject();
        String jsonStr = gen.getAsString();
        System.debug('jsonMaterials '+jsonStr);
        
        return jsonStr;
    }
    
}