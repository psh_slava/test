@isTest
private class LicenseKeyTests {
   /* 
    @testSetup static void setup() {
        Account a = new Account();
        a.Name = 'Test Account';
        a.Type = 'Customer';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Total_Contract_Value__c = 100000;
        o.PSO_Total_Amount__c = 0;
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Closed - Won';
        o.Forecast_Category__c = 'Won';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        CSM_License_Record__c lkr = new CSM_License_Record__c();
        lkr.Account__c = a.Id; 
        lkr.Expires__c = System.today();
        lkr.Licensed_Items__c = 500;
        lkr.Type__c = 'Evaluation';
        insert lkr;
        
        CSM_License_Record__c testKey = new CSM_License_Record__c();
        testKey.Current_Key__c = 'Yes';
        testKey.Maintenance__c = 'Instance 1';
        testKey.License_Name__c = 'TestCompany';
        testKey.Type__c = 'Subscription';
        testKey.Deal_Type_Manual__c = 'Direct';
        testKey.Purchase_Scenario2__c = 'New Logo';
        testKey.Primary_Contact__c = c.Id;
        //Initially expired
        testKey.Expires__c = Date.newInstance(2018,1,31);
        testKey.Account__c = a.Id;
        testKey.Hosted__c = true;
        insert testKey;
        
        CSM_License_Record__c upsellKey = new CSM_License_Record__c();
        upsellKey.Current_Key__c = 'Yes';
        upsellKey.Maintenance__c = 'Instance 1';
        upsellKey.License_Name__c = 'TestUpsell';
        upsellKey.Type__c = 'Subscription';
        upsellKey.Deal_Type_Manual__c = 'Direct';
        upsellKey.Purchase_Scenario2__c = 'Upsell';
        upsellKey.Primary_Contact__c = c.Id;
        upsellKey.Expires__c = Date.newInstance(2018,1,31);
        upsellKey.Account__c = a.Id;
        upsellKey.Hosted__c = true;
        insert upsellKey;
        
        CSM_License_Record__c renewalKey = new CSM_License_Record__c();
        renewalKey.Current_Key__c = 'Yes';
        renewalKey.Maintenance__c = 'Instance 1';
        renewalKey.License_Name__c = 'TestRenewal';
        renewalKey.Type__c = 'Bubble';
        renewalKey.Deal_Type_Manual__c = 'Direct';
        renewalKey.Purchase_Scenario2__c = 'Renewal';
        renewalKey.Former_Key__c = upsellKey.Id;
        renewalKey.Primary_Contact__c = c.Id;
        renewalKey.Expires__c = Date.newInstance(2018,1,31);
        renewalKey.Account__c = a.Id;
        renewalKey.Hosted__c = true;
        insert renewalKey;
        
        CSM_License_Record__c upsellBubble = new CSM_License_Record__c();
        upsellBubble.Current_Key__c = 'Yes';
        upsellBubble.Maintenance__c = 'Instance 1';
        upsellBubble.License_Name__c = 'TestUpsell2';
        upsellBubble.Type__c = 'Bubble';
        upsellBubble.Deal_Type_Manual__c = 'Direct';
        upsellBubble.Purchase_Scenario2__c = 'Upsell';
        upsellBubble.Former_Key__c = renewalKey.Id;
        upsellBubble.Primary_Contact__c = c.Id;
        upsellBubble.Expires__c = Date.newInstance(2018,1,31);
        upsellBubble.Account__c = a.Id;
        upsellBubble.Hosted__c = true;
        insert upsellBubble;
        
        Contract cnt = new Contract();
        cnt.StartDate = Date.today();
        cnt.AccountId = a.Id;
        insert cnt;
        
        Product2 p = new Product2();
        p.Name = 'CSM Subscription License';
        insert p;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = p.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = cnt.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        s.SBQQ__Bundle__c = true;
        insert s;   
        
        SBQQ__Subscription__c s2 = new SBQQ__Subscription__c();
        s2.SBQQ__Quantity__c = 15;
        s2.SBQQ__Product__c = p.Id;
        s2.SBQQ__Account__c = a.Id;
        s2.SBQQ__Contract__c = cnt.Id;
        s2.Instance_Number__c = 'Instance 1';
        s2.Instance_Name__c = 'TestInst';
        s2.SBQQ__Bundle__c = true;
        insert s2; 
    }   
    
    @isTest static void testExpiredKeysandTrigger(){ 
        Test.startTest();
        String chron = '0 0 23 * * ?';
        LicenseKeyExpirationBatch lkeb = new LicenseKeyExpirationBatch();
        System.schedule('LicenseKeyExpirationBatch', chron, lkeb);
        Database.executeBatch(lkeb);
        Test.stopTest();
        
        CSM_License_Record__c testSuccess = [Select Current_Key__c From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        System.assertEquals('No', testSuccess.Current_Key__c);
    }
    
    @isTest static void testBubbleandBurst(){
        CSM_License_Record__c testSuccess = [Select Current_Key__c From CSM_License_Record__c Where License_Name__c = 'TestBubble'];
        CSM_License_Record__c testSuccess2 = [Select Current_Key__c From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        System.assertEquals('Yes', testSuccess.Current_Key__c);
        System.assertEquals('No', testSuccess2.Current_Key__c);
    }
    
    static testmethod void testRenewalHandler(){
        CSM_License_Record__c testSuccess = [Select Id, Former_Key__c From CSM_License_Record__c Where License_Name__c = 'TestRenewal'];
        CSM_License_Record__c testSuccess2 = [Select Id, Renewal_Key__c From CSM_License_Record__c Where License_Name__c = 'TestUpsell'];
        System.assertEquals(testSuccess2.Id, testSuccess.Former_Key__c);
        System.assertEquals(testSuccess.Id, testSuccess2.Renewal_Key__c);
        
        //CSM_License_Record__c testSuccess3 = [Select Current_Key__c From CSM_License_Record__c Where License_Name__c = 'TestRenewal'];
        //System.assertEquals('No', testSuccess3.Current_Key__c);      
    }

    @isTest static void testmakeLKGCallout() {
        Contract ct = [Select Id, AccountId, Account.Name From Contract Limit 1];
        
        Test.startTest();
        PageReference pageRef = Page.LicenseKeyGenerator;
        PageRef.getParameters().put('id', String.valueOf(ct.Id));
        PageRef.getParameters().put('accountid', String.valueOf(ct.AccountId));
        Test.setCurrentPage(pageRef);
        
        LicenseKeyGeneratorController lkgc = new LicenseKeyGeneratorController();
        Test.setMock(HttpCalloutMock.class, new LicenseKeyGenCalloutMock());
        //lkgc.addLicenseKey();
        lkgc.saveKeys();
        CSM_License_Record__c lkr = [Select Id From CSM_License_Record__c Limit 1];
       
        Test.stopTest();
    } */
}