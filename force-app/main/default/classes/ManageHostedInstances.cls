/*
* Updated September 2019 to create On Premise Instance records as well as Hosted, delineated by Record Type
*/
public class ManageHostedInstances {
    
    public static void createHostedInstances(Contract c){
        String quoteId;
        String accountId;
        String prodExternalDNS;
        String testExternalDNS;
        String camExternalDNS;
        Boolean dataEncryption = false;
        Boolean wdsnapshot = false;
        Boolean contentDeliveryNetwork = false;
        Boolean trustedAgent = false;
        //Boolean additionalStorage = false;
        
        String hostedRT = Schema.SObjectType.Hosted_Instance__c.getRecordTypeInfosByName().get('Hosted').getRecordTypeId();
        String onPremRT = Schema.SObjectType.Hosted_Instance__c.getRecordTypeInfosByName().get('OnPremise').getRecordTypeId();
        
        List<Hosted_Instance__c> newInstances = new List<Hosted_Instance__c>();
        
        Opportunity o;
        try { 
            o = [Select Record_Type_Name__c From Opportunity Where Id = :c.SBQQ__Opportunity__c]; 
        } 
        catch(Exception e){ 
            System.debug('Exception caught----' + e.getMessage()); 
        }
        String oppRT;
        if(o != null){ oppRT = o.Record_Type_Name__c; }
        accountId = c.AccountId;
        quoteId = c.SBQQ__Quote__c;
        System.debug('Quote Id on Contract-----'+quoteId);
        System.debug('Opportunity Record Type-----'+oppRT);
        
        //**Generate On Premise Instance records if it's an On Premise customer
        if(oppRT == 'New Logo' && c.Hosting_Model__c == 'On Premise'){
            List<SBQQ__Subscription__c> allSubs = [Select Id, SBQQ__ProductName__c, Instance_Number__c
                                                   From SBQQ__Subscription__c 
                                                   Where SBQQ__Product__r.Quote_Group_Name__c = 'Licensed Software'
                                                   And (SBQQ__Product__r.Family = 'CSM'
                                                        Or SBQQ__Product__r.Family = 'CAM')
                                                   And SBQQ__Contract__c = :c.Id];
            System.debug('All subs ---- ' + allSubs);
            Map<String, Set<String>> subByInstanceMap = new Map<String, Set<String>>();
            Set<String> instances = new Set<String>();
            
            //Get number of instances
            for(SBQQ__Subscription__c s : allSubs){
                instances.add(s.Instance_Number__c);
            }
            System.debug('Number of instances for this Account ---- ' + instances.size());
            
            //For each instance, group license lines by product name
            for(String i : instances){
                Set<String> thisInstanceProducts = new Set<String>();
                for(SBQQ__Subscription__c s : allSubs){
                    if(s.Instance_Number__c == i){
                        thisInstanceProducts.add(s.SBQQ__ProductName__c);
                    }
                }
                subByInstanceMap.put(i, thisInstanceProducts);
            }
            System.debug('Subs by Instance Map ---- ' + subByInstanceMap);
            
            //Loop through each instance and create an On Premise Instance record for each
            for(String instance : subByInstanceMap.keySet()){
                
                //Are there CSM Licenses? If yes, create CSM On Premise Instance record
                System.debug('Contains CSM? ---' + subByInstanceMap.get(instance).contains('CSM'));
                String productsString = '';
                for(String s:subByInstanceMap.get(instance)) {
                    productsString += (productsString==''?'':',')+s;
                }
                if(productsString.contains('CSM Subscription License') || 
                   productsString.contains('CSM Enterprise License') || 
                   productsString.contains('CSM Maintenance & Support')){                
                    Hosted_Instance__c csmOnPrem = new Hosted_Instance__c();
                    csmOnPrem.RecordTypeId = onPremRT;
                    csmOnPrem.Quote__c = quoteId;
                    csmOnPrem.Account__c = accountId;
                    csmOnPrem.Contract__c = c.Id;          
                    csmOnPrem.Product_Type__c = 'CSM';
                    csmOnPrem.Instance_Num__c = instance;
                    csmOnPrem.Instance_Environment_Type__c = 'Production';
                    newInstances.add(csmOnPrem); 
                }
                
                //Are there CAM Licenses? If yes, create CAM On Premise Instance record
                System.debug('Contains CAM? ---' + subByInstanceMap.get(instance).contains('CAM'));
                if(productsString.contains('CAM Subscription License') ||
                   productsString.contains('CAM Disc/Inv Subscription License') || 
                   productsString.contains('CAM Maintenance & Support')){
                    Hosted_Instance__c camOnPrem = new Hosted_Instance__c();
                    camOnPrem.RecordTypeId = onPremRT;
                    camOnPrem.Quote__c = quoteId;
                    camOnPrem.Account__c = accountId;
                    camOnPrem.Contract__c = c.Id;
                    camOnPrem.Product_Type__c = 'CAM';
                    camOnPrem.Instance_Num__c = instance;
                    camOnPrem.Instance_Environment_Type__c = 'Production';
                    newInstances.add(camOnPrem);
                }
            }
        }
        
        //**Generate Hosted Instance records if it's a Cherwell Hosted customer
        if(quoteId != null && oppRT == 'New Logo' && c.Hosting_Model__c == 'Cherwell Hosted'){
            
            SBQQ__Quote__c cQuote = [Select Id, Time_Zone__c, Server_Location__c, Server_Language__c, Trusted_Agent_Required__c,
                                     Preferred_Test_Server_DNS_Name_for_CSM__c, Preferred_Prod_Server_DNS_Name_for_CSM__c, Preferred_Prod_Server_DNS_Name_for_CAM__c
                                     From SBQQ__Quote__c
                                     Where Id = :quoteId];
            trustedAgent = cQuote.Trusted_Agent_Required__c;
            prodExternalDNS = cQuote.Preferred_Prod_Server_DNS_Name_for_CSM__c;
            testExternalDNS = cQuote.Preferred_Test_Server_DNS_Name_for_CSM__c;
            camExternalDNS = cQuote.Preferred_Prod_Server_DNS_Name_for_CAM__c;
            
            //Get all hosting lines and group them by instance number
            List<SBQQ__Subscription__c> allSubs = [Select Id, SBQQ__ProductName__c, Instance_Number__c
                                                   From SBQQ__Subscription__c 
                                                   Where SBQQ__Product__r.Family = 'Hosting Services'
                                                   And SBQQ__Contract__c = :c.Id];
            System.debug('All subs ---- ' + allSubs);
            Map<String, Set<String>> subByInstanceMap = new Map<String, Set<String>>();
            Set<String> instances = new Set<String>();
            
            //Get number of instances
            for(SBQQ__Subscription__c s : allSubs){
                instances.add(s.Instance_Number__c);
            }
            System.debug('Number of instances for this Account ---- ' + instances.size());
            
            //For each instance, group hosting lines by product name
            for(String i : instances){
                Set<String> thisInstanceProducts = new Set<String>();
                for(SBQQ__Subscription__c s : allSubs){
                    if(s.Instance_Number__c == i){
                        thisInstanceProducts.add(s.SBQQ__ProductName__c);
                    }
                }
                subByInstanceMap.put(i, thisInstanceProducts);
            }
            System.debug('Subs by Instance Map ---- ' + subByInstanceMap);
            
            //Loop through each instance and create Hosted Instance records for each, with specific hosting product information
            for(String instance : subByInstanceMap.keySet()){
                
                //Is there CSM Hosting? If yes, create CSM Production and Test Hosted Instance records
                if(subByInstanceMap.get(instance).contains('CSM Hosting Service')){
                    
                    //Is there data encryption?
                    if(subByInstanceMap.get(instance).contains('Data Encryption Service')){ dataEncryption = true; }
                    //Is there weekly database snapshots?
                    if(subByInstanceMap.get(instance).contains('Weekly Database Snapshots')){ wdsnapshot = true; }
                    //Is there content delivery network?
                    if(subByInstanceMap.get(instance).contains('Content Delivery Network')){ contentDeliveryNetwork = true; }
                    //Is there additional storage?
                    //if(subByInstanceMap.get(instance).contains('Hosting Additional Storage (1TB)')){ additionalStorage = true; }
                    
                    Hosted_Instance__c newPI = new Hosted_Instance__c();
                    newPI.RecordTypeId = hostedRT;
                    newPI.Quote__c = quoteId;
                    newPI.Account__c = accountId;
                    newPI.Contract__c = c.Id;          
                    newPI.Product_Type__c = 'CSM';
                    newPI.Database_Encrypted__c = dataEncryption;
                    //newPI.Instance_Num__c = (String)ar.get('Instance_Number__c');
                    newPI.Instance_Num__c = instance;
                    newPI.External_DNS__c = prodExternalDNS;
                    newPI.Instance_Environment_Type__c = 'Production';
                    newPI.Trusted_Agent_Required__c = trustedAgent;
                    newPI.Content_Delivery_Network__c = contentDeliveryNetwork;
                    newPI.Weekly_Database_Snapshot__c = wdsnapshot;
                    //newPI.Additional_Storage__c = additionalStorage;
                    newInstances.add(newPI); 
                    
                    Hosted_Instance__c newTI = new Hosted_Instance__c();
                    newTI.RecordTypeId = hostedRT;
                    newTI.Quote__c = quoteId;
                    newTI.Account__c = accountId;
                    newTI.Contract__c = c.Id;
                    newTI.Product_Type__c = 'CSM';
                    newTI.Database_Encrypted__c = dataEncryption;
                    newTI.Instance_Num__c = instance;
                    newTI.External_DNS__c = testExternalDNS;
                    newTI.Instance_Environment_Type__c = 'Test';
                    newTI.Trusted_Agent_Required__c = trustedAgent;
                    newTI.Content_Delivery_Network__c = contentDeliveryNetwork;
                    newTI.Weekly_Database_Snapshot__c = wdsnapshot;
                    //newPI.Additional_Storage__c = additionalStorage;
                    newInstances.add(newTI);  
                }
                
                //Is there CAM Hosting? 
                if(subByInstanceMap.get(instance).contains('CAM Hosting Service')){
                    Hosted_Instance__c newCamI = new Hosted_Instance__c();
                    newCamI.RecordTypeId = hostedRT;
                    newCamI.Quote__c = quoteId;
                    newCamI.Account__c = accountId;
                    newCamI.Contract__c = c.Id;
                    newCamI.Product_Type__c = 'CAM';
                    //No CherwellGO!, only applies to CSM
                    //No Data Encryption Service, only applies to CSM
                    //No Weekly Database Snapshot, only applies to CSM
                    //No Content Delivery Network, only applies to CSM
                    newCamI.Instance_Num__c = instance;
                    newCamI.External_DNS__c = camExternalDNS;
                    newCamI.Instance_Environment_Type__c = 'Production';
                    newInstances.add(newCamI);
                }   
            }
        }
        System.debug(newInstances.size());
        if(newInstances.size()>0){
            System.debug('Inserting new Instances---------' + newInstances);
            insert newInstances; 
        }
        c.Process_SaaS__c = false; 
    }
}