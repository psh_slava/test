/*
* @class name:   CustomUserLookupController
* @created:      By Dileep Allada in May 2020
* @test class:   CustomUserLookupControllerTest.apxc
* @initiated by: CustomUserLookup.vfp, OpportunityTeamEdit.vfp
* @description: 
*    Performs the user/partner user lookup on the customer Opportunity Team Edit page
* @modifcation log:
*    Jul 2020 - Naomi Harmon - Updated to look at Partner Involvement object to choose which Partner Users should be available to add to an Opportunity Team
*
*/

public with sharing class CustomUserLookupController {
    
    public User User{get;set;} // new account to create
    public List<User> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public string usertype{get;set;} // userType keyword
    public string opportunityId{get;set;} // userType keyword
    
    public CustomUserLookupController() {
        
        //get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        usertype = System.currentPageReference().getParameters().get('type');
        opportunityId = System.currentPageReference().getParameters().get('oppid');
        runSearch();  
    }
    
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 
    
    // run the search and return the records found. 
    private List<User> performSearch(string searchString) {
        String soql;
        Try{   
            IF(usertype == 'User')
            {
                If(string.IsNotBlank(searchString))
                {
                    System.Debug('Test1');
                    soql = 'select id, name from User';
                    soql = soql +  ' where name LIKE \'%' + searchString +'%\' AND IsActive = true Order By Name Asc';
                    soql = soql + ' limit 25';
                }
                Else
                {
                    System.Debug('Test1');
                    soql = 'select id, name from User';
                    soql = soql +  ' where IsActive = true Order By Name Asc';
                    soql = soql + ' limit 25';
                }
            }                       
            Else IF(usertype == 'Partner')
            {
                Set<id> AccountIdset = New Set<id>();
                
                List<Partner_Involvement__c> partnersInv = [Select Partner__c From Partner_Involvement__c Where Opportunity__c =: opportunityId];
                
                /*opportunity Opp = [Select id,Referral_PartnerId__c,Sales_Assist_PartnerId__c,Fulfillment_Partner__c,RFP_Partner__c,Reseller_Partner__c,
                                   Strategic_Partner__c,Services_Delivered_By__c,Training_Delivered_By__c,Support_Partner__c from Opportunity where id =: opportunityId];*/
                
                if(partnersInv != null && partnersInv.size() > 0){
                for(Partner_Involvement__c pi : partnersInv){
                    AccountIdset.add(pi.Partner__c);
                }
                }
                
                /*IF(Opp.Referral_PartnerId__c != Null)
                {
                    AccountIdset.Add(Opp.Referral_PartnerId__c);
                } 
                
                IF(Opp.Sales_Assist_PartnerId__c != Null)
                {
                    AccountIdset.Add(Opp.Sales_Assist_PartnerId__c);
                } 
                
                IF(Opp.Fulfillment_Partner__c != Null)
                {
                    AccountIdset.Add(Opp.Fulfillment_Partner__c);
                } 
                
                IF(Opp.RFP_Partner__c != Null)
                {
                    AccountIdset.Add(Opp.RFP_Partner__c);
                } 
                IF(Opp.Reseller_Partner__c != Null)
                {
                    AccountIdset.Add(Opp.Reseller_Partner__c);
                } 
                
                IF(Opp.Strategic_Partner__c != Null)
                {
                    AccountIdset.Add(Opp.Strategic_Partner__c);
                } 
                
                IF(Opp.Services_Delivered_By__c != Null)
                {
                    AccountIdset.Add(Opp.Services_Delivered_By__c);
                } 
                
                IF(Opp.Training_Delivered_By__c != Null)
                {
                    AccountIdset.Add(Opp.Training_Delivered_By__c);
                } 
                
                IF(Opp.Support_Partner__c != Null)
                {
                    AccountIdset.Add(Opp.Support_Partner__c );
                } */
                
                Map<Id,Contact> MapContacts;
                IF(AccountIdset.size()>0)
                {
                    MapContacts = New Map<Id,Contact>([Select id,AccountId from Contact Where AccountId IN : AccountIdset]);
                } 
                
                String newSetStr = '' ;
                
                Map<id,User> UserMap = New Map<id,User>([Select id,ContactId from User Where contactId IN:MapContacts.Keyset()]);
                IF(UserMap.size()>0){ 
                    Set<id> UserId = UserMap.KeySet();
                    
                    String idString = '(\'';
                    for (Id thisId : UserId ) {
                        System.debug(idString );
                        idString += thisId + '\',\'';
                        System.debug(idString );
                    }
                    System.debug(idString );
                    idString = idString.substring(0,idString.length()-2); //
                    idString += ')';   
                    System.debug(idString );
                    
                    If(string.IsNotBlank(searchString))
                    {
                        System.Debug('Test1');
                        soql = 'select id, name from User';
                        soql = soql +  ' where name LIKE \'%' + searchString +'%\' AND IsActive = true and userType =\'PowerPartner\' AND ID IN' + idString + 'Order By Name Asc';
                        soql = soql + ' limit 25';
                    }
                    Else
                    {
                        System.Debug('Test1'+UserId);
                        soql = 'select id, name from User';
                        soql = soql +  ' where IsActive = true and userType =\'PowerPartner\' AND ID IN' + idString + 'Order By Name Asc' ;
                        soql = soql + ' limit 25';
                    }
                }   
                
            }
            System.debug(soql);
            return database.query(soql); 
        }
        Catch(Exception e)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'No matching partner users found. Please make sure you have listed the Partner Account as involved in the Opportunity and that you have your partner user(s) enabled.'));
            return null;
        }   
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
}