@isTest
public class QuoteLineItemsRelatedListControllerTest {
	
	@isTest(SeeAllData=false)    
    static void QuoteLineItemsRelatedListController_Test(){
     	
     	Opportunity oppty = new Opportunity();
        oppty.Name = 'Test';
        oppty.Amount = 0;
        oppty.StageName = 'Pre-Qualification';
        oppty.CloseDate = System.today();
        oppty.Forecast_Category__c = 'Omitted';
        insert oppty;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Opportunity2__c = oppty.Id;
        insert q;
        
        Product2 p = new Product2();
        p.Name = 'Testing Product - Please Ignore';
        p.Family = 'Hosting Services';
        p.Product_Type__c = 'Hosting Services';
        p.CAM_License_Type__c = 'CAM Subscription';
        p.License_Model__c = 'Subscription';
        insert p;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__PriorQuantity__c = 1;
        ql.SBQQ__UpgradedQuantity__c = 1;
        ql.SBQQ__Quote__c = q.id;
        ql.SBQQ__Product__c = p.id;
        ql.SBQQ__NetPrice__c = 10;
        insert ql;
        
        PageReference newPage = Page.QuoteLineItemsRelatedList;
        newPage.getParameters().put('id', q.Id);
        Test.setCurrentPage(newPage);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(q);
        QuoteLineItemsRelatedListController controller = new QuoteLineItemsRelatedListController(sc);       
        controller.populateRows();   
        
        controller.itemsPerPage = 1;    
        controller.doSortRows();
        
        controller.orderBy = 'OwnerId';
        controller.doSortRows();  
        controller.sortOrder = 'DESC';
        controller.doSortRows(); 
                                
        controller.nextPage();
        controller.goToLast();
        controller.prevPage();
        controller.goToFirst();            
    }
}