@isTest
Public class mergeUtilTest 
{
    @testSetup
    public Static Void setup() 
    {
        List<LeadLifeCycleMapping__c> lfcCustomSetting = new List<LeadLifeCycleMapping__c>();
        
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Person_Status__c' ,Lead_Api_Name__c ='Status' ,OnCreate__c = true));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'OwnerID' ,Lead_Api_Name__c ='OwnerID' ,OnCreate__c = true));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Related_Lead__c' ,Lead_Api_Name__c ='Id' ,OnCreate__c = true));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Score_at_TQL__c' ,Lead_Api_Name__c ='mkto71_Lead_Score__c' ,OnCreate__c = false));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Score_at_TAL__c' ,Lead_Api_Name__c ='mkto71_Lead_Score__c' ,OnCreate__c = false));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Recycle_Reason__c' ,Lead_Api_Name__c ='Back_To_Nurture_Explanation__c' ,OnCreate__c = false));
        
        insert lfcCustomSetting;
        
        list<ContactLifeCycleMapping__c> lyfCycleMapping =new list<ContactLifeCycleMapping__c> ();
        
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Lifecycle_Stage__c', isDefault__c=True, Default_Value__c='Inquiry' ,OnCreate__c =False)); 
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Lifecycle_Status__c',  isDefault__c=True, Default_Value__c='Inactive' ,OnCreate__c =False)); 
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='OwnerId', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='OwnerId' ));      
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Lead_Source_Detail__c', isDefault__c=False,OnCreate__c=True, Contact_Api_Name__c='Lead_Source_Detail__c'));
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Lead_Source__c', isDefault__c=False,OnCreate__c=True, Contact_Api_Name__c='LeadSource'));
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Person_Status__c', isDefault__c=False,OnCreate__c=True, Contact_Api_Name__c='Contact_Status__c')); 
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Priority__c', isDefault__c=False,  OnCreate__c=True, Contact_Api_Name__c='Priority__c')); 
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Related_Account__c', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='AccountId')); 
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Related_Contact__c', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='Id'));
        lyfCycleMapping.add(new ContactLifeCycleMapping__c(Name='Generated_By__c', isDefault__c=False, OnCreate__c=True, Contact_Api_Name__c='Generated_By__c'));
        insert lyfCycleMapping;
             
    }
    @isTest
    public static void leadMerge_DeleteLead(){
        
        List<lead> leadLstAllActive = new list<lead>();
        Lead l1 = new lead(status='Working' ,LastName= 'Lead1', Company='abc' ,CurrencyIsoCode= 'EUR',Lead_Score__c= '1');
        Lead l2 = new lead(status ='Engaged',Lead_Sub_Status__c= 'Discover' ,LastName= 'Lead2', Company='abc',CurrencyIsoCode='EUR' ,Lead_Score__c ='2');
        
        leadLstAllActive.add(l1);
        leadLstAllActive.add(l2);
        system.debug('leadLstAllActive'+leadLstAllActive) ;
        insert leadLstAllActive ;
        system.debug('leadLstAllActive__lst'+[select id, Related_Contact__c, Related_Lead__c ,Lifecycle_Status__c from Lifecycle__c  where Related_Lead__c =: l2.id]) ;
        
        merge l1 l2 ;
        
        List<lead> leadLstOneActive = new list<lead>();
        Lead l3 = new lead(status='Working' ,LastName= 'Lead1', Company='abc' ,CurrencyIsoCode= 'EUR',Lead_Score__c= '1');
        Lead l4 = new lead(status ='Engaged',Lead_Sub_Status__c= 'Discover' ,LastName= 'Lead2', Company='abc',CurrencyIsoCode='EUR' ,Lead_Score__c ='2');
        leadLstOneActive.add(l3) ;
        leadLstOneActive.add(l4) ;
        insert leadLstOneActive ;
        
        list<lifecycle__c> lyfl3 = [select id, Related_Contact__c, Related_Lead__c ,Lifecycle_Status__c from Lifecycle__c  where Related_Lead__c =: l3.id limit 1 ] ;
        system.debug('lyfl3__lst'+lyfl3) ;
        lyfl3[0].Lifecycle_Status__c = 'Inactive' ;
        update lyfl3 ; 
        system.debug('lyfl3'+lyfl3) ;
        merge l3 l4 ;
        
        List<lead> leadLstNoActive = new list<lead>();
        Lead l5 = new lead(status='Working' ,LastName= 'Lead1', Company='abc' ,CurrencyIsoCode= 'EUR',Lead_Score__c= '1');
        Lead l6 = new lead(status ='Engaged',Lead_Sub_Status__c= 'Discover' ,LastName= 'Lead2', Company='abc',CurrencyIsoCode='EUR' ,Lead_Score__c ='2');
        leadLstNoActive.add(l5) ;
        leadLstNoActive.add(l6) ;
        insert leadLstNoActive ;
        
        list<lifecycle__c> lyfLst = [select id, Related_Contact__c, Related_Lead__c ,Lifecycle_Status__c from Lifecycle__c  where Related_Lead__c =: l5.id or Related_Lead__c =: l6.id ] ;
        system.debug('lyfLst__lst'+lyfLst) ;
        for(lifecycle__c lyf :lyfLst){
            lyf.Lifecycle_Status__c = 'Inactive' ;
        }
        update lyfLst ;
        merge l5 l6 ; 
        
        
    }
    @isTest
    public static void contactMerge_DeleteLead(){
  
        account acc=new account(name='Test Account 1');
        insert acc;
       
        list<contact> conLstAllActive=new list<contact>();
       
        contact cont1=new contact(lastname='Test Cont 1',Contact_Status__c = 'Engaged', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id,mkto71_Lead_Score__c=20 ,IsLead_Generated__c = false ,CurrencyIsoCode = 'USD',email ='TestSourceDetail@test.com');        
        contact cont2=new contact(lastname='Test Cont 2',Contact_Status__c = 'Engaged', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id,mkto71_Lead_Score__c=20 ,IsLead_Generated__c = false ,CurrencyIsoCode = 'USD' ,email ='TestSourceDetail3@test.com');
        
        conLstAllActive.add(cont2);
        conLstAllActive.add(cont1);
        
        insert conLstAllActive ;
        merge cont1 cont2 ;
        
        list<contact> conLstOneActive=new list<contact>();
       
        contact cont3=new contact(lastname='Test Cont 1' ,Contact_Status__c = 'Back To Nurture', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id,mkto71_Lead_Score__c=20 ,IsLead_Generated__c = FALSE ,email ='TestSourceDetail2@test.com' ,CurrencyIsoCode = 'USD');
        contact cont4=new contact(lastname='Test Cont 2',Contact_Status__c = 'Back To Nurture', Generated_By__c= 'Test Generartor 1', Lead_Source_Detail__c='Test SourceDetail 1', LeadSource='test Source 1' , Priority__c='Standard Priority', AccountId=acc.id,mkto71_Lead_Score__c=20 ,IsLead_Generated__c = FALSE ,email ='TestSourceDetail1@test.com' ,CurrencyIsoCode = 'USD');
        
        conLstOneActive.add(cont3);
        conLstOneActive.add(cont4);
        insert conLstOneActive ;
        
        list<lifecycle__c> lyfl3 = [select id, Lifecycle_Status__c from Lifecycle__c  where Related_Lead__c =: cont3.id  ] ;
        
        if(lyfl3.size()>0 && lyfl3 != null){
            lyfl3[0].Lifecycle_Status__c = 'Inactive';
            update lyfl3 ;
        }     
        merge cont3 cont4 ;
        
        
    }
}