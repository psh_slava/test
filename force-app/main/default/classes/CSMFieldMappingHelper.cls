// Helper class for getting field mappings and their metadata
public class CSMFieldMappingHelper
{
    private static Map<String,Map<String, Schema.DescribeFieldResult>> cachedFieldDescribes {get;set;}
    private static Map<String,Schema.DescribeSObjectResult> cachedObjectDescribes {get;set;}

    private static Map<String, List<CSM_Field_Mapping__c>> cachedFieldMappingsBySObjectName;
    private static Map<String, List<CSM_Field_Mapping__c>> cachedFieldMappingsByCSMObjectName;
    private static Map<String, List<CSM_Field_Mapping__c>> cachedFieldMappingsByCSMFieldId;
    private static Map<String, List<CSM_Field_Mapping__c>> cachedFieldMappingsBySFQualifiedFieldName;

    private static Map<String, CSM_Object_Mapping__c> cachedObjectMappingsBySObjectName;
    private static Map<String, CSM_Object_Mapping__c> cachedObjectMappingsByCSMObjectName;
    private static Map<String, CSM_Object_Mapping__c> cachedObjectMappingsByCSMObjectId;

    public class CSMFieldMappingHelperException extends Exception {}
    public class CSMRequiresAutoRetryException extends Exception {}

    private static void addCachedDescribe(String objName){
        objName = objName.toLowerCase(); 
        if(cachedFieldDescribes == null){cachedFieldDescribes = new Map<String,Map<String, Schema.DescribeFieldResult>>();}
        if(cachedObjectDescribes == null){cachedObjectDescribes = new Map<String, Schema.DescribeSObjectResult>();}
        if(!cachedFieldDescribes.containsKey(objName)){
            Schema.DescribeSObjectResult[] results = Schema.describeSObjects(new List<String>{objName});
            for(Schema.DescribeSObjectResult res : results){  
                cachedObjectDescribes.put(objName,res);
                cachedFieldDescribes.put(objName,new Map<String,Schema.DescribeFieldResult>()); 
                if(res.isAccessible() && (res.isUpdateable() || res.isCreateable()) ){
                    Map<String, Schema.SObjectField> objFields = res.fields.getMap();//Schema.SObjectType.Contact.fields.getMap();
                    List<String> fieldNames = new List<String>(objFields.keySet()); 
                    for(String fieldName : fieldNames){ 
                        Schema.SObjectField field = objFields.get(fieldName);
                        Schema.DescribeFieldResult fieldDescribeRes = field.getDescribe(); 
                        String realFieldName = fieldDescribeRes.getName();
                        cachedFieldDescribes.get(objName).put(realFieldName.toLowerCase(),fieldDescribeRes);
                    }
                }
            }
        }
    }

    public static Boolean doesFieldExist(String objName, String fieldName) {
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if (cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            return true;
        }
        return false;
    }

    public static Boolean isFieldRequired(string objName, String fieldName) {
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return  !fieldDescribeRes.isNillable();
        }
        return false;
    }

    public static Boolean canUpdateField(String objName, String fieldName){ 
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return  (fieldDescribeRes.isAccessible() && fieldDescribeRes.isUpdateable());
        }
        return false;
    
    }  

    public static Boolean canCreateField(String objName, String fieldName){ 
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return  (fieldDescribeRes.isAccessible() && fieldDescribeRes.isCreateable());
        }
        return false;
    
    } 

    public static Boolean canAccessField(String objName, String fieldName){ 
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return  (fieldDescribeRes.isAccessible() );
        }
        return false;
    
    }

    public static String getFieldType(String objName, String fieldName){ 
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return  String.valueOf(fieldDescribeRes.getType() );
        }
        return null;
    }

    public static Integer getFieldLength(String objName, String fieldName) {
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return  fieldDescribeRes.getLength();
        }
        return null;
    }

    public static Set<String> getParentObjectNames(String objName, String fieldName){ 
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            Set<String> parentNames = new Set<String>();
            for(Schema.sObjectType parType : fieldDescribeRes.getReferenceTo()){
                parentNames.add(String.valueOf(parType));
            }
            return  parentNames;
        }
        return null;
    }

    public static String getParentRelationshipName(String objName, String fieldName){
        objName = objName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        addCachedDescribe(objName);
        if(cachedFieldDescribes.containsKey(objName) && cachedFieldDescribes.get(objName).containsKey(fieldName)) {
            Schema.DescribeFieldResult fieldDescribeRes = cachedFieldDescribes.get(objName).get(fieldName);
            return fieldDescribeRes.getRelationshipName(); 
        }
        return null;
    }

    public static sObject getSObjectWithName(String objName){ 
        objName = objName.toLowerCase(); 
        addCachedDescribe(objName); 
        Schema.DescribeSObjectResult des = cachedObjectDescribes.get(objName);
        Schema.sObjectType sobjtype = des.getSObjectType(); 
        return sobjtype.newSobject();

    }

    public static Schema.DescribeSObjectResult getSObjectDescribeWithName(String objName){ 
        objName = objName.toLowerCase(); 
        addCachedDescribe(objName); 
        return cachedObjectDescribes.get(objName);  
    }

    public static Map<String, List<CSM_Field_Mapping__c>> getAllFieldMappingsBySObjectName() {
        if (cachedFieldMappingsBySObjectName == null) {

            cachedFieldMappingsBySObjectName = new Map<String, List<CSM_Field_Mapping__c>>();

            for(CSM_Field_Mapping__c fm : CSM_Field_Mapping__c.getAll().values()) {
                if(fm.IsActive_Mapping__c){
                    List<CSM_Field_Mapping__c> fms = new List<CSM_Field_Mapping__c>();

                    if (cachedFieldMappingsBySObjectName.containsKey(fm.Salesforce_Object_Name__c)) {
                        fms = cachedFieldMappingsBySObjectName.get(fm.Salesforce_Object_Name__c);
                    }

                    fms.add(fm);
                    cachedFieldMappingsBySObjectName.put(fm.Salesforce_Object_Name__c, fms);
                }
            }
        }

        return cachedFieldMappingsBySObjectName;
    }

    public static Map<String, List<CSM_Field_Mapping__c>> getAllFieldMappingsByCSMObjectName() {
        if (cachedFieldMappingsByCSMObjectName == null) {

            cachedFieldMappingsByCSMObjectName = new Map<String, List<CSM_Field_Mapping__c>>();
            for(CSM_Field_Mapping__c fm : CSM_Field_Mapping__c.getAll().values()) {
                if(fm.IsActive_Mapping__c){
                    List<CSM_Field_Mapping__c> fms = new List<CSM_Field_Mapping__c>();

                    if (cachedFieldMappingsByCSMObjectName.containsKey(fm.CSM_Business_Object_Name__c)) {
                        fms = cachedFieldMappingsByCSMObjectName.get(fm.CSM_Business_Object_Name__c);
                    }

                    fms.add(fm);
                    cachedFieldMappingsByCSMObjectName.put(fm.CSM_Business_Object_Name__c, fms);
                }
            }
        }
        
        return cachedFieldMappingsByCSMObjectName;
    }

    public static Map<String, List<CSM_Field_Mapping__c>> getAllFieldMappingsByCSMFieldId() {
        if (cachedFieldMappingsByCSMFieldId == null) {

            cachedFieldMappingsByCSMFieldId = new Map<String, List<CSM_Field_Mapping__c>>();

            for(CSM_Field_Mapping__c fm : CSM_Field_Mapping__c.getAll().values()) {
                if(fm.IsActive_Mapping__c){
                    List<CSM_Field_Mapping__c> fms = new List<CSM_Field_Mapping__c>();

                    if (cachedFieldMappingsByCSMFieldId.containsKey(fm.CSM_Field_Id__c)) {
                        fms = cachedFieldMappingsByCSMFieldId.get(fm.CSM_Field_Id__c);
                    }

                    fms.add(fm);
                    cachedFieldMappingsByCSMFieldId.put(fm.CSM_Field_Id__c, fms);
                }
            }
        }
        
        return cachedFieldMappingsByCSMFieldId;
    }

    public static Map<String, List<CSM_Field_Mapping__c>> getAllFieldMappingsBySFQualifiedFieldName() {
        if (cachedFieldMappingsBySFQualifiedFIeldName == null) {

            cachedFieldMappingsBySFQualifiedFIeldName = new Map<String, List<CSM_Field_Mapping__c>>();

            for(CSM_Field_Mapping__c fm : CSM_Field_Mapping__c.getAll().values()) {
                if(fm.IsActive_Mapping__c){
                    String qualifiedFieldName = fm.Salesforce_Object_Name__c + '.' + fm.Salesforce_Field_Name__c;
                    List<CSM_Field_Mapping__c> fms = new List<CSM_Field_Mapping__c>();

                    if (cachedFieldMappingsBySFQualifiedFIeldName.containsKey(qualifiedFieldName)) {
                        fms = cachedFieldMappingsBySFQualifiedFIeldName.get(qualifiedFieldName);
                    }

                    fms.add(fm);
                    cachedFieldMappingsBySFQualifiedFIeldName.put(qualifiedFieldName, fms);
                }
            }
        }
        
        return cachedFieldMappingsBySFQualifiedFIeldName;
    }

    public static Map<String, CSM_Object_Mapping__c> getAllObjectMappingsBySObjectName() {
        if (cachedObjectMappingsBySObjectName == null) {
            cachedObjectMappingsBySObjectName = new Map<String, CSM_Object_Mapping__c>();

            for(CSM_Object_Mapping__c om : CSM_Object_Mapping__c.getAll().values()) {
                cachedObjectMappingsBySObjectName.put(om.Salesforce_Object_Name__c, om);
            }
        }

        return cachedObjectMappingsBySObjectName;
    }

    public static Map<String, CSM_Object_Mapping__c> getAllObjectMappingsByCSMObjectName() {
        if (cachedObjectMappingsByCSMObjectName == null) {
            cachedObjectMappingsByCSMObjectName = new Map<String, CSM_Object_Mapping__c>();

            for(CSM_Object_Mapping__c om : CSM_Object_Mapping__c.getAll().values()) {
                cachedObjectMappingsByCSMObjectName.put(om.CSM_Business_Object_Name__c, om);
            }
        }

        return cachedObjectMappingsByCSMObjectName;
    }

    public static Map<String, CSM_Object_Mapping__c> getAllObjectMappingsByCSMObjectId() {
        if (cachedObjectMappingsByCSMObjectId == null) {
            cachedObjectMappingsByCSMObjectId = new Map<String, CSM_Object_Mapping__c>();

            for(CSM_Object_Mapping__c om : CSM_Object_Mapping__c.getAll().values()) {
                cachedObjectMappingsByCSMObjectId.put(om.CSM_Business_Object_Id__c, om);
            }
        }

        return cachedObjectMappingsByCSMObjectId;
    }

    public static Map<String, Map<String, String>> getMappedFieldChanges(String eventType, SObject oldObj, SObject newObj) {
        Map<String, Map<String, String>> mappedFieldChanges = new Map<String, Map<String, String>>();
        String objectTypeName = String.valueOf(newObj.getSObjectType());
        Map<String, List<CSM_Field_Mapping__c>> allFieldMappings = getAllFieldMappingsBySObjectName();

        Set<String> excludedFields = new Set<String>{ 'CreatedById', 'LastModifiedById', 'CreatedDate', 'LastModifiedDate'};
        if (allFieldMappings.containsKey(objectTypeName)) {
            for(CSM_Field_Mapping__c fm : allFieldMappings.get(objectTypeName)) {

                if(String.isNotBlank(fm.Salesforce_Field_Name__c) &&
                    !excludedFields.contains(fm.Salesforce_Field_Name__c) &&
                    ((eventType == CSMConstants.SYNC_EVENT_TYPE_OUTBOUND && fm.Outbound__c) ||
                        (eventType == CSMConstants.SYNC_EVENT_TYPE_INBOUND && fm.Inbound__c))) {
                    
                    // If Inbound, we'll skip fields that are not editable
                    if (eventType == CSMConstants.SYNC_EVENT_TYPE_INBOUND) {
                        // Don't proceed if we're dealing with a new object but the current field does not
                        // allow setting on inserts.  We exclude Id from this check because Id always comes 
                        // back as non-createable.
                        if (newObj.Id == null && !canCreateField(objectTypeName, fm.Salesforce_Field_Name__c) && fm.Salesforce_Field_Name__c != 'Id') continue;
                        
                        // Don't proceed if we're dealing with an existing object but the current field does
                        // not allow setting on updates. We exclude Id from this check because Id always comes 
                        // back as non-updateable.
                        if (newObj.Id != null && !canUpdateField(objectTypeName, fm.Salesforce_Field_Name__c) && fm.Salesforce_Field_Name__c != 'Id') continue;
                    }


                    String oldValue = '';
                    String newValue = '';
                    Boolean compare = true;

                    // If field is flagged for lookup via external id, we need to compare the external id,
                    // not the actual lookup id.
                    if (oldObj != null) {
                        if (fm.Look_up_using_External_Id__c) {
                            // Check if the external id is populated in the new object.  
                            // If not, we'll compare using the lookup id.
                            String parentRelName = getParentRelationshipName(objectTypeName, fm.Salesforce_Field_Name__c);
                            CSM_Object_Mapping__c pom = getParentObjectMapping(objectTypeName, fm.Salesforce_Field_Name__c);

                            SObject newParentObj = newObj.getSObject(parentRelName);
                            if (newParentObj != null) {
                                if (newParentObj.get(pom.Salesforce_External_Id_Field_Name__c) != null) {
                                    newValue = String.valueOf(newParentObj.get(pom.Salesforce_External_Id_Field_Name__c));
                                }
                            } else if(newObj.get(fm.Salesforce_Field_Name__c) != null) {
                                newValue = String.valueOf(newObj.get(fm.Salesforce_Field_Name__c));
                            }

                            SObject oldParentObj = oldObj.getSObject(parentRelName);
                            if (oldParentObj != null) {
                               if (oldParentObj.get(pom.Salesforce_External_Id_Field_Name__c) != null) {
                                    oldValue = String.valueOf(oldParentObj.get(pom.Salesforce_External_Id_Field_Name__c));
                                } 
                            } else if(oldObj.get(fm.Salesforce_Field_Name__c) != null) {
                                oldValue = String.valueOf(oldObj.get(fm.Salesforce_Field_Name__c));
                            }

                            compare = (newValue != oldValue);


                        } else {
                            if(newObj.get(fm.Salesforce_Field_Name__c) != null) {
                                newValue = String.valueOf(newObj.get(fm.Salesforce_Field_Name__c));
                            }

                            if(oldObj.get(fm.Salesforce_Field_Name__c) != null) {
                                oldValue = String.valueOf(oldObj.get(fm.Salesforce_Field_Name__c));
                            }

                            if ((newObj.get(fm.Salesforce_Field_Name__c) == null && oldObj.get(fm.Salesforce_Field_Name__c) == null) ||
                                newObj.get(fm.Salesforce_Field_Name__c) == oldObj.get(fm.Salesforce_Field_Name__c) ||
                                newValue == oldValue) {

                                compare = false;
                            } 

                        }

                        if (compare) {
                            System.debug('Changed ' + fm.Salesforce_Field_Name__c + '- old: ' + oldValue + ', new: ' + newValue);
                        } else {
                            System.debug('No change ' + fm.Salesforce_Field_Name__c + '- old: ' + oldValue + ', new: ' + newValue);
                        }
                    } 


                    if (compare) {

                        mappedFieldChanges.put(fm.Salesforce_Field_Name__c, new Map<String, String>{
                            'oldValue' => oldValue,
                            'newValue' => newValue
                            });
                    } 
                }
            }
        }
        System.debug('mappedFieldChanges');
        System.debug(mappedFieldChanges);

        return mappedFieldChanges;
    }

    // Lookup fields can have multiple parent types (polymorphic field).  We'll only return the parent that
    // is mapped in CSM Object Mapping. Note that this means we currently don't support the case where both
    // parent types are mapped.  Example of this is Owner field which can be mapped to either User or 
    // Group (Queue). If both objects are mapped, we wouldn't know which one to use.
    public static CSM_Object_Mapping__c getParentObjectMapping(String objectTypeName, String lookupFieldName) {
        String parentObjectName;
        Boolean isParentObjectMapped = false;
        Map<String, CSM_Object_Mapping__c> objectMappingByObjectName = CSMFieldMappingHelper.getAllObjectMappingsBySObjectName();
        for(String po : getParentObjectNames(objectTypeName, lookupFieldName)){
            parentObjectName = po;
        
            if (!objectMappingByObjectName.containsKey(po)) continue;

            if (!isParentObjectMapped) {
                isParentObjectMapped = true;
            } else {
                throw new CSMFieldMappingHelperException('Found multiple possible parent objects mapped for lookup field ' + objectTypeName + '.' + lookupFieldName);
            }           

        }

        if (!isParentObjectMapped) {
            throw new CSMFieldMappingHelperException(parentObjectName + ', the parent object pointed to by ' + objectTypeName + '.' + lookupFieldName + ', is not configured in CSM Object Mapping');
        }

        return objectMappingByObjectName.get(parentObjectName);
    }

    // Returns list of mapped Salesforce field names
    public static Set<String> getMappedSalesforceFields(String objectTypeName) {
        Set<String> fields = new Set<String>();
        CSM_Object_Mapping__c om = CSMFieldMappingHelper.getAllObjectMappingsBySObjectName().get(objectTypeName);
        System.debug('DEBUG: CSMFieldMappingHelper objectTypeName     ' + objectTypeName);
        System.debug('DEBUG: CSMFieldMappingHelper om     ' + om);
        for(CSM_Field_Mapping__c fm : getAllFieldMappingsBySObjectName().get(objectTypeName)) {
            System.debug('DEBUG: CSMFieldMappingHelper om     ' + fm);
            if (String.isNotBlank(fm.Salesforce_Field_Name__c)) {
                fields.add(fm.Salesforce_Field_Name__c);
            }
            System.debug('DEBUG: CSMFieldMappingHelper fields     ' + fields);
            if (fm.Look_up_using_External_Id__c) {
                String parentRelName = getParentRelationshipName(objectTypeName, fm.Salesforce_Field_Name__c);
                
                CSM_Object_Mapping__c pom = getParentObjectMapping(objectTypeName, fm.Salesforce_Field_Name__c);
                String parentObjectName = pom.Salesforce_Object_Name__c;

                fields.add(parentRelName + '.' + pom.Salesforce_External_Id_Field_Name__c);

                // User parent object will not have this field
                if (doesFieldExist(parentObjectName, 'CSM_Sync_Status__c')) {
                    // Add the CSM Sync Status field of the parent.  This will be used later to figure out when
                    // to do an auto-retry when the external id of the parent has not been populated yet from Live to SF.
                    fields.add(parentRelName + '.CSM_Sync_Status__c');
                }

            }
        }
        return fields;
    }


    // Sets SObject field, performing any necessary conversions based on field type
    public static void setSFFieldValue(SObject newObj, String fieldName, String value, Boolean useExternalIdForLookup){  
        try {
            String objName = String.valueOf(newObj.getSObjectType());

            // Don't proceed if we're dealing with a new object but the current field does not
            // allow setting on inserts.  We exclude Id from this check because Id always comes 
            // back as non-createable.
            if (newObj.Id == null && !canCreateField(objName, fieldName) && fieldName != 'Id') return;
            
            // Don't proceed if we're dealing with an existing object but the current field does
            // not allow setting on updates. We exclude Id from this check because Id always comes 
            // back as non-updateable.
            if (newObj.Id != null && !canUpdateField(objName, fieldName) && fieldName != 'Id') return;

            if (fieldName == 'Id' && String.isBlank(value)) return;

            String fieldType = getFieldType(objName,fieldName); 
            if(fieldType != null) fieldType = fieldType.toLowerCase();
            
            System.debug(objName + '-' + fieldName + ' ' + fieldType + '=' + value);

            value = value.trim();
            
            // if lookup field but the incoming value is not an id, then assume it's an external id
            if (fieldType == 'reference') {
                if (String.isNotBlank(value) && useExternalIdForLookup) {
                    // For Owner field, we need to check if it's a standard type.  If not, we won't populate it
                    // to let the system assign a default value;
                    if (fieldName == 'OwnerId') {
                        List<User> users = [SELECT Id FROM User WHERE Email = :value AND UserType = 'Standard'];
                        if (users.isEmpty()) return;
                    }

                    String parentRelName = getParentRelationshipName(objName, fieldName);
                    CSM_Object_Mapping__c pom = getParentObjectMapping(objName, fieldName);
                    String parentObjectName = pom.Salesforce_Object_Name__c;
                    
                    SObject parentObj = getSObjectWithName(parentObjectName);

                    parentObj.put(pom.Salesforce_External_ID_Field_Name__c, value);
                    newObj.putSObject(parentRelName, parentObj);
                // We won't set the field if it's blank and it's a user system field to let the 
                // system assign a default value.
                } else if (String.isNotBlank(value) || !CSMConstants.SYSTEM_USER_FIELDS.contains(fieldName)){
                    // For Owner field, we need to check if it's a standard type. If not, we won't populate it
                    // to let the system assign a default value.
                    if (fieldName == 'OwnerId') {
                        Id ownerId = (Id)value;
                        List<User> users = [SELECT Id FROM User WHERE Id = :ownerId AND UserType = 'Standard'];
                        if (users.isEmpty()) return;
                    }

                    newObj.put(fieldName, value);
                }
            } else if(fieldType == 'string' || fieldType == 'email'|| fieldType == 'url'|| fieldType == 'textarea' || fieldType == 'phone'|| fieldType == 'multipicklist'|| fieldType == 'combobox'|| fieldType == 'base64' || fieldType=='picklist'){
                // truncate to max field length
                newObj.put(fieldName, value.left(getFieldLength(objName, fieldName)));
            } else if(fieldType == 'boolean' ){
                newObj.put(fieldName, Boolean.valueOf(value));
            } else if(fieldType == 'date' ){
                Date d = null;
                if (!String.isBlank(value)) {
                    // CSM date comes over in the format: M/d/yyyy but in case the SF date field
                    // is mapped to a CSM datetime field, we'll parse the whole thing and convert
                    // to local.
                    String[] tokens = value.split(' ');
                    String[] dateTokens = tokens[0].split('/');
                    if (tokens.size() == 1) {
                        d = Date.newInstance(Integer.valueOf(dateTokens[2]), Integer.valueOf(dateTokens[0]),
                            Integer.valueOf(dateTokens[1]));
                    } else {
                        String[] timeTokens = tokens[1].split(':');
                        // convert to military time if past 12PM (noon)
                        if (tokens[2] == 'pm' && timeTokens[0] != '12') {
                            timeTokens[0] = String.valueOf(Integer.valueOf(timeTokens[0]) + 12);
                        } else if (tokens[2] == 'am' && timeTokens[0] == '12') {
                            timeTokens[0] = '0';
                        }

                        Datetime dt = Datetime.valueOfGmt(String.format('{0}-{1}-{2} {3}:{4}:00',
                            new String[]{ dateTokens[2], dateTokens[0], dateTokens[1],
                                timeTokens[0], timeTokens[1]})); 
                        d = dt.date();
                    } 
                    
                }

                newObj.put(fieldName, d); 
            } else if(fieldType == 'datetime'){
                Datetime dt = null;
                if (!String.isBlank(value)) {
                    // CSM datetime comes over in the CSM server's timezone in the format: M/d/yyyy h:mm a
                    // But in case SF datetime field is mapped to a CSM date only field,
                    // we'll assume 00:00:00 time.
                    String[] tokens = value.toLowerCase().split(' ');
                    String[] dateTokens = tokens[0].split('/');
                    if (tokens.size() == 1) {
                        dt = Datetime.valueOfGmt(String.format('{0}-{1}-{2} 00:00:00',
                            new String[]{ dateTokens[2], dateTokens[0], dateTokens[1]}));
                    } else {
                        String[] timeTokens = tokens[1].split(':');
                        // convert to military time if past 12PM (noon)
                        if (tokens[2] == 'pm' && timeTokens[0] != '12') {
                            timeTokens[0] = String.valueOf(Integer.valueOf(timeTokens[0]) + 12);
                        } else if (tokens[2] == 'am' && timeTokens[0] == '12') {
                            timeTokens[0] = '0';
                        }

                        Datetime csmDateTime = Datetime.valueOfGmt(String.format('{0}-{1}-{2} {3}:{4}:00',
                            new String[]{ dateTokens[2], dateTokens[0], dateTokens[1],
                                timeTokens[0], timeTokens[1]}) );
                        //Datetime csmDateTime = Datetime.newInstance(Integer.valueOf(dateTokens[2]),
                        //    Integer.valueOf(dateTokens[0]), Integer.valueOf(dateTokens[1]),
                        //    Integer.valueOf(timeTokens[0]), Integer.valueOf(timeTokens[1]), 0);
                        TimeZone tz = TimeZone.getTimeZone(CSMAPIWrapper.csmTimezone);

                        System.debug(value);
                        System.debug(csmDateTime);
                        System.debug(tz.getOffset(csmDateTime));

                        dt = csmDateTime.addSeconds(-1 * (tz.getOffset(csmDateTime)/1000));
                    }
                }

                newObj.put(fieldName, dt);

            } else if(fieldType == 'double'  || fieldType == 'percent' || fieldType == 'currency'  ){
                value = value.replace(',','').replace(' ','');
                try{
                    newObj.put(fieldName,Double.valueOf(value)); 
                } catch(Exception ex){
                    try{
                        newObj.put(fieldName,Decimal.valueOf(value));
                    } catch(Exception ex2){
                        newObj.put(fieldName,Integer.valueOf(value));
                    }
                }
            } else if(fieldType == 'int' || fieldtype == 'integer' ){
                value = value.replace(',','').replace(' ','');
                newObj.put(fieldName,Integer.valueOf(value)); 
            } else if(fieldType == 'id'  ){
                newObj.put(fieldName,Id.valueOf(value));
            }
        } catch(Exception e) {
            throw new CSMFieldMappingHelperException('Problem parsing field: ' + fieldName + ', value: ' + value + '\r\n' + e.getMessage(), e);
        }
    }

    public static String setCSMFieldValue(sObject obj, String sfFieldName, Boolean useExternalIdForLookup){
        if (sfFieldName==null) return '';
        String objType = String.valueOf(obj.getSObjectType());
        String sfFieldType = getFieldType(objType, sfFieldName);
        
        System.debug('DEBUG: type + obj + fieldname' + sfFieldType+' '+obj+' '+sfFieldName);
        if(sfFieldType != null) sfFieldType = sfFieldType.toLowerCase();
        String csmFieldValue;

        if (obj.get(sfFieldName) == null) return '';
                
        if(sfFieldType == 'date'){
            Date d = Date.valueOf(obj.get(sfFieldName));
            csmFieldValue = String.valueOf(Integer.valueOf(d.month())+'/'+Integer.valueOf(d.day())+'/'+Integer.valueOf(d.year()));
        } else if(sfFieldType == 'datetime'){
            // CSM expects date to be in their server's timezone.
            Datetime dt = Datetime.valueof(obj.get(sfFieldName));
            csmFieldValue = dt.format('M/d/yyyy h:mm a', CSMAPIWrapper.csmTimezone);
        } else if (sfFieldType == 'reference' && useExternalIdForLookup) {
            String sfValue = String.valueOf(obj.get(sfFieldName));
            if (String.isBlank(sfValue)) {
                csmFieldValue = '';
            } else {
                String parentRelName = getParentRelationshipName(objType, sfFieldName);
                SObject parentObj = obj.getSObject(parentRelName);
                CSM_Object_Mapping__c pom = getParentObjectMapping(objType, sfFieldName);
                String parentObjectName = pom.Salesforce_Object_Name__c;

                csmFieldValue = String.valueOf(parentObj.get(pom.Salesforce_External_Id_Field_Name__c));

                if (String.isBlank(csmFieldValue)) {
                    String parentObjSyncStatus = String.valueOf(parentObj.get('CSM_Sync_Status__c'));
                    if (parentObjSyncStatus == CSMConstants.SYNC_STATUS_SYNCHRONIZED) {
                        throw new CSMRequiresAutoRetryException('The parent record ' + objType + '.' + sfFieldName + ' = ' + sfValue + 
                            ' is awaiting an inbound sync for the CSM Record Id.  Putting sync entry back in the queue.');
                    } else {
                        throw new CSMFieldMappingHelperException('The parent record ' + objType + '.' + sfFieldName + ' = ' + sfValue + 
                            ' is not yet syncd to CSM.  Please sync the parent record first then retry syncing the child.');
                    }
                }
            }
        } else{
            csmFieldValue = String.valueOf(obj.get(sfFieldName));
        }
        System.debug('DEBUG: ' + csmFieldValue);
        return csmFieldValue;
    }
}