@isTest

public class SignaturePackageFlowControllerTest {
    
    @testSetup static void setup(){
        /*Signature_Package_Definitions__c spd = new Signature_Package_Definitions__c();
        spd.OC_Signer1_Assignment__c = 'Dynamic';
        spd.OC_Signer2_Assignment__c = 'Fixed';
        spd.Total_Contract_Value_Threshold__c = 250000;
        spd.Professional_Services_Total_Threshold__c = 250000;
        spd.OC_Signer2_Email__c = 'tester@cherwell.test';
        spd.OC_Signer2_Name__c = 'Tester Test';
        spd.SOW_Signer1_Assignment__c = 'Fixed';
        spd.SOW_Signer1_Email__c = 'tester+pso@cherwell.test';
        
        insert spd;*/
        
        List<Signature_Routing_Rule__c> routingrules = new List<Signature_Routing_Rule__c>();
        Signature_Routing_Rule__c rule1 = new Signature_Routing_Rule__c(Name='OC > 250k', Object_API_Name__c = 'SBQQ__Quote__c', Document__c = 'Order Confirmation',
                                                                        Conditions_Met__c = 'All', Routing_Type__c = 'Static', Signer_Name__c = 'Test Signer', Active__c = true,
                                                                        Signer_Email__c = 'test@test.com', Signer_Title__c = 'Master Tester');
        routingrules.add(rule1);
        Signature_Routing_Rule__c rule2 = new Signature_Routing_Rule__c(Name='OC > 250k', Object_API_Name__c = 'SBQQ__Quote__c', Document__c = 'Order Confirmation',
                                                                        Conditions_Met__c = 'All', Routing_Type__c = 'Dynamic', Active__c = true,
                                                                        Dynamic_Signer_Id_Field_API_Name__c = 'SBQQ__SalesRep__r.ManagerId');
        routingrules.add(rule2);
        Signature_Routing_Rule__c rule3 = new Signature_Routing_Rule__c(Name='PS SOW', Object_API_Name__c = 'SBQQ__Quote__c', Document__c = 'Professional Services SOW',
                                                                        Conditions_Met__c = 'All', Routing_Type__c = 'Static', Active__c = true,
                                                                        Signer_Name__c = 'Test PSO', Signer_Email__c = 'psotest@test.com', Signer_Title__c = 'Master Signer');
        routingrules.add(rule3);
        Signature_Routing_Rule__c rule4 = new Signature_Routing_Rule__c(Name='LS SOW', Object_API_Name__c = 'SBQQ__Quote__c', Document__c = 'Learning Services SOW',
                                                                        Conditions_Met__c = 'All', Routing_Type__c = 'Static', Active__c = true,
                                                                        Signer_Name__c = 'Test LS', Signer_Email__c = 'lstest@test.com', Signer_Title__c = 'Master Trainer');
        routingrules.add(rule4);
        insert routingrules;
        
        List<Signature_Routing_Condition__c> conditions = new List<Signature_Routing_Condition__c>();
        Signature_Routing_Condition__c con1 = new Signature_Routing_Condition__c(Index__c = 10, Tested_Field_API_Name__c = 'SBQQ__Opportunity2__r.Total_Contract_Value__c', 
                                                                                 Condition__c = 'less or equal to', Value__c = '250000', Signature_Routing_Rule__c = rule1.Id); 
        conditions.add(con1);
        Signature_Routing_Condition__c con2 = new Signature_Routing_Condition__c(Index__c = 10, Tested_Field_API_Name__c = 'SBQQ__Opportunity2__r.Total_Contract_Value__c', 
                                                                                 Condition__c = 'greater than', Value__c = '250000', Signature_Routing_Rule__c = rule2.Id); 
        conditions.add(con2); 
        Signature_Routing_Condition__c con3 = new Signature_Routing_Condition__c(Index__c = 10, Tested_Field_API_Name__c = 'SBQQ__Opportunity2__r.PSO_Total_Amount__c', 
                                                                                 Condition__c = 'greater than', Value__c = '0', Signature_Routing_Rule__c = rule3.Id); 
        conditions.add(con3); 
        Signature_Routing_Condition__c con4 = new Signature_Routing_Condition__c(Index__c = 10, Tested_Field_API_Name__c = 'SBQQ__Opportunity2__r.PSO_Total_Amount__c', 
                                                                                 Condition__c = 'greater than', Value__c = '0', Signature_Routing_Rule__c = rule4.Id); 
        conditions.add(con4); 
        insert conditions;
        
        List<Ironclad_Workflow_Paths__c> iwps = new List<Ironclad_Workflow_Paths__c>();
        Ironclad_Workflow_Paths__c iwp1 = new Ironclad_Workflow_Paths__c(Name = 'ocOnly', Workflow_URL__c = 'https://test.testclass');
        iwps.add(iwp1);
        Ironclad_Workflow_Paths__c iwp2 = new Ironclad_Workflow_Paths__c(Name = 'fullPkg', Workflow_URL__c = 'https://test.testfullclass');
        iwps.add(iwp2);
        
        insert iwps;
        
               
        User m = new User();
        m.LastName = 'Manager';
        m.Alias = 'mtest';
        m.CommunityNickname = 'manager';
        m.Email = 'testmanager@cherwell.test';
        m.Username = 'testmanager@cherwell.test';
        m.TimeZoneSidKey = 'America/Los_Angeles';
    	m.EmailEncodingKey = 'UTF-8';
     	m.LanguageLocaleKey = 'en_US';
     	m.LocaleSidKey = 'en_US';
        m.ProfileId = UserInfo.getProfileId();
        insert m;
        
        User u = new User();
        u.LastName = 'User';
        u.Alias = 'utest';
        u.CommunityNickname = 'user';
        u.Email = 'testuser@cherwell.test';
        u.Username = 'testuser@cherwell.test';
        u.ManagerId = m.Id;
        u.TimeZoneSidKey = 'America/Los_Angeles';
    	u.EmailEncodingKey = 'UTF-8';
     	u.LanguageLocaleKey = 'en_US';
     	u.LocaleSidKey = 'en_US';
        u.ProfileId = UserInfo.getProfileId();
        insert u;
        
        Account a = new Account();
        a.Name = 'test account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Total_Contract_Value__c = 100000;
        //o.TCV__c = 100000;
        o.PSO_Total_Amount__c = 0;
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discovery';
        o.Forecast_Category__c = 'Pipeline';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__SalesRep__c = u.Id;
        q.Billing_Contact__c = c.Id;
        q.License_Key_Contact__c = c.Id;
        q.PO_Required__c = 'No';
        q.Tax_Exempt__c = 'No';
        q.Term__c = '3';
        q.SBQQ__BillingCity__c = 'Colorado Springs';
        q.SBQQ__BillingCountry__c = 'United States';
        q.SBQQ__BillingStreet__c = '123 Test Street';
        q.SBQQ__BillingPostalCode__c = '12345';
        q.SBQQ__BillingState__c = 'Colorado';
        q.SBQQ__BillingName__c = 'Test Account';
        q.Regional_Quote_Terms__c = 'North America';
        insert q;
        
        Document d = new Document();
        d.Name = 'testdoc';
        d.Body = Blob.valueOf('testbody');
        d.AuthorId = UserInfo.getUserId();
        d.FolderId = UserInfo.getUserId();
        insert d;
        
        SBQQ__QuoteDocument__c qd = new SBQQ__QuoteDocument__c();
        qd.Name = 'testquote';
        qd.SBQQ__Quote__c = q.Id;
        qd.SBQQ__Opportunity__c = o.Id;
        qd.SBQQ__DocumentId__c = d.Id;
        insert qd;
    }
  
    public static testmethod void testSuccess() {
        
        SBQQ__Quote__c q = [Select Id, CreatedDate From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = true;
        q.Convert_to_Order_Confirm__c = true;
        q.Convert_to_Order_Confirmation_Timestamp__c = System.now().addDays(-1);
        System.debug('Quote created ----'+ q.CreatedDate + '----- timestamp ----' + q.Convert_to_Order_Confirmation_Timestamp__c);
        update q;
        
        Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        SignaturePackageFlowController spfc = new SignaturePackageFlowController(sc);
        
        PageReference pageRef = Page.SignaturePackageFlow;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        pageRef.getParameters().put('flow', 'ocOnly');
        Test.setCurrentPage(pageRef);
       
        spfc.createSignPkg();
        Test.stopTest();
    }

    public static testmethod void testNotPrimary() {
        
        SBQQ__Quote__c q = [Select Id From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = false;
        q.Convert_to_Order_Confirm__c = true;
        update q;
        
        Test.StartTest(); 
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        SignaturePackageFlowController spfc = new SignaturePackageFlowController(sc);
        
        PageReference pageRef = Page.SignaturePackageFlow;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        pageRef.getParameters().put('flow', 'ocOnly');
        Test.setCurrentPage(pageRef);
        
        spfc.createSignPkg();
        Test.StopTest();    
    }

     public static testmethod void testNotOC() {
        
        SBQQ__Quote__c q = [Select Id From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = true;
        q.Convert_to_Order_Confirm__c = false;
        update q;
        
        Test.StartTest(); 
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        SignaturePackageFlowController spfc = new SignaturePackageFlowController(sc);
        
        PageReference pageRef = Page.SignaturePackageFlow;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        pageRef.getParameters().put('flow', 'ocOnly');
        Test.setCurrentPage(pageRef);
        
        spfc.createSignPkg();
        Test.StopTest();    
    }
    
     public static testmethod void testOverThreshold() {
        
        SBQQ__Quote__c q = [Select Id From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = true;
        q.Convert_to_Order_Confirm__c = true;
        update q;
         
        Opportunity o = [Select Id From Opportunity Limit 1];
        o.Total_Contract_Value__c = 1500000;
        //o.TCV__c = 1500000;
        update o;
        
        Test.StartTest(); 
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        SignaturePackageFlowController spfc = new SignaturePackageFlowController(sc);
        
        PageReference pageRef = Page.SignaturePackageFlow;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        pageRef.getParameters().put('flow', 'ocOnly');
        Test.setCurrentPage(pageRef);
        
        spfc.createSignPkg();
        Test.StopTest();    
    }
    
    public static testmethod void testFullPackage() {
        Product2 p = new Product2();
        p.Name = 'Cherwell GO!';
        p.Quote_Group_Name__c = 'Professional Services';
        insert p;
        
        SBQQ__Quote__c q = [Select Id From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = true;
        q.Convert_to_Order_Confirm__c = true;
        //q.Convert_to_Order_Confirmation_Timestamp__c = System.now().addDays(-2);
        update q;
       
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = p.Id;
        ql.SBQQ__Quantity__c = 1;
        insert ql;
        
        Opportunity o = [Select Id From Opportunity Limit 1];
        o.PSO_Total_Amount__c = 150000;
        update o;
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'SOW_Final';
        cv.PathOnClient = 'SOW.pdf';
        cv.VersionData = Blob.valueOf('testcontent');
        cv.IsMajorVersion = true;
        insert cv;
        
        List<ContentDocument> documents = [Select Id, Title
                                          From ContentDocument];
        
        ContentDocumentLink cdl1 = new ContentDocumentLink();
        cdl1.ContentDocumentId = documents[0].id;
        cdl1.LinkedEntityId = o.Id;
        cdl1.ShareType = 'V';
        insert cdl1;
        
        q.Convert_to_Order_Confirmation_Timestamp__c = System.now().addDays(-2);
        update q;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        SignaturePackageFlowController spfc = new SignaturePackageFlowController(sc);
        
        PageReference pageRef = Page.SignaturePackageFlow;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        pageRef.getParameters().put('flow', 'fullPkg');
        pageRef.getParameters().put('withDocs', String.valueOf(true));
        Test.setCurrentPage(pageRef);
        spfc.createSignPkg();
        Test.StopTest(); 
        
    }
    
    public static testmethod void testLSSOW() {
        
        Product2 p2 = new Product2();
        p2.Name = 'PS Configuration & Implementation - Hourly';
        p2.Quote_Group_Name__c = 'Professional Services';
        insert p2;
        
        SBQQ__Quote__c q = [Select Id From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = true;
        q.Convert_to_Order_Confirm__c = true;
        //q.Convert_to_Order_Confirmation_Timestamp__c = System.now().addDays(-2);
        update q;
       
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = p2.Id;
        ql.SBQQ__Quantity__c = 1;
        insert ql;
               
        Opportunity o = [Select Id From Opportunity Limit 1];
        o.PSO_Total_Amount__c = 150000;
        update o;
               
        ContentVersion cv2 = new ContentVersion();
        cv2.Title = 'Training_SOW_Final';
        cv2.PathOnClient = 'SOW.pdf';
        cv2.VersionData = Blob.valueOf('testcontent');
        cv2.IsMajorVersion = true;
        insert cv2;
        
        List<ContentDocument> documents2 = [Select Id, Title
                                          From ContentDocument 
                                          Order By CreatedDate DESC];
        
        ContentDocumentLink cdl2 = new ContentDocumentLink();
        cdl2.ContentDocumentId = documents2[0].id;
        cdl2.LinkedEntityId = o.Id;
        cdl2.ShareType = 'V';
        insert cdl2;
        
        q.Convert_to_Order_Confirmation_Timestamp__c = System.now().addDays(-2);
        update q;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        SignaturePackageFlowController spfc = new SignaturePackageFlowController(sc);
        
        PageReference pageRef = Page.SignaturePackageFlow;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        pageRef.getParameters().put('flow', 'fullPkg');
        pageRef.getParameters().put('withDocs', String.valueOf(true));
        Test.setCurrentPage(pageRef);
        spfc.createSignPkg();
        Test.StopTest(); 
        
    }
}