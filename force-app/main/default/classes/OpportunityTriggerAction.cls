/*
TEST CLASS = OpportunityTriggerTest
*/
public Class OpportunityTriggerAction{
    
    /*VERIFY THAT THERE IS ATLEAST ONE OPPORTUNITY CONTACT ROLE ON OPPORTUNITY
    ELSE THROW ERROR*/
    public void validateOpportunityContactRoles(Map<Id,Opportunity> newMap){
        
        for(Opportunity oppObj : [Select id, (Select Id, OpportunityId, ContactId, isPrimary From OpportunityContactRoles) from Opportunity where id in :newMap.keySet() ]){
            
            List<OpportunityContactRole> oppContactRole = oppObj.OpportunityContactRoles;
            System.debug('#Opp > ' + oppContactRole);
            
            if(oppContactRole == null || oppContactRole.isEmpty())
                newMap.get(oppObj.id).addError('Opportuntiy Must Have a Contact Role');
        }
        
    }//End Of method FindOpportunityContactRoles()
    
}