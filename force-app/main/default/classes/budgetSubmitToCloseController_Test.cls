@isTest
public class budgetSubmitToCloseController_Test{

 Public static testmethod void budgetSubmitToClose()
 {
   Test.StartTest();
    Account acc = new Account (Name ='testAc1');
        insert acc;
    Contact con = new Contact(lastname = 'conTest1', AccountID = acc.id) ;
        insert con ;    
     pse__Proj__c Project = new pse__Proj__c(pse__Account__c = acc.id,name='test',pse__Project_Manager__c= con.id,pse__Is_Active__c = true,
                                             pse__Start_Date__c=system.today()+1,pse__End_Date__c = system.today()+30); 
        Insert Project;  
     pse__Budget__c Budget = new pse__Budget__c(name='test Budget',pse__Project__c=Project.id,  pse__Type__c = 'PNB',
                                                Order_Confirmation_Number__c = 'PNB',pse__Status__c = 'Approved',pse__Effective_Date__c = system.today()+15,
                                                Budgeted_Hours__c = 10,pse__Amount__c =12.00,TimecardOpen__c = 2); 
        Insert Budget;  
        System.debug(Budget);
              
     ApexPages.StandardController sc = new ApexPages.StandardController(Budget);
     budgetSubmitToCloseController testbudgetPlan = new budgetSubmitToCloseController(sc);   
     
      PageReference pageRef = Page.budgetSubmitToClose;
      Test.setCurrentPage(pageRef);      
      testbudgetPlan.doupdate();  
       Test.StopTest();                      
 }
  Public static testmethod void budgetSubmitToClose1()
 {
   Test.StartTest();
    Account acc = new Account (Name ='testAc1');
        insert acc;
    Contact con = new Contact(lastname = 'conTest1', AccountID = acc.id) ;
        insert con ;    
     pse__Proj__c Project = new pse__Proj__c(pse__Account__c = acc.id,name='test',pse__Project_Manager__c= con.id,pse__Is_Active__c = true,
                                             pse__Start_Date__c=system.today()+1,pse__End_Date__c = system.today()+30); 
        Insert Project;  
     pse__Budget__c Budget = new pse__Budget__c(name='test Budget',pse__Project__c=Project.id,  pse__Type__c = 'PNB',
                                                Order_Confirmation_Number__c = 'PNB',pse__Status__c = 'Approved',pse__Effective_Date__c = system.today()+15,
                                                Budgeted_Hours__c = 10,pse__Amount__c =12.00,TimecardOpen__c = 0); 
        Insert Budget;  
        System.debug(Budget);
              
     ApexPages.StandardController sc = new ApexPages.StandardController(Budget);
     budgetSubmitToCloseController testbudgetPlan = new budgetSubmitToCloseController(sc);   
     
      PageReference pageRef = Page.budgetSubmitToClose;
      Test.setCurrentPage(pageRef);      
      testbudgetPlan.doupdate();  
       Test.StopTest();                      
 }
 }