@isTest

public class ContractTriggerTest {
    
    public static testmethod void insertContract(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contract c = new Contract();
        c.AccountId = a.Id;
        c.StartDate = Date.today();
        c.ContractTerm = 12;
        c.Status = 'Draft';
        insert c;
        
        Product2 p = new Product2();
        p.Name = 'CSM Subscription License';
        insert p;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = p.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = c.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        insert s; 
        
        c.Maintenance_End_Date__c = Date.today().addDays(365);
        c.Maintenance_Start_Date__c = Date.today().addDays(1);
        update c;
    }

}