// Structure to hold the response data from a business object record read request.  Also encapsulates specific
// parsing logic.
public class CSMBusinessObjectRecordWrapper
{
	public class FieldWrapper {
		public Boolean dirty;
		public String displayName;
		public String fieldId;
		public String name;
		public String value;
	}

	public class LinkWrapper {
		public String name;
		public String url;
	}

	public String busObId;
	public String busObPublicId;
	public String busObRecId;
	public List<FieldWrapper> fields; 
	public List<LinkWrapper> links;
	public String errorCode;
	public String errorMessage;
	public Boolean hasError;

	public class CSMBusinessObjectRecordWrapperException extends Exception{}

	public static CSMBusinessObjectRecordWrapper parseJSON(String jsonStr) {
		CSMBusinessObjectRecordWrapper borw = (CSMBusinessObjectRecordWrapper)JSON.deserialize(jsonStr, CSMBusinessObjectRecordWrapper.class);

		// Remove fields that are not mapped to reduce the size of the object
		Set<String> mappedFieldIds = CSMFieldMappingHelper.getAllFieldMappingsByCSMFieldId().keySet();
		List<FieldWrapper> fieldsToRemove = new List<FieldWrapper>();
		for(FieldWrapper f : borw.fields) {
			if (!mappedFieldIds.contains(f.fieldId)) fieldsToRemove.add(f);
		}

		Set<FieldWrapper> fieldSet = new Set<FieldWrapper>(borw.fields);
		fieldSet.removeAll(fieldsToRemove);

		borw.fields = new List<FieldWrapper>(fieldSet);

		return borw;
	}

	public static String getRequestBody(SObject obj) {
		CSMBusinessObjectRecordWrapper borw = transformToCSM(obj);
		String borwJSON = JSON.serialize(borw);
        
		// Put any special transformations here like changing property names and
		// formatting date values, if needed.
		
		return borwJSON;
	}

	public static CSMBusinessObjectRecordWrapper transformToCSM(SObject obj) {
		// map SObj to CSM here using field mapping in custom settings
		String objType = String.valueOf(obj.getSObjectType());
		CSM_Object_Mapping__c objMap = CSMFieldMappingHelper.getAllObjectMappingsBySObjectName().get(objType);
		String objId = objMap.CSM_Business_Object_Id__c;
		String objRecIdField = objMap.Salesforce_External_ID_Field_Name__c;
		
		Map<String, List<CSM_Field_Mapping__c>> fieldMap = CSMFieldMappingHelper.getAllFieldMappingsBySObjectName();
		//Debugs
		System.debug('DEBUG: objId ' + objId);
		System.debug('DEBUG: sobjectType ' + objType);
		System.debug('DEBUG: fieldMap ' + fieldMap);

		if(fieldMap.containsKey(objType)){
			CSMBusinessObjectRecordWrapper cmsborw = new CSMBusinessObjectRecordWrapper();
			List<FieldWrapper> cmsborwFields = new List<FieldWrapper>();
			cmsborw.busObId = objId;
			if(obj.get(objRecIdField)!=null){
				cmsborw.busObRecId = String.valueOf(obj.get(objRecIdField));
			}
			List<CSM_Field_Mapping__c> fieldList = fieldMap.get(objType);
			for(CSM_Field_Mapping__c f : fieldList){
				if (!f.Outbound__c) continue;

				FieldWrapper fw = new FieldWrapper();
				fw.dirty = true;
				fw.fieldId = f.CSM_Field_Id__c;
				fw.name = f.CSM_Field_Name__c;

				fw.value = CSMFieldMappingHelper.setCSMFieldValue(obj, f.Salesforce_Field_Name__c, 
					f.Look_up_using_External_Id__c);
				
				System.debug('DEBUG: cmsborw.fields ' + cmsborwFields);
				System.debug('DEBUG: fw ' + fw);
				cmsborwFields.add(fw);
			}
			cmsborw.fields = cmsborwFields;

			// Do additional transformation logic here that the generic mapping logic cannot handle.
			if (String.isNotBlank(objMap.Custom_Transformation_Class__c)) {
				Type t = Type.forName(objMap.Custom_Transformation_Class__c);
				ICSMCustomTransformationHandler cth = (ICSMCustomTransformationHandler)t.newInstance();
				cmsborw = cth.doAdditionalTransformFromSFtoCSM(obj, cmsborw);
			}

			System.debug('DEBUG: cmsborw ' + cmsborw);
			return cmsborw;
		}
		return null;
	}

	public SObject transformToSObject() {
		// map CSM to SObj here using field mapping in custom settings
		CSM_Object_Mapping__c om = CSMFieldMappingHelper.getAllObjectMappingsByCSMObjectId().get(busObId);
		SObject obj = CSMFieldMappingHelper.getSObjectWithName(om.Salesforce_Object_Name__c);

		// Resolve Salesforce Id first so we know if we're creating or updating.  setSFFieldValue
		// looks at createable vs updateable access on the field and will need this info to know
		// when to check for either.
		String qualifiedSalesforceIdFieldName = om.Salesforce_Object_Name__c + '.Id';
		CSM_Field_Mapping__c idMap = 
			CSMFieldMappingHelper.getAllFieldMappingsBySFQualifiedFieldName().get(qualifiedSalesforceIdFieldName)[0];

		// Retrieve CSM value mapped to Salesforce Id
		String csmSalesforceIdValue = '';
		for(FieldWrapper f : fields) {
			if (f.fieldId == idMap.CSM_Field_Id__c) {
				CSMFieldMappingHelper.setSFFieldValue(obj, idMap.Salesforce_Field_Name__c, f.value, false);
				break;
			}
		}

		// resolve Salesforce Id using CSM Record Id if Salesforce Id is null
		if (obj.Id == null && String.isNotBlank(busObRecId)) {
			String query = 'SELECT Id FROM ' + om.Salesforce_Object_Name__c + ' WHERE ' + om.Salesforce_External_Id_Field_Name__c + ' = :busObRecId';
			List<SObject> existingObjs = Database.query(query);

			if (existingObjs.size() == 1) obj.Id = existingObjs[0].Id;
			else if (existingObjs.size() > 1) {
				throw new CSMBusinessObjectRecordWrapperException(String.format('Found {0} records with CSM Record Id = {1}',
					new String[] { String.valueOf(existingObjs.size()), busObRecId }));
			}
		}

		// Now, proceed with the mapping, but skip Id since we've already mapped it.
		Map<String, List<CSM_Field_Mapping__c>> fieldMaps = CSMFieldMappingHelper.getAllFieldMappingsByCSMFieldId();
		for(FieldWrapper f : fields) {
			if(fieldMaps.containsKey(f.fieldId)) {
				for(CSM_Field_Mapping__c fm : fieldMaps.get(f.fieldId)) {
					if (String.isNotBlank(fm.Salesforce_Field_Name__c) && 
						fm.Salesforce_Object_Name__c + '.' + fm.Salesforce_Field_Name__c != qualifiedSalesforceIdFieldName &&
						fm.Inbound__c) {

						CSMFieldMappingHelper.setSFFieldValue(obj, fm.Salesforce_Field_Name__c, f.value, fm.Look_up_using_External_Id__c);
					}
				}				
			}
		}

		// Do additional transformation logic here that the generic mapping logic cannot handle.
		if (String.isNotBlank(om.Custom_Transformation_Class__c)) {
			Type t = Type.forName(om.Custom_Transformation_Class__c);
			ICSMCustomTransformationHandler cth = (ICSMCustomTransformationHandler)t.newInstance();
			obj = cth.doAdditionalTransformFromCSMtoSF(this, obj);
		}

		return obj;
	}

}