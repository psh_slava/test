trigger LimitDiscoverOrgLeads on Lead (before insert) {
   
    //Define dailyLimit variable and set default to 100
    Decimal dailyLimit = 100;
    
    Set<Id> ownerIds = new set<Id>();
    map<Id,Integer> leadCountMap = new map<Id,Integer>();
    
    //Use custom setting to set limits
    List<DiscoverOrg_Contacts_Limit__c> batchSettings = [Select Daily_Limit__c
                                                         From DiscoverOrg_Contacts_Limit__c
                                                         Limit 1];
    if(!batchSettings.isEmpty()){
        dailyLimit = batchSettings[0].Daily_Limit__c;
    }
    
    //Set up email message to send out to user at daily limit of contacts
    OrgWideEmailAddress owa = [Select Id, DisplayName, Address From OrgWideEmailAddress Where Address = 'sfdchelp@cherwell.com' Limit 1];
    EmailTemplate templateId = [Select Id From EmailTemplate Where Name = 'DiscoverOrg Daily Import Limit Reached_Leads'];
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    Messaging.SingleEmailMessage userNotif = new Messaging.SingleEmailMessage();
    userNotif.setTemplateId(templateId.Id);
    userNotif.setSaveAsActivity(False);
    userNotif.setOrgWideEmailAddressId(owa.Id);
    userNotif.setTargetObjectId(Userinfo.getUserId());
    List<String> toAddresses = new List<String>();
    toAddresses.add(Userinfo.getUserEmail());
    userNotif.setToAddresses(toAddresses);
    
    System.debug('Trigger size - - '+trigger.new.size());
    
    for(Lead l : trigger.new){
            ownerIds.add(l.OwnerId);
            leadCountMap.put(l.OwnerId,0);
    }
    
    Map<Id,User> userMap = new Map<Id,User>([Select Name From User Where Id in :ownerIds]);
     
    AggregateResult[] results = [Select CreatedById, COUNT(Id)
                                      From Lead
                                     Where CreatedDate = TODAY
                                       And CreatedById in :ownerIds
                                       And DSCORGPKG__DiscoverOrg_Created_On__c != null
                                  Group By CreatedById];
    System.debug(results);
    for(AggregateResult ar : results){
        leadCountMap.put((Id)ar.get('CreatedById'),(Integer)ar.get('expr0'));
        System.debug('Result------'+ar);
    }
    
    for(Lead l : trigger.new){
        if(l.DSCORGPKG__DiscoverOrg_Created_On__c != null){
            leadCountMap.put(l.OwnerId, leadCountMap.get(l.OwnerId)+1);
            System.debug(leadCountMap.get(l.OwnerId));
            if(leadCountMap.get(l.OwnerId)==dailyLimit){
                emails.add(userNotif);
            }
            if(leadCountMap.get(l.OwnerId) > dailyLimit){
                l.addError('Error: You can only create '+ dailyLimit + ' Leads per day from DiscoverOrg. Thank you for cooperating!');
            }
        }
    }
    System.debug(emails.size());
    Messaging.sendEmail(emails,false);

}