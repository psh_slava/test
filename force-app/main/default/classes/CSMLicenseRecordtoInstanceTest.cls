@isTest

public class CSMLicenseRecordtoInstanceTest {
    
    public static testmethod void testnewkey(){
        
        Account a = new Account(Name = 'Test Account', BillingStreet = '123 Street', BillingCity = 'Cityville');
        a.BillingCountry = 'United States';
        a.BillingState = 'Colorado';
        a.BillingPostalCode = '11111';
        insert a;
        
        Contact c = new Contact(LastName = 'Tester', AccountId = a.Id, Phone = '1111111111');
        insert c;
        
        Opportunity o = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = 'Pre-Qualification', 
                                        AccountId = a.Id, Primary_Contact__c = c.Id);
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c(Quote_Name__c = 'Test Quote', SBQQ__Account__c = a.Id, 
                                              SBQQ__Opportunity2__c = o.Id, SBQQ__StartDate__c = Date.today());
        insert q;
        
        Hosted_Instance__c hi = new Hosted_Instance__c(Instance_Num__c = 'Instance 1', Account__c = a.Id, Product_Type__c = 'CSM', Instance_Environment_Type__c = 'Production',
                                                       Maintenance_Contact__c = 'test@test.test', External_DNS__c = 'test.cherwell.com', Quote__c = q.Id);
        insert hi;
        
        CSM_License_Record__c key = new CSM_License_Record__c(Account__c = a.Id, Type__c = 'Subscription', Hosted__c = true, License_Type__c = 'CSM', Expires__c = Date.today().addDays(30),
                                                             Licensed_Items__c = 20, License_Key__c = 'testCSMKeytoInstance82019', Maintenance__c = 'Instance 1', Purchase_Scenario2__c = 'New Logo',
                                                             License_Name__c = 'TestCo', Deal_Type_Manual__c = 'Direct', Primary_Contact__c = c.Id);
        insert key;
        
        CSM_License_Record__c newKey = [Select Id, Hosted_Instance__c From CSM_License_Record__c Limit 1];
        System.assertEquals(newKey.Hosted_Instance__c, hi.Id);
        
    }

}