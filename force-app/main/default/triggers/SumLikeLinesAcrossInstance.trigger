trigger SumLikeLinesAcrossInstance on Contract (after insert, after update) {
    
    //Only applies to license lines

    Id contractId;
    Map<String,Decimal> lineQuantityMap = new Map<String,Decimal>();
    
    for(Contract c : trigger.new){
        contractId = c.Id;
    }
    
    AggregateResult[] summedLines = [Select SBQQ__Product__c, Instance_Number__c, SUM(SBQQ__Quantity__c)
                                      From SBQQ__Subscription__c
                                      Where SBQQ__Contract__c = :contractId
                                      //And SBQQ__Bundle__c = false
                                      And SBQQ__Product__r.Name LIKE '%License%'
                                      Group By SBQQ__Product__c, Instance_Number__c];
    System.debug('Results-----' + summedLines);
    
    for(AggregateResult ar : summedLines){
        string prod = (String)ar.get('SBQQ__Product__c');
        string inst = (String)ar.get('Instance_Number__c');
        string lineKey = prod + inst;
        lineQuantityMap.put(prod+inst,(Decimal)ar.get('expr0'));
    }
    
    List<SBQQ__Subscription__c> allSubs = [Select Id, SBQQ__Product__c, Instance_Number__c, SBQQ__Quantity__c
                                             From SBQQ__Subscription__c
                                            Where SBQQ__Contract__c = :contractId
                                            //And SBQQ__Bundle__c = false
                                            And SBQQ__Product__r.Name LIKE '%License%'];
    
    List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
    
    for(SBQQ__Subscription__c s : allSubs){
        string sProd = s.SBQQ__Product__c;
        string sInst = s.Instance_Number__c;
        string sKey = sProd + sInst;
        
        lineQuantityMap.put(sKey, lineQuantityMap.get(sKey));
        System.debug(lineQuantityMap.get(sKey));
        
        s.Total_Count_of_Same_Product__c = (Decimal)lineQuantityMap.get(sKey);
        subsToUpdate.add(s);        
    }
    
    update subsToUpdate; 

}