@isTest  (seeAllData = true) 

public class BookingsProcessorTest {  
    
      public static testmethod void testInvoiceCreation(){
        
        Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        
        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2020', Description = 'Price Book 2020 Products', IsActive = true );
        insert pb;
        
        Product2 prod = new Product2(Name = 'CSM Subscription License', SBQQ__SubscriptionTerm__c = 1, IsActive = true);
        insert prod;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=150.00,Product2Id=prod.Id, CurrencyIsoCode = 'USD', Pricebook2Id=Test.getStandardPricebookId(), IsActive= true); 
        insert pbe;     
        
        Account acc = new Account (name='Test Account');
        insert acc;
        
        Contact con = new Contact(AccountId = acc.Id, LastName = 'Tester2', Email = 'test2@test.test');
        insert con;
        
        Opportunity opp = new Opportunity(AccountId = acc.Id, StageName = 'Order Processing', Forecast_Category__c = 'Commit', Close_Reason__c = 'Relationship/Reputation', 
                                          CurrencyIsoCode = 'USD', CloseDate = Date.today(), Name = 'Test Opp', Primary_Contact__c = con.Id, Hosting_Model__c = 'On Premise');
        opp.Pricebook2id=Test.getStandardPricebookId();      
        insert opp;
        
        OpportunityLineItem oppLine = new OpportunityLineItem( pricebookentryid=pbe.Id,TotalPrice=2000, Quantity = 2,Opportunityid = opp.Id);
        insert oppLine;       
        
        Contract cnt = new Contract();
        cnt.StartDate = Date.today();
        cnt.EndDate = Date.today().addYears(3);
        cnt.AccountId = acc.Id;
        cnt.Status = 'Draft';
        insert cnt;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__RenewalQuantity__c = 10;
        s.SBQQ__SubscriptionStartDate__c = Date.today();
        s.SBQQ__ListPrice__c = 1500;
        s.SBQQ__RenewalPrice__c = 135;
        s.SBQQ__Product__c = prod.Id;
        s.SBQQ__Account__c = acc.Id;
        s.SBQQ__Contract__c = cnt.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        s.SBQQ__Bundle__c = true;
        insert s;   
        
        //cnt.Create_Booking__c = false;
        //update cnt;
        
        cnt.Create_Booking__c = true;
        update cnt;
        
    }
    
     public static testmethod void offsetRenewal(){
        Date yearFromNow = Date.today().addDays(365);
        Date yearAgo = Date.today().addDays(-365);
        Date todaysDate = Date.today();
        SBQQ__Subscription__c sub = [Select SBQQ__Contract__c, SBQQ__EndDate__c
                                     From SBQQ__Subscription__c
                                     //Where SBQQ__SubscriptionStartDate__c > :yearAgo
                                     Where Upsell_in_Last_Term_Year__c = true
                                     //And SBQQ__RevisedSubscription__c != null
                                     And SBQQ__EndDate__c < :yearFromNow
                                     And SBQQ__EndDate__c > :todaysDate
                                     AND SBQQ__Contract__r.Status = 'Activated'
                                     AND SBQQ__Contract__r.SBQQ__RenewalOpportunity__c != null
                                     AND SBQQ__Contract__r.SBQQ__RenewalOpportunity__r.IsClosed = false
                                     AND SBQQ__Contract__r.SBQQ__RenewalQuoted__c = true
                                     Limit 1];
        Contract con = [Select Id, SBQQ__RenewalOpportunity__r.Id From Contract 
                        Where Id = :sub.SBQQ__Contract__c];
        //con.SBQQ__RenewalQuoted__c = true;
        //update con;
        List<Id> conIds = new List<Id>();
        conIds.add(con.Id);
        
         List<Id> renewalOppIds = new List<Id>();
         renewalOppIds.add(con.SBQQ__RenewalOpportunity__r.Id);
         System.debug('offsetRenewal test - ' + conIds + renewalOppIds + sub.SBQQ__EndDate__c);
        BookingsProcessor.offsetRenewal(conIds, renewalOppIds, sub.SBQQ__EndDate__c);
    }
    
    public static testmethod void updateOffsetTest(){
        
        Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        
        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2020', Description = 'Price Book 2020 Products', IsActive = true );
        insert pb;
        
        Product2 prod = new Product2(Name = 'CSM Subscription License', SBQQ__SubscriptionTerm__c = 1, IsActive = true);
        insert prod;
        
        PricebookEntry pbe = new PricebookEntry(unitprice=150.00,Product2Id=prod.Id, CurrencyIsoCode = 'USD', Pricebook2Id=Test.getStandardPricebookId(), IsActive= true); 
        insert pbe;     
        
        Account acc = new Account (name='Test Account');
        insert acc;
        
        Contact con = new Contact(AccountId = acc.Id, LastName = 'Tester2', Email = 'test2@test.test');
        insert con; 
        
        Contract cnt = new Contract();
        cnt.StartDate = Date.today().addYears(-1);
        cnt.EndDate = Date.today().addYears(2);
        cnt.AccountId = acc.Id;
        cnt.Status = 'Draft';
        insert cnt;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__RenewalQuantity__c = 10;
        s.SBQQ__SubscriptionStartDate__c = Date.today().addDays(-51);
        s.SBQQ__ListPrice__c = 1500;
        s.SBQQ__RenewalPrice__c = 135;
        s.SBQQ__Product__c = prod.Id;
        s.SBQQ__Account__c = acc.Id;
        s.SBQQ__Contract__c = cnt.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        s.SBQQ__Bundle__c = true;
        insert s;
        
        Id invoicingOppRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
        
        Opportunity opp = new Opportunity(AccountId = acc.Id, StageName = 'Win Pending', RecordTypeId = invoicingOppRTId, Forecast_Category__c = 'Commit', Close_Reason__c = 'Relationship/Reputation', 
                                          CurrencyIsoCode = 'USD', CloseDate = Date.today(), Name = 'Test Opp', Primary_Contact__c = con.Id, Hosting_Model__c = 'On Premise');
        opp.Pricebook2id=Test.getStandardPricebookId();      
        insert opp;
        
        OpportunityLineItem oppLine = new OpportunityLineItem( pricebookentryid=pbe.Id, UnitPrice = 1620, Offset_Amount_for_Renewal_Booking__c = -5000,Quantity = 2,Opportunityid = opp.Id, Original_Subscription_Id__c = s.Id);
        insert oppLine;      
        
        opp.CloseDate = Date.today().addDays(4);
        update opp;
        
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(opp);
        
        BookingsProcessor.updateOffsetOnInvoice(opps);        
    }
    
   
}