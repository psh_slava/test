/**
* @author DSherman
* @author Conga Services
* @Date 10/12/2016
* @version 1.00
* @description Lead triggers--including sharing access
*/
public class LeadHelper {
	/**
	* @author DSherman
	* @author Conga Services
	* @version 1.00
	* @Date 10/12/2016
	* @description Grants secondary owner sharing access. PER REQUEST: This method intentionally does not handle sharing removal
	*/
	/**
	* @version 2.00
	* @Date 12/12/2016
	* @description Commenting out code. Trigger, Trigger Dispatcher, Trigger States have been deleted. If code is required in future - recreate and uncomment LeadHelper/LeadHelperTest (AHAFEZ)
	*/
//public static void grantLeadAccess(List<Lead> newLeads, Map<Id,SObject> oldLeads) {
//		if(TriggerHelper.DoExecute('Lead.Grant_Lead_Access')) {

//			List<LeadShare> sharesToCreate = new List<LeadShare>();
	
//			for(Lead lead : newLeads) {

//				//Checking for nulls first, then check if it meets one of the following conditions:
//				//It is an insert (oldLeads is null) and it has a secondary owner OR
//				//There is a secondary owner now and there wasn't before OR
//				//There is a different secondary owner than existed previously 
//				//Again, it was requested that this class not handle removal of sharing access when secondary owner status is lost
//				if( (lead.Secondary_OwnerId__c != lead.OwnerId) &&
//					((oldLeads == null && lead.Secondary_OwnerId__c != null) || 
//					(lead.Secondary_OwnerId__c != null && oldLeads.get(lead.Id).get('Secondary_OwnerId__c') == null) || 
//					(lead.Secondary_OwnerId__c != null && lead.Secondary_OwnerId__c != oldLeads.get(lead.Id).get('Secondary_OwnerId__c')) )) {

//						LeadShare ls = new LeadShare();
//							ls.LeadAccessLevel = 'Edit';
//							ls.LeadId = lead.Id;
//							ls.RowCause = 'Manual';
//							ls.UserOrGroupId = lead.Secondary_OwnerId__c;
//						sharesToCreate.add(ls);
//				}
//			}
			
//			if(sharesToCreate.size() > 0) {
//				Database.insert(sharesToCreate,false);
//			}

//		}
//	}
}