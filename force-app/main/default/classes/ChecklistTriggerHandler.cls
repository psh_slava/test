public class ChecklistTriggerHandler {
    
    public static void createChecklistItems(List<Checklist__c> clists){
        
        String checklistItemRTId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Checklist Item').getRecordTypeId();
        System.debug('checklistItemRTId ----- ' + checklistItemRTId);
        
        List<Task> activitiesToInsert = new List<Task>();
        Set<String> rtIds = new Set<String>();
        for(Checklist__c cl : clists){
            String rtId = String.valueOf(cl.RecordTypeId).subString(0,15);
            System.debug('checklistRTId ----- ' + rtId);
            rtIds.add(rtId);
        }
        List<Checklist_Activities__c> allListItems = [Select Checklist_Record_Type_Id__c, Comments__c, 
                                                           Due_Date_Days_from_Today__c, Item_Order_Number__c, Priority__c, 
                                                           Stage_Section__c, Subject_Label__c
                                                           From Checklist_Activities__c
                                                           Where Checklist_Record_Type_Id__c IN :rtIds];
        System.debug('All Checklist Activities list items ----- ' + allListItems);
        
        for(Checklist__c cl : clists){
            for(Checklist_Activities__c cla : allListItems){
            
                IF(cl.Account_Recruitment_Stage__c == cla.Stage_Section__c)
                {
                Integer days = Integer.valueOf(cla.Due_Date_Days_from_Today__c);
                Task t = new Task();
                t.WhatId = cl.Id;
                t.Status = 'Open';
                t.Subject = cla.Subject_Label__c;
                t.Description = cla.Comments__c;
                if(days != null){ t.ActivityDate = Date.today().addDays(days); }
                t.Priority = cla.Priority__c;
                t.Stage__c = cla.Stage_Section__c;
                t.RecordTypeId = checklistItemRTId;
                t.OwnerId = cl.Account_Owner_Id__c;
                t.ReminderDateTime = Datetime.now().addDays(days);
                t.IsReminderSet = true;
                activitiesToInsert.add(t); 
                }           
            }
        }
        
        if(activitiesToInsert.size()>0){
            System.debug('Inserting tasks ----- ' + activitiesToInsert);
            insert activitiesToInsert;
        }  
    }
    
   
}