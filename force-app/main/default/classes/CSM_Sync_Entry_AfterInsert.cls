/** 
* CSM_Sync_Entry_AfterInsert  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class CSM_Sync_Entry_AfterInsert extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
        if (!System.isBatch() && TriggerHelper.DoExecute('CSM_Sync_Entry__c.CSMSync')) {
    	    CentralSyncEntryHelper.enqueueSyncEntryProcessor();
            System.debug('Queued CentralSyncEntryHelper...');
            CSMSyncEntryHelper.enqueueSyncEntryProcessor();
            System.debug('Queued CSMSyncEntryHelper...');
           
        }
    }
    /** 
    * @author CRMCulture
    * @version 1.00
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}
}