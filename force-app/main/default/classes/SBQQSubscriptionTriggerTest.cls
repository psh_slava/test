@isTest
public class SBQQSubscriptionTriggerTest {
    
    public static testmethod void newSubsTest(){
        Account a = new Account(Name = 'Test Account');
        Contract c = new Contract(AccountId = a.Id, Hosting_Model__c = 'Cherwell Hosted');
        Product2 p = new Product2(Name = 'Test Product');
        SBQQ__Subscription__c revisedSub = new SBQQ__Subscription__c(SBQQ__Quantity__c = 10, SBQQ__Product__c = p.Id,
                                                                    SBQQ__Account__c = a.Id, SBQQ__Contract__c = c.Id);
        insert revisedSub;
        
        revisedSub.Instance_Number__c = 'Instance 1';
        revisedSub.Hosting_Model__c = null;
        revisedSub.Instance_Name__c = null;
        update revisedSub;

        delete revisedSub;
    }
}