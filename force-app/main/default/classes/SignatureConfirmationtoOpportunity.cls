global class SignatureConfirmationtoOpportunity implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        Opportunity opp;
        
        Integer idIndex;
        Integer cIndex1;
        Integer cIndex1b;
        Integer cIndex2;
        
        String oppId;
        String flowURL;
        
        String subject = email.subject.toLowerCase();
        
        if(subject.contains('contract signed and fully executed') || subject.contains('order confirmation with') || email.plainTextBody.contains('has been completed')){
            String emailbody = email.plainTextBody;
            System.debug(emailbody);
            
            idIndex = email.plainTextBody.IndexOf('006');
            cIndex1 = email.plainTextBody.IndexOf('https://');
            cIndex1b = email.plainTextBody.IndexOf('workflow/');
            //cIndex2 = email.plainTextBody.IndexOf('<',cIndex1b);
            
            if(idIndex != null){
                System.debug(idIndex);
                oppId = email.plainTextBody.substring(idIndex,idIndex+15);
                System.debug(oppId);
                flowURL = email.plainTextBody.substring(cIndex1,cIndex1b+33);
                System.debug(flowURL);
            }
            
            try{
                opp = [Select Id, Ironclad_Workflow_URL__c, Ironclad_Workflow_Status__c From Opportunity Where Id =:oppId];
                if(opp != null){
                    opp.Ironclad_Workflow_URL__c = flowURL;
                    opp.Ironclad_Workflow_Status__c = 'Signed';
                    update opp;
                }    
            } 
            catch (QueryException e) {
                System.debug('Query issue:  ' + e);
            }
            
        } return result;
    }
}