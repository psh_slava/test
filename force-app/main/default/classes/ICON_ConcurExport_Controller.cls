/*
	File export class for Concur Integration - Output, Empoyees, Accounts, and Project files
	john.mccarvill@iconatg.com - 4/23/2018

 */
public with sharing class ICON_ConcurExport_Controller {

	
	//string containing formatted IIF file contents 
	public String FileOutput {get; set;}
	public string FileName {get; set;}
	
	public string sType {get; set;}
	
	public ICON_ConcurExport_Controller(){		

		//get type to process (Employees, Projects, Accounts)
		sType = ApexPages.currentPage().getParameters().get('Type');
		
	}
	
	public PageReference GenerateExport() {

		//get the file contents
		String sOutput = GenerateFileContents();
		
		//set the contents to the property
		this.FileOutput = sOutput;

		//remain on page - content-type should initiate download of text file 
		return(null);
	}
	
	
	
	public String GenerateFileContents() {
		
		//generate the text file records
		string sOutput = ''; 
		
		//maps to organize output
		map<string,string> mapResources = new map<string,string>();
		map<string,string> mapProjects = new map<string,string>();
		map<string,string> mapAssignments = new map<string,string>();

		list<string> listResources = new list<string>(); 
		set<string> setProjectIds = new set<string>(); 
		set<string> setAssignmnetIds = new set<string>();

		//----------------------------------------------------------------------------------------------------------------
		if (sType=='All')
		{

			//header row
			string sLine = 'DELETE,NAME,LEVEL_01_CODE,LEVEL_02_CODE,LEVEL_03_CODE\r\n';
			sOutput +=  sLine; 
			
			
			//********************************************************* Employees
			this.FileName='Cherwell-PSAData.csv';
			
			list<Contact> listResourcesSource = [SELECT Id, Name, Email, Concur_Employee_ID__c 
											FROM Contact 
											WHERE pse__Is_Resource__c=true 
											AND pse__Is_Resource_Active__c=true
											AND Concur_Employee_ID__c!=null];
			
			for(Contact pContact:listResourcesSource)
			{
				sLine = ',"' + pContact.Name + '", ' +  pContact.Concur_Employee_ID__c  +  ',,\r\n';
				mapResources.put(pContact.Id, sLine);
				
				string sSortKey = pContact.Name + '%' + pContact.Id;
				listResources.add(sSortKey);
			}
			
			//sort by name
			listResources.sort();
			
			
			
			//********************************************************* Projects/Assignments
			list<pse__Assignment__c> listAssignmentsProj = [Select p.Name, p.pse__Assignment_Number__c, p.pse__Resource__r.Email, p.pse__Resource__r.Concur_Employee_ID__c, p.pse__Resource__r.Id, 
											p.pse__Resource__c, p.pse__Project__r.pse__Project_ID__c, p.pse__Project__r.pse__Is_Active__c, 
											p.pse__Project__r.pse__Account__c, p.pse__Project__c, p.Id, 
											p.pse__Project__r.pse__Account__r.Name, p.pse__Project__r.Name , p.pse__Closed_for_Expense_Entry__c
											From pse__Assignment__c p
											WHERE p.pse__Project__r.pse__Is_Active__c=true
											AND p.pse__Closed_for_Expense_Entry__c=false
											AND p.pse__Resource__r.Email!=null];
			
			for(pse__Assignment__c pAssignment:listAssignmentsProj)
			{
				
				//resource-project				
				sLine = ',"' + pAssignment.pse__Project__r.Name + '", ' + pAssignment.pse__Resource__r.Concur_Employee_ID__c + ',' + pAssignment.pse__Project__r.pse__Project_ID__c + ',\r\n';
				string sKey = pAssignment.pse__Resource__r.Id + '-' + pAssignment.pse__Project__r.Id;
				
				//record project id
				setProjectIds.add(pAssignment.pse__Project__r.Id);
				
				//put in map				
				mapProjects.put(sKey, sLine);
				
				
				//resource-project-assignment
				if (pAssignment.pse__Closed_for_Expense_Entry__c==false) 
				{
					sLine = ',"' + pAssignment.Name + '", ' + pAssignment.pse__Resource__r.Concur_Employee_ID__c + ',' + pAssignment.pse__Project__r.pse__Project_ID__c + ',' + pAssignment.pse__Assignment_Number__c + '\r\n';
					sKey = pAssignment.pse__Resource__r.Id + '-' + pAssignment.pse__Project__r.Id + '-' + pAssignment.Id;
					
					//record assignment id
					setAssignmnetIds.add(pAssignment.Id);
					
					//put in map
					mapAssignments.put(sKey, sLine);
				}
			}

			//now iterate and output text to file
			
			for(string sResourceKey:listResources)
			{
				//get Id portion of sort key
				string sResourceId = sResourceKey.split('%')[1]; 
				
				//output resource
				sLine = mapResources.get(sResourceId);
				sOutput += sLine;
				
				//output each project for this resource
				for(string sProjectId:setProjectIds)
				{
					string sProjectKey = sResourceId + '-' + sProjectId;
					if(mapProjects.containsKey(sProjectKey))
					{
						sLine = mapProjects.get(sProjectKey);
						sOutput += sLine;

						//now output all assignments for resource-project
						for(string sAssignmentId:setAssignmnetIds)
						{
							string sAssignmentKey = sResourceId + '-' + sProjectId + '-' + sAssignmentId;
							if(mapAssignments.containsKey(sAssignmentKey))
							{
								sLine = mapAssignments.get(sAssignmentKey);
								sOutput += sLine;
							}
						}						
							
					} 
				}
				
			}
			
			
		}

		//return output
		return(sOutput);		
	}


}