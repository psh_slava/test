/* TEST CLASS = TaskTriggerTest */

trigger Task_Trigger on Task (after insert) {
    
    //Suggest if we need Handler for task trigger too
    TaskTriggerHandler taskHandlerObj = new TaskTriggerHandler();
    
    if( Trigger.isAfter && Trigger.isInsert ){  
    
        //Contact associated to the task should be the primary contact on the opportunity
        taskHandlerObj.afterInsert(Trigger.new);
    }

}