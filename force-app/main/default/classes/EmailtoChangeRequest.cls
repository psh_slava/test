global class EmailtoChangeRequest implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        SLX__Change_Control__c req;
        
        Integer sIndex = email.plainTextBody.IndexOf('. ');
        String truncBody;
        
        if(sIndex > 200){
            sIndex = 190;
            truncBody = email.plainTextBody.substring(0,sIndex)+'...\n\n - - See full email below in Additional Details - -';
        }
        //String truncBody = email.plainTextBody.substring(0,sIndex)+'...\n\n - - See full email below in Additional Details - -';
        
        try{
            User u = [SELECT Id, Name, Email, Department
                      FROM User
                      WHERE Email = :env.fromAddress
                      LIMIT 1];
            req = new SLX__Change_Control__c();
            req.SLX__Description__c = truncBody;
            req.Additional_Details_of_Req__c = email.htmlBody;
            req.Name = email.subject;
            req.SLX__Date_Requested__c = Date.today();
            req.Submitted_via__c = 'Email';
            req.Priority__c = 'Undefined';          
            req.SLX__Requestor__c = u.Id;   
            req.Department__c = u.Department;    
            insert req;
        }
        catch (QueryException e){
            System.debug('Query issue: ' + e);
            req = new SLX__Change_Control__c();
            req.SLX__Description__c = truncBody;
            req.Additional_Details_of_Req__c = email.htmlBody;
            req.Name = email.subject;
            req.SLX__Date_Requested__c = Date.today();
            req.Submitted_via__c = 'Email';
            req.Priority__c = 'Undefined'; 
            req.Administrator_Notes__c = 'Email sent from: ' + email.fromName + '. No user currently exists with this email address.';
            insert req;
        }
          if(email.textAttachments != null)
        {
            // Save attachments, if any
            for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
              Attachment attachment = new Attachment();
             
              attachment.Name = tAttachment.fileName;
              attachment.Body = Blob.valueOf(tAttachment.body);
              attachment.ParentId = req.Id;
              insert attachment;
            }
        }
        if(email.binaryAttachments != null)
        {
            for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
              Attachment attachment = new Attachment();
             
              attachment.Name = bAttachment.fileName;
              attachment.Body = bAttachment.body;
              attachment.ParentId = req.Id;
              insert attachment;
            }
        }
        result.success = true;
        
        return result;
    }
}