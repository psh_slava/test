@isTest
public class OpportunityTrigger_Test {
    static{
        List<LeadLifeCycleMapping__c> lfcCustomSetting = new List<LeadLifeCycleMapping__c>();
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Person_Status__c' ,Lead_Api_Name__c ='Status' ,OnCreate__c = true));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'OwnerID' ,Lead_Api_Name__c ='OwnerID' ,OnCreate__c = true));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Related_Lead__c' ,Lead_Api_Name__c ='Id' ,OnCreate__c = true));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Score_at_TQL__c' ,Lead_Api_Name__c ='mkto71_Lead_Score__c' ,OnCreate__c = false));  
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Score_at_TAL__c' ,Lead_Api_Name__c ='mkto71_Lead_Score__c' ,OnCreate__c = false));
        lfcCustomSetting.add(new LeadLifeCycleMapping__c(name = 'Recycle_Reason__c' ,Lead_Api_Name__c ='Back_To_Nurture_Explanation__c' ,OnCreate__c = false));
        insert lfcCustomSetting;
        
        Account acc = new Account(Name = 'Test Account 1', BillingState = 'Colorado', BillingCountry = 'United States');
        insert acc;
        
        List<Contact> newContact = new List<Contact>();
        newContact.add(new Contact(FirstName = 'Test Contact', LastNAme = '1', email = 'test1@test.com', Accountid = acc.id, Contact_status__c = 'New'));
        newContact.add(new Contact(FirstName = 'Test Contact', LastNAme = '2', email = 'test3@test.com', Accountid = acc.id, Contact_status__c = 'New'));
        newContact.add(new Contact(FirstName = 'Test Contact', LastNAme = '3', email = 'test2@test.com', Accountid = acc.id, Contact_status__c = 'New'));
        insert newContact;
        System.debug('Test Class >> ' + [Select id, Related_Contact__c, Lifecycle_status__c from Lifecycle__c]);
        
        List<Lifecycle__c> lifeCycle = [Select id, Related_Contact__c, Lifecycle_status__c from Lifecycle__c];
        lifeCycle[0].related_Contact__c = newCOntact[0].id;
        lifeCycle[1].lifecycle_stage__c = 'TQL';
        lifeCycle[1].related_Contact__c = newCOntact[1].id;
        lifeCycle[1].lifecycle_status__c = 'Active';
        lifeCycle[1].lifecycle_stage__c = 'AQL';
        lifeCycle[2].related_Contact__c = newCOntact[2].id;
        lifeCycle[1].lifecycle_stage__c = 'TAL';
        update lifeCycle;
        
        List<Opportunity> newOpp = new List<Opportunity>();
        newOpp.add(new Opportunity(Name = 'Test Opportuntiy', accountid = acc.Id, closedate = date.today(), stagename = 'Pre-Qualification', Primary_Contact__c = newContact[0].id, Primary_Competitor__c = 'Axios Systems'));
        newOpp.add(new Opportunity(Name = 'Test Opportuntiy', accountid = acc.Id, closedate = date.today(), stagename = 'Qualify', Primary_Contact__c = newContact[1].id, Forecast_Category__c = 'Omit', Primary_Competitor__c = 'Axios Systems'));
        newOpp.add(new Opportunity(Name = 'Test Opportuntiy', accountid = acc.Id, closedate = date.today(), stagename = 'Closed - Won', Primary_Contact__c = newContact[2].id, Forecast_Category__c = 'Won', Primary_Competitor__c = 'Axios Systems'));
        newOpp.add(new Opportunity(Name = 'Test Opportuntiy', accountid = acc.Id, closedate = date.today(), stagename = 'Pre-Qualification', Primary_Contact__c = newContact[1].id, Forecast_Category__c = 'Omit', Primary_Competitor__c = 'Axios Systems'));
        insert newOpp;
        
        List<OpportunityContactRole> ocr = new List<OpportunityContactRole>();
        ocr.add(new OpportunityContactRole(OpportunityId = newOpp[0].id, contactid = newContact[0].id, isprimary = true));
        ocr.add(new OpportunityContactRole(OpportunityId = newOpp[1].id, contactid = newContact[1].id, isprimary = true));
        ocr.add(new OpportunityContactRole(OpportunityId = newOpp[2].id, contactid = newContact[2].id, isprimary = true));
        insert ocr;
    }
    
    @isTest 
    public static void testLeadInsertActions1(){
        Test.startTest();    
        
        List<Opportunity> newOpp = [Select id from Opportunity];
        
        List<Lifecycle__c> lifeCycle1 = [Select id, Related_Contact__c, Lifecycle_status__c from Lifecycle__c];
        lifeCycle1[0].related_Opportunity__c = newOpp[0].id;
        update lifeCycle1;
        
        newopp[0].stageName = 'Qualify';
        newOpp[0].Forecast_Category__c = 'Omit';
        newopp[1].stageName = 'Pre-Qualification';
        newOpp[1].Forecast_Category__c = 'Omit';
        update newopp;

        Test.stopTest();
        
    }
    @isTest 
    public static void testLeadInsertActions2(){
        contact con= [select id from contact limit 1];
        List<Opportunity> newOppl = new List<Opportunity>();
        newOppl.add(new Opportunity(Name = 'Test Opportuntiy', closedate = date.today(), stagename = 'Pre-Qualification', Primary_Contact__c = con.id, Primary_Competitor__c = 'Axios Systems'));
        newOppl.add(new Opportunity(Name = 'Test Opportuntiy', closedate = date.today(), stagename = 'Pre-Qualification', Primary_Contact__c = con.id, Primary_Competitor__c = 'Axios Systems'));
        newOppl.add(new Opportunity(Name = 'Test Opportuntiy1', closedate = date.today(), stagename = 'Develop', Primary_Contact__c = con.id, Primary_Competitor__c = 'Axios Systems'));
        newOppl.add(new Opportunity(Name = 'Test Opportuntiy2', closedate = date.today(), stagename = 'Develop', Primary_Contact__c = con.id, Primary_Competitor__c = 'Axios Systems'));
        newOppl.add(new Opportunity(Name = 'Test Opportuntiy3', closedate = date.today(), stagename = 'Pre-Qualification', Primary_Contact__c = con.id, Primary_Competitor__c = 'Axios Systems'));
        insert newOppl;
        insert new OpportunityContactRole(contactid = con.id, opportunityid = newOppl[0].id, isPrimary = true);
        insert new Lifecycle__c(related_opportunity__c = newOppl[0].id, related_contact__c = con.id, Lifecycle_status__c = 'Active');
        insert new OpportunityContactRole(contactid = con.id, opportunityid = newOppl[1].id, isPrimary = true);
        insert new Lifecycle__c(related_opportunity__c = newOppl[1].id, related_contact__c = con.id, Lifecycle_status__c = 'Inactive');
        insert new OpportunityContactRole(contactid = con.id, opportunityid = newOppl[2].id, isPrimary = true);
        insert new Lifecycle__c(related_opportunity__c = newOppl[2].id, related_contact__c = con.id, Lifecycle_status__c = 'Active');
        insert new OpportunityContactRole(contactid = con.id, opportunityid = newOppl[3].id, isPrimary = true);
        insert new Lifecycle__c(related_opportunity__c = newOppl[3].id, related_contact__c = con.id, Lifecycle_status__c = 'Active');
        insert new OpportunityContactRole(contactid = con.id, opportunityid = newOppl[4].id, isPrimary = true);
        insert new Lifecycle__c(related_opportunity__c = newOppl[4].id, related_contact__c = con.id, Lifecycle_status__c = 'Active');
        
        newoppl[0].stagename='Qualify';
        newoppl[0].Forecast_Category__c = 'Omit';
        newoppl[0].Primary_Competitor__c = 'Axios Systems';
        update newoppl[0];
        newoppl[0].stageName = 'Closed - Won';
        newoppl[0].Forecast_Category__c = 'Won';
        update newoppl[0];
        newoppl[0].stagename='Pre-Qualification';
        newoppl[0].Forecast_Category__c = 'Omit';
        newoppl[0].Primary_Competitor__c = 'Axios Systems';
        update newoppl[0];
        newoppl[0].stageName = 'Closed - Lost';
        newoppl[0].Forecast_Category__c = 'Omit';
        newoppl[0].Close_Actions__c = 'No Actions';
        newoppl[0].Close_Reason__c = 'Price';
        update newoppl[0]; 
        newoppl[1].stageName = 'Closed - Lost';
        newoppl[1].Forecast_Category__c = 'Omit';
        newoppl[1].Close_Actions__c = 'No Actions';
        newoppl[1].Close_Reason__c = 'Price';
        update newoppl[1]; 
        newoppl[2].stageName = 'Closed - Lost';
        newoppl[2].Forecast_Category__c = 'Omit';
        newoppl[2].Close_Actions__c = 'No Actions';
        newoppl[2].Close_Reason__c = 'Price';
        update newoppl[2]; 
       // newoppl[3].stageName = 'Closed No Sale';
       // newoppl[3].Forecast_Category__c = 'Omit';
       // newoppl[3].Close_Actions__c = 'No Actions';
       // newoppl[3].Close_Reason__c = 'Other';
       // update newoppl[3];
        newoppl[4].stageName = 'Closed - Lost';
        newoppl[4].Forecast_Category__c = 'Omit';
        newoppl[4].Close_Actions__c = 'No Actions';
        newoppl[4].Close_Reason__c = 'Price';
        update newoppl[4]; 
        
    }
   
    @isTest
    public static void testContactRoles(){
        Account a = new Account(name='Test Company', billingstreet = '123 test st.', billingcountry = 'united states', billingstate = 'Texas');
        a.BillingPostalCode = '78726';
        a.BillingCity = 'testville';
        insert a;
         
        Contact con = new Contact(AccountId = a.Id, LastName = 'Tester2', Email = 'test2@test.test');
        insert con;
                
        Opportunity o = new Opportunity(AccountId = a.Id, StageName = 'Order Processing', Forecast_Category__c = 'Commit', Close_Reason__c = 'Relationship/Reputation', 
                                        CurrencyIsoCode = 'USD', CloseDate = Date.today(), Name = 'Test Opp', Hosting_Model__c = 'On Premise', Primary_Contact__c = con.Id);
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__StartDate__c = Date.today();
        q.Term__c = '3';
        q.SBQQ__Primary__c = true;
        q.SBQQ__PrimaryContact__c = con.Id;
        q.License_Key_Contact__c = con.Id;
        q.Technical_Maintenance_Contact__c = con.Id;
        q.Billing_Contact__c = con.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.ApprovalStatus__c = 'Approved';
        q.SBQQ__Status__c = 'Approved';
        insert q;
        
        o.SBQQ__PrimaryQuote__c = q.Id;
        update o;
        
        o.StageName = 'Closed - Won';
        update o; 
      
    } 
}