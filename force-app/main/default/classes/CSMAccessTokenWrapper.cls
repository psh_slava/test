// Structure to hold the response data from a token request.  Also encapsulates specific
// parsing logic.
public class CSMAccessTokenWrapper
{
	public String access_token;
	public String refresh_token;

	// The following properties have illegal names (have illegal characters or
	// use reserved words) in the JSON response so we're renaming with an _x to
	// distinguish from the original properties.
	public Datetime issued_x;
	public Datetime expires_x;

	public static CSMAccessTokenWrapper parseJSON(String jsonStr) {
		// For most fields, we'll do safe type parsing.  So as long as the types and field names
		// match, we get automatic parsing.
		CSMAccessTokenWrapper a = (CSMAccessTokenWrapper)JSON.deserialize(jsonStr, CSMAccessTokenWrapper.class);

		// However, there's a property in the json string that starts with a . which can't be
		// a variable name in Apex.  So we'll manually parse this.  We also need to do a 
		// manual date conversion as the date format is not standard.
		Map<String, Object> parsedValue = (Map<String, Object>)JSON.deserializeUntyped(jsonStr);
		a.issued_x = UtilityClass.convertToDatetime(String.valueOf(parsedValue.get('.issued')));
		a.expires_x = UtilityClass.convertToDatetime(String.valueOf(parsedValue.get('.expires')));

		return a;
	}

	public static Boolean isTokenValid(String accessToken, Datetime expiryDtm, Integer requestTimeout) {
    	if (String.isBlank(accessToken) || expiryDtm == null) return false;

    	Long diffMs = expiryDtm.getTime() - Datetime.now().getTime();

    	// Allow >2 mins buffer from actual expiry 
    	return diffMs > requestTimeout;
    }
}