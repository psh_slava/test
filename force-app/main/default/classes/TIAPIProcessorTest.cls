@isTest
public class TIAPIProcessorTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    public static testMethod void testTIAPI() {
        TI_API_Settings__c settings = new TI_API_Settings__c();
        settings.API_Key__c = 'testaccesskey';
        settings.Base_URL__c = 'testurl.test.test/';
        settings.Customer_ClientId__c = '12345-6789';
        insert settings;
        
        TI_License_Access__c access = new TI_License_Access__c();
        access.Name = 'Training Package';
        access.courseTagIds__c = '56da1ce8-decb-55ee-9d05-c97b857c5d16,1234-5678-987';
        access.courseIds__c = '1234567890';
        access.learningPathIds__c = '1234567890';
        access.courseTagLabels__c = 'VLL';
        access.productLicenseSKU__c = 'training-package';
        access.productLicenseName__c = 'training-package';
        access.productLicenseId__c = '72d0e1cb-edf3-4118-95c0-fadb3e1ed5fa';
        insert access;
        
        Account a = new Account();
        a.Name = 'Test Account';
        a.TI_Employee_SKU__c = 'testemployeelicenseid-1234';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        c.Email = 'test@test.com';
        c.Learner__c = true;
        c.Status__c = 'Active';
        c.TI_Access_Last_Updated__c = Date.today().addDays(-5);
        c.TILBP__TIID__c = 'testtiid-12345';
        insert c;
        
        Contact c2 = new Contact();
        c2.LastName = 'Tester';
        c2.AccountId = a.Id;
        c2.Email = 'test2@test.com';
        c2.Learner__c = true;
        c2.Status__c = 'Active';
        //c2.TI_Access_Last_Updated__c = Date.today().addDays(-5);
        c2.TILBP__TIID__c = 'testtiid-12346';
        insert c2;
        
        Product2 p = new Product2();
        p.Name = 'Training Package (Default)';
        p.StockKeepingUnit = 'training-package';
        p.Family = 'Learning Services';
        insert p;
        
        Product2 pc = new Product2();
        pc.Name = 'Administration Foundations Self-Paced';
        pc.StockKeepingUnit = 'af-self-pcd';
        pc.Family = 'Learning Services';
        pc.TI_Course_Slug__c = 'af-self-pcd';
        insert pc;
        
        Product2 pb = new Product2();
        pb.Name = 'VLL 1 Year Subscription';
        pb.StockKeepingUnit = 'video-learning-1-year-subscription';
        pb.Family = 'Learning Services';
        pb.TI_Bundle_Slug__c = 'video-learning-1-year-subscription';
        insert pb;
        
        Product2 plp = new Product2();
        plp.Name = 'Developer Essentials Training';
        plp.StockKeepingUnit = 'developer-essentials';
        plp.Family = 'Learning Services';
        plp.TI_Learning_Path_Slug__c = 'developer-essentials';
        insert plp;
        
        Asset at = new Asset();
        at.Name = 'Test Training Product Purchase';
        at.Product2Id = p.Id;
        at.AccountId = a.Id;
        at.Quantity = 1;
        //at.ProductFamily = 'Learning Services';
        //at.StockKeepingUnit = 'training-package';
        insert at;
        
        //Tests for trigger run on Asset insert
        Asset at2 = new Asset();
        at2.Name = 'Test Training Course Purchase';
        at2.AccountId = a.Id;
        at2.Quantity = 1;
        at2.Product2Id = pc.Id;
        at2.ContactId = c.Id;
        insert at2;
        
        Asset at3 = new Asset();
        at3.Name = 'Test Training Bundle Purchase';
        at3.AccountId = a.Id;
        at3.Quantity = 1;
        at3.Product2Id = pb.Id;
        at3.ContactId = c.Id;
        insert at3;
        
        Asset at4 = new Asset();
        at4.Name = 'Test Training Learning Path Purchase';
        at4.AccountId = a.Id;
        at4.Quantity = 1;
        at4.Product2Id = plp.Id;
        at4.ContactId = c.Id;
        insert at4;
        
        TIAPIWrapper.licenseWrapper lw = new TIAPIWrapper.licenseWrapper();
        lw.accountName = 'test company';
        lw.companyLicenseId = '1234';
        lw.companyParentLicenseId = '5678';
        lw.employeeLicenseId = '9999';
        lw.productLicenseId = '5555';
        
        String jsonString = '{"id": "e3a230a4-06b1-47c5-aa46-fe5f9e8bbab3","name": "Test Client","sku": "test-client-sku","licenses": '+
                         '[{"id": "2a779a49-50eb-48de-a997-96d6aa6252ff","name": "Main","sku": "main","createdAt": "2018-02-15T14:06:03.084Z","updatedAt": '+
                         '"2018-02-15T14:06:03.084Z","accessDays": null,"seatsLimit": null,"learningPathIds": ["7c117319-60d1-45fd-bf1d-3d92bb824a84"],"courseIds": '+
                         '["13b808e9-4064-4a0b-b36d-8e8a4e242260", "8ae5bdab-e318-5ff1-b630-bfe01a1a09be"],"courseTags": [{ "id": "591a2429-e3b0-5a79-b18a-d5e35cb444ba", "label": "Test Tag" }],'+
                         '"parentLicenseId": "1234-5678"}]}';
        ResponseResult.parse(jsonString);
        
        TIRecordWrapper.getBodyLicenseCreate('clientId', 'sku', 'parentLicenseId', 'accountName', '72d0e1cb-edf3-4118-95c0-fadb3e1ed5fa');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TIAPIProcessorCalloutMock());
        TIAPIWrapper.reparentSKUs('test company', 'training-package', lw);
        String jobId = System.schedule('ScheduleTIAPITest', CRON_EXP, new TIAPIProcessorScheduler());
        
        TIAPIProcessorBatch tp = new TIAPIProcessorBatch();
        Database.executebatch(tp);
        
        Test.stopTest();//will sync back up with async operations if future/batch called before this
    }
    
}