@isTest
public class updateCampaigntoLifeCycle_Test {

 Public static testmethod void Campaignmethodactive()
 {
    Test.StartTest();
    Account acc = new Account (Name ='testAc1');
        insert acc;
        
        Campaign Cam = New Campaign (Name='Test Campaign');    
        Insert cam;
         Campaign Cam1 = New Campaign (Name='Test Campaign1');    
        Insert cam1;
         Campaign Cam2 = New Campaign (Name='Test Campaign2');    
        Insert cam2;
        
       Contact con = new Contact(lastname = 'conTest1', AccountID = acc.id) ;
        
        insert con ;
        List<CampaignMember> lstcm = New List<CampaignMember>();
       
        CampaignMember cm = New CampaignMember(Campaignid = Cam.id,contactid=con.id,Status='Opened');
        CampaignMember cm1 = New CampaignMember(Campaignid = Cam1.id,contactid=con.id,Status='Opened');
        CampaignMember cm2 = New CampaignMember(Campaignid = Cam2.id,contactid=con.id,Status='Opened');
 
        lstcm.Add(cm);
        lstcm.Add(cm1);
        lstcm.Add(cm2);
        
        Insert lstcm;
                
        lifecycle__c lyf1 = new lifecycle__c( Related_Contact__c = con.id, Lifecycle_Status__c ='active' , Lifecycle_Stage__c = 'SQL' ,Start_Date__c=system.today(),AQL_Entry_Date__c=system.today()) ;
       
        insert lyf1 ;
         List<CampaignMember> updatelstcm = New List<CampaignMember>();
        For(CampaignMember C :lstcm)
        {
        c.Lifecycle__c = lyf1.id;
        updatelstcm.Add(C);
        }
        Update updatelstcm;
        updateCampaigntoLifeCycle  B = New updateCampaigntoLifeCycle ();
        Database.executeBatch(B);
          Test.StopTest();
 
 }
  Public static testmethod void CampaignmethodCompleted()
 {
    Test.StartTest();
    Account acc = new Account (Name ='testAc1');
        insert acc;
        
        Campaign Cam = New Campaign (Name='Test Campaign');    
        Insert cam;
         Campaign Cam1 = New Campaign (Name='Test Campaign1');    
        Insert cam1;
         Campaign Cam2 = New Campaign (Name='Test Campaign2');    
        Insert cam2;
        
       Contact con = new Contact(lastname = 'conTest1', AccountID = acc.id) ;
        
        insert con ;
        List<CampaignMember> lstcm = New List<CampaignMember>();
       
        CampaignMember cm = New CampaignMember(Campaignid = Cam.id,contactid=con.id,Status='Opened');
        CampaignMember cm1 = New CampaignMember(Campaignid = Cam1.id,contactid=con.id,Status='Opened');
        CampaignMember cm2 = New CampaignMember(Campaignid = Cam2.id,contactid=con.id,Status='Opened');
 
        lstcm.Add(cm);
        lstcm.Add(cm1);
        lstcm.Add(cm2);
        
        Insert lstcm;
                
        lifecycle__c lyf1 = new lifecycle__c( Related_Contact__c = con.id, Lifecycle_Status__c ='Completed' ,Start_Date__c=system.today(),AQL_Entry_Date__c=system.today()) ;
       
        insert lyf1 ;
         List<CampaignMember> updatelstcm = New List<CampaignMember>();
        For(CampaignMember C :lstcm)
        {
        c.Lifecycle__c = lyf1.id;
        updatelstcm.Add(C);
        }
        Update updatelstcm;
        updateCampaigntoLifeCycle  B = New updateCampaigntoLifeCycle ();
        Database.executeBatch(B);
          Test.StopTest();
 
 }
}