public class ResponseResult {
     
    public TIUser[] users;
    public PageInfo pageInfo;
    public courseTags[] courseTags;
    public String id;
    public license[] licenses;

    public class TIUser {
        public String id, sfContactId, externalCustomerId;
        public String accessSlug, email;
    }
    public class pageInfo {
        public Integer total;
        public String cursor;
        public Boolean hasMore;
    }
    public class purchasedCourse {
        public String courseId, status;
    }
    
    public class courseTags {
        public String id, label;
    }
    
    public class license {
        public String id, name;
        //public Integer accessDays, seatsLimit;
        //public String parentLicenseId;
    }
    
    public static ResponseResult parse(String jsonString) {
        return (ResponseResult)JSON.deserialize(jsonString, ResponseResult.class);
    }  
    
    
}