/*
* @trigger name:   MarketplaceCustomerConversion
* @created:        By Naomi Harmon in May 2020
* @description: 
*    Initiaties MarketplaceCustomerTriggerHandler
* @modifcation log:
*
*/

trigger MarketplaceCustomerTrigger on Marketplace_Customer__c (before insert) {
    
    MarketplaceCustomerTriggerHandler handler = new MarketplaceCustomerTriggerHandler();
    
    handler.convertCustomer(Trigger.new);

}