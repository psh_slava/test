/*******************************************************************************
Name              : QuoteApprovalsController 
Description       : Controller class of the VF Page component QuoteApprovals that will list the current Quote Approvals records under specific Quote.
Revision History  : - 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue                                           
----------------------------------------------------------------------------------------
1. Sebastián Suarez    01/09/2018                Dennis Palmer        TK-0000958 - (https://sovcrm.my.salesforce.com/a101N00000HyQGM)
*******************************************************************************/
public class QuoteApprovalsController {
    
    public Id quote_Id {get; set;}
    public String url {get; set;}   

    public List<sbaa__Approval__c> approvals {
        get{
             return [select sbaa__Rule__c, sbaa__Rule__r.Name from sbaa__Approval__c where Quote__c = :quote_Id ];
           }
        set;
    }
}