public with sharing class CSMSyncEntryHistoryRetrySetController {

    private ApexPages.StandardSetController cntrl;
	private List<CSM_Sync_Entry_History__c> selectedSyncEntries;
    private List<CSM_Sync_Entry_History__c> selectedLIVEEntries = new List<CSM_Sync_Entry_History__c>();
    private List<CSM_Sync_Entry_History__c> selectedCentralEntries = new List<CSM_Sync_Entry_History__c>();

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CSMSyncEntryHistoryRetrySetController(ApexPages.StandardSetController stdController) {
        cntrl = stdController;
        selectedSyncEntries = (List<CSM_Sync_Entry_History__c>)stdController.getSelected();

    }

    public PageReference retry() {
        for (CSM_Sync_Entry_History__c c : selectedSyncEntries){
            if(c.Endpoint_System__c == 'LIVE'){ selectedLIVEEntries.add(c); }
            if(c.Endpoint_System__c == 'Central'){ selectedCentralEntries.add(c); }
        }
        String errMsg = CSMSyncEntryHelper.enqueueSyncEntry(selectedLIVEEntries);
        String errMsg2 = CentralSyncEntryHelper.enqueueSyncEntry(selectedCentralEntries);
        

        if (String.isNotBlank(errMsg)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg));
            return null;
        }
        
        if (String.isNotBlank(errMsg2)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg2));
            return null;
        }

        return cntrl.cancel();
    }
}