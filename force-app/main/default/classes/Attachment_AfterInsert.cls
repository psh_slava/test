/** 
* Attachment_AfterInsert  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Attachment_AfterInsert  extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @date 20140701
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
       	if (TriggerHelper.DoExecute('Attachment.CSMSync')) {
            Set<Id> attIds = new Set<Id>();
            for(Attachment a : (List<Attachment>)tp.newList){
                String objTypeName = String.valueOf(a.ParentId.getSObjectType());
                if(objTypeName == 'Resource_Request__c'){
                    attIds.add(a.Id);
                }
            }
            if(attIds.size()>0){
                CSMAPIWrapper.uploadBusinessObjectAttachmentAsync(attIds);
            }
        }
    }
    /** 
    * @author CRMCulture
    * @version 1.00, 20140701
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}
}