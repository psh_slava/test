@isTest
global class AzureAPICalloutMock implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest request){
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{ "@odata.context": "https://graph.microsoft.com/beta/$metadata#directoryObjects", "value": [ '+
                         ' {             "@odata.type": "#microsoft.graph.user",             "id": "4782e723-f4f4-4af3-a76e-25e3bab0d896",             "deletedDateTime": null,'+
                         '            "accountEnabled": true,             "ageGroup": null,           "businessPhones": [                "+1 858 555 0110"            ],'+
                         '            "city": "San Diego",             "createdDateTime": "2017-07-29T02:48:32Z",             "companyName": null,             "consentProvidedForMinor": null,'+
                         '            "country": "United States",            "department": "Sales & Marketing",            "displayName": "Alex Wilber",'+
                         '            "employeeId": null,            "faxNumber": null,            "givenName": "Alex",            "imAddresses": ['+
                         '                "AlexW@M365x214355.onmicrosoft.com"             ],             "isResourceAccount": null,             "jobTitle": "Marketing Assistant",'+
                         '            "legalAgeGroupClassification": null,             "mail": "AlexW@M365x214355.onmicrosoft.com",             "mailNickname": "AlexW",'+
                         '            "mobilePhone": null,             "onPremisesDistinguishedName": "null",             "officeLocation": "131/1104",             "onPremisesDomainName": null,'+
                         '            "onPremisesImmutableId": null,             "onPremisesLastSyncDateTime": null,             "onPremisesSecurityIdentifier": null,             '+
                         '"onPremisesSamAccountName": null,             "onPremisesSyncEnabled": null,             "onPremisesUserPrincipalName": null,             "otherMails": [],'+
                         '            "passwordPolicies": "DisablePasswordExpiration",            "passwordProfile": null,            "postalCode": "92121",            "preferredDataLocation": null,'+
                         '            "preferredLanguage": "en-US",            "surname": "Wilberson",             "userType": "Member"             },'+
                          ' {             "@odata.type": "#microsoft.graph.user",             "id": "testid-f4f4-4af3-a76e-25e3bab0d896",             "deletedDateTime": null,'+
                         '            "accountEnabled": true,             "ageGroup": null,           "businessPhones": [                "+1 858 555 0110"            ],'+
                         '            "city": "San Diego",             "createdDateTime": "2017-07-29T02:48:32Z",             "companyName": null,             "consentProvidedForMinor": null,'+
                         '            "country": "United States",            "department": "Sales & Marketing",            "displayName": "Alex Wilber",'+
                         '            "employeeId": null,            "faxNumber": null,            "givenName": "Alex",            "imAddresses": ['+
                         '                "AlexW@M365x214355.onmicrosoft.com"             ],             "isResourceAccount": null,             "jobTitle": "Marketing Assistant",'+
                         '            "legalAgeGroupClassification": null,             "mail": "AlexW@M365x214355.onmicrosoft.com",             "mailNickname": "AlexW",'+
                         '            "mobilePhone": null,             "onPremisesDistinguishedName": "null",             "officeLocation": "131/1104",             "onPremisesDomainName": null,'+
                         '            "onPremisesImmutableId": null,             "onPremisesLastSyncDateTime": null,             "onPremisesSecurityIdentifier": null,             '+
                         '"onPremisesSamAccountName": null,             "onPremisesSyncEnabled": null,             "onPremisesUserPrincipalName": null,             "otherMails": [], '+
                         '            "passwordPolicies": "DisablePasswordExpiration",            "passwordProfile": null,            "postalCode": "92121",            "preferredDataLocation": null,'+
                         '            "preferredLanguage": "en-US",            "surname": "Wilber",             "userType": "Member"             }           ]         } ');
        response.setStatusCode(200);
        return response;
    }

}