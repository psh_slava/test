/** 
* @author CRMCulture
* @version 1.00
* @description Test functionality
*/
@isTest
public class TaskBatchableTest {
	
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	public static User u;

	/**
	* @author CRMCulture
	* @version 1.00
	* @description Test Happy Path of code 
	*/
	public static testMethod void test_method_1() {  

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
		UserRole r = [SELECT Id FROM UserRole WHERE Name='Inside Sales'];
		User adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

		System.runAs(adminUser) {
       	User testUser = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='testUser' + UserInfo.getOrganizationId() +  '@testorg.com', 
            ManagerId = adminUser.Id);
 		insert testUser;
 		u = testUser;
 		}

 		System.runAs(u) {
 			List<Lead> leadsCreated = createData(200,3,3);

 			 Rejected_Activity_Types__c cs = new Rejected_Activity_Types__c();
 			 cs.Name='Note';
 			 insert cs;

			Test.startTest();

				String jobId = System.schedule('ScheduleTaskBatchableTest', CRON_EXP, new TaskBatchableScheduler());

				TaskBatchable tb = new TaskBatchable();
				tb.query = 'SELECT Id, First_Outreach_Date__c FROM Lead WHERE isConverted = false';
				Database.executebatch(tb);

				TaskBatchableHelper tbh = new TaskBatchableHelper();
				tbh.activityCount(leadsCreated);
				tbh.firstOutreach(leadsCreated);

			Test.stopTest();//will sync back up with async operations if future/batch called before this
			List<Lead> leadsInTest = [SELECT Activity_Count__c, First_Outreach_Date__c FROM Lead WHERE Id IN :leadsCreated];
			//System.assertEquals(6, leadsInTest[0].Activity_Count__c);
			//System.assertEquals(Date.newInstance(2020, 10, 10),leadsInTest[0].First_Outreach_Date__c);
		}
	}/**
	* @author CRMCulture
	* @version 1.00
	* @description Test Exceptions To code 
	*/
	public static testMethod void test_method_2() {  
		//Construct most objects and DML in TestingUtility.TestData class
		TestingUtility.TestData td = new TestingUtility.TestData(); 
		// Use td.Accounts[0]...etc for DML 
		//ApexPages.StandardController sc = new ApexPages.StandardController(td.Accounts[0]);
		//SubProjectViewController cn = new SubProjectViewController(sc);
		Test.startTest();
		Test.stopTest();//will sync back up with async operations if future/batch called before this
	}/**
	* @author CRMCulture
	* @version 1.00
	* @description Test With base user
	*/
	public static testMethod void test_method_3() {  
		//Construct most objects and DML in TestingUtility.TestData class
		TestingUtility.TestData td = new TestingUtility.TestData(); 
		// Use td.Accounts[0]...etc for DML 
		User adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
       	User salesUser = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testUser' + UserInfo.getOrganizationId() +  '@testorg.com',
            ManagerId = adminUser.Id);
 		insert salesUser;
		//ApexPages.StandardController sc = new ApexPages.StandardController(td.Accounts[0]);
		//SubProjectViewController cn = new SubProjectViewController(sc);
		Test.startTest();
		System.runAs(salesUser){
			//Execute your test script within here
         System.debug('Current User: ' + UserInfo.getUserName());
         System.debug('Current Profile: ' + UserInfo.getProfileId());
		}
		Test.stopTest();//will sync back up with async operations if future/batch called before this
	}


	public static List<Lead> createData(Integer numLeads, Integer numTasks, Integer numEvents) {

		List<Lead> leadsToInsert = new List<Lead>();
		List<Task> tasksToInsert = new List<Task>();
		List<Event> eventsToInsert = new List<Event>();

		for(Integer i = 0; i < numLeads; i++) {

			Lead l = new Lead();
			l.LastName = 'Test';
			l.Company = 'Test';
			leadsToInsert.add(l);
		}

		insert leadsToInsert;

		for(Integer i = 0; i < numLeads; i++) {
			Id leadId = leadsToInsert[i].Id;

			for(Integer j = 0; j < numTasks; j++) {
				Task t = new Task();
				t.ActivityDate = Date.newInstance(2010, 10, 10)+j;
				t.OwnerId = UserInfo.getUserId();
				t.Subject = 'Test';
				t.Status = 'Completed';
				t.WhoId = leadId;
				tasksToInsert.add(t);
			}

			for(Integer k = 0; k < numEvents; k++) {
				Event e = new Event();
				e.ActivityDate = Date.newInstance(2010, 10, 10)+k;
				e.IsAllDayEvent = true;
				e.OwnerId = UserInfo.getUserId();
				e.Subject = 'Test';
				e.WhoId = leadId;
				eventsToInsert.add(e);
			}
		}

		insert tasksToInsert;
		insert eventsToInsert;

		return leadsToInsert;

	}
}