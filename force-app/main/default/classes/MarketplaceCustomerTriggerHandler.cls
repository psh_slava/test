/*
* @class name:   MarketplaceCustomerTriggerHandler
* @created:      By Naomi Harmon in May 2020
* @test class:   MarketplaceCustomerTriggerHandlerTest.apxc
* @initiated by: MarketplaceCustomerTrigger.apxt
* @description: 
*    Converts the customer Marketplace Customer object record to standard Account and Contact records and links those standard records to the custom record
* @modifcation log:
*
*/

public class MarketplaceCustomerTriggerHandler {
    
    public void convertCustomer(List<Marketplace_Customer__c> customers){
        //Summary:
        //When a new Marketplace Customer record is created,
        //convert that person to a Salesforce Account & Contact
        //1. First look to see if that Contact (Exact email match) already exists. 
        //2. If Contact exists, connect this Contact and its Account to the customer record
        //3. If Contact does not exist, check Salesforce for an exact Account name match
        //4. If Account exists, create this Contact under that Account and attach both to the customer record
        //5. If Account does not exist, create both the Account and then this Contact under that Account and attach both to the customer record
        Set<String> customerEmails = new Set<String>();
        Set<String> customerCompanies = new Set<String>();
        Map<String, Marketplace_Customer__c> emailToCustomerMap = new Map<String,Marketplace_Customer__c>();
        for(Marketplace_Customer__c customer : customers){
            customerEmails.add(customer.Email__c);
            customerCompanies.add(customer.Company_Name__c);
            emailToCustomerMap.put(customer.Email__c, customer);
        }
        
        List<Contact> existingContacts;
        if(customerEmails != null && customerEmails.size() > 0){
            try{
                existingContacts = [Select Id, Email, AccountId, Account.Name
                                    From Contact
                                    Where Email IN :customerEmails];
            } catch (Exception e){
                System.debug('Exception caught while looking for existing Contacts with email IN ' + customerEmails);
            }    
        }
        
        List<Account> existingAccounts;
        if(customerCompanies != null && customerCompanies.size() > 0){
            try{
                existingAccounts = [Select Id, Name
                                    From Account
                                    Where Name IN :customerCompanies];
            } catch (Exception e){
                System.debug('Exception caught while looking for existing Accounts with name IN ' + customerCompanies);
            }    
        }
        
        for(String email : emailToCustomerMap.keySet()){
            Marketplace_Customer__c thisCustomer = emailToCustomerMap.get(email);
            Boolean thisCustomerExists = false;
            Boolean thisAccountExists = false;
            //1. First look to see if that Contact (Exact email match) already exists. 
            if(existingContacts != null && existingContacts.size() > 0){
                for(Contact c : existingContacts){
                    if(c.Email == email){
                        //2. If Contact exists, connect this Contact and its Account to the customer record
                        thisCustomerExists = true;
                        System.debug('Email match for customer ' + thisCustomer.Id + ' was found on Contact Id ' + c.Id);
                        thisCustomer.Account__c = c.AccountId;
                        thisCustomer.Contact__c = c.Id;
                    }
                }
            }
            if(thisCustomerExists == false){
                //3. If Contact does not exist, check Salesforce for an exact Account name match
                if(existingAccounts != null && existingAccounts.size() > 0){
                    for(Account a : existingAccounts){
                        if(thisCustomer.Company_Name__c == a.Name){
                            //4. If Account exists, create this Contact under that Account and attach both to the customer record
                            thisAccountExists = true;
                            System.debug('Account match for customer ' + thisCustomer.Id + ' was found on Account Id ' + a.Id);
                            thisCustomer.Account__c = a.Id;
                            thisCustomer.Contact__c = createNewContact(thisCustomer, a.Id);
                        }
                    }
                }
                if(thisAccountExists == false){
                    //5. If Account does not exist, create both the Account and then this Contact under that Account and attach both to the customer record
                    Id newAccountId = createNewAccount(thisCustomer);
                    thisCustomer.Account__c = newAccountId;
                    thisCustomer.Contact__c = createNewContact(thisCustomer, newAccountId);
                }
                
            }
        }
        
    }
    
    public Id createNewAccount(Marketplace_Customer__c customer){
        System.debug('Creating new Account for customer Id ' + customer.Id);
        Account newAccount = new Account();
        //Map all fields
        newAccount.Name = customer.Company_Name__c;
        newAccount.BillingStreet = customer.Street_Address__c;
        newAccount.BillingCity = customer.City__c;
        newAccount.State_External_Source__c = customer.State_or_Province__c;
        newAccount.BillingPostalCode = customer.Postal_code__c;
        newAccount.Country_External_Source__c = customer.Country__c;
        newAccount.AccountSource = 'Web Direct';
        try{
            insert newAccount;
        } catch (Exception e){
            customer.Conversion_Error__c = e.getMessage();
            System.debug('Exception caught while inserting new Account...' + e.getMessage());
        }
        return newAccount.Id;
    }
    
    public Id createNewContact(Marketplace_Customer__c customer, Id accountId){
        System.debug('Creating new Contact for customer Id ' + customer.Id);
        Contact newContact = new Contact();
        //Map all fields
        newContact.FirstName = customer.First_Name__c;
        newContact.LastName = customer.Last_Name__c;
        newContact.AccountId = accountId;
        newContact.Email = customer.Email__c;
        newContact.LeadSource = 'Web Direct';
        newContact.Title = customer.Title__c;
        newContact.Phone = customer.Phone__c;
        try{
            insert newContact;
        } catch (Exception e){
            customer.Conversion_Error__c = e.getMessage();
            System.debug('Exception caught while inserting new Contact...' + e.getMessage());
        }
        return newContact.Id;
    }
}