trigger RequestTrigger on Request__c (after insert, after update) {
    
    if(Trigger.isInsert){
        CommunityGroups.setGroupAccess(Trigger.New);
        CommunityGroups.alertGroupManagers(Trigger.New);  
        
    }
    
    if(Trigger.isUpdate){
        
        List<Request__c> forAlertHandler = new List<Request__c>();  
        List<Request__c> forApprovalHandler = new List<Request__c>();  
        for(Request__c r : Trigger.new){
            if((Trigger.oldMap.get(r.Id).Status__c != 'New' && Trigger.newMap.get(r.Id).Status__c == 'New') ||
               (Trigger.oldMap.get(r.Id).Contact__c == null && Trigger.newMap.get(r.Id).Status__c != null && r.Status__c == 'New')){
                forAlertHandler.add(r);
            }
            
            if(Trigger.oldMap.get(r.Id).Status__c != 'Accepted' && Trigger.newMap.get(r.Id).Status__c == 'Accepted'){
                forApprovalHandler.add(r);
            }
        }
        if(forAlertHandler.size()>0){ CommunityGroups.alertGroupManagers(forAlertHandler); }
        if(forApprovalHandler.size()>0){ CommunityGroups.setGroupAccess(forApprovalHandler); }
        
        //On Partner Deal Registration requests, when an Opportunity is linked, auto-create Partner Involvement
        List<Request__c> dealRegRequests = new List<Request__c>();
        for(Request__c r : Trigger.new){
            if(Trigger.oldMap.get(r.Id).Opportunity__c != Trigger.newMap.get(r.Id).Opportunity__c && Trigger.newMap.get(r.Id).Opportunity__c != null){
                dealRegRequests.add(r);
            }
        }
        if(dealRegRequests.size()>0){
            PartnerInvolvementTriggerAction.autoCreateInvolvement(dealRegRequests);
        }
    
    
    }    

}