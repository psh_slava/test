public class UserTriggerAction {
    
    public void addPartnerUserToPublicGroup(List<User> partnerUsers){
        
        List<Group> prmGroups = [SELECT Id, DeveloperName, Name 
                                 FROM Group
                                 Where Name LIKE 'Partner Community Users -%'];
        Map<String,Id> regionToGroupMap = new Map<String,Id>();  
        
        for(Group g : prmGroups){
            regionToGroupMap.put(g.Name.substringAfter('- '), g.Id);
        }
        System.debug('RegionToGroupMap = ' + regionToGroupMap);
        
        List<Profile> userProfiles = [Select Name, Id, UserType, UserLicenseId
                                      From Profile];
        Map<Id, String> profileIdToNameMap = new Map<Id, String>();
        
        for(Profile p : userProfiles){
            profileIdToNameMap.put(p.Id, p.Name);
        }
        System.debug('ProfileIdtoNameMap = ' + profileIdToNameMap);
        
        List<GroupMember> newGroupMembers = new List<GroupMember>();
        
        for(User pU : partnerUsers){
            Id correspondingGroupId;
            String profileName = profileIdToNameMap.get(pu.ProfileId);
            if(profileName.contains('TAP')){
                System.debug('Partner user is TAP region');
                correspondingGroupId = regionToGroupMap.get('TAP');
            }
            else if(pu.Global_Region__c != null){
                System.debug('Partner user Global Region = ' + pU.Global_Region__c);
                correspondingGroupId = regionToGroupMap.get(pu.Global_Region__c);
            }
            System.debug('This corresponding Group Id = ' + correspondingGroupId);
            if(correspondingGroupId != null){
                GroupMember thisMemb = new GroupMember();
                thisMemb.GroupId = correspondingGroupId;
                thisMemb.UserOrGroupId = pu.Id;
                newGroupMembers.add(thisMemb);
            }
        }
        
        if(newGroupMembers != null && newGroupMembers.size() > 0){
            System.debug('Add Public Group members...' + newGroupMembers);
            insert newGroupMembers;
        }
        
    }

}