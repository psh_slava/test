trigger Hosted_InstanceTrigger on Hosted_Instance__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {
        
        // Author: Naomi Harmon
        // Last updated: 8/2019
        
        //Initiate sync with CSM
        TriggerFactory.createTriggerDispatcher(Hosted_Instance__c.sObjectType);

}