@isTest

public class ChecklistTriggerHandlerTest {
    
    public static testmethod void testCreate(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='testerchecklisttriggerhandler@test.test', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testerchecklisttriggerhandler@test.test');
        insert u;
        
        String partnerRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        String partnerChecklistRTId = Schema.SObjectType.Checklist__c.getRecordTypeInfosByName().get('Partner Onboarding Checklist').getRecordTypeId();
        partnerChecklistRTId = String.valueOf(partnerChecklistRTId).subString(0,15);
        System.debug(partnerChecklistRTId);
        
        List<Checklist_Activities__c> customSettingList = new List<Checklist_Activities__c>();
        Checklist_Activities__c ca = new Checklist_Activities__c(Checklist_Record_Type_Id__c = partnerChecklistRTId,
                                                                Due_Date_Days_from_Today__c = 7,
                                                                Item_Order_Number__c = 1, Name = 'test1', Priority__c = 'Normal',
                                                                Stage_Section__c = 'Initial',
                                                                Subject_Label__c = 'Do This');
        customSettingList.add(ca);
        Checklist_Activities__c ca2 = new Checklist_Activities__c(Checklist_Record_Type_Id__c = partnerChecklistRTId,
                                                                Due_Date_Days_from_Today__c = 14,
                                                                Item_Order_Number__c = 2, Name = 'test2', Priority__c = 'Normal',
                                                                Stage_Section__c = 'Approved',
                                                                Subject_Label__c = 'Do That');
        customSettingList.add(ca2);
        insert customSettingList;
        
        Account a = new Account(name='Test Account', RecordTypeId = partnerRTId, OwnerId = u.Id);
        insert a;
        
        Checklist__c ckl = new Checklist__c(Account__c = a.Id, RecordTypeId = partnerChecklistRTId);
        insert ckl;
    }

}