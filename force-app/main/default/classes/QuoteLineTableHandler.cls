public class QuoteLineTableHandler {
    
    public static string getTableBody(List<SBQQ__QuoteLine__c> lines){
        String htmlbody = '';
        Integer columnNum = 2;
        
        //Get Quote so that we can pull status and conditional print columns
        SBQQ__Quote__c q = [Select Id, ApprovalStatus__c, Product_Column_Discount__c, Product_Column_Net_Unit_Price__c,
                            Product_Column_List_Price__c, Total_Contract_Value__c
                            From SBQQ__Quote__c
                            Where Id = :lines[0].SBQQ__Quote__c];
        
        //open table
        htmlBody = '<table border="1" style="border-collapse: collapse"><caption></caption>'+
            '<tr><th>Product Description</th>'+
            '<th>Qty</th>';
        if(q.Product_Column_List_Price__c == true){
            htmlBody += '<th>List Unit Price</th>';
            columnNum++;
        }
        if(q.Product_Column_Discount__c == true && q.ApprovalStatus__c == 'Approved'){
            htmlBody += '<th>Discount</th>';
            columnNum++;
        }
        if(q.Product_Column_Net_Unit_Price__c == true && q.ApprovalStatus__c == 'Approved'){
            htmlBody += '<th>Net Unit Price</th>';
            columnNum++;
        }
        htmlBody += '<th>First Year Amount</th></tr>';
        
        //format currency values in table according to string below
        List<String> args = new String[]{'0','number','###,###,##0.##'};
            String currencySymbol;
        String point = '.';
        Decimal fyTotalDec = 0;
        Decimal tcvDec = 0;
        if(q.ApprovalStatus__c == 'Approved'){ tcvDec = q.Total_Contract_Value__c; }
        
        //iterate over list and output columns/data into table rows
        for(SBQQ__QuoteLine__c line : lines){
            if(line.CurrencyIsoCode == 'USD'){ currencySymbol = '$ '; }
            if(line.CurrencyIsoCode == 'EUR'){ currencySymbol = '€ '; }
            if(line.CurrencyIsoCode == 'GBP'){ currencySymbol = '£ '; }
            else if( currencySymbol == null){ currencySymbol = line.CurrencyIsoCode + ' '; }
            String productDesc = line.SBQQ__Description__c;
            Decimal quantDec = line.SBQQ__Quantity__c.setScale(0);
            Decimal netPriceDec = line.Unit_Price__c;
            Decimal listPriceDec = line.List_Unit_Price__c;
            String discountPrint = line.Discount_Print__c;
            Decimal fyAmountDec = 0;
            //if(line.Price_Per_Year__c != null){ fyAmountDec = line.Price_Per_Year__c; }
            if(line.SBQQ__Quote__r.ApprovalStatus__c != 'Approved'){ fyAmountDec = line.List_Per_Year__c; }
            else if(line.SBQQ__Quote__r.ApprovalStatus__c == 'Approved' && line.Price_Per_Year__c != null){ fyAmountDec = line.Price_Per_Year__c; }
            //format decimal variables 
            String quant = String.format(quantDec.format(), args);
            String netPrice = currencySymbol + String.format(netPriceDec.format(), args);
            if(!netPrice.contains(point)){ netPrice += '.00'; }
            if(netPrice.substring(netPrice.indexOf(point)).length() == 1){ netPrice += '0'; } 
            String listPrice = currencySymbol + String.format(listPriceDec.format(), args);
            if(!listPrice.contains(point)){ listPrice += '.00'; }
            if(listPrice.substring(listPrice.indexOf(point)).length() == 1){ listPrice += '0'; } 
            String fyAmount = currencySymbol + String.format(fyAmountDec.format(), args);
            if(!fyAmount.contains(point)){ fyAmount += '.00'; }
            if(fyAmount.substring(fyAmount.indexOf(point)).length() == 1){ fyAmount += '0'; } 
            fyTotalDec += fyAmountDec;
            if(line.SBQQ__Quote__r.ApprovalStatus__c != 'Approved'){ tcvDec += line.SBQQ__ListTotal__c; }
            
            htmlBody += '<tr><td width = "75%">' + productDesc + '</td><td width="5%" align="center">' + quant + '</td>';
            if(q.Product_Column_List_Price__c == true){
                htmlBody += '<td width="5%" align="center">' + listPrice + '</td>';
            }
            if(q.Product_Column_Discount__c == true && q.ApprovalStatus__c == 'Approved'){
                htmlBody += '<td width="5%" align="center">' + discountPrint + '</td>';
            }
            if(q.Product_Column_Net_Unit_Price__c == true && q.ApprovalStatus__c == 'Approved'){
                htmlBody += '<td width="5%" align="center">' + netPrice + '</td>';
            }
            htmlBody += '<td width="5%" align="right">' + fyAmount + '</td></tr>' ;
        }
        
        //format footer decimal values
        String fyTotal = currencySymbol + String.format(fyTotalDec.format(), args);
        if(!fyTotal.contains(point)){ fyTotal += '.00'; }
        if(fyTotal.substring(fyTotal.indexOf(point)).length() == 2){ fyTotal += '0'; }
        String totalContractValue = currencySymbol + String.format(tcvDec.format(), args);
        if(!totalContractValue.contains(point)){ totalContractValue += '.00'; }
        if(totalContractValue.substring(totalContractValue.indexOf(point)).length() == 2){ totalContractValue += '0'; }
        
        //footer row
        htmlBody += '<tr><td colspan="'+ columnNum +'" align="right"><b>First Year Total:</br></td><td align="right">'+ fyTotal +'</td></tr>' +
            '<tr><td colspan="'+ columnNum +'" align="right"><b>Total Contract Value:</b></td><td align="right">'+ totalContractValue + '</td></tr>';
        
        //close table
        htmlBody += '</table>';        
        return htmlBody;
    }
    
}