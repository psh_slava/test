/** 
* CSM_License_Record_AfterInsert
* @description  Class to handle Trigger handler
* @return void
*/
public class CSM_License_Record_AfterInsert  extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author NaomiHarmon
    * @date 20190528
    * @version 2.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void 
    */
    public override void mainEntry(TriggerParameters tp) {
        List<SObject> toUpdate = new List<SObject>();
          for(SObject so : tp.newList){
                if(so.getSObjectType()==CSM_License_Record__c.SObjectType){
                    if(so.get('License_Type__c')=='CSM'){
                        toUpdate.add(so);
                    }
                }
            }  
        if(toUpdate.size() > 0){
            if (TriggerHelper.DoExecute('CSM_License_Record__c.CSMSync') && TriggerHelper.DoExecute('Outbound.CSMSync')) {
             CSMSyncEntryHelper.enqueueSyncEntry(tp.oldMap, toUpdate); 
            }   
        }
        if (TriggerHelper.DoExecute('CSM_License_Record__c.CentralSync') && TriggerHelper.DoExecute('Outbound.CSMSync')) {
             CentralSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.newList); 
            }
    }
}