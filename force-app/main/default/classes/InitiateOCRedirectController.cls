/*
* @class name:   InitiateOCRedirectController
* @created:      By Naomi Harmon in Sep 2020
* @test class:   InitiateOCRedirectControllerTest.apxc
* @initiated by: InitiateOCRedirect.vfp
* @description: 
*    Evalutes Quote record to see if "Convert to Order Confirmation" checkbox is checked, and  
*    redirects user accordingly
* @modifcation log:
*
*/

public class InitiateOCRedirectController {
    public SBQQ__Quote__c quote;

    public InitiateOCRedirectController(ApexPages.StandardController sc) {
        this.quote = (SBQQ__Quote__c)sc.getRecord();
    }
    
    public PageReference evaluateOC() {

        //Safeguard against quotes being sent - should only be Order Confirmations
        if (quote.Convert_to_Order_Confirm__c == false){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'This Quote has not been converted to an Order Confirmation. <br> '+
                                                            'Please return to your Quote, check the Convert to Order Confirmation box and fill out the required fields. '+
                                                            'Then, Initiate OC Document once again. <br>'+
                                                            'If you have any issues, please contact your Sales Ops team. ');
            ApexPages.addMessage(myMsg); 
            return null;
        }
        
        //Safeguard against non-primary OCs being sent - should only be from Quote records that are Primary
        if (quote.SBQQ__Primary__c == false){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Order Confirmations can only be sent from Quotes marked Primary. Please visit the Primary Quote record or Go Back and mark this Quote as Primary before sending.');
            ApexPages.addMessage(myMsg); 
            return null;
        }
        
        //Redirect to DocuSign CLM
        Boolean runningInASandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        String redirectURL;
        if(runningInASandbox){
         redirectURL = 'https://uatna11.springcm.com/atlas/doclauncher/eos/Order Confirmation?aid=14817&eos[0].Id=' + 
                    quote.Id + '&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=' + quote.Name + '&eos[0].ScmPath=/Salesforce/Quote/';
           //redirectURL = 'https://uatna11.springcm.com/atlas/doclauncher/eos/Order Confirmation?aid=14817&eos[0].Id={!SBQQ__Quote__c.Id}&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name={!SBQQ__Quote__c.Quote_Name__c}&eos[0].ScmPath=/Salesforce/Quote/' ;

  
        }
        if(!runningInASandbox){
               redirectURL = 'https://na11.springcm.com/atlas/doclauncher/eos/Order Confirmation?aid=21218&eos[0].Id=' + 
                    quote.Id + '&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=' + quote.Name + '&eos[0].ScmPath=/Salesforce/Quote/';
                   
        }
        
        PageReference clmPage = new PageReference(redirectUrl);
        System.debug('Redirect page to------'+clmPage);
        clmPage.setRedirect(true);
        return clmPage;
        
    }
    
}