/*
* @class name:   ContractTriggerHandler
* @created:      By Naomi Harmon in OCt 2019
* @test class:   ContractTriggerTest.apxc
* @initiated by: ContractTrigger.apxt
* @description: 
*    Initiaties related methods/classes from the Contract Trigger
* @modifcation log:
*    Updated by Naomi Harmon in Apr 2020  - added initiation of BookingsProcessor methods, triggered by the checking of the Create Booking checkbox field or by updates to existing Subscription lines' amounts
*
*/

public class ContractTriggerHandler {
    
    
    public void onBeforeInsert(List<Contract> newList){
        for(Contract c : newList){
            ManageHostedInstances.createHostedInstances(c);
        }
        
        ContractTriggerAction.carryOverContractTerms(null, null, newList);
        
    }
    
    public void onBeforeUpdate(Map<Id, Contract> oldMap, Map<Id, Contract> newMap){
        
        ContractTriggerAction.carryOverContractTerms(oldMap, newMap, null);
        
        //<<<< would like to bulkify this better in the future >>>>
        for(Id contractId : newMap.keySet()){ 
            if(oldMap.get(contractId).Process_SaaS__c == false && newMap.get(contractId).Process_SaaS__c == true){
                System.debug('Initating new Hosted Instance(s)-----');
                ManageHostedInstances.createHostedInstances(newMap.get(contractId));
            }
        }
        //<<<< >>>>
        
        List<Contract> processInvoices = new List<Contract>();
        for(Id contractId : newMap.keyset()){
            Contract c = newMap.get(contractId);
            if(oldMap.get(contractId).Net_Total_Subscriptions__c != newMap.get(contractId).Net_Total_Subscriptions__c &&
               c.EndDate > Date.today().addDays(365) &&
               BookingsProcessor.recursionControl())
            {
                processInvoices.add(c); 
            }
        }
        if(processInvoices != null && processInvoices.size()>0){
            //BookingsAction.bookMidTerm(processInvoices);
            BookingsProcessor.bookMidTerm(processInvoices);
        }
        
        
    }
    
    public void onAfterInsert(List<Contract> newList){
        for(Contract c : newList){
            if(c.SBQQ__Quote__c != null){
                ContractTriggerAction.populateContractTerms(c.Id, c.SBQQ__Quote__c);
            }
        } 
    }
    
    public void onAfterUpdate(Map<Id, Contract> oldMap, Map<Id, Contract> newMap){   
        
        //Send any new Contract through the BookingsAction to book mid-term if term is more than 1 year
        List<Contract> contractsToBook = new List<Contract>();
        for(Id contractId : newMap.keyset()){
            Contract c = newMap.get(contractId);
            if(oldMap.get(contractId).Create_Booking__c == false && 
               c.Create_Booking__c == true &&
               c.EndDate > Date.today().addDays(365) &&
               BookingsProcessor.recursionControl())
            {
                contractsToBook.add(c); 
            }
        }
        if(contractsToBook != null && contractsToBook.size()>0){
            BookingsProcessor.bookMidTerm(contractsToBook);
        }
        
        //Evaluate offsets on Subscriptions when next booking is Renewal
        List<Id> contractsToOffset = new List<Id>();
        List<Id> renewalOpps = new List<Id>();
        Date earliestEndDate = Date.today();
        
        for(Id contractId : newMap.keyset()){
            Contract c = newMap.get(contractId);        
            if(oldMap.get(contractId).SBQQ__RenewalQuoted__c == false && 
               c.SBQQ__RenewalQuoted__c == true){
                   contractsToOffset.add(c.Id);
                   renewalOpps.add(c.SBQQ__RenewalOpportunity__c);
                   if(c.EndDate < earliestEndDate){
                       earliestEndDate = c.EndDate;
                   }
               }
        }
        if(contractsToOffset != null && contractsToOffset.size()>0 && BookingsProcessor.recursionControl()){
            BookingsProcessor.offsetRenewal(contractsToOffset, renewalOpps, earliestEndDate);
        }
        
        //If Maintenance dates or support period is edited, push values to Subscriptions
        List<Id> toupdate = new List<Id>();
        for(Id contractId : newMap.keySet()){
            if((oldMap.get(contractId).Maintenance_Start_Date__c != newMap.get(contractId).Maintenance_Start_Date__c) ||
               (oldMap.get(contractId).Maintenance_End_Date__c != newMap.get(contractId).Maintenance_End_Date__c) ||
               (oldMap.get(contractId).Hosting_Model__c != newMap.get(contractId).Hosting_Model__c) ||
               (oldMap.get(contractId).Extended_Grace_Period__c != newMap.get(contractId).Extended_Grace_Period__c)){
                   toupdate.add(contractId);
               }
        }
        if(toupdate.size()>0){
            MaintenanceDates.updateSubscriptions(toUpdate);
        }
        //<<<< >>>>
        
    }        
    
}