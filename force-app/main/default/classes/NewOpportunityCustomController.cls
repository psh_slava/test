public class NewOpportunityCustomController {
  public List<Contract> LstContract { get; set; }
  public List<Contract> UpsellLstContract { get; set; }
  public List<Contract> RenewLstContract { get; set; }
  public Account a { get; set; }
  public Id accountId { get; set; }
  //public Contract ctr { get; set; }
  public Opportunity Opp { get; set; }
  public Contact cont { get; set; }
  public Id primContact { get; set; }
  list<SelectOption> options { get; set; }
  public String selectedRecordType { get; set; }
  //public id contractid { get; set; }
  ApexPages.StandardController standardController;
  public Boolean upsellHasContracts { get; set; }
  public Boolean isNew { get; set; }
  public Boolean newLogoHasContracts { get; set; }
  public Boolean noContracts { get; set; }
  public Boolean renewalHasContracts { get; set; }
  //public Boolean readyToSave { get; set; }
  public List<Recordtype> Rtlst { get; set; }

  public NewOpportunityCustomController() {
    //this.Acc = (Account) standardController.getRecord();
    isNew = true;
    Opp = new Opportunity();
    accountId = ApexPages.currentPage().getParameters().get('Id');
    if (ApexPages.currentPage().getParameters().get('conId') != '') {
      primContact = ApexPages.currentPage().getParameters().get('conId');
    }
    if (primContact == null) {
      ApexPages.Message noPrimary = new ApexPages.Message(
        ApexPages.Severity.WARNING,
        'Before you create an Opportunity, please go back and enter a Primary Contact on this Account.'
      );
      ApexPages.addMessage(noPrimary);
      isNew = false;
    }

    /* if (a.Primary_Contact__c == null) {
ApexPages.Message msg = new Apexpages.Message(
ApexPages.Severity.Error,
'This Account does not have a Primary Contact.'
);
} */

    /* try {
if (a.Primary_Contact__c == null) {
throw new Exception(
'There is no Primary Contact listed for this Account. <br>' +
'Please add a Primary Contact to this Account before creating an Opportunity.'
);
return null;
}
} catch (Exception e) {
ApexPages.Message msg = new ApexPages.Message(
ApexPages.Severity.ERROR,
e.getMessage()
);
ApexPages.addMessage(msg);
return null;
} */

    System.debug('accountid = ' + accountId);
    opp.AccountId = accountId;
    if (primContact != null) {
      opp.Primary_Contact__c = primContact;
    }
    UpsellLstContract = new List<contract>();
    RenewLstContract = new List<contract>();
    upsellHasContracts = false;
    newLogoHasContracts = false;
    noContracts = false;
    renewalHasContracts = false;
    Rtlst = [
      SELECT Name
      FROM RecordType
      WHERE
        SobjectType = 'Opportunity'
        AND IsActive = TRUE
        AND Name IN ('New Logo', 'Upsell', 'Renewal')
    ];
    //a = [SELECT id, Primary_Contact__c FROM Account WHERE id = :Acc.id];
    a = [
      SELECT id, name, Primary_Contact__c
      FROM Account
      WHERE id = :accountId
    ];
    LstContract = [
      SELECT
        id,
        ContractNumber,
        SBQQ__Opportunity__r.Name,
        Status,
        Contract_Status__c,
        SBQQ__ExpirationDate__c,
        SBQQ__RenewalOpportunity__c,
        SBQQ__RenewalForecast__c,
        CSM_License_Count__c,
        CAM_Licenses__c,
        Hosting_Model__c,
        Instances__c,
        Billing_Thru_Partner__c,
        Account.name,
        StartDate,
        EndDate,
        Name
      FROM Contract
      WHERE Accountid = :accountId
    ];
    getRecordTypes();
  }

  public pagereference doCancel() {
    // Contact C = [Select id,Accountid from contact where accountid =:acc.id limit 1];
    //opp.Primary_Contact__c = c.id;
    //PageReference pr = new PageReference('/' + acc.id);
    PageReference pr = new PageReference('/' + accountId);
    System.debug(pr);
    pr.setRedirect(true);
    return pr;
  }

  public list<SelectOption> getRecordTypes() {
    options = new List<SelectOption>();
    options.add(new SelectOption('', '--None--'));
    //Account a = [SELECT id, Account_Status__c FROM Account WHERE id = :Acc.id];
    Account a = [
      SELECT id, Account_Status__c, name
      FROM Account
      WHERE id = :accountId
    ];
    //options.add(new SelectOption(' ', ' '));

    for (RecordType rt : Rtlst) {
      if (
        rt != null &&
        a.Account_Status__c == 'Active' &&
        (rt.name == 'Upsell' ||
        rt.Name == 'Renewal')
      ) {
        options.add(new SelectOption(rt.ID, rt.Name));
      } else if (rt != null && (rt.name == 'New Logo')) {
        options.add(new SelectOption(rt.ID, rt.Name));
      }
    }
    return options;
  }

  /*   public PageReference dosubmit() {
        PageReference pr = new PageReference(
            '/apex/NewOpportunityErrorPage?lookupcmpgn=1&accid=' +
            //acc.id +
            accountId +
            '&source=Sales Generated&conrel=1&status=Active'
        );
        pr.setRedirect(true);
        return pr;
    } */

  public PageReference redirectRecordtypepage() {
    Recordtype Rt = [
      SELECT Id, Name
      FROM RecordType
      WHERE sobjecttype = 'Opportunity' AND Id = :selectedRecordType
    ];

    cont = [SELECT id, name FROM contact WHERE id = :opp.Primary_Contact__c];

    // New Logo //
    if (Rt.Name == 'New Logo') {
      if (LstContract.Size() > 0) {
        newLogoHasContracts = true;
        isNew = false;
        noContracts = false;
        renewalHasContracts = false;
        ApexPages.Message msg = new Apexpages.Message(
          ApexPages.Severity.Warning,
          'This is an active Customer who already has a Contract with Cherwell. If you are adding new products, click "Go Back" and select "Upsell" as your Opportunity record type. If you hit the "Continue" button, your new Opportunity will result in a separate Contract for this customer.'
        );
        ApexPages.addmessage(msg);
        PageReference pageRef = Page.NewOpportunityCustomPage;
        return pageRef;
      } else {
        /* PageReference pr = new PageReference(
                    '/apex/NewOpportunityErrorPage?lookupcmpgn=1&accid=' +
                    //acc.id +
                    accountId +
                    '&conname=' +
                    cont.Name +
                    '&CF00N5000000AVDuS_lkid=' +
                    cont.id +
                    '&RecordType=' +
                    selectedRecordType +
                    '&source=Sales Generated&conrel=1&status=Active&'
                ); 
                pr.setRedirect(true);
                return pr; */
        opp.Name = 'accountId';
        //opp.Primary_Contact__c = cont.id;
        opp.AccountId = accountId;
        opp.StageName = 'Pre-Qualification';
        opp.CloseDate = date.today();
        System.debug('oppId = ' + opp.Id);
        insert opp;
        PageReference resPg = new PageReference('/' + opp.id);
        resPg.setRedirect(true);

        return resPg;
      }

      // Upsell //
    } else if (Rt.Name == 'Upsell') {
      date dt = system.Today();

      for (Contract c : LstContract) {
        if (C.Status == 'Activated' && c.SBQQ__ExpirationDate__c > dt) {
          UpsellLstContract.Add(C);
        }
      }

      if (UpsellLstContract.Size() == 0) {
        noContracts = true;
        upsellHasContracts = false;
        newLogoHasContracts = false;
        isNew = false;
        renewalHasContracts = false;
        ApexPages.Message msg = new Apexpages.Message(
          ApexPages.Severity.Warning,
          'This customer has no Contracts available to upsell. Please contact the Sales Operations or Salesforce Administration team to update the customer information or for additional guidance.'
        );
        ApexPages.addmessage(msg);
        PageReference pageRef = Page.NewOpportunityCustomPage;
        return pageRef;
      } else if (UpsellLstContract.Size() >= 1) {
        /* else if (UpsellLstContract.Size() > 1) {
upsellHasContracts = true;
newLogoHasContracts = false;
isNew = false;
noContracts = false;
renewalHasContracts = false;
} */

        /* ContractAmender amender = new ContractAmender();
QuoteModel quote = amender.load(UpsellLstContract[0].id);

Opportunity Opp = [
SELECT id, SBQQ__AmendedContract__c
FROM Opportunity
WHERE SBQQ__AmendedContract__c = :UpsellLstContract[0].id
ORDER BY createdDate DESC
LIMIT 1
];

Opp.Primary_Contact__c = cont.id;
update Opp; */
        opp.Name = 'accountId';
        opp.Primary_Contact__c = cont.id;
        opp.AccountId = accountId;
        opp.StageName = 'Pre-Qualification';
        opp.CloseDate = date.today();
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName()
          .get('Upsell')
          .getRecordTypeId();
        insert opp;
        PageReference resPg = new PageReference('/' + opp.id);
        resPg.setRedirect(true);

        return resPg;
      }

      // Renewal //
    } else if (Rt.Name == 'Renewal') {
      date dt = system.Today();
      for (Contract c : LstContract) {
        if (C.Status == 'Activated') {
          RenewLstContract.Add(C);
        }
      }
      if (RenewLstContract.Size() == 1) {
        if (RenewLstContract[0].SBQQ__RenewalForecast__c == false) {
          RenewLstContract[0].SBQQ__RenewalForecast__c = true;
          // RenewLstContract[0].Primary_Renewal_Contact__c = opp.Primary_Contact__c;
          update RenewLstContract[0];
          contract c = [
            SELECT id, SBQQ__RenewalOpportunity__c
            FROM Contract
            WHERE ID = :RenewLstContract[0].id
          ];
          Opportunity op = [
            SELECT id, Primary_Contact__c
            FROM opportunity
            WHERE id = :c.SBQQ__RenewalOpportunity__c
          ];
          op.Primary_Contact__c = cont.id;
          update op;
          PageReference pr = new PageReference(
            '/' + c.SBQQ__RenewalOpportunity__c
          );
          pr.setRedirect(true);
          return pr;
        } else {
          PageReference pr = new PageReference(
            '/apex/Renewalcontractupdate?id=' +
            RenewLstContract[0].id +
            '&conname=' +
            cont.Name +
            '&CF00N5000000AVDuS_lkid=' +
            cont.id
          );
          pr.setRedirect(true);
          return pr;
        }
      } else if (RenewLstContract.Size() > 1) {
        upsellHasContracts = false;
        newLogoHasContracts = false;
        isNew = false;
        noContracts = false;
        renewalHasContracts = true;
      } else if (RenewLstContract.Size() == 0) {
        noContracts = true;
        upsellHasContracts = false;
        newLogoHasContracts = false;
        isNew = false;
        renewalHasContracts = false;
        ApexPages.Message msg = new Apexpages.Message(
          ApexPages.Severity.Warning,
          'This customer has no Contracts available to renew. Please contact the Sales Operations or Salesforce Administration team to update the customer information or for additional guidance.'
        );
        ApexPages.addmessage(msg);
        PageReference pageRef = Page.NewOpportunityCustomPage;
        return pageRef;
      }
    }

    return null;
  }

  public PageReference goBack() {
    PageReference pg = new PageReference(
      '/apex/NewOpportunityCustomPage?id=' +
      //Acc.id +
      accountId +
      '&conname=' +
      cont.Name +
      '&CF00N5000000AVDuS_lkid=' +
      cont.id
    );
    pg.setRedirect(true);
    return pg;
  }

  public PageReference Pageredirdctopp() {
    opp.Name = 'accountId';
    //opp.Primary_Contact__c = cont.id;
    opp.AccountId = accountId;
    opp.StageName = 'Pre-Qualification';
    opp.CloseDate = date.today();
    insert opp;
    PageReference resPg = new PageReference('/' + opp.id);
    resPg.setRedirect(true);

    return resPg;
    /*PageReference resPg = new PageReference(
'/006/e?accId=' +
accountId +
'&opp11=Pre-Qualification' +
'&opp3=' +
a.name +
'&CF00N5000000AVDuS_lkid=' +
cont.id +
'&conId=' +
cont.id
); 
resPg.setRedirect(true);
return resPg; */
  }

  /*   public PageReference Saveto() {
        //opportunity opp = new opportunity();//
        //opp.AccountId = accountId;//
        opp.Primary_Contact__c = primContact;
        insert opp;
        PageReference resPg = new PageReference('/' + opp.id);
        resPg.setRedirect(true);
        return resPg;
    } */
}