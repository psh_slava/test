global class CSMSyncEntryHistoryPurgeBatch implements Database.Batchable<sObject> {
	
	private Integer purgeNDays;
	
	global CSMSyncEntryHistoryPurgeBatch(Integer purgeNDays) {
		this.purgeNDays = purgeNDays;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id FROM CSM_Sync_Entry_History__c WHERE CreatedDate < LAST_N_DAYS:' +
			String.valueOf(purgeNDays);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}