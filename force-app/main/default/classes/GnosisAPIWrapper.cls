/*
* @class name:   GnosisAPIWrapper
* @created:      By Naomi Harmon in Oct 2020
* @test class:   GnosisAPIControllerTest.apxc
* @initiated by: GnosisAPIController.apxc
* @description: 
*    Client side stub for API with Gnosis LMS
* @modifcation log:
*           
*/

public class GnosisAPIWrapper {
    
    public class GnosisAPIWrapperException extends Exception {}
    
    //*Private helper method
    private static String invoke(String operation, String method, String requestBody){
        
        //Access authentication values stored in Gnosis API Settings custom setting           
        Gnosis_API_Settings__c apiSettings;
        // Assume a separate set of settings for Sandbox and Production environments.  This will protect against
        // inadvertent access to production from a sandbox environment when the sandbox is refreshed with data from
        // production. 
        Boolean runningInASandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        if(Test.isRunningTest()){ runningInASandbox = true; }
        apiSettings = Gnosis_API_Settings__c.getInstance(runningInASandbox ? 'Sandbox' : 'Production');
          
        String endpointURL = apiSettings.Endpoint_URL__c;
        String authToken = apiSettings.Auth_Token__c;
        System.debug('Endpoint URL === '+endpointURL);      
        System.debug('Authorization Token === ' + authToken);
        
        //Add auth token and function to body
        String fullBody;
        fullBody  = 'wstoken=' + authToken + '&';
        fullBody += 'wsfunction=' + operation + '&';
        fullBody += 'wsrestformat=json&';
        fullBody += requestBody;
        System.debug('Full Body === ' + fullBody);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointURL);
        request.setMethod(method);
        request.setHeader('Accept', '*/*');
        request.setHeader('Content-Length','0');
        request.setHeader('Host','cherwelluat.gnosisconnect.com');
        request.setBody(fullBody);
        
        HTTPResponse response = http.send(request);
        
        if (response.getStatusCode() == 200) {
            String responseBody = response.getBody();
            return responseBody;
        } 
        if (response.getStatusCode() == 429) {
            return '429';        
        }
        else {
            system.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus() + ' ' + response.getBody());
            throw new GnosisAPIWrapperException('Problem invoking operation (' + operation + '): ' + response);          
        }
    }
    
    //*Public methods
    //Convert record information to correct parameters
    public static String formatPurchaseBody(Asset a){

        List<String> allParams = new List<String>();             
        //allParams.add('wsfunction=core_user_create_company');
        //allParams.add('wsrestformat=json');
        
        //Convert date parameters to correct format
        String asset_purchase_date = String.valueOf(a.PurchaseDate.month()) + '/' + String.valueOf(a.PurchaseDate.day()) + '/' + String.valueOf(a.PurchaseDate.year());
        allParams.add('asset_purchase_date='+asset_purchase_date);        
        String asset_expiry_date = String.valueOf(a.Expiry_Date__c.month()) + '/' + String.valueOf(a.Expiry_Date__c.day()) + '/' + String.valueOf(a.Expiry_Date__c.year());
        allParams.add('asset_expiry_date='+asset_expiry_date);
        
        allParams.add('asset_product_code='+a.StockKeepingUnit);
        allParams.add('sf_contact_id='+a.ContactId);
        allParams.add('account_record_type='+a.Account_Record_Type__c);
        allParams.add('ct_first_name='+a.Contact.FirstName);
        allParams.add('ct_last_name='+a.Contact.LastName);
        allParams.add('ct_email='+a.Contact.Email);
        // allParams.add('account_name='+a.Account.Name);
        allParams.add('account_name='+ EncodingUtil.URLENCODE(a.Account.Name,'UTF-8'));
        allParams.add('credit_point='+String.valueOf(a.Number_of_Credits__c));
        allParams.add('sf_sale_id='+a.Id);
        allParams.add('sf_account_id='+a.AccountId);
        // !! This parameter should be removed
        allParams.add('account_max_users='+10000); 
        // !!
        allParams.add('sale_value='+a.Price_USD__c);
        allParams.add('sf_account_csa_email='+a.Account.Customer_Success_Advocate__r.Email);
        //allParams.add('source='+a.Source__c);
        //allParams.add('trans_id='+a.Transaction_Id__c);
        
        // Requirement: We will continue to use the Partner Business Unit to house all partners. 
        // We will need to create three separate teams:
        // 1.  Authorized Partner (Channel)
        // 2.  Managed Service Provider (MSP)
        // 3.  Alliance Partner (TAP) 
        // The team designation will need to be populated by the integration for partners, 
        // allowing for assignment to multiple teams if required by SFDC designation. 
        //       
        List<String> partnerTeams = new List<String>();
        if(a.Account.Partner_Types__c != null){
            String[] accountPartnerTypes = a.Account.Partner_Types__c.split(';');
            Set<String> accountPartnerTypesSet = new Set<String>(accountPartnerTypes);
            // 3.  Alliance Partner (TAP) 
            if(accountPartnerTypes.contains('Technology Alliance Partner (TAP)') || a.Account.Type == 'Alliance Partner'){ 
                partnerTeams.add('Alliance Partner (TAP)'); 
            }
            // 2.  Managed Service Provider (MSP)
            if(accountPartnerTypes.contains('Managed Service Provider Partner (MSP)') || a.Account.Type == 'MSP'){ 
                partnerTeams.add('Managed Service Provider (MSP)'); 
            }
            // 1.  Authorized Partner (Channel)
            if(accountPartnerTypesSet.contains('Technology Alliance Partner (TAP)')){
                accountPartnerTypesSet.remove('Technology Alliance Partner (TAP)');
            }
            if(accountPartnerTypesSet.contains('Managed Service Provider Partner (MSP)')){
                accountPartnerTypesSet.remove('Managed Service Provider Partner (MSP)');
            }
            if(a.Account.Type == 'Authorized Partner' || (a.Account_Record_Type__c == 'Partner' && accountPartnerTypesSet.size() > 0)){
                partnerTeams.add('Authorized Partner (Channel)');
            }
        }
        allParams.add('partner_types_c='+partnerTeams);
        
        String body = String.join(allParams, '&');
        
        System.debug('Formatted Body === ' + body);
        return body;
    }//End formatPurchaseBody
    
    //Take Asset information any new Learning Services purchase and send that information to Gnosis LMS 
    public static String sendPurchaseDetails(String scope, Asset assetRecord){
       
        String operation = 'core_user_create_company';        
        String method = 'POST';
        
        //'scope' is already formatted using above formatBody method
        String responseBody = invoke(operation, method, scope);
        System.debug('DEBUG: responseBody ' + responseBody); 
        return responseBody;
        
    }//End sendPurchaseDetails
    
    //Convert record information to correct parameters
    public static String formatStatusBody(Contact c, String thisStatus){
        
        List<String> allParams = new List<String>();             
        allParams.add('sf_contact_id='+c.Id);   
        
        //User Status (0 for inactive, 1 for active) 
        Integer statusBoolean;
        if(thisStatus == 'Active'){ statusBoolean = 1; }
        if(thisStatus == 'Inactive'){ statusBoolean = 0; }
        
        allParams.add('active_status='+statusBoolean);
        
        String body = String.join(allParams, '&');   
        System.debug('Formatted Body === ' + body);
        return body;
    }//End formatStatusBody
    
    //When a Contact is inactivated, send status updates to Gnosis LMS
    public static String sendContactStatus(String scope, Contact person){
        
        String operation = 'core_user_update_contact_status';        
        String method = 'POST';
        
        //'scope' is already formatted using above formatBody method
        String responseBody = invoke(operation, method, scope);
        System.debug('DEBUG: responseBody ' + responseBody);
        return responseBody;
        
    }//End sendContactStatus
 
    //Convert record information to correct parameters
    public static String formatAccountStatusBody(Account a, String thisStatus){
        
        List<String> allParams = new List<String>();             
        allParams.add('sf_contact_id='+a.Id);   
        
        //User Status (0 for inactive, 1 for active) 
        Integer statusBoolean;
        if(thisStatus == 'Active'){ statusBoolean = 1; }
        if(thisStatus == 'Inactive'){ statusBoolean = 0; }
        
        allParams.add('active_status='+statusBoolean);
        
        String body = String.join(allParams, '&');   
        System.debug('Formatted Body === ' + body);
        return body;
    }//End formatAccountStatusBody
    
    //When an Account is inactivated, send status updates to Gnosis LMS
    public static String sendAccountStatus(String scope, Account company){
        
        String operation = 'core_user_update_account_status';        
        String method = 'POST';
        
        //'scope' is already formatted using above formatBody method
        String responseBody = invoke(operation, method, scope);
        System.debug('DEBUG: responseBody ' + responseBody);
        return responseBody;
        
    }//End sendAccountStatus
    
    //Convert record information to correct parameters
    public static String formatContactDetailsBody(Contact c){
        List<String> allParams = new List<String>();             
        allParams.add('sf_contact_id='+c.Id); 
        allParams.add('ct_first_name='+c.FirstName);
        allParams.add('ct_last_name='+c.LastName);
        allParams.add('ct_email='+c.Email);
        allParams.add('account_name='+EncodingUtil.URLENCODE(c.Account_Name__c,'UTF-8'));
        allParams.add('account_record_type='+c.Account_Record_Type__c);
        allParams.add('sf_account_id='+c.AccountId);
        allParams.add('partner_types_c='+c.Account.Partner_Types__c);
        
        String body = String.join(allParams, '&');
        
        System.debug('Formatted Body === ' + body);
        return body;
    }//End formatContactDetailsBody
    
    //When a Contact's relevant details change, send updated information to Gnosis LMS
    public static String sendContactDetails(String scope, Contact person){
        String operation = 'core_user_create_update_contact';        
        String method = 'POST';
        
        //'scope' is already formatted using above formatBody method
        String responseBody = invoke(operation, method, scope);
        System.debug('DEBUG: responseBody ' + responseBody); 
        return responseBody;
    }//End sendContactDetails
}