//** Client side stub for Azure AD (Internal Directory) API.
//** Author: Naomi Harmon
//** Last updated: 7/2019

public class AzureAPIWrapper {
    
    public class AzureAPIWrapperException extends Exception {}
    
    private static final Integer requestTimeout = 120000;
    private static Azure_API_Settings__c apiSettings;
    private static Boolean newAccessToken = false;
    public static String accessToken;
    
    static {
        // Assumes a separate set of settings for Sandbox and Production environments.  This will protect against
        // inadvertent access to production from a sandbox environment when the sandbox is refreshed with data from
        // production. 
        Boolean runningInASandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        apiSettings = Azure_API_Settings__c.getInstance(runningInASandbox ? 'Sandbox' : 'Production');
        if(test.isRunningTest()){ apiSettings = Azure_API_Settings__c.getInstance('Test'); }
    }
    
    //*Start of private helper methods
    
    //private static void setAccessToken() {
    private static string getAccessToken(){
        //Get a fresh token for each call
        HttpRequest request = new HttpRequest();
        request.setEndpoint(String.format('{0}/{1}/oauth2/token', 
                                          new String[]{ apiSettings.Token_Endpoint__c, apiSettings.Tennant_Id__c }));
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Accept', 'application/json');
        request.setTimeout(requestTimeout);
        request.setMethod('POST');
        String jsonS = 'resource='+ apiSettings.Base_Url__c +'/&client_id='+ apiSettings.Client_Id__c + '&grant_type=client_credentials&client_secret=' + EncodingUtil.urlEncode(apiSettings.API_Key__c,'UTF-8');
        System.debug('jsonBody -----' + jsonS);
        request.setBody(jsonS);
        
        System.debug('DEBUG: AzureAPIWrapper request is ' + request);
        Http http = new Http();
        HTTPResponse response = http.send(request);
        String responseBody = response.getBody();
        System.debug('DEBUG: AzureAPIWrapper response ' + responseBody);
        if (response.getStatusCode() == 200) {
            Map<String,Object> jsonResponse = (Map<String,Object>)JSON.deserializeUntyped(responseBody);
            accessToken = (String)jsonResponse.get('access_token');
            System.debug('Access token - ' + accessToken);
            return accessToken;
        } else {
            throw new AzureAPIWrapperException('Problem getting access token: ' + responseBody);
        }        
    }
    
    private static String invoke(String operation, String method, String requestBody){        
        //Access and authentication values stored in Azure AD API Settings custom setting
        String accessToken = getAccessToken();     
        System.debug('AccessToken = ' + accessToken);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(operation);
        request.setMethod(method);
        request.setHeader('Authorization', 'Bearer ' + accessToken);
        request.setHeader('Content-Type', 'application/json');
        request.SetHeader('Accept','application/json;odata.metadata=minimal;odata.streaming=true;IEEE754Compatible=false;charset=utf-8');
        
        HTTPResponse response = http.send(request);
        
        if (response.getStatusCode() == 200) {
            String responseBody = response.getBody();
            return responseBody;
        } 
        else {
            List<String> headerKeys = response.getHeaderKeys();
            Map<String,String> headerMap = new Map<String,String>();
            for(String s : headerKeys){
                headerMap.put(s,response.getHeader(s));
            }
            system.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus() + ' ' + headerMap);
            throw new AzureAPIWrapperException('Problem invoking operation (' + operation + '): ' + response);          
        }
    } 
    
    //*Start of public methods
    
    //1. Query to get all Contacts in specific employee group    
    //Retrieves list of group members from the All-Users-SSO group (the Security Group all employees get)
    public static List<AzureAPIResponseWrapper.cls_value> getGroupMembers() {
        String endpointURL = apiSettings.Base_URL__c;
        
        Boolean keepRunning = true;
        Integer counter = 1;
        String operation = endpointURL + '/beta/groups/' + apiSettings.Group_Id_All_Users_SSO__c + '/members';
        String method = 'GET';
        String responseBody;
        String nextLink;
        AzureAPIResponseWrapper parsedResponse;
        List<AzureAPIResponseWrapper.cls_value> listResponse = new List<AzureAPIResponseWrapper.cls_value>();
        
        while(keepRunning == true){
            if(counter!=1){ 
                operation = nextLink; 
            } 
            String thisResponse = invoke(operation, method, null);
            System.debug('DEBUG: thisReponse ' + thisResponse);
            parsedResponse = AzureAPIResponseWrapper.parseJSON(thisResponse);
            listResponse.addAll(parsedResponse.value);
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(thisResponse);
            if(results.get('@odata.nextLink') != null){
                counter++;
                nextLink = (String)results.get('@odata.nextLink');
                System.debug('Response #'+counter);
            }
            if(results.get('@odata.nextLink') == null){
                keepRunning = false;
                System.debug('End of response.');
            }
        }
        
        return listResponse; 
    }
    
    //2. Query to get all Contacts in Saleforce under our Cherwell Software, LLC. Account 
    //(happens in batch job)
    
    //3. Note differences:
    //For each Employee Contact record, does it exist in the Group Member callout response?
    public static void upsertEmployees(List<Contact> allEmployees, List<AzureAPIResponseWrapper.cls_value> groupMembers){
        Id employeeRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        List<Contact> contactsToUpsert = new List<Contact>();
        Map<Contact, AzureAPIResponseWrapper.cls_value> userMap = new Map<Contact, AzureAPIResponseWrapper.cls_value>();        
        Map<String, AzureAPIResponseWrapper.cls_value> memberMap = new Map<String, AzureAPIResponseWrapper.cls_value>();
        Boolean updateContact;
        List<AzureAPIResponseWrapper.cls_value> existingContacts = new List<AzureAPIResponseWrapper.cls_value>();
        
        //put all group members into a map with key being their Azure AD id
        for(AzureAPIResponseWrapper.cls_value memb : groupMembers){
            memberMap.put(memb.id,memb);
        }
        
        System.debug('Start number of Group Members....' + groupMembers.size());
        
        AzureAPIResponseWrapper.cls_value currentEmp;
        for(Contact c : allEmployees){
            updateContact = false;
            //Only look at Contacts with an Azure AD Id. Employee Contacts should only be created via this integration and so should always have an Azure Id.
            if(c.Azure_Id__c != null){
                try{
                    currentEmp = memberMap.get(c.Azure_Id__c);
                } catch (Exception e){
                    System.debug('Exception caught while looking through Group Member map for Contact ----' + e.getMessage());
                }
                if(currentEmp != null){ 
                    //If Salesforce Contact exists in response, 
                    //Then ensure the accountEnabled property is set to True and onPremisesDistinguishedName is not like ‘*Old Employees*’ or ‘*Exit Transition*’. 
                    if(currentEmp.accountEnabled == false || currentEmp.onPremisesDistinguishedName.contains('%Old Employees%') || currentEmp.onPremisesDistinguishedName.contains('%Exit Transition%')){
                        c.Status__c = 'Inactive';
                        //contactsToUpsert.add(c);
                        updateContact = true;
                    }
                    //If employee is valid and exists, then check for updates on Title, Email, Phone, and First/Last Name. 
                    userMap.put(c,membermap.get(c.Azure_Id__c));
                    if(c.Title != currentEmp.jobTitle){ c.Title = currentEmp.jobTitle; updateContact = true; }
                    if(c.Email != currentEmp.mail){ c.Email = currentEmp.mail; updateContact = true; }
                    if(c.LastName != currentEmp.surname){ c.LastName = currentEmp.surname; updateContact = true; }
                    if(c.FirstName != currentEmp.givenName){ c.FirstName = currentEmp.givenName; updateContact = true; }
                    if(!currentEmp.businessPhones.isEmpty() && currentEmp.businessPhones!= null){ 
                        if(c.Phone != currentEmp.businessPhones[0]){ c.Phone = currentEmp.businessPhones[0]; updateContact = true; }
                        }
                    if(c.Department != currentEmp.department){ c.Department = currentEmp.department; updateContact = true; }
                    if(updateContact == true){
                        contactsToUpsert.add(c);
                    }
                }
                if(currentEmp == null){
                    //If in Salesforce, not in AD, then mark SF Contact as Inactive
                    c.Status__c = 'Inactive';
                    contactsToUpsert.add(c);
                }
            }
        }
                
        //If in AD, not in Salesforce, create new
        //for(AzureAPIResponseWrapper.cls_value memb : groupMembers.value){
        Map<String, Contact> allEmployeeContacts = new Map<String, Contact>();
        for(Contact con : [Select Azure_Id__c From Contact Where AccountId = : apiSettings.Cherwell_Account_Id__c]){
            allEmployeeContacts.put(con.Azure_Id__c, con);
        }
        
        for(AzureAPIResponseWrapper.cls_value memb : groupMembers){
            if(allEmployeeContacts.get(memb.id) == null){
                //Filter out group members who
                //1) have no email address
                //2) have no last name
                if(memb.mail != null && memb.surname != null){
                    Contact newCon = new Contact();
                    newCon.Azure_Id__c = memb.id;
                    newCon.AccountId = apiSettings.Cherwell_Account_Id__c;
                    newCon.FirstName = memb.givenName;
                    newCon.Department = memb.department;
                    newCon.LastName = memb.surname;
                    newCon.Title = memb.jobTitle;
                    if(memb.businessPhones != null && !memb.businessPhones.isEmpty()){ newCon.Phone = memb.businessPhones[0]; }
                    newCon.Email = memb.mail;
                    if(memb.accountEnabled == true && 
                       !memb.onPremisesDistinguishedName.contains('%Old Employees%') && 
                       !memb.onPremisesDistinguishedName.contains('%Exit Transition%')){ 
                           newCon.Status__c = 'Active';
                       } else { newCon.Status__c = 'Inactive'; }
                    newCon.RecordTypeId = employeeRTId;
                    contactsToUpsert.add(newCon);
                }
            }
        }
        if(contactsToUpsert != null && contactsToUpsert.size() > 0){
            System.debug('Upserting Contacts....' + contactsToUpsert);
            upsert contactsToUpsert;
        }
    }
}