trigger SBQQQuoteTrigger on SBQQ__Quote__c (before insert, before update) {
    
    Set<Id> quoteIds = new Set<Id>();
    Set<Id> oppIds = new Set<Id>();
    for(SBQQ__Quote__c q : Trigger.New){
        quoteIds.add(q.Id);
    }
    System.debug('Quote ids ----' + quoteIds);
    
    try {
        List<SBQQ__QuoteLine__c> qls = [Select Id, SBQQ__Description__c, SBQQ__Quantity__c, List_Unit_Price__c, CurrencyIsoCode, SBQQ__Quote__r.Total_Contract_Value__c,
                                        Discount_Print__c, Unit_Price__c, Price_Per_Year__c, SBQQ__ListTotal__c, SBQQ__Quote__c, List_Per_Year__c, SBQQ__Quote__r.ApprovalStatus__c
                                        From SBQQ__QuoteLine__c
                                        Where SBQQ__Quote__c = :quoteIds
                                        And SBQQ__Hidden__c = false];
        
        for(SBQQ__Quote__c q : Trigger.New){
            List<SBQQ__QuoteLine__c> embeddedList = new List<SBQQ__QuoteLine__c>();
            for(SBQQ__QuoteLine__c ql : qls){
                if(ql.SBQQ__Quote__c == q.Id){
                    embeddedList.add(ql);
                }
            }
            q.Quote_Line_Table__c = QuoteLineTableHandler.getTableBody(embeddedList);
        }
            
    } catch (Exception e){
        System.debug('Exception caught while looking for Quote Lines: ' + e.getMessage());
    }
}