public class IndustryValueMapping {
    
    public static void updateIndustry(List<Account> accts, List<Lead> lds){
        
        //Get current Industry mapping values from custom setting
        List<Industry_Mappings__c> industryMappingList = [Select Name, Industry__c, Vertical__c, NAICS_Code__c
                                                          From Industry_Mappings__c];
        Map<String,String> industryMap = new Map<String,String>();
        Map<String,String> verticalMap = new Map<String,String>();
        
        for(Industry_Mappings__c im : industryMappingList){
            industryMap.put(im.NAICS_Code__c, im.Industry__c);
            verticalMap.put(im.NAICS_Code__c, im.Vertical__c);
        }
        
        String currentIndustry;
        String currentVertical;
        String newIndustry;
        String newVertical;
        
        if(accts != null){
            for(Account a : accts){
                currentIndustry = a.Industry;
                currentVertical = a.Vertical__c;
                if(a.NAICS_Code_for_Industry_Mapping__c != null && a.NAICS_Code_for_Industry_Mapping__c.length()>3){
                    String matchValue = a.NAICS_Code_for_Industry_Mapping__c.subString(0,4);
                    if(industryMap.get(matchValue) != null){
                        newIndustry = industryMap.get(matchValue);
                        newVertical = verticalMap.get(matchValue);
                        if(currentIndustry != newIndustry){ a.Industry = newIndustry; }
                        if(currentVertical != newVertical){ a.Vertical__c = newVertical; }
                    }
                    if(industryMap.get(matchValue) == null && a.NAICS_Code_for_Industry_Mapping__c.length()>2){
                        matchValue = a.NAICS_Code_for_Industry_Mapping__c.subString(0,3);
                        if(industryMap.get(matchValue) != null){
                            newIndustry = industryMap.get(matchValue);
                            newVertical = verticalMap.get(matchValue);
                            if(currentIndustry != newIndustry){ a.Industry = newIndustry; }
                            if(currentVertical != newVertical){ a.Vertical__c = newVertical; }
                        }
                    }
                }
            }
        }
        
        if(lds != null){
            for(Lead l : lds){
                currentIndustry = l.Industry;
                currentVertical = l.Vertical__c;
                if(l.NAICS_Code_for_Industry_Mapping__c != null && l.NAICS_Code_for_Industry_Mapping__c.length()>3){
                    String matchValue = l.NAICS_Code_for_Industry_Mapping__c.subString(0,4);
                    if(industryMap.get(matchValue) != null){
                        newIndustry = industryMap.get(matchValue);
                        newVertical = verticalMap.get(matchValue);
                        if(currentIndustry != newIndustry){ l.Industry = newIndustry; }
                        if(currentVertical != newVertical){ l.Vertical__c = newVertical; }
                    }
                    if(industryMap.get(matchValue) == null && l.NAICS_Code_for_Industry_Mapping__c.length()>2){
                        matchValue = l.NAICS_Code_for_Industry_Mapping__c.subString(0,3);
                        if(industryMap.get(matchValue) != null){
                            newIndustry = industryMap.get(matchValue);
                            newVertical = verticalMap.get(matchValue);
                            if(currentIndustry != newIndustry){ l.Industry = newIndustry; }
                            if(currentVertical != newVertical){ l.Vertical__c = newVertical; }
                        }
                    }
                }
            }
        }        
    }
    
}