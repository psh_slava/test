@isTest

public class RequestTriggerTest {
    
    public static testmethod void testRequest(){
       
        Id partnerRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        
        Account a2 = new Account(Name='Test Partner', Account_Status__c = 'Active', Partner_Types__c = 'Reseller Partner; Support Partner; Referral Partner',
                                 RecordTypeId = partnerRTId);
        insert a2;
        
        Contact c = new Contact(LastName='Tester',Phone='1111111111', AccountId = a2.Id);
        insert c;
        
        Opportunity o = new Opportunity(CloseDate = Date.today(), Primary_Contact__c = c.Id, AccountId = a2.Id, Name='Test Opp',
                                        StageName = 'Discover', Forecast_Category__c = 'Omit');
        insert o;
        
        Request__c r = new Request__c();
        r.RecordTypeId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Partner Opportunity Registration').getRecordTypeId();
        r.Nature_of_Partner_Involvement_Here__c = 'Referral';
        r.Account__c = a2.Id;
        insert r;
        
        r.Opportunity__c = o.Id;
        update r;
    }   
}