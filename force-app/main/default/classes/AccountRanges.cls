/*
* @class name:   AccountRanges
* @created:      By Naomi Harmon in Nov 2020
* @test class:   AccountRangesTest.apxc
* @initiated by: AccountTrigger.apxt
* @description: 
*    Runs "roll-up" calculations for ReferenceEdge to set Employee Range, License Range, and Product List
* @modifcation log:
*    
*/

public class AccountRanges {
    
    public static void checkSetRanges(List<Account> triggerNew, Map<Id, Account> triggerOldMap){
        List<Account> theseAccounts = [Select Id, CSM_Total_Licenses__c, CSM_License_Range__c, CAM_Total_Licenses__c, CAM_License_Range__c,
                                      NumberOfEmployees, Employee_Range__c, CSM_License_Ct_from_Subscription__c, CAM_License_Ct_from_Subscription__c, CSMe_License_Ct_from_Subscription__c,
                                      PPM_License_Ct_from_Subscription__c, HRSM_License_Ct_from_Subscription__c, Product__c
                                       From Account
                                       Where Id IN : triggerNew];
        List<Account> accountsToUpdate = new List<Account>();
        for(Account a : theseAccounts){
            Boolean isChanged = false;
            //Has CSM license count changed?          
            if(a.CSM_Total_Licenses__c != triggerOldMap.get(a.Id).CSM_Total_Licenses__c){
                isChanged = true;
                if(a.CSM_Total_Licenses__c == 0){ a.CSM_License_Range__c = null; }
                if(a.CSM_Total_Licenses__c > 0 && a.CSM_Total_Licenses__c <= 125){ a.CSM_License_Range__c = 'CSM 1-125'; } 
                if(a.CSM_Total_Licenses__c > 125 && a.CSM_Total_Licenses__c <= 250){ a.CSM_License_Range__c = 'CSM 126-250'; } 
                if(a.CSM_Total_Licenses__c > 250 && a.CSM_Total_Licenses__c <= 500){ a.CSM_License_Range__c = 'CSM 251-500'; }
                if(a.CSM_Total_Licenses__c > 500 && a.CSM_Total_Licenses__c <= 1000){ a.CSM_License_Range__c = 'CSM 501-1000'; }
                if(a.CSM_Total_Licenses__c > 1000 && a.CSM_Total_Licenses__c <= 2000){ a.CSM_License_Range__c = 'CSM 1001-2000'; }
                if(a.CSM_Total_Licenses__c > 2000){ a.CSM_License_Range__c = 'CSM 2001+'; }
            }
            //Has CAM license count changed?
            if(a.CAM_Total_Licenses__c != triggerOldMap.get(a.Id).CAM_Total_Licenses__c){
                isChanged = true;
                if(a.CAM_Total_Licenses__c == 0){ a.CAM_License_Range__c = null; }
                if(a.CAM_Total_Licenses__c > 0 && a.CAM_Total_Licenses__c <= 1000){ a.CAM_License_Range__c = 'CAM 1-1000'; } 
                if(a.CAM_Total_Licenses__c > 1000 && a.CAM_Total_Licenses__c <= 5000){ a.CAM_License_Range__c = 'CAM 1001-5000'; } 
                if(a.CAM_Total_Licenses__c > 5000 && a.CAM_Total_Licenses__c <= 10000){ a.CAM_License_Range__c = 'CAM 5001-10000'; }
                if(a.CAM_Total_Licenses__c > 10000 && a.CAM_Total_Licenses__c <= 15000){ a.CAM_License_Range__c = 'CAM 10001-15000'; }
                if(a.CAM_Total_Licenses__c > 15000){ a.CAM_License_Range__c = 'CAM 15001+'; }
            }
            //Has employee range changed?
            if(a.NumberOfEmployees != triggerOldMap.get(a.Id).NumberOfEmployees){
                isChanged = true;
                if(a.NumberOfEmployees == 0){ a.Employee_Range__c = null; }
                if(a.NumberOfEmployees > 0 && a.NumberOfEmployees <= 499){ a.Employee_Range__c = 'Emp Range 1-499'; }
                if(a.NumberOfEmployees > 499 && a.NumberOfEmployees <= 999){ a.Employee_Range__c = 'Emp Range 500-999'; }
                if(a.NumberOfEmployees > 999 && a.NumberOfEmployees <= 4999){ a.Employee_Range__c = 'Emp Range 1000-4999'; }
                if(a.NumberOfEmployees > 4999 && a.NumberOfEmployees <= 9999){ a.Employee_Range__c = 'Emp Range 5000-9999'; }
                if(a.NumberOfEmployees > 9999){ a.Employee_Range__c = 'Emp Range 10,000+'; }
            }
            //Have products changed?
            //List<String> currentProducts = new List<String>(a.Product__c.split(';'));
            List<String> currentProducts = new List<String>();
            Boolean productUpdate = false;
            if(a.CSM_License_Ct_from_Subscription__c > 0 && a.CSM_License_Ct_from_Subscription__c != triggerOldMap.get(a.Id).CSM_License_Ct_from_Subscription__c){ currentProducts.add('CSM'); productUpdate = true; }
            if(a.CAM_License_Ct_from_Subscription__c > 0 && a.CAM_License_Ct_from_Subscription__c != triggerOldMap.get(a.Id).CAM_License_Ct_from_Subscription__c){ currentProducts.add('CAM'); productUpdate = true; }
            if(a.CSMe_License_Ct_from_Subscription__c > 0 && a.CSMe_License_Ct_from_Subscription__c != triggerOldMap.get(a.Id).CSMe_License_Ct_from_Subscription__c){ currentProducts.add('CSMe'); productUpdate = true; }
            if(a.PPM_License_Ct_from_Subscription__c > 0 && a.PPM_License_Ct_from_Subscription__c != triggerOldMap.get(a.Id).PPM_License_Ct_from_Subscription__c){ currentProducts.add('PPM'); productUpdate = true; }
            if(a.HRSM_License_Ct_from_Subscription__c > 0 && a.HRSM_License_Ct_from_Subscription__c != triggerOldMap.get(a.Id).HRSM_License_Ct_from_Subscription__c){ currentProducts.add('HRSM'); productUpdate = true; }
            
            if(productUpdate == true){
                String newProductList = String.join(currentProducts,';');
                if(a.Product__c != newProductList){ a.Product__c = newProductList; isChanged = true; }
            }
            
            if(isChanged == true){
                accountsToUpdate.add(a);
            }
        }   
        if(accountsToUpdate != null && accountsToUpdate.size()>0){
            update accountsToUpdate;
        }
    }
}