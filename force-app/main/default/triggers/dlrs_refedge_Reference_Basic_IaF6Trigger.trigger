/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_refedge_Reference_Basic_IaF6Trigger on refedge__Reference_Basic_Information__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(refedge__Reference_Basic_Information__c.SObjectType);
}