/** 
* @author CRMCulture
* @version 1.00
* @description Schedulable Class functionality
*/
public class TaskBatchableScheduler implements Schedulable {
	/** 
	* @author CRMCulture
	* @version 1.00
	* @description What to execute when schedule runs
	*/
	public void execute(SchedulableContext sc) {
		TaskBatchable tb = new TaskBatchable();
		tb.query = 'SELECT Id, First_Outreach_Date__c FROM Lead WHERE isConverted = false';
		Database.executebatch(tb);
	}
}