trigger CSMLicenseRecordTrigger on CSM_License_Record__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    TriggerFactory.createTriggerDispatcher(CSM_License_Record__c.sObjectType); 
    
    if(Trigger.isBefore && !Trigger.isDelete){
        List<CSM_License_Record__c> hostedKeys = new List<CSM_License_Record__c>();
        for(CSM_License_Record__c clr : Trigger.new){
            if(clr.Hosted__c == true){
                hostedKeys.add(clr);
            }
        }
        if(hostedKeys != null && hostedKeys.size()>0){
            System.debug('Initiating CSMLicenseRecordtoInstance for: ' + hostedKeys);
            CSMLicenseRecordtoInstance.createLink(hostedKeys);
        }
    }
    
    if(!Trigger.isDelete){
        for(CSM_License_Record__c c : Trigger.new){
            //if(   c.License_Type__c == 'CSM'){ 
            //    TriggerFactory.createTriggerDispatcher(CSM_License_Record__c.sObjectType); 
            //}
            if(Trigger.isBefore && Trigger.isInsert){
                if(c.Purchase_Scenario2__c != null){
                    if(c.Purchase_Scenario2__c.contains('New Logo') && c.Maintenance__c == 'Instance 1' && c.License_Type__c == 'CSM'){
                        c.Username__c = LicenseKeyCredentialsGenerator.generateUsername(c.Id, c.Account_Name__c);
                        c.Password__c = LicenseKeyCredentialsGenerator.generatePassword(c.Id);
                    }
                }
            }
            if(Trigger.isBefore && Trigger.isInsert){
                if(c.Purchase_Scenario2__c != null){
                    if(c.Generated_in_Salesforce__c == true && c.Former_Key__c == null && (c.Purchase_Scenario2__c.contains('Upsell') || c.Purchase_Scenario2__c.contains('Renewal'))){
                        LicenseKeyTriggerHandler.getFormerKey(c);
                    }
                }
            }
            if(Trigger.isAfter && Trigger.isInsert){
                
                LicenseKeyContacts.populateLicenseKeyContacts(c.Id, c.Account__c);
                
                if(c.Type__c == 'Burst!' || c.Type__c == 'Bubble'){
                    LicenseKeyTriggerHandler.burstKeyEffect(c.Id, c.Maintenance__c, c.License_Type__c, c.Account__c);      
                }
                if(c.Purchase_Scenario2__c != null){
                    if(c.Former_Key__c != null && !c.Purchase_Scenario2__c.contains('Renewal')){
                        LicenseKeyTriggerHandler.makeFormerKey(c.Former_Key__c);
                    }
                    if(c.Former_Key__c != null && c.Purchase_Scenario2__c.contains('Renewal')){
                        LicenseKeyTriggerHandler.issueRenewal(c.Id, c.Former_Key__c);
                    }
                }           
            }
        }
    }
}