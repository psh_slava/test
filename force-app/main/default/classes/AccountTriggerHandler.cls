public class AccountTriggerHandler {
   
    public static void updateCommunityMemberCode(List<Account> accounts){
        
        List<Id> accountIds = new List<Id>();
        for(Account a : accounts){
            accountIds.add(a.Id);
        }
        
        List<Contact> allContacts = [Select Id, Community_Member_Type_Code__c, Account_Status__c, Account_Record_Type__c
                                     From Contact
                                     Where AccountId IN :accountIds
                                     And Status__c != 'Inactive'
                                     And Community_Member__c = true];
        
        List<Contact> contactsToUpdate = new List<Contact>();
        for(Contact c : allContacts){
            String memberCode = 'Non-member';
            if(c.Account_Status__c == 'Inactive'){ memberCode = 'Inactive'; }
            if(c.Account_Status__c == 'Active'){
                if(c.Account_Record_Type__c == 'Customer'){ memberCode = 'Customer'; }
                if(c.Account_Record_Type__c == 'Partner' ){ memberCode = 'Partner';  }
            }
            if(c.Account_Status__c == 'Prospect'){ memberCode = 'Prospect'; }
            if(c.Community_Member_Type_Code__c != memberCode){
                c.Community_Member_Type_Code__c = memberCode;
                contactsToUpdate.add(c);
            } 
        }
        if(contactsToUpdate.size()>0){
            try{
                update contactsToUpdate;
            } catch(Exception e){
                System.debug('AccountTriggerHandler_Exception caught while trying to update Contacts: ' + e.getMessage());
            }
        }     
    }
     public static void createChecklistItems(List<Account> NewAccountlist){
        
        String checklistItemRTId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Checklist Item').getRecordTypeId();
        System.debug('checklistItemRTId ----- ' + checklistItemRTId);
        
        List<Task> activitiesToInsert = new List<Task>();
        Set<String> rtIds = new Set<String>();
        Set<id> accountIds = new Set<id>();
        For(Account Acc :NewAccountlist)
        {
           accountIds.Add(acc.id);
        }
        List<Checklist__c> clists = [Select id,Account_Owner_Id__c,Account_Recruitment_Stage__c,Account__c,RecordTypeId from Checklist__c Where Account__c IN: accountIds];
        IF(clists.Size()>0)
        {
        for(Checklist__c cl : clists){
            String rtId = String.valueOf(cl.RecordTypeId).subString(0,15);
            System.debug('checklistRTId ----- ' + rtId);
            rtIds.add(rtId);
        }
        List<Checklist_Activities__c> allListItems = [Select Checklist_Record_Type_Id__c, Comments__c, 
                                                           Due_Date_Days_from_Today__c, Item_Order_Number__c, Priority__c, 
                                                           Stage_Section__c, Subject_Label__c
                                                           From Checklist_Activities__c
                                                           Where Checklist_Record_Type_Id__c IN :rtIds];
        System.debug('All Checklist Activities list items ----- ' + allListItems);
        
        for(Checklist__c cl : clists){
            for(Checklist_Activities__c cla : allListItems){
            
                IF(cl.Account_Recruitment_Stage__c == cla.Stage_Section__c)
                {
                Integer days = Integer.valueOf(cla.Due_Date_Days_from_Today__c);
                Task t = new Task();
                t.WhatId = cl.Id;
                t.Status = 'Open';
                t.Subject = cla.Subject_Label__c;
                t.Description = cla.Comments__c;
                if(days != null){ t.ActivityDate = Date.today().addDays(days); }
                t.Priority = cla.Priority__c;
                t.Stage__c = cla.Stage_Section__c;
                t.RecordTypeId = checklistItemRTId;
                t.OwnerId = cl.Account_Owner_Id__c;
                t.ReminderDateTime = Datetime.now().addDays(days);
                t.IsReminderSet = true;
                activitiesToInsert.add(t); 
                }           
            }
        }
        
        if(activitiesToInsert.size()>0){
            System.debug('Inserting tasks ----- ' + activitiesToInsert);
            insert activitiesToInsert;
        } 
      }   
    }
    
}