/*******************************************************************************
Name              : QuoteApprovalsControllerTest
Description       : Testmethods QuoteApprovalsController apex class
Revision History  : - 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue                                           
----------------------------------------------------------------------------------------
1. Sebastián Suarez    01/10/2018               Dennis Palmer        TK-0000958 - (https://sovcrm.my.salesforce.com/a101N00000HyQGM)
*******************************************************************************/
@isTest (seeAllData=false)
public class QuoteApprovalsControllerTest {
    
    @isTest (seeAllData=false)
    public static void Controller_Test(){
        
        QuoteApprovalsController c = new QuoteApprovalsController();
        c.quote_Id = null;
        c.url = 'www.test.com';
        system.debug(c.Approvals); 
        system.debug(c.url); 
    }
}