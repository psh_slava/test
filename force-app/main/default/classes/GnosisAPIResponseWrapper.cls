public class GnosisAPIResponseWrapper {
    
    //Class for parsing response 
    public String status;
    public String message;
    
    public static GnosisAPIResponseWrapper parse(String json) {
        return (GnosisAPIResponseWrapper) System.JSON.deserialize(json, GnosisAPIResponseWrapper.class);
    }
}