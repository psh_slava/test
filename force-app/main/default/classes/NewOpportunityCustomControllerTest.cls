@isTest
public class NewOpportunityCustomControllerTest{
    testmethod static void newLogoNoContracts(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
        
        PageReference myVfPage = Page.NewOpportunityCustomPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('conId', c.Id);
        NewOpportunityCustomController nOpp = new NewOpportunityCustomController();
        //nOpp.Pageredirdctopp();
        nOpp.doCancel();
        nOpp.getRecordTypes();
        Id newLogoRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Logo').getRecordTypeId();
        nOpp.selectedRecordType = newLogoRTId;
        nOpp.redirectRecordtypepage();
        nOpp.goBack();
    }
    
    testmethod static void newLogoWithContracts(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.Status = 'Activated';
        con.StartDate = Date.today().addDays(-5);
        con.EndDate = Date.today().addDays(360);
        insert con;
        
        con.Status = 'Activated';
        update con;
        
        PageReference myVfPage = Page.NewOpportunityCustomPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('conId', c.Id);
        NewOpportunityCustomController nOpp = new NewOpportunityCustomController();
        nOpp.Pageredirdctopp();
        nOpp.doCancel();
        nOpp.getRecordTypes();
        Id newLogoRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Logo').getRecordTypeId();
        nOpp.selectedRecordType = newLogoRTId;
        nOpp.redirectRecordtypepage();
        nOpp.goBack();
    }
    
    testmethod static void upsellNoContracts(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
        
        PageReference myVfPage = Page.NewOpportunityCustomPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('conId', c.Id);
        NewOpportunityCustomController nOpp = new NewOpportunityCustomController();
        nOpp.getRecordTypes();
        Id upsellRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        nOpp.selectedRecordType = upsellRTId;
        nOpp.redirectRecordtypepage();
    }
    
    testmethod static void upsellWithContracts(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.Status = 'Activated';
        con.StartDate = Date.today().addDays(-5);
        con.EndDate = Date.today().addDays(360);
        insert con;
        
        con.Status = 'Activated';
        update con;
        
        PageReference myVfPage = Page.NewOpportunityCustomPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('conId', c.Id);
        NewOpportunityCustomController nOpp = new NewOpportunityCustomController();
        nOpp.getRecordTypes();
        Id upsellRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        nOpp.selectedRecordType = upsellRTId;
        nOpp.redirectRecordtypepage();
    }
    
    testmethod static void renewalNoContracts(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
        
        PageReference myVfPage = Page.NewOpportunityCustomPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('conId', c.Id);
        NewOpportunityCustomController nOpp = new NewOpportunityCustomController();
        nOpp.getRecordTypes();
        Id renewalRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        nOpp.selectedRecordType = renewalRTId;
        nOpp.redirectRecordtypepage();
    }
    
    testmethod static void renewalWithContracts(){
        Account a = new Account();
        a.Name = 'Banana Peels Incorporated';
        a.BillingStreet = '123 Street';
        a.BillingCity = 'Testville';
        a.BillingCountry = 'United States';
        a.BillingPostalCode = '12345';
        a.BillingState = 'Texas';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.Email = 'test@test.com';
        c.AccountId = a.Id;
        insert c;
        
        Contract con = new Contract();
        con.AccountId = a.Id;
        //con.Status = 'Activated';
        con.StartDate = Date.today().addDays(-5);
        con.EndDate = Date.today().addDays(360);
        insert con;
        
        con.Status = 'Activated';
        update con;
        
        PageReference myVfPage = Page.NewOpportunityCustomPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        ApexPages.currentPage().getParameters().put('conId', c.Id);
        NewOpportunityCustomController nOpp = new NewOpportunityCustomController();
        nOpp.getRecordTypes();
        Id renewalRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
        nOpp.selectedRecordType = renewalRTId;
        nOpp.redirectRecordtypepage();
    }
}