trigger dlrs_pse_Timecard_HeaderTrigger on pse__Timecard_Header__c (before insert, before update, before delete, after delete, after insert, after undelete, after update) {
    dlrs.RollupService.triggerHandler(pse__Timecard_Header__c.SObjectType);
}