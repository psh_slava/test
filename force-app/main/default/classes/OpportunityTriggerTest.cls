@isTest
public class OpportunityTriggerTest {
    
    public static Account aNew ;
    public static Opportunity oNew ;
    public static Contact cNew ;
    
    @testsetup
    public static void testsetupdata(){
        aNew = TestDataFactory.createAccount(true, 'Test Account');
        oNew = TestDataFactory.createOpportunity(true, 'Test Opportunity', aNew);
        cNew = TestDataFactory.createContact(true, 'Test', 'Contact', aNew);
    }
    
    @isTest
    public static void OpportunityUpdateTest(){ 
        Contact cNew = [Select id, name from Contact where Name like 'Test Contact%' limit 1];
        Opportunity opp = [Select id, name from Opportunity where Name like 'Test%' limit 1];
        
        try{
            update opp; 
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Opportuntiy Must Have a Contact Role') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
        
    }
    
    @isTest
    public static void testContactRoles(){
        Account a = new Account(name='Test Company');
        insert a;
         
        Contact con = new Contact(AccountId = a.Id, LastName = 'Tester2', Email = 'test2@test.test');
        insert con;
                
        Opportunity o = new Opportunity(AccountId = a.Id, StageName = 'Order Processing', Forecast_Category__c = 'Commit', Close_Reason__c = 'Relationship/Reputation', 
                                        CurrencyIsoCode = 'USD', CloseDate = Date.today(), Name = 'Test Opp', Primary_Contact__c = con.Id, Hosting_Model__c = 'On Premise');
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__StartDate__c = Date.today();
        q.Term__c = '3';
        q.SBQQ__Primary__c = true;
        q.SBQQ__PrimaryContact__c = con.Id;
        q.License_Key_Contact__c = con.Id;
        q.Technical_Maintenance_Contact__c = con.Id;
        q.Billing_Contact__c = con.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.ApprovalStatus__c = 'Approved';
        q.SBQQ__Status__c = 'Approved';
        insert q;
        
        o.SBQQ__PrimaryQuote__c = q.Id;
        o.StageName = 'Closed - Won';
        update o;
        
        
    }
    
}