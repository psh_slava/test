/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_SLX_Change_ControlTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_SLX_Change_ControlTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new SLX__Change_Control__c());
    }
}