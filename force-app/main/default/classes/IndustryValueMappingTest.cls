@isTest
public class IndustryValueMappingTest {
    
    public static testmethod void testIndustryValueMapping(){
        
        List<Industry_Mappings__c> ims = new List<Industry_Mappings__c>();
        Industry_Mappings__c im1 = new Industry_Mappings__c(Name = '111', NAICS_Code__c = '111', 
                                                        Industry__c = 'Agriculture, Forestry, Fishing and Hunting', Vertical__c = 'Crop Production');
        ims.add(im1);
        Industry_Mappings__c im2 = new Industry_Mappings__c(Name = '2131', NAICS_Code__c = '2131', 
                                                        Industry__c = 'Mining', Vertical__c = 'Support Activities for Mining');
        ims.add(im2);
        insert ims;
        
        Lead l = new Lead(LastName = 'Tester', Company = 'Test Co.', Email = 'test1@test.test', DSCORGPKG__NAICS_Codes__c = '111332');
        insert l;
        
        Account a = new Account(Name = 'Test Company', 	DSCORGPKG__NAICS_Codes__c = '213112');
        insert a;
        
        Lead l2 = new Lead(LastName = 'Tester', Company = 'Test Co.', Email = 'test2@test.test', DSCORGPKG__NAICS_Codes__c = '213112');
        insert l2;
        
        Lead l3 = new Lead(LastName = 'Tester', Company = 'Test Co.', Email = 'test2@test.test', DSCORGPKG__NAICS_Codes__c = '213');
        insert l3;
        
        Account a2 = new Account(Name = 'Test Company2', DSCORGPKG__NAICS_Codes__c = '111332');
        insert a2;
        
        Account a3 = new Account(Name = 'Test Company3', DSCORGPKG__NAICS_Codes__c = '111');
        insert a3;
        
        a = [Select Industry, Vertical__c From Account Where Id = :a.Id Limit 1];
        l = [Select Industry, Vertical__c From Lead Where Id = :l.Id Limit 1];
        
        System.assertEquals(a.Industry, 'Mining');
        System.assertEquals(l.Vertical__c, 'Crop Production');
        
    }

}