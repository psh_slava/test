@isTest
public class SBQQQuoteTriggerTest {
    
    public static testmethod void testTrigger(){
        Account a = new Account(name='Test Account');
        insert a;
        
        Contact c = new Contact(AccountId = a.Id, LastName = 'Tester', Email = 'test@test.com');
        
        Opportunity o = new Opportunity(Name = 'Test Opp', CloseDate = Date.today().addDays(30), AccountId = a.Id, Primary_Contact__c = c.Id,
                                       StageName = 'Discover', Forecast_Category__c = 'Omit');
        insert o;
        
        Product2 p = new Product2();
        p.Name = 'CSM Subscription License';
        insert p;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__StartDate__c = Date.today();
        q.Term__c = '3';
        q.SBQQ__Opportunity2__c = o.Id;
        q.Product_Column_Discount__c = true;
        q.Product_Column_List_Price__c = true;
        q.Product_Column_Net_Unit_Price__c = true;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Product__c = p.Id; 
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Quantity__c = 50;
        insert ql;
        
        q.Quote_Name__c = 'Test Quote';
        q.ApprovalStatus__c = 'Approved';
        update q;
    }  
}