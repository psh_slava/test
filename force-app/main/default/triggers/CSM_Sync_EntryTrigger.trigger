/**
 * CSM_Sync_Entry__c trigger
 *   
 * @author CRMCulture  
 * @version 1.00 
 * @param Platform after insert, after update, before insert, before update
*/ 
trigger CSM_Sync_EntryTrigger on CSM_Sync_Entry__c (
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {
	TriggerFactory.createTriggerDispatcher(CSM_Sync_Entry__c.sObjectType);
}