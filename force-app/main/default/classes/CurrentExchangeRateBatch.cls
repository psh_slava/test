/*
* @class name:   CurrentExchangeRateBatch
* @created:      By Naomi Harmon in May 2020
* @test class:   CurrentExchangeRateTest.apxc
* @initiated by: CurrentExchangeRateScheduler.apxc
* @description: 
*    Batch job to update open, non-USD, future-dated Opportunities daily with new Dated Exchange Rates that get pushed from our Netsuite integration 
*    (to mimic Salesforce OOB functionality related to converted fields in reports)
* @modifcation log:
*	 Sep 2020 - Naomi Harmon - updated batch Query to look for any Opportunity (closed or open) with a date greater or equal to today. This is to keep closed, future-dated Opps current.
*/

public class CurrentExchangeRateBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    //Get all opps that should be updated:
    //All opps with Close Date in the future that are in a foreign currency and whose dated exchange rate doesn't match
    public List<String> exception_List = new List<String>();
    public Database.QueryLocator start(Database.BatchableContext BC) {
        
        Date todaysDate = Date.today();
        String usd = 'USD';
        String query = 'SELECT Id, CloseDate, CurrencyIsoCode, StageName, Dated_Exchange_Rate__c ' +        
            //'FROM Opportunity WHERE IsClosed = false ' + 
            'FROM Opportunity WHERE CloseDate >  = :todaysDate ' + 
            'AND CurrencyIsoCode != :usd';
        
        return Database.getQueryLocator(query);
    }
    
    //Call class to execute
    public void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        System.debug('Executing Current Dated Exchange Rate BATCH...');
        
        //Only update opps that need updating
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        Map<Id,Decimal> originalOppToRateMap = new Map<Id,Decimal>();
        for(Opportunity originalOpp : scope){
            originalOppToRateMap.put(originalOpp.Id, originalOpp.Dated_Exchange_Rate__c);
        }
        
        List<Opportunity> processedOpps = CurrentExchangeRate.processBatch(scope);

        for(Opportunity updatedOpp : processedOpps){
            System.debug(' Comparing ' + originalOppToRateMap.get(updatedOpp.Id) + ' to ' + updatedOpp.Dated_Exchange_Rate__c );
            if ( originalOppToRateMap.get(updatedOpp.Id) != updatedOpp.Dated_Exchange_Rate__c){
                oppsToUpdate.add(updatedOpp);
            }
        }
        
        System.debug('# of Opps to update : ' + oppsToUpdate.size());
        Boolean allOrNone = false;
        if(oppsToUpdate != null && oppsToUpdate.size()> 0){
            Database.SaveResult[] SaveResultList = Database.update(oppsToUpdate, allOrNone);
            
            for(integer i = 0; i < oppsToUpdate.size(); i++){
                String msg='';
                If(!SaveResultList[i].isSuccess()){
                    msg += '\n' + SaveResultList[i].id + '\n'+'Error: "';        
                    for(Database.Error err: SaveResultList[i].getErrors()){  
                        msg += err.getmessage()+'"';
                    } 
                }
                if(msg!='')
                    exception_List.add(msg);
            }    
        }
    }
    
    public void finish(Database.BatchableContext BC){ 
        
        AsyncApexJob a = [SELECT Id,Status,JobType,NumberOfErrors,JobItemsProcessed,TotalJobItems,CompletedDate,ExtendedStatus 
                          FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        
        //Send an email to SFDC Admin team notifying of job completion
        List<String> toAddresses = new List<String>();
        toAddresses.add('sfdchelp@cherwell.com');
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setSubject('Failures from Dated Exchange Rate update batch - ' + a.Status);
        mail.setSaveAsActivity(false);
        mail.setPlainTextBody
            ('The batch Apex job completed on  ' + a.CompletedDate + ',\n\n' +
             'Job Status : ' + a.Status + '\n'+
             'Total Batches processed : ' + a.TotalJobItems + '\n'+
             'Number of Batches processed : ' + a.JobItemsProcessed + '\n' +
             'Number of Failures : '+ a.NumberOfErrors + '\n' +
             'Exception List : \n' + exception_List);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } 
    
}