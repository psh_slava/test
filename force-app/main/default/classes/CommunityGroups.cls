/*
 * CommunityGroups
 * @author: Naomi Harmon
 * @last modified: 11/2019
 * @related classes: 
 * @test class: CommunityGroupsTest
 * @function: Method to automatically update the Community Groups field on a Contact when a group access request record is approved/declined
*/

public class CommunityGroups {
    
    public static void alertGroupManagers(List<Request__c> cgRequestRecords){
        
        Id groupRequestRTId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Community Group Application').getRecordTypeId();
        System.debug('Community Group Application request RT Id - ' + groupRequestRTId);
        
        //For new Community Group Application requests,
        //Get list of related Community Group Managers
        //and send email to group managers letting them know someone needs to be reviewed
        List<Request__c> newJoins = new List<Request__c>();
        List<Id> requestIds = new List<Id>();
        List<Id> cgIds = new List<Id>();
        for(Request__c cgr : cgRequestRecords){
            if(cgr.RecordTypeId == groupRequestRTId && cgr.Status__c == 'New'){
                newJoins.add(cgr);
                cgIds.add(cgr.Community_Group__c);
            }
        }
        
        List<Community_Group_Manager__c> allGroupManagers = new List<Community_Group_Manager__c>();
        Map<Id, List<String>> groupManagersMap = new Map<Id, List<String>>();
        if(newJoins.size()>0){
            allGroupManagers = [Select Employee_Contact__c, Employee_Contact__r.Email, Community_Group__c
                                From Community_Group_Manager__c
                                Where Employee_Contact__r.Status__c = 'Active'
                                And Community_Group__c IN : cgIds]; 
        }
        for(Id groupId : cgIds){
            List<String> thisGroupsManagers = new List<String>();
            for(Community_Group_Manager__c manager : allGroupManagers){
                if(manager.Community_Group__c == groupId){
                    thisGroupsManagers.add(manager.Employee_Contact__r.Email);
                }
            }
            if(thisGroupsManagers.size()>0){
                groupManagersMap.put(groupId, thisGroupsManagers);
            }
        }
        
        String emailTemplateName = 'New_Community_Group_Join_Request_needs_Approval';
        String subject;
        String body;
        EmailTemplate et;
        OrgWideEmailAddress[] owea;
            
        try{              
            et = [SELECT Id, Name, Subject, Body FROM EmailTemplate WHERE DeveloperName =:emailTemplateName];      
            subject = et.subject;
            body = et.body;
            
            owea = [select Id, Address from OrgWideEmailAddress where DisplayName = 'Cherwell Community' LIMIT 1];

        } catch (Exception e){
            System.debug('Email Template with name = '+ emailTemplateName + ' not found.');
        }
        
        List<string> toAddresses = new List<string>();
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        
        if(et != null){
            for(Request__c njr : newJoins){
                //Get recipient list
                toAddresses = groupManagersMap.get(njr.Community_Group__c);
                List<String> ccAddresses = new List<String>();
                if(njr.Owner.Email != null){ ccAddresses.add(njr.Owner.Email); }
                if(njr.AssignedTo__c != null){ ccAddresses.add(njr.AssignedTo__c); }
                
                System.debug('To Addresses - ' + toAddresses);
                System.debug('CC Addresses - ' + ccAddresses);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if ( owea.size() > 0 ) { mail.setOrgWideEmailAddressId(owea.get(0).Id); }
                mail.setTemplateId(et.Id);
                mail.setToAddresses(toAddresses);
                if(ccAddresses.size()>0){ mail.setCcAddresses(ccAddresses); }
                mail.setTargetObjectId(njr.Contact__c);
                mail.setTreatTargetObjectAsRecipient(true);
                mail.setWhatId(njr.Id);
                mail.setSaveAsActivity(false);
                mail.setUseSignature(false);
                allmsg.add(mail);
            } 
        }
        
        try {
            System.debug('Sending emails - ' + allmsg.size());
            System.debug('Sending emails - ' + allmsg);
            
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(allmsg); 
            if(!results.get(0).isSuccess()){        
                System.debug('Send Error: ' + results.get(0).getErrors()[0].getMessage());     
            } else {        
                System.debug('Email Send Successful!');    
            } 
            //Messaging.sendEmail(allmsg,false);
            return;
        } catch (Exception e) {
            System.debug('Exception caught while trying to send new join request emails - ' + e.getMessage());
        }        
    }
    
    public static void setGroupAccess(List<Request__c> cgRequestRecords){
        
        //Get all approved community group access request records
        //Get multi-select Community Group picklist field from Contact
        //Make sure picklist contains all approved groups
        //Picklist may contain groups that don't have a corresponding community group member record
        
        List<String> contactIds = new List<String>();
        List<Contact> contactsToUpdate = new List<Contact>();
        Map<Id,String> contactGroupMap = new Map<Id,String>();
        
        Id groupRequestRTId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Community Group Application').getRecordTypeId();
        System.debug('Community Group Application request RT Id - ' + groupRequestRTId);
        
        for(Request__c cgr : cgRequestRecords){
            if(cgr.RecordTypeId == groupRequestRTId){
                if(cgr.Status__c == 'Accepted' && cgr.Contact__c != null){
                    contactIds.add(cgr.Contact__c);
                    contactGroupMap.put(cgr.Contact__c, cgr.Community_Group_Name__c);
                    System.debug('Community Group Name = ' + cgr.Community_Group_Name__c);
                }       
            }
        }
        
        if(contactIds != null && contactIds.size() > 0){
            List<Contact> allContacts = [Select Id, Community_Groups__c 
                                         From Contact 
                                         Where Id IN :contactIds];
            for(Contact c : allContacts){
                List<String> existingGroups = new List<String>();
                Set<String> newGroupSet = new Set<String>();
                String newGroup = contactGroupMap.get(c.Id);
                String thisMembersGroupsConcat; 
                if(c.Community_Groups__c != null){
                    existingGroups.addAll(c.Community_Groups__c.split(';'));
                    newGroupSet.addAll(existingGroups);
                    System.debug('Set with existing...' + newGroupSet);   
                    newGroupSet.add(newGroup);
                    System.debug('Set existing and new...' + newGroupSet);
                    thisMembersGroupsConcat = String.join((Iterable<String>)newGroupSet, ';');
                    System.debug('This member\'s group list...' + thisMembersGroupsConcat);
                    c.Community_Groups__c = thisMembersGroupsConcat;
                    
                }
                if(c.Community_Groups__c == null){ 
                    c.Community_Groups__c = newGroup; 
                }
                contactsToUpdate.add(c);
            }
            
            if(contactsToUpdate.size() > 0){
                update contactsToUpdate;
            }
        }
    }
}