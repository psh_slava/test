/**
* @author DSherman
* @author Conga Services
* @version 1.00
* @Date 10/12/2016
* @description Houses all triggers on Lead
*/
public class TaskHelper {
	/**
	* @author DSherman
	* @author Conga Services
	* @version 2.00
	* @date 10/12/2016
	* @description Counts tasks and events which meet specific criteria for a given Lead. 
	*/

	//See trigger state classes for map building
	//public static void countCompletedTasks(Map<Id, Integer> incomingLeads) {
	//	if(TriggerHelper.DoExecute('Task.count_Completed_Tasks') && incomingLeads != null) {
	//		List<Lead> leadsToUpdate = new List<Lead>();

	//		List<Rejected_Activity_Types__c> rat = [SELECT Name FROM Rejected_Activity_Types__c];
	//		Set<String> ratSet = new Set<String>();

	//		for(Rejected_Activity_Types__c r : rat) {
	//			ratSet.add(r.Name);
	//		}
		
	//		//Create aggregate queries for task and event (below)
	//		AggregateResult[] tasksByWhoId = [SELECT COUNT(Id)cnt, WhoId FROM Task WHERE 
	//																				Type NOT IN :ratSet AND
	//																				Non_Type_Countable_CriteriaFx__c = true AND
	//																				WhoId IN :incomingLeads.keySet()
	//																				GROUP BY WhoId];


	//		//set aggregate query amount to value corresponding to Id in Map
	//		for(AggregateResult ar : tasksByWhoId) {
	//				Id leadId = String.valueOf(ar.get('WhoId'));
	//				if(incomingLeads.containsKey(leadId)) {
	//					Integer count = Integer.valueOf(ar.get('cnt'));
	//					incomingLeads.put(leadId,count);
	//				}
	//			}


	//		AggregateResult[] eventsByWhoId = [SELECT COUNT(Id)cnt, WhoId FROM Event WHERE 
	//																					Type NOT IN :ratSet AND
	//																					Non_Type_Countable_CriteriaFx__c = true AND
	//																					WhoId IN :incomingLeads.keySet()
	//																					GROUP BY WhoId];


	//		//Set value of aggregate query and add it to the value from task addition, which is transferred using the map
	//		for(AggregateResult ar : eventsByWhoId) {
	//				Id leadId = String.valueOf(ar.get('WhoId'));
	//				if(incomingLeads.containsKey(leadId)) {
	//					Integer count = incomingLeads.get(leadId);
	//					Integer eventCount = Integer.valueOf(ar.get('cnt'));
	//					count += eventCount;
	//					incomingLeads.put(leadId,count);
	//				}
	//		}

	//		//Get leads, update number and insert them. Try-catch to hide validation rule errors (per request)
	//		for(Id leadId : incomingLeads.keySet()) {
	//			Lead lead = new Lead(Id=leadId);
	//			lead.Activity_Count__c = incomingLeads.get(leadId);
	//			leadsToUpdate.add(lead);
	//		}


			
	//			if(leadsToUpdate.size() > 0) {
	//				Database.SaveResult[] results = Database.update(leadsToUpdate, false);	
			 
	//			}

	//	}
	//}

	///**
	//* @author DSherman
	//* @author Conga Services
	//* @version 2.00
	//* @date 10/12/2016
	//* @description Sets First Outreach Date on Lead when specific criteria are met for a Task 
	//*/

	//public static void firstOutreach(List<Task> incomingTasks) {
	//	if(TriggerHelper.DoExecute('Task.first_outreach_date')) {

	//		List<Task> possibleOutreachTasks = new List<Task>();
	//		List<Lead> leadsToUpdate = new List<Lead>();
	//		Set<Id> leadsProcessed = new Set<Id>();

	//		//Find records that meet outreach criteria. Requeried as this is required to check UserRole 
	//		possibleOutreachTasks = [SELECT ActivityDate, OwnerId, WhoId FROM Task WHERE 
	//																			IsClosed = true AND 
	//																			ActivityDate != null AND
	//																			Type != 'HubSpot Task' AND
	//																			Type != 'Note' AND
	//																			Type != 'Sales Tracked Open' AND
	//																			Type != 'Sales Tracked Order' AND
	//																			(Owner.UserRole.Id = '00E500000015JGX' OR
	//																			Owner.UserRole.Id = '00E500000015Jmi' OR
	//																			Owner.UserRole.Id = '00E500000015JGS') AND
	//																			(CreatedBy.UserRole.Id = '00E500000015JGX' OR
	//																			 CreatedBy.UserRole.Id = '00E500000015Jmi' OR
	//																			 CreatedBy.UserRole.Id = '00E500000015JGS') AND
	//																			 Id IN :incomingTasks
	//																			 ORDER BY ActivityDate ASC];
	//		System.debug('poss Outreach Task size: '+possibleOutreachTasks.size());
			
	//		//Check, and potentially set and update first outreach date. Set present to keep from looping over tasks multiple times
	//		for(Task t : possibleOutreachTasks) {
				
	//			if(t.WhoId != null && !leadsProcessed.contains(t.WhoId) && (t.whoId.getSobjectType() == Schema.Lead.SObjectType)) {
	//				Lead lead = new Lead(Id=t.WhoId);
					
	//				if((lead.First_Outreach_Date__c == null) || (lead.First_Outreach_Date__c > t.ActivityDate) ) {
	//					lead.First_Outreach_Date__c = t.ActivityDate;
	//					leadsToUpdate.add(lead);
	//					leadsProcessed.add(lead.Id);
	//				}
	//			}
				
	//		}
			
	//		if(leadsToUpdate.size() > 0) {
	//			Database.SaveResult[] results = Database.update(leadsToUpdate,false);
	//		}
	//	}
	//}

	/**
	* @author DSherman
	* @author Conga Services
	* @version 2.00
	* @date 10/12/2016
	* @description Sets the scoping call checkbox on Opp when below conditions are met. Set serves same purpose as above. 
	*/
 
	public static void setScopingCall(List<Task> incomingTasks) {
		if(TriggerHelper.DoExecute('Task.Set_Scoping_Call')) {

			List<Opportunity> oppsToUpdate = new List<Opportunity>();
			Set<Id> finishedOpps = new Set<Id>();

			for(Task t : incomingTasks) {
				if(t.Type == 'Scoping Call' && t.IsClosed == true && (t.whatId.getSobjectType() == Schema.Opportunity.SObjectType)) {
					Opportunity o = new Opportunity(Id=t.whatId);
						if(!finishedOpps.contains(o.Id)) {
							o.IsScoping_Call_Complete__c = true;
							oppsToUpdate.add(o);
							finishedOpps.add(o.Id);
					}
				}
			}
			
			if(oppsToUpdate.size() > 0) {
				Database.SaveResult[] results = Database.update(oppsToUpdate, false);
			}
			
		}
	}
}