public class OpportunityTrigger_Handler{
    
    OpportunityTrigger_Action action = new OpportunityTrigger_Action();
    
    public void onAfterInsert(list<Opportunity> newList){
        action.LifecycleRecordUpdate_OnOpportunityInsert(newList);
        action.queueUpdatePrimaryCampaignAttribution(newList);
        
    }
    
    @future
    public static void tagContactCommChannels(Set<Id> contactIds, Map<Id,String> mapContactToRole){
        System.debug('mapContactToRole === ' + mapContactToRole);
        List<Contact> contactsToTag = new List<Contact>(); 
        List<Contact> cRoles = [Select Id, License_Key__c, Billing__c, Primary_Support__c
                                From Contact
                                Where Id IN :contactIds];
        System.debug('cRoles === ' + cRoles);
        for(Contact cR : cRoles){
            if(mapContactToRole.get(cR.Id) != null && mapContactToRole.get(cR.Id) == 'License Key'){
                cR.License_Key__c = true;
            }
            if(mapContactToRole.get(cR.Id) != null && mapContactToRole.get(cR.Id) == 'Billing'){
                cR.Billing__c = true;
            }
            
            if(mapContactToRole.get(cR.Id) != null && mapContactToRole.get(cR.Id) == 'Primary Support'){
                cR.Primary_Support__c = true;
            }
            contactsToTag.add(cR);
        }
        update contactsToTag;
    }
    
    public void onAfterUpdate(Map<Id,Opportunity> newMap, Map<Id,Opportunity> oldMap){
        
        action.LifecycleRecordUpdate_OnOpportunityUpdate(newMap, oldMap);
        
        List<Opportunity> invoiceOpps = new List<Opportunity>();
        for(Opportunity opp : newMap.values()){
            if(opp.Record_Type_Name__c == 'Invoicing' && opp.CloseDate != oldMap.get(opp.Id).CloseDate && opp.IsClosed == false){
                invoiceOpps.add(opp);
            }
        }
        if(invoiceOpps.size()>0 && BookingsProcessor.RecursionControl()){
            BookingsProcessor.updateOffsetOnInvoice(invoiceOpps);
            
        }
        
    }     
}