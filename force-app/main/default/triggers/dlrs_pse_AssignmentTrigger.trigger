/*
      Purpose:
            Your purpose here...
            
      Initiative: ...Initiative...
      Author:     Alan Birchenough
      Company:    Icon Cloud Consulting
      Contact:    alan.birchenough@iconatg.com
      Created:    4/12/18
*/

trigger dlrs_pse_AssignmentTrigger on pse__Assignment__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    dlrs.RollupService.triggerHandler();
}