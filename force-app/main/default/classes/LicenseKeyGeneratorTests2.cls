@isTest
public class LicenseKeyGeneratorTests2 {
    
    @isTest static void autogenKey(){
        Account a = new Account();
        a.Name = 'Test Account';
        a.Type = 'Customer';
        insert a;
        
        Contact cont = new Contact();
        cont.LastName = 'testerson';
        cont.AccountId = a.Id;
        cont.Email = 'tester@test.com';
        insert cont;
        
        Opportunity oppt = new Opportunity();
        oppt.Name = 'Test Upsell';
        oppt.StageName = 'Discovery';
        oppt.Forecast_Category__c = 'Pipeline';
        oppt.CloseDate = Date.today().addDays(5);
        //oppt.Close_Reason__c = 'Cost/Price';
        oppt.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Logo').getRecordTypeId();
        oppt.AccountId = a.Id;
        insert oppt;        
        
        Product2 prdct = new Product2();
        prdct.Name = 'CSM Subscription License';
        insert prdct;        
        
        Contract ct = new Contract();
        ct.StartDate = Date.today().addDays(365);
        ct.ContractTerm = 36;
        ct.AccountId = a.Id;
        ct.Status = 'Draft';
        //cnt.SBQQ__Opportunity__c = o.Id;
        insert ct;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = prdct.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = ct.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        s.SBQQ__Bundle__c = true;
        insert s;   
        
        SBQQ__Subscription__c s2 = new SBQQ__Subscription__c();
        s2.SBQQ__Quantity__c = 15;
        s2.SBQQ__Product__c = prdct.Id;
        s2.SBQQ__Account__c = a.Id;
        s2.SBQQ__Contract__c = ct.Id;
        s2.Instance_Number__c = 'Instance 1';
        s2.Instance_Name__c = 'TestInst';
        s2.SBQQ__Bundle__c = true;
        insert s2; 
        
        ct.SBQQ__Opportunity__c = oppt.Id;        
        ct.Status = 'Activated';
        update ct;
        
        SBQQ__Quote__c qte= new SBQQ__Quote__c();
        qte.SBQQ__StartDate__c = Date.today().addDays(5);
        qte.SBQQ__Account__c = ct.AccountId; 
        qte.SBQQ__Opportunity2__c = oppt.Id;
        qte.SBQQ__Primary__c = true;
        qte.Term__c = '3';
        qte.SBQQ__MasterContract__c = ct.Id;
        qte.License_Key_Contact__c = cont.Id;
        insert qte;
        
        SBQQ__QuoteLine__c qtel = new SBQQ__QuoteLine__c();
        qtel.SBQQ__Product__c = prdct.Id;
        qtel.SBQQ__Quote__c = qte.Id;
        qtel.SBQQ__Quantity__c = 500;
        insert qtel;
        
        oppt.StageName = 'Closed - Won';
        oppt.Close_Reason__c = 'Cost/Price';
        oppt.Forecast_Category__c = 'Won';
        oppt.SBQQ__PrimaryQuote__c = qte.Id;
        oppt.Primary_Competitor__c = 'Other';
        update oppt;
             
        LicenseKeyAutoGenerator.generateKeyRows(ct.Id);
    }
    
}