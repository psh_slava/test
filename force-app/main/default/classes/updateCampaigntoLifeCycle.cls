global class updateCampaigntoLifeCycle implements Database.Batchable<sObject>{

  
   global Database.QueryLocator start(Database.BatchableContext BC){
      String query='Select id,Related_Contact__c,Lifecycle_Status__c from Lifecycle__c where Related_Contact__c!= Null and (Lifecycle_Status__c =\'Completed\' OR Lifecycle_Status__c=\'Active\')';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Lifecycle__c> scope){
     List<string> Contactidlst = New List<string>();
     for(Lifecycle__c L : scope)
     {
       
       Contactidlst.Add(L.Related_Contact__c);
      
     }
     List<contact> Contactlst = [Select id,(Select id,Contactid,Lifecycle__c from CampaignMembers )from Contact where id IN :Contactidlst];
     Map<id,campaignMember> updatecampaignMemberlst = New Map<Id,campaignMember>();
     For(Lifecycle__c L : scope)
     {
       System.debug(L);
        For(contact C : Contactlst)
        {
          For(campaignMember CM :c.CampaignMembers)
          {
             IF(L.Related_Contact__c == CM.Contactid && C.id == CM.Contactid)
             {
                CM.Lifecycle__c = L.id;
                updatecampaignMemberlst.Put(CM.id,CM);
             }
          }
        }
     }
     
     If(updatecampaignMemberlst.Size()>0)
     {
        Database.update(updatecampaignMemberlst.values());
     }
    }

   global void finish(Database.BatchableContext BC){
   }
}