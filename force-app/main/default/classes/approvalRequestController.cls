public class approvalRequestController {
    public List<SBQQ__QuoteLine__c> qlines{ 
        set;
        get{
            qlines = new List<SBQQ__QuoteLine__c>();            
            for( SBQQ__QuoteLine__c ql : [SELECT SBQQ__ProductName__c, SBQQ__Quantity__c, Approval_Flag_External_Use__c, SBQQ__ListPrice__c, Discount_Percent__c, Unit_Price__c, SBQQ__NetTotal__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quoteId AND SBQQ__Product__r.SBQQ__ExcludeFromOpportunity__c = FALSE  ]){
               qlines.add(ql);               
            }
            return qlines ;
        }
    }
    public Id quoteId {get; set;}   
    public approvalRequestController(){           
    }
}