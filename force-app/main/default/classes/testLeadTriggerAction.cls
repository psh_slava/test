@isTest
Public class testLeadTriggerAction 
{
    
    @testSetup
    public Static Void setup() 
    {
     
        List<LeadLifeCycleMapping__c> lfcCustomSetting = new List<LeadLifeCycleMapping__c>();
        
        LeadLifeCycleMapping__c fieldSet1 = new LeadLifeCycleMapping__c(name = 'Person_Status__c' ,Lead_Api_Name__c ='Status' ,OnCreate__c = true);
        LeadLifeCycleMapping__c fieldSet2 = new LeadLifeCycleMapping__c(name = 'OwnerID' ,Lead_Api_Name__c ='OwnerID' ,OnCreate__c = true);
        LeadLifeCycleMapping__c fieldSet3 = new LeadLifeCycleMapping__c(name = 'Related_Lead__c' ,Lead_Api_Name__c ='Id' ,OnCreate__c = true);
        LeadLifeCycleMapping__c fieldSet4 = new LeadLifeCycleMapping__c(name = 'Score_at_TQL__c' ,Lead_Api_Name__c ='mkto71_Lead_Score__c' ,OnCreate__c = false);  
        LeadLifeCycleMapping__c fieldSet5 = new LeadLifeCycleMapping__c(name = 'Score_at_TAL__c' ,Lead_Api_Name__c ='mkto71_Lead_Score__c' ,OnCreate__c = false);
        LeadLifeCycleMapping__c fieldSet6 = new LeadLifeCycleMapping__c(name = 'Recycle_Reason__c' ,Lead_Api_Name__c ='Back_To_Nurture_Explanation__c' ,OnCreate__c = false);
        
        lfcCustomSetting.add(fieldSet1);
        lfcCustomSetting.add(fieldSet2);
        lfcCustomSetting.add(fieldSet3);
        lfcCustomSetting.add(fieldSet4);
        lfcCustomSetting.add(fieldSet5);
        lfcCustomSetting.add(fieldSet6);
        insert lfcCustomSetting;
        
        String testLbl = label.BDR_Profile_IDs ;
     
    }
    
    @isTest 
    public static void testLeadInsertActions() 
    {
        List<lead> leadLst = new list<lead>();
                        
        Lead l1 = new lead(status='Working' ,phone='1234567890',LastName= 'Lead1', Company='abc' ,CurrencyIsoCode= 'EUR',Lead_Score__c= '1');
        
        Lead l2 = new lead(status ='Engaged',phone='1234567890',Lead_Sub_Status__c= 'Discover' ,LastName= 'Lead2', Company='abc',CurrencyIsoCode='EUR' ,Lead_Score__c ='2');
        
        Lead l3 = new lead(status ='Back to Nurture',phone='1234567890',Lead_Sub_Status__c='',LastName= 'Lead3',Company= 'abc',CurrencyIsoCode= 'EUR',Lead_Score__c='3');
        
        Lead l4 = new lead(status ='Invalid Request',phone='1234567890',Lead_Sub_Status__c='' ,LastName= 'Lead4',  Company='abc',CurrencyIsoCode='EUR' ,Lead_Score__c='4');
        
        Lead l5 = new lead(status='New' ,phone='1234567890',Lead_Sub_Status__c= 'NA',LastName= 'Lead5', Company='abc' ,CurrencyIsoCode= 'EUR',mkto71_Lead_Score__c=500);
        
        Lead l6 = new lead(status='New' ,phone='1234567890',Lead_Sub_Status__c= 'NA',LastName= 'Lead6', Company='abc' ,CurrencyIsoCode= 'EUR',mkto71_Lead_Score__c=500);
        
        Lead l7 = new lead(status='New' ,phone='1234567890',Lead_Sub_Status__c= 'NA',LastName= 'Lead6', Company='abc' ,CurrencyIsoCode= 'EUR',Behavior_Score__c = 12 ,mkto71_Lead_Score__c=90);

        
        leadLst.add(l1);
        leadLst.add(l2);
        leadLst.add(l3);
        leadLst.add(l4);
        leadLst.add(l5); 
        leadLst.add(l6); 
        leadLst.add(l7);
        insert leadLst ;
          
        leadLst[0].status ='New';
        leadLst[0].mkto71_Lead_Score__c =200;
        leadLst[2].status ='Engaged';
        leadLst[1].status ='Back to Nurture';
        leadLst[4].status ='Invalid Request';
        leadLst[3].status ='Working';
        leadLst[5].mkto71_Lead_Score__c=90;
        leadLst[5].Behavior_Score__c = 12 ;
                 
        update leadLst;
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadLst[0].id);
        Test.startTest();
        LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        Test.stopTest();
        
        delete leadLst[5] ;

    }
    
}