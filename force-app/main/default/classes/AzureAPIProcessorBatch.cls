global class AzureAPIProcessorBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    //Get all contacts already on Cherwell Software, LLC. Account
	global Database.QueryLocator start(Database.BatchableContext BC) {
        
        //Using a field set to determine which fields need to be queried. Allows for easier updates in the future
        Id cherwellAccountId = [Select Cherwell_Account_Id__c From Azure_API_Settings__c Limit 1].Cherwell_Account_Id__c;
        String query = 'Select Id';
        for(Schema.FieldSetMember f : Schema.SObjectType.Contact.fieldSets.getMap().get('Contact_Fields_for_API_Query').getFields()){				        	
                    query += ',' + f.getFieldPath();
                }
                query += ' FROM Contact WHERE AccountId =: cherwellAccountId ';
        
		return Database.getQueryLocator(query);
        
	}
    
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        
        //Call class to execute
        System.debug('Executing Azure API BATCH...');
        AzureAPIWrapper.upsertEmployees(scope, AzureAPIWrapper.getGroupMembers());
        
	}
    
    global void finish(Database.BatchableContext BC){ } 

}