/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_refedge_Reference_Basic_InfoaFwTest
{
    @IsTest
    private static void testTrigger()
    {
Account a = new Account(name='test account');
insert a;
        
refedge__Reference_Basic_Information__c rbi = new refedge__Reference_Basic_Information__c();
        rbi.refedge__Account__c = a.Id;
        insert rbi;
        
dlrs.RollupService.testHandler(new refedge__Reference_Basic_Information__c());
    }
}