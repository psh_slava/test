public class CSMLicenseRecordtoInstance {
    
    public static void createLink(List<CSM_License_Record__c> keys){
        
        //for each key passed in the method,
        //get the matching Hosted Instance record by querying Hosted Instances on Account and by Instance #
        //Same Account, Same Instance Number = the matching Hosted Instance 
        
        List<Id> accountIds = new List<Id>();
        for(CSM_License_Record__c key : keys){
            accountIds.add(key.Account__c);
        }
        
        Map<String, String> instanceMap = new Map<String, String>();
        String instanceIdentifier;
        String keyIdentifier;
        
        List<Hosted_Instance__c> allInstances = [Select Id, Account__c, Instance_Num__c, Product_Type__c
                                                 From Hosted_Instance__c
                                                 Where Account__c IN :accountIds
                                                 And Instance_Environment_Type__c = 'Production'];
        for(Hosted_Instance__c hi : allInstances){
            instanceIdentifier = hi.Account__c + ' ' + hi.Instance_Num__c + ' ' + hi.Product_Type__c;
            System.debug('Instance identifer = ' + instanceIdentifier);
            instanceMap.put(instanceIdentifier, hi.Id);
        }
        
        for(CSM_License_Record__c key : keys){
            try{
                keyIdentifier = key.Account__c + ' ' + key.Maintenance__c + ' ' + key.License_Type__c;
                System.debug('Key identifier = ' + keyIdentifier);
                System.debug('Matching instance = ' + instanceMap.get(keyIdentifier));
                key.Hosted_Instance__c = instanceMap.get(keyIdentifier);  
            } catch (Exception e){
                System.debug('Exception caught while looking for matching Hosted Instance record for CSM Key: ' + key.Id);
            }           
        }        
    }
}