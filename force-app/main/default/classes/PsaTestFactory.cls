/*
      Purpose:
            Your purpose here...
            
      Initiative: ...Initiative...
      Author:     Alan Birchenough
      Company:    Icon Cloud Consulting
      Contact:    alan.birchenough@iconatg.com
      Created:    4/12/18
*/

public with sharing class PsaTestFactory {
    public static pse__Region__c createRegion() {
        pse__Region__c region = new pse__Region__c(
                Name = 'Test Region'
        );
        insert region;
        return region;
    }

    public static pse__Proj__c createProject(pse__Region__c region) {
        return createProject(null, region, false);
    }

    public static pse__Proj__c createProject(Contact projMgr, pse__Region__c region, boolean useReportingMgr) {
        pse__Proj__c project = new pse__Proj__c(
                Name='Test Project',
                pse__Is_Active__c=true,
                pse__Is_Billable__c=true,
                pse__Allow_Timecards_Without_Assignment__c=true,
                pse__Allow_Expenses_Without_Assignment__c=true,
                pse__Project_Manager__c = projMgr != null ? projMgr.Id : null,
                pse__Start_Date__c = Date.today().toStartOfWeek(),
                pse__End_Date__c = Date.today().toStartOfWeek().addMonths(6),
                pse__Region__c = region.Id,
                Use_Reporting_Manager_as_Approver__c = useReportingMgr
        );

        insert project;
        return project;
    }
}