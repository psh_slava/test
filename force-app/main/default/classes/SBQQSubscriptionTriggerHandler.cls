/*
 * SBQQSubscriptionTriggerHandler
 * @author: Naomi Harmon
 * @last modified: 4/2020
 * @related classes: 
 * @test class: SBQQSubscriptionTriggerTest
*/

public class SBQQSubscriptionTriggerHandler {
    
    public void onBeforeInsert(List<SBQQ__Subscription__c> newList){
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c sub : newList){
            //Fill in important fields if blank
            if(sub.Instance_Number__c == null){ sub.Instance_Number__c = 'Instance 1'; }
            if(sub.Instance_Name__c == null){ sub.Instance_Name__c = sub.Account_Name__c; }             
        }
    }
    
    public void onBeforeUpdate(Map<Id, SBQQ__Subscription__c> oldMap, Map<Id, SBQQ__Subscription__c> newMap){
        //Fill in important fields if blank
        for(SBQQ__Subscription__c sub : newMap.values()){
            if(sub.Instance_Number__c == null){ sub.Instance_Number__c = 'Instance 1'; }
            if(sub.Instance_Name__c == null){ sub.Instance_Name__c = sub.SBQQ__Account__c; }
        }   
    }   

}