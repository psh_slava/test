/*
* @class name:   OpportunityTeamEditController
* @created:      By Dileep Allada in May 2020
* @test class:   OpportunityTeamEditControllerTest.apxc
* @initiated by: OpportunityTeamEdit.vfp
* @description: 
*    Allows users outside of the Opportunity Owner (i.e. Channel Managers) to add users/partner users to an Opportunity Team. 
*    Partner Users available in lookup are filtered by which Partner Accounts are listed as involved on that Opportunity.
* @modifcation log:
*
*/

public class OpportunityTeamEditController {
    
    public List<OpportunityTeamMember> lstOT  = new List<OpportunityTeamMember>();
    public List<innerClass> lstInner {get;set;}
    public String selectedRowIndex {get;set;}  
    public Integer count = 5;
    public Integer rowIndex {get;set;}
    Public Id currentRecordId{Get;Set;}
    Public string Partner{get;set;}
  
    public OpportunityTeamEditController(ApexPages.StandardController controller) {
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        lstInner = new List<innerClass>();
        addMore();
        selectedRowIndex = '0';
    }
    
    public List<SelectOption> getdynamiclist() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Partner','Partner'));       
        options.add(new SelectOption('User','User'));

        return options;
    }
    
    Public pagereference OpportunityTeam()
    {
        OpportunityTeamUsers__c[] permissionedUsers = [SELECT SetupOwnerId, SetupOwner.Type FROM OpportunityTeamUsers__c];
        
        //Boolean Flag = False;
        System.debug(UserInfo.getUserId());
        For(OpportunityTeamUsers__c user : permissionedUsers){        
            IF(user.SetupOwnerId == UserInfo.getUserId())
            {             
                pagereference Pr = New pagereference('/'+currentRecordId);               
                for(Integer j = 0;j<lstInner.size();j++)
                {
                    System.debug(lstInner[j].OT.UserId);                    
                    lstInner[j].OT.OpportunityId = currentRecordId; 
                    If(lstInner[j].OT.UserId <> Null)
                    {                       
                        lstOT.add(lstInner[j].OT);
                    }                    
                }                
                insert lstOT;
                
                pr.setRedirect(True);               
                Return Pr;              
            }
        }
        
        pagereference  Ref = New pagereference('/apex/OpportunityTeamEdit');
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'You do not have access to this button. Please contact your manager or the Salesforce Admin team.'));
        Return Ref;
        
    }
    
    //add one more row
    public void Add()
    {   
        count = count+1;
        // addMore();  
        innerClass objInnerClass = new innerClass(count);
        lstInner.add(objInnerClass);     
    }
    
    /*Begin addMore*/
    public void addMore()
    {
        Integer c ;
        For(c = 1; c <= count ; c++)
        {
            innerClass objInnerClass = new innerClass(count);
            lstInner.add(objInnerClass);    
            system.debug('lstInner---->'+lstInner);   
        }         
    }
    
    public void Del()
    {
        system.debug('selected row index---->'+selectedRowIndex);
        
        lstInner.remove(Integer.valueOf(selectedRowIndex)-1);
        count = count - 1;     
    }/*End del*/
    
    
    /*Inner Class*/
    public class innerClass
    {       
        /*recCount acts as a index for a row. This will be helpful to identify the row to be deleted */
        public String recCount {get;set;}
        public OpportunityTeamMember OT {get;set;}
        
        public innerClass(Integer intCount)
        {
            recCount = String.valueOf(intCount);        
            OT = new OpportunityTeamMember();
            OT.TeamMemberRole = 'Partner Sales';
        } 
    }    
}