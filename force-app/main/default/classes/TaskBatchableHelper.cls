/**
* @author Conga Services
* @author Dan Sherman
* @version 1.00
* @date 10/27/2016
* @description Create methods to help a batchable calss update leads to populate activity count and first ourtreach date
*/
public class TaskBatchableHelper {
    /**
    * @author Conga Services
    * @author Dan Sherman
    * @version 1.00
    * @date 10/27/2016
    * @description Method should count activities which meet the type and other criteria specified in the custom setting and formula field referenced below
    */
    public void activityCount(List<Lead> leads) {

        System.debug('leads size: '+leads.size());
        List<Lead> leadsToUpdate = new List<Lead>();

        //Create a map of leads and the eventual number that will be added as the Activity Count
        Map<Id,Integer> leadMap = new Map<Id,Integer>();
        for(Lead lead : leads) {
            leadMap.put(lead.Id,0);
        }

        //Set the task and event lists to include countable activities, utilizing the Rejected Activity Types Custom setting
        //The custom setting contains a list of all types of activities which should not count toward countable tasks
        List<Rejected_Activity_Types__c> rat = [SELECT Name FROM Rejected_Activity_Types__c];
        Set<String> ratSet = new Set<String>();
        for(Rejected_Activity_Types__c r : rat) {
                ratSet.add(r.Name);
        }

        AggregateResult[] tasksByWhoId = [SELECT COUNT(Id)cnt, WhoId FROM Task WHERE 
                                                                                    Type NOT IN :ratSet AND
                                                                                    Non_Type_Countable_CriteriaFx__c = true AND
                                                                                    WhoId IN :leadMap.keySet()
                                                                                    GROUP BY WhoId];

        AggregateResult[] eventsByWhoId = [SELECT COUNT(Id)cnt, WhoId FROM Event WHERE 
                                                                                    Type NOT IN :ratSet AND
                                                                                    Non_Type_Countable_CriteriaFx__c = true AND
                                                                                    WhoId IN :leadMap.keySet()
                                                                                    GROUP BY WhoId];


        //update the count for each lead in the map for tasks and then do the same for Events
        for(AggregateResult ar : tasksByWhoId) {
            Id leadId = String.valueOf(ar.get('WhoId'));
            if(leadMap.containsKey(leadId)) {
                Integer count = Integer.valueOf(ar.get('cnt'));
                leadMap.put(leadId,count);
            }
        }

        for(AggregateResult ar : eventsByWhoId) {
            Id leadId = String.valueOf(ar.get('WhoId'));
            if(leadMap.containsKey(leadId)) {
                Integer count = leadMap.get(leadId);
                Integer eventCount = Integer.valueOf(ar.get('cnt'));
                count += eventCount;
                leadMap.put(leadId,count);
            }
        }


        //Update leads
        for(Id lead : leadMap.keySet()) {
            Lead nl = new Lead(Id=lead);
            nl.Activity_Count__c = leadMap.get(lead);
            leadsToUpdate.add(nl);
        }

        if(leadsToUpdate.size() > 0) {
            Database.SaveResult[] dsr = Database.update(leadsToUpdate, false);
        }

    }



    /**
    * @author Conga Services
    * @author Dan Sherman
    * @version 1.00
    * @date 10/27/2016
    * @description Method should update the first outreach date if an acitivty exists with an earlier outreach date or the value on the lead is currently null
    */
    public void firstOutreach(List<Lead> leadsList) {

        List<Task> possibleOutreachTasks = new List<Task>();
        List<Event> possibleOutreachEvents = new List<Event>();
        List<Lead> leadsToUpdate = new List<Lead>();
        Map<Id,Date> leadDateMap = new Map<Id,Date>();
                

        Map<Id,Date> leadMap = new Map<Id,Date>();
        for(Lead l : leadsList) {
            leadMap.put(l.Id,l.First_Outreach_Date__c);
        }

        //Set the task and event lists to include countable activities, utilizing the Rejected Activity Types Custom setting
        //The custom setting contains a list of all types of activities which should not count toward countable tasks
        List<Rejected_Activity_Types__c> rat = [SELECT Name FROM Rejected_Activity_Types__c];
        Set<String> ratSet = new Set<String>();
        for(Rejected_Activity_Types__c r : rat) {
                ratSet.add(r.Name);
        }

        //Find records that meet outreach criteria. 
        possibleOutreachTasks = [SELECT ActivityDate, OwnerId, WhoId FROM Task WHERE 
                                                                                Type NOT IN :ratSet AND
                                                                                Non_Type_Countable_CriteriaFx__c = true AND
                                                                                WhoId IN :leadMap.keySet() AND
                                                                                ActivityDate != null
                                                                                ];
        
        possibleOutreachEvents = [SELECT ActivityDate, ActivityDateTime, WhoId FROM Event WHERE
                                                                                Type NOT IN :ratSet AND
                                                                                Non_Type_Countable_CriteriaFx__c = true AND
                                                                                WhoId IN :leadMap.keySet()
                                                                                ];

        //Conditionally add the date of the activity to the leadDateMap, a map of Lead Id to smallest activity date
        for(Task t : possibleOutreachTasks) {
            if(t.ActivityDate != null && (leadMap.get(t.WhoId) == null || leadMap.get(t.WhoId) > t.ActivityDate)) leadDateMap.put(t.WhoId, t.ActivityDate);

            if(t.ActivityDate != null && leadDateMap.containsKey(t.WhoId) && leadDateMap.get(t.WhoId) > t.ActivityDate ) leadDateMap.put(t.WhoId, t.ActivityDate);
        }

        //For the event check, first ensure that there is either an ActivityDate or an ActivityDateTime, then check (if the date has been added to leadDateMap)
        //whether, if present, either existing date is smaller than what is in the list now. If so, replace.
        //If not, check if either date is smaller than what is in the leadMap list. If so, add to the leadDateMap
        for(Event e : possibleOutreachEvents) {

            if( e.ActivityDate != null && leadDateMap.containsKey(e.WhoId) && leadDateMap.get(e.WhoId) > e.ActivityDate ) {
                leadDateMap.put(e.WhoId, e.ActivityDate);
            } 
            else if( e.ActivityDate != null && (leadMap.get(e.WhoId) == null || leadMap.get(e.WhoId) > e.ActivityDate) ) {
                leadDateMap.put(e.WhoId, e.ActivityDate);
            }

            if( e.ActivityDateTime != null && leadDateMap.containsKey(e.WhoId) && (leadDateMap.get(e.WhoId) > e.ActivityDateTime || leadDateMap.get(e.WhoId) == null) )  {
                leadDateMap.put(e.WhoId, e.ActivityDateTime.date());
            }
            else if( e.ActivityDateTime != null && (leadMap.get(e.WhoId) == null || leadMap.get(e.WhoId) > e.ActivityDateTime) ) {
                leadDateMap.put(e.WhoId, e.ActivityDateTime.date());    
            }       
        }
        
        for(Id lead : leadDateMap.keySet()) {
            Lead nl = new Lead(Id=lead);
            nl.First_Outreach_Date__c = leadDateMap.get(lead);
            leadsToUpdate.add(nl);
        }
        
        if(leadsToUpdate.size() > 0) {
            Database.SaveResult[] results = Database.update(leadsToUpdate,false);

            for(Database.SaveResult sr : results) {
                if(!sr.isSuccess()) {
                    Database.Error err = sr.getErrors()[0];
                    System.debug(err);
            }
        }


        }

    }
}