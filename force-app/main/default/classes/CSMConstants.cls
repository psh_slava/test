public class CSMConstants
{
	public static final String SYNC_STATUS_SYNCHRONIZED = 'Synchronized'; 
	public static final String SYNC_STATUS_ERROR = 'Error'; 
	public static final String SYNC_STATUS_PENDING = 'Pending'; 
	public static final String SYNC_STATUS_SKIPPED = 'Skipped'; 
	public static final String SYNC_STATUS_RETRIED = 'Retried'; 
	public static final String SYNC_STATUS_AUTO_RETRIED = 'Auto-Retried'; 

	public static final String SYNC_EVENT_TYPE_OUTBOUND = 'Outbound';
	public static final String SYNC_EVENT_TYPE_INBOUND = 'Inbound';
    
    public static final List<String> CSM_SYNC_FIELDS = new List<String>{ 
        'CSM_Sync_Status__c',
        'CSM_Sync_Message__c',
        'CSM_Last_Synced_Date__c'};
                
    public static final List<String> CENTRAL_SYNC_FIELDS = new List<String>{
         'Central_Sync_Status__c',
         'Central_Sync_Message__c',
         'Central_Last_Synced_Date__c'};
                            
                            
    public static final Set<String> SYSTEM_USER_FIELDS = new Set<String>{
         'CreatedById',
         'LastModifiedById',
         'OwnerId'};
}