public class Renewalcontractupdatecontroller {
    public Contract con { get; set; }
    public Contract c { get; set; }
    public String Cname { get; set; }
    public Contact cont { get; set; }
    public RecordType Rtype { get; set; }
    public Boolean Flag { get; set; }
    public List<Opportunity> Renewalopplst { get; set; }
    ApexPages.StandardController standardController;
    
    public Renewalcontractupdatecontroller(ApexPages.StandardController controller) {
        this.con = (Contract) controller.getRecord();
        Cname = apexpages.currentpage().getparameters().get('conname');
        Flag = false;
        Renewalopplst = new List<Opportunity>();
        c = [SELECT id, SBQQ__RenewalOpportunity__c, SBQQ__RenewalForecast__c, SBQQ__RenewalQuoted__c
             FROM Contract
             WHERE ID = :con.id
             LIMIT 1];
        cont = [SELECT id, name FROM contact WHERE Name = :Cname LIMIT 1];
        Rtype = [SELECT Name
                 FROM RecordType
                 WHERE SobjectType = 'Opportunity'
                 AND IsActive = TRUE
                 AND Name IN ('Renewal')
                 LIMIT 1];
    }
    public Pagereference OverallAction() {
        if (C != null && c.SBQQ__RenewalForecast__c == true) {
            ApexPages.Message msg = new Apexpages.Message(
                ApexPages.Severity.Warning,
                'This is an active Contract with Cherwell that already has an exisiting Renewal Opportunity. Please click the "Go to Existing Renewal Opportunity" button to navigate to the existing Renewal Opportunity. If you would like to create a new Renewal Opportunity, click the "Create New Renewal Opportunity" button.'
            );
            ApexPages.addmessage(msg);
            Renewalopplst = [SELECT id, CloseDate, StageName, SBQQ__RenewedContract__c, Name, Primary_Contact__c
                             FROM opportunity
                             WHERE id = :c.SBQQ__RenewalOpportunity__c]; 
            PageReference pageRef = Page.Renewalcontractupdate;
            
        } else if (C != null && c.SBQQ__RenewalForecast__c == false) {
            C.SBQQ__RenewalForecast__c = true;
            update C;
            
            if(Renewalopplst.size() > 0) {
                Opportunity Opp;
                opp = Renewalopplst[0]; 
                Opp.primary_contact__c = cont.id;
                update Opp;
            }
            PageReference pr = new PageReference('/' + c.SBQQ__RenewalOpportunity__c);
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }
    
    public Pagereference redirectrnewal() {
        C.SBQQ__RenewalForecast__c = false;
        C.SBQQ__RenewalOpportunity__c = null;
        C.SBQQ__RenewalQuoted__c = false;
        update C;
        C.SBQQ__RenewalForecast__c = true;
        update C;
        c = [SELECT id, SBQQ__RenewalOpportunity__c FROM Contract WHERE ID = :c.id];
        try{ 
            Opportunity Opp = [SELECT id, primary_contact__c
                               FROM opportunity
                               WHERE id = :c.SBQQ__RenewalOpportunity__c];
            Opp.primary_contact__c = cont.id;
            update Opp;
            PageReference pr = new PageReference('/' + c.SBQQ__RenewalOpportunity__c);
            pr.setRedirect(true);
            return pr;
        } catch(Exception e){
            System.debug('Exception caught while trying to update new Renewal Opp' + e.getMessage());
            return null;
        }
    }
    
    public Pagereference existigOpportunity() {
        PageReference pr = new PageReference('/' + c.SBQQ__RenewalOpportunity__c);
        pr.setRedirect(true);
        return pr;
    }
}