/**
* @author Conga
* @author DSherman
* @version 1.00
* @Date 11/15/2016
* @description Tests LeadHelper 
*/
@isTest
private class LeadHelperTest {
	
	//Ids of the two owners
	//static Id ownerId;
	//static Id secOwnerId;

	/**
	* @author Conga
	* @author DSherman
	* @version 1.00
	* @Date 11/15/2016
	* @description Tests successful creation of a sharing rule, testing that the number of sharing records created matches the intended number
	*/
	/**
	* @version 2.00
	* @Date 12/12/2016
	* @description Commenting out code. Trigger, Trigger Dispatcher, Trigger States have been deleted. If code is required in future - recreate and uncomment LeadHelper/LeadHelperTest (AHAFEZ)
	*/
	//@isTest
	//static void testShareCreation(){

	//	Test.startTest();
	//		List<Lead> leadsToCreate = createData(210,100);
	//		insert leadsToCreate;
	//		update leadsToCreate;
	//	Test.stopTest();
		

	//	List<LeadShare> leadShares = [SELECT LeadId, UserOrGroupId FROM LeadShare WHERE LeadId IN :leadsToCreate AND UserOrGroupId = :secOwnerId];
	//	//System.assertEquals(100,leadShares.size());
	//}


	////Heper method which creates 2 users and Leads, assigning the first user to Lead Owner for all records (number to create is passed in)
	////It then assigns the secOwner to leads as the secondary owner up to the number of intended secondary owners
	////Returns the list for insertion and assertion in the class above
	//private static List<Lead> createData (Integer numToCreate, Integer numWithSecondary) {

	//	Integer secondaryPopulated = 0;
	//	List<Lead> leadsCreated = new List<Lead>();
	//	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

	//	User owner = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
 //           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
 //           LocaleSidKey='en_US', ProfileId = p.Id,
 //           TimeZoneSidKey='America/Los_Angeles', UserName='testUser' + UserInfo.getOrganizationId() +  '@testorg.com');
	//	insert owner;

	//	User secOwner = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
 //           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
 //           LocaleSidKey='en_US', ProfileId = p.Id, 
 //           TimeZoneSidKey='America/Los_Angeles', UserName='testUser2' + UserInfo.getOrganizationId() +  '@testorg.com');
	//	insert secOwner;

	//	ownerId = owner.Id;
	//	secOwnerId = secOwner.Id;


	//	for(Integer i = 0; i < numToCreate; i++) {
	//		Lead l = new Lead();
	//		l.Status = 'New';
	//		l.LastName = 'Test'+i;
	//		l.Company = 'TestCo'+i;

	//		if(secondaryPopulated < numWithSecondary) {
	//			l.Secondary_OwnerId__c = secOwnerId;
	//			secondaryPopulated++;
	//		}

	//		leadsCreated.add(l);
	//	}

	//	return leadsCreated;

	//}
}