@IsTest 
public with sharing class OpportunityTeamEditControllerTest {
      
    @isTest public static void testOpportunityTeamEditController(){
        //Grab the record type Id for Account Record Type "Partner"
        String partnerRtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();

        //Create an Account to be the pretend Partner involved on the Opportunity
        Account partnerAcct = new Account(Name = 'Test Partner', Authorized_Partner__c = true, Partner_Types__c = 'Referral Partner', Account_Status__c = 'Active',
                                         Type = 'Authorized Partner', RecordTypeId = partnerRtId);
        insert partnerAcct;
        
        //Create the Opportunity that you're pretending to push the Opportunity Team Edit button on
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test';
        oppty.Amount = 0;
        oppty.Referral_PartnerId__c = partnerAcct.Id;
        oppty.StageName = 'Pre-Qualification';
        oppty.CloseDate = System.today();
        oppty.Forecast_Category__c = 'Omit';
        insert oppty;
        
        //The controller references a custom setting called OpportunityTeamUsers__c, so create a record to pull into that query too
        OpportunityTeamUsers__c otu = new OpportunityTeamUsers__c();
        otu.SetupOwnerId = UserInfo.getUserId();
        insert otu;
        
        //Pretend to push the button by launching the OpportunityTeamEdit page
        PageReference newPage = Page.OpportunityTeamEdit;
        newPage.getParameters().put('id', oppty.Id);
        Test.setCurrentPage(newPage);
        
        //Call the controller
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppty);
        OpportunityTeamEditController controller = new OpportunityTeamEditController(sc);       
        //Call individual methods in the controller
        controller.getdynamiclist();
        controller.add();
        controller.addMore();
        controller.OpportunityTeam();
        //controller.del(); //This one's more difficult to call because you have to set an index, so let's just see if we can get by without it :)          
    }
}