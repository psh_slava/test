// Structure to hold the response data from a business object record save request.  
// Also encapsulates specific parsing logic.
public class CSMBusinessObjectRecordResponseWrapper
{
	public class FieldValidationErrorWrapper {
		public String error;
		public String errorCode;
		public String fieldId;
	}

	public String busObPublicId;
	public String busObRecId;
	public List<FieldValidationErrorWrapper> fieldValidationErrors;
	public String errorCode;
	public String errorMessage;
	public Boolean hasError;

	public static CSMBusinessObjectRecordResponseWrapper parseJSON(String jsonStr) {
		CSMBusinessObjectRecordResponseWrapper borw = (CSMBusinessObjectRecordResponseWrapper)JSON.deserialize(jsonStr, CSMBusinessObjectRecordResponseWrapper.class);
		return borw;
	}
}