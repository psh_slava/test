@isTest
public class LimitDiscoverOrgLeadsTest {
    
    public testMethod static void testContactImport(){
        //create the custom setting
        DiscoverOrg_Contacts_Limit__c batchSettings = new DiscoverOrg_Contacts_Limit__c(
            Name = 'Default',
            Daily_Limit__c = 100);
        insert batchSettings;
              
        User u = [Select Id From User Where Name = 'Naomi Harmon'];
        
        List<Lead> lImport = new List<Lead>();
        for(Integer i = 0; i<100; i++){
            Lead l = new Lead();
            l.lastName = 'Tester'+ i;
            l.Company = 'Test Company'+i;
            l.DSCORGPKG__DiscoverOrg_Created_On__c = DateTime.NOW();
            l.Email = 'tester'+i+'@cherwell.test';
            l.OwnerId = u.Id;
            lImport.add(l);
        }
        insert lImport;
        
        List<Lead> inserts = [Select Id
                                  From Lead];
        System.debug('Leads successfully inserted - - '+inserts.size());
        
        Lead l101 = new Lead(lastName = 'test', company='Test Co',
                             DSCORGPKG__DiscoverOrg_Created_On__c = DateTime.NOW(),
                             email = 'lasttest@cherwell.com');
        try{
            insert l101;
        } catch (DMLException e){
            System.debug('The Lead could not be inserted: ' + e.getMessage());
        }

    }

}