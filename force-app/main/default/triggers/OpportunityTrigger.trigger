/**
* Opportunity trigger
*   
* @author CRMCulture  
* @version 1.00 
* @param Platform after insert, after update, before insert, before update
*/ 

//Updated 7/15/19: Naomi Harmon

trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(!System.isFuture()){
        TriggerFactory.createTriggerDispatcher(Opportunity.sObjectType);
    }
    
    OpportunityTrigger_Handler handler = new OpportunityTrigger_Handler();
    
    if(trigger.isAfter){
        if(Trigger.IsInsert){
            system.debug('Opportunity After Insert');
            handler.onAfterInsert(trigger.new);
        }
        if(Trigger.IsUpdate){
            system.debug('Opportunity After Update');
            handler.onAfterUpdate(trigger.newMap, trigger.oldMap);
        }
    }
    
    if(trigger.isBefore && trigger.isUpdate){        
        //is CPQ Closed Won
        Set<Id> contactIds = new Set<Id>();
        //List<Contact> contactsToTag = new List<Contact>();
        List<Id> accountsToUpdate = new List<Id>();
        List<Id> oppsToContract = new List<Id>();
        Map<Id, String> mapContactToRole = new Map<Id, String>();
        for(Opportunity oppty : trigger.new){
            if(oppty.SBQQ__PrimaryQuote__c != null && Trigger.oldMap.get(oppty.Id).IsWon == false && oppty.IsWon == true){
                //Contract Opp
                oppsToContract.add(oppty.Id);
                oppty.SBQQ__Contracted__c = true;
                //Push Customer's original contract date
                if(oppty.Record_Type_Name__c == 'New Logo' && oppty.Account.Customer_Original_Contract_Date__c == null){
                    accountsToUpdate.add(oppty.AccountId);
                }
                
                //Tag Contact Roles
                contactIds.add(oppty.SBQQ__PrimaryQuote__r.License_Key_Contact__c);
                contactIds.add(oppty.SBQQ__PrimaryQuote__r.Billing_Contact__c);
                contactIds.add(oppty.Primary_Contact__c);
                mapContactToRole.put(oppty.SBQQ__PrimaryQuote__r.License_Key_Contact__c, 'License Key');
                mapContactToRole.put(oppty.SBQQ__PrimaryQuote__r.Billing_Contact__c, 'Billing');
                mapContactToRole.put(oppty.Primary_Contact__c, 'Primary Support');
            }
        }
       
        if(contactIds.size() > 0){
           /*List<Contact> cRoles = [Select Id, License_Key__c, Billing__c, Primary_Support__c
                                    From Contact
                                    Where Id IN :contactIds];
            for(Contact cR : cRoles){
                if(mapContactToRole.get(cR.Id) != null && mapContactToRole.get(cR.Id) == 'License Key'){
                    cR.License_Key__c = true;
                }
                if(mapContactToRole.get(cR.Id) != null && mapContactToRole.get(cR.Id) == 'Billing'){
                    cR.Billing__c = true;
                }
                
                if(mapContactToRole.get(cR.Id) != null && mapContactToRole.get(cR.Id) == 'Primary Support'){
                    cR.Primary_Support__c = true;
                }
                contactsToTag.add(cR);
            }
            update contactsToTag;*/
            OpportunityTrigger_handler.tagContactCommChannels(contactIds, mapContactToRole);
        }
        
        //Capture dated exchange rate value when Opp is created or its Close Date is updated 
        List<Opportunity> exrtOpps = new List<Opportunity>(); 
        for(Opportunity oppt : Trigger.new){ 
            if((Trigger.oldMap.get(oppt.Id).CloseDate != oppt.CloseDate) || oppt.Dated_Exchange_Rate__c == null 
                || Trigger.oldMap.get(oppt.Id).CurrencyIsoCode != oppt.CurrencyIsoCode){ 
                   exrtOpps.add(oppt); 
               } 
        } if(exrtOpps != null && exrtOpps.size()>0){ 
            CurrentExchangeRate.captureOnOppty(exrtOpps); 
        }
    }
    
}