/*
TEST CLASS = OpportunityTriggerTest
*/

/*Naomi Harmon 12/17/18: Deactivating trigger to allow for CPQ Upsell quotes to be created automatically. New Primary Contact required
                                 validation and Process Builder flow will
                                 help check for existing Contact Role*/

trigger Opportunity_Trigger on Opportunity (after insert, after update, before insert, before update){
/*
    OpportunityTriggerHandler  oppHandler = new OpportunityTriggerHandler ();
    
    //BEFORE UPDATE
    if((Trigger.isAfter && Trigger.isInsert)){
        
        //THIS VARAIBLE WILL STOP OPP CONTACT VALIDATION RULE
           RecursionController.stopOppContactRoleValidation = true;
        
    }
    
    //BEFORE UPDATE
    if(( Trigger.isBefore && Trigger.isUpdate))
        oppHandler.beforeUpdate(Trigger.oldMap, Trigger.newMap);
*/        
}