trigger AssignUpsellEligibilityonSubscriptions on Contract (after insert, after update) {
    //Only applies to bundle header lines

    Id contractId;
    Map<String,Datetime> lineDateMap = new Map<String,Datetime>();
    
    for(Contract c : trigger.new){
        contractId = c.Id;
    }
    
    AggregateResult[] maxLineDates = [Select SBQQ__Product__c, Instance_Number__c, MAX(CreatedDate)
                                      From SBQQ__Subscription__c
                                      Where SBQQ__Contract__c = :contractId
                                      And SBQQ__Bundle__c = true
                                      And (NOT SBQQ__Product__r.Name LIKE '%License%')
                                      Group By SBQQ__Product__c, Instance_Number__c];
    System.debug('Results-----' + maxLineDates);
    
    for(AggregateResult ar : maxLineDates){
        string prod = (String)ar.get('SBQQ__Product__c');
        string inst = (String)ar.get('Instance_Number__c');
        string lineKey = prod + inst;
        lineDateMap.put(prod+inst,(Datetime)ar.get('expr0'));
    }
    
    List<SBQQ__Subscription__c> allSubs = [Select Id, Upsell_Eligible_2__c, SBQQ__Product__c, Instance_Number__c, CreatedDate
                                             From SBQQ__Subscription__c
                                            Where SBQQ__Contract__c = :contractId
                                            And (NOT SBQQ__Product__r.Name LIKE '%License%')
                                            And SBQQ__Bundle__c = true];
    List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
    
    for(SBQQ__Subscription__c s : allSubs){
        string sProd = s.SBQQ__Product__c;
        string sInst = s.Instance_Number__c;
        string sKey = sProd + sInst;
        
        lineDateMap.put(sKey, lineDateMap.get(sKey));
        System.debug('Key Date----'+lineDateMap.get(sKey));
        System.debug('Created Date----'+s.CreatedDate);
        if(lineDateMap.get(sKey) == s.CreatedDate){
            //s.Upsell_Eligible__c = true;
            s.Upsell_Eligible_2__c = 'True';
            subsToUpdate.add(s);
            System.debug('Set Upsell to Eligible');
        }
        if(lineDateMap.get(sKey) != s.CreatedDate){
            s.Upsell_Eligible_2__c = 'False';
            subsToUpdate.add(s);
            System.debug('Upsell not Eligible');
        }
               
    }
    
    update subsToUpdate;

}