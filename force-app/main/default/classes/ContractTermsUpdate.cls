public class ContractTermsUpdate {
    
    public static void updatecontractterms(Contract c, Id quoteId){
        
        List<Contract_Term_Mappings__c> fieldMappings = [Select Mirror_Field_API_Name__c, Update__c, From_Object_API_Name__c, From_Field_API_Name__c
                                                         From Contract_Term_Mappings__c
                                                         Where Active__c = true];
        
        //Get all fields from Quote that are mapped
        String query = 'SELECT Id, ';
        for(Contract_Term_Mappings__c mapping : fieldMappings)
        {
            if(mapping.From_Object_API_Name__c == 'SBQQ__Quote__c'){
                String theName = mapping.Mirror_Field_API_Name__c;
            query += theName + ',';
            }
            if(mapping.From_Object_API_Name__c != 'SBQQ__Quote__c'){
                String theName = mapping.From_Object_API_Name__c + '.' + mapping.From_Field_API_Name__c;
                query += theName + ',';
            }
        }
        // Trim last comma
        query = query.subString(0, query.length() - 1);
        // Finalize query string
        query += ' FROM SBQQ__Quote__c Where Id = \'' + quoteId + '\' Limit 1';
        //End get all fields
        
        SBQQ__Quote__c q;
        try{
            q = Database.query(query);
        } catch (Exception e){
            System.debug('Exception caught while querying for Quote - ' + e.getMessage());
        }
        
        for(Contract_Term_Mappings__c ctm : fieldMappings){
            if(ctm.From_Object_API_Name__c == 'SBQQ__Quote__c'){
                if(c.get(ctm.Mirror_Field_API_Name__c)==null && q.get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                }
                if(c.get(ctm.Mirror_Field_API_Name__c) != null && ctm.Update__c == true && q.get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                } 
            }
            if(ctm.From_Object_API_Name__c != 'SBQQ__Quote__c'){
                
                if(c.get(ctm.Mirror_Field_API_Name__c)==null && q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                }
                if(c.get(ctm.Mirror_Field_API_Name__c) != null && ctm.Update__c == true && q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                } 
            }
        }
    }    
}