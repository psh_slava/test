public class LeadTriggerHandler
{
    LeadTriggerAction action  = new LeadTriggerAction();
    MergeUtil util = new MergeUtil();
    
    //ON INSERT OF LEAD 
    public void onAfterInsert(list<lead> newLst){
        Boolean triggerEnabled = true;
        try{
            triggerEnabled = [Select Enabled__c 
                              From Lifecycle_Trigger_Settings__c
                              Where Name = 'LeadTriggerAction'
                              Limit 1].Enabled__c;
        } catch (Exception e){
            System.debug('No Lifecycle Trigger Setting Found');
        }
        if(triggerEnabled){ action.createNewLifeCycle(newLst); }
    }
    public void onBeforeDelete(Map<Id, lead> oldMap){
        
        //update related lifecycle status when lead is merged
        util.leadMergeProcessBeforeDelete(oldMap);
    }
    
    public void onAfterDelete(Map<Id, Lead> oldMap){      
        //delete the orphan records if the del;ete is not due to merge process
        //action.deleteLifeCycleRecords(oldMap.values());
        
        //update related lifecycle status when lead is merged
        util.leadMergeProcessAfterDelete(oldMap);
    }
    public void onAfterUpdate(map<id , lead> oldMap , map<id , lead> newMap){
                Boolean triggerEnabled = true;
        try{
            triggerEnabled = [Select Enabled__c 
                              From Lifecycle_Trigger_Settings__c
                              Where Name = 'LeadTriggerAction'
                              Limit 1].Enabled__c;
        } catch (Exception e){
            System.debug('No Lifecycle Trigger Setting Found');
        }
        if(triggerEnabled){ action.lyfcycleUpdate(oldMap,newMap); }
        
    }
}