@isTest
private class LicenseKeyTriggerHandlerTest {
    
    private static testmethod void testTriggerAndHandler(){
        Account a = new Account(Name='Test Account');
        insert a;
        
        Contact c = new Contact(LastName='Tester', AccountId = a.Id, Email = 'test@test.test');
        insert c;
        
        CSM_License_Record__c eval = new CSM_License_Record__c(Account__c = a.Id, License_Type__c = 'CSM', Licensed_Items__c = 15, Maintenance__c = 'Instance 1',
                                                               License_Key__c = 'testkey1234507182019', Type__c = 'Evaluation', Expires__c = Date.today().addDays(14));
        insert eval;
        
        CSM_License_Record__c newLogo = new CSM_License_Record__c(Account__c = a.Id, License_Type__c = 'CSM', Licensed_Items__c = 15, Maintenance__c = 'Instance 1',
                                                                  License_Key__c = 'testkey67891007182019', Type__c = 'Subscription', Purchase_Scenario2__c = 'New Logo', Expires__c = Date.today().addDays(365),
                                                                  Primary_Contact__c = c.Id, Former_Key__c = eval.Id, License_Name__c = 'TestCoLicense1');
        insert newLogo;
        
        CSM_License_Record__c upsell = new CSM_License_Record__c(Account__c = a.Id, License_Type__c = 'CSM', Licensed_Items__c = 20, Maintenance__c = 'Instance 1',
                                                                 License_Key__c = 'testkey11121307182019', Type__c = 'Subscription', Purchase_Scenario2__c = 'Upsell', Expires__c = Date.today().addDays(365),
                                                                 Generated_in_Salesforce__c = true, Primary_Contact__c = c.Id, License_Name__c = 'TestCoLicense1');
        insert upsell;
        
        CSM_License_Record__c renewal = new CSM_License_Record__c(Account__c = a.Id, License_Type__c = 'CSM', Licensed_Items__c = 20, Maintenance__c = 'Instance 1',
                                                                  License_Key__c = 'testkey11121307182019r', Type__c = 'Subscription', Purchase_Scenario2__c = 'Renewal', Expires__c = Date.today().addDays(700),
                                                                  Generated_in_Salesforce__c = true, Primary_Contact__c = c.Id, License_Name__c = 'TestCoLicense1', Renewal_Start_Date_Manual__c = Date.today().addDays(4));
        insert renewal;
        
        CSM_License_Record__c burst = new CSM_License_Record__c(Account__c = a.Id, License_Type__c = 'CSM', Licensed_Items__c = 200, Maintenance__c = 'Instance 1', Deal_Type_Manual__c = 'Direct',
                                                                License_Key__c = 'testkey14151607182019', Type__c = 'Burst!', Purchase_Scenario2__c = 'Upsell', Expires__c = Date.today().addDays(36),
                                                                Generated_in_Salesforce__c = true, Primary_Contact__c = c.Id, License_Name__c = 'TestCoLicense1');
        insert burst;        
    }
}