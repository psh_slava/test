// Logic for creating and processing sync entries
public class CSMSyncEntryHelper
{
    // used for outbound sync entry
    public static void enqueueSyncEntry(Map<Id, SObject> oldMap, List<SObject> newObjs) {
        List<CSM_Sync_Entry__c> syncEntries = new List<CSM_Sync_Entry__c>();
        List<SObject> objsToUpdate = new List<SObject>();
        
        Schema.SObjectType objectType = newObjs[0].getSObjectType();
        String objectTypeName = String.valueOf(objectType);
        
        for(SObject newObj : newObjs) {
            Boolean noSync = false;
            try{
                noSync = (Boolean)newObj.get('Do_Not_Sync_with_CSM__c');
            } catch (Exception e){
                System.debug('No_CSM_Sync__c field not found on SObject: ' + objectType);
            }
            if(noSync == false){
                SObject oldObj = null;
                if (oldMap != null && !oldMap.isEmpty()) {
                    oldObj = oldMap.get(newObj.Id);
                }
                
                // only enqueue if there's a change to at least 1 mapped field
                Map<String, Map<String, String>> mappedFieldChanges = 
                    CSMFieldMappingHelper.getMappedFieldChanges(CSMConstants.SYNC_EVENT_TYPE_OUTBOUND, oldObj, newObj);
                
                if (!mappedFieldChanges.isEmpty()) {
                    
                    CSM_Sync_Entry__c syncEntry = new CSM_Sync_Entry__c(
                        Event_Type__c = CSMConstants.SYNC_EVENT_TYPE_OUTBOUND,
                        Endpoint_System__c = 'LIVE',
                        Source_Object_Name__c = objectTypeName,
                        Source_Record_ID__c = newObj.Id,
                        Sync_User__c = UserInfo.getName(),
                        Original_Data__c = JSON.serialize(mappedFieldChanges)
                    );
                    
                    syncEntries.add(syncEntry);
                    
                    SObject newObjToUpdate = objectType.newSObject();
                    newObjToUpdate.Id = newObj.Id;
                    newObjToUpdate.put('CSM_Sync_Status__c', CSMConstants.SYNC_STATUS_PENDING);
                    newObjToUpdate.put('CSM_Sync_Message__c', null);
                    
                    objsToUpdate.add(newObjToUpdate);
                    
                }
            }
        }
        
        if (!syncEntries.isEmpty()) insert syncEntries;
        
        if (!objsToUpdate.isEmpty()) {
            TriggerHelper.TemporaryOveride(objectTypeName + '.CSMSync', false);
            update objsToUpdate;
        }
    }
    
    // used for inbound sync entry
    public static void enqueueSyncEntry(String postData) {
        Map<String, String> parsedValue = (Map<String, String>)JSON.deserialize(postData, Map<String, String>.class);
        
        CSM_Sync_Entry__c syncEntry = new CSM_Sync_Entry__c(
            Endpoint_System__c = 'LIVE',
            Event_Type__c = CSMConstants.SYNC_EVENT_TYPE_INBOUND,
            Source_Object_Name__c = parsedValue.get('business_object'),
            Source_Record_ID__c = parsedValue.get('record_id'),
            Sync_User__c = parsedValue.get('last_modified_by')
        );
        
        // Save snapshot of CSM data at time of receiving request from CSM.
        // When we process this sync entry, Actual Data, which is what will
        // be used for processing, may not be the same as 
        // Original Data, but we save Original Data anyway for tracking.
        // If we get an error getting data from CSM, we create the sync entry
        // anyway for tracking, storing the error in the Original Data.  
        try {
            CSMBusinessObjectRecordWrapper borw = CSMAPIWrapper.getBusinessObjectRecordByRecordId(syncEntry.Source_Object_Name__c, syncEntry.Source_Record_ID__c);
            syncEntry.Original_Data__c = JSON.serialize(borw);
        } catch(Exception e) {
            syncEntry.Original_Data__c = e.getMessage() + '\r\n' + e.getStackTraceString();
        }
        
        insert syncEntry;
    }
    
    private static Boolean isUserAuthorizedToProcessSync(Id userId){
        if (CSMAPIWrapper.requiredPermSetsForRetry == null ||
            CSMAPIWrapper.requiredPermSetsForRetry.isEmpty()) return true;
        
        
        List<PermissionSetAssignment> permSets = 
            [SELECT Id 
             FROM PermissionSetAssignment 
             WHERE AssigneeId= :userId
             AND PermissionSet.Name IN :CSMAPIWrapper.requiredPermSetsForRetry];
        
        if (permSets.size() == CSMAPIWrapper.requiredPermSetsForRetry.size()) {
            return true;
        }
        
        return false;
    }
    
    // used for retrying errored sync entries
    public static String enqueueSyncEntry(List<CSM_Sync_Entry_History__c> sehs) {
        try {
            String syncUser = UserInfo.getName();
            Id syncUserId = UserInfo.getUserId();
            
            if (!isUserAuthorizedToProcessSync(syncUserId)) {
                return 'Current user doesn\'t have the required permissions to process sync entries.';
            }
            
            if (sehs == null || sehs.size() == 0) {
                return 'There is no sync entry to process.';
            }
            
            // Check that all sync entries are in error.  We only retry errored ones.
            List<CSM_Sync_Entry_History__c> syncEntryHistories = 
                [SELECT Event_Type__c, Original_Data__c, Source_Object_Name__c, Endpoint_System__c,
                 Source_Record_ID__c, Sync_User__c, Status__c, Retry_Count__c
                 FROM CSM_Sync_Entry_History__c
                 WHERE Id IN :sehs
                 AND (Endpoint_System__c = 'LIVE'
                      OR Endpoint_System__c = null)
                 AND Status__c = :CSMConstants.SYNC_STATUS_ERROR];
            
            if (syncEntryHistories.size() < sehs.size()) {
                if (sehs.size() == 1) {
                    return 'You can only retry failed sync entries.';
                } else {
                    return 'Found ' + String.valueOf(sehs.size() - syncEntryHistories.size()) + ' sync entry(s) that are not in Error.  You can only retry failed sync entries.';
                }
            }
            
            List<CSM_Sync_Entry__c> seToInsert = new List<CSM_Sync_Entry__c>();
            List<CSM_Sync_Entry_History__c> sehToUpdate = new List<CSM_Sync_Entry_History__c>();
            
            
            // create new Sync Entry record, copying values from the given history record
            for(CSM_Sync_Entry_History__c seh : syncEntryHistories) {
                CSM_Sync_Entry__c syncEntry = new CSM_Sync_Entry__c(
                    Endpoint_System__c = seh.Endpoint_System__c,
                    Event_Type__c = seh.Event_Type__c,
                    Original_Data__c = seh.Original_Data__c,
                    Source_Object_Name__c = seh.Source_Object_Name__c,
                    Source_Record_ID__c = seh.Source_Record_ID__c,
                    Sync_User__c = syncUser,
                    Sync_Entry_Retry__c = seh.Id,
                    Retry_Count__c = (seh.Retry_Count__c == null ? 1 : (seh.Retry_Count__c + 1))
                );
                
                seToInsert.add(syncEntry);
                
                // update status of history record to Retried if it's not auto-retry
                if (seh.Status__c != CSMConstants.SYNC_STATUS_AUTO_RETRIED) {
                    sehToUpdate.add(new CSM_Sync_Entry_History__c(Id = seh.Id, Status__c = CSMConstants.SYNC_STATUS_RETRIED));
                }
            }
            
            if (seToInsert.size() > 0) insert seToInsert;
            
            if (sehToUpdate.size() > 0) update sehToUpdate;
        } catch(Exception e) {
            System.debug(e);
            return 'There was a problem retrying the sync entry(s): ' + e.getMessage();
        }
        
        return '';
        
    }
    
    // dequeues next sync entry
    public static List<CSM_Sync_Entry__c> dequeueSyncEntry() {
        Map<Id, CSM_Sync_Entry__c> syncEntries = new Map<Id, CSM_Sync_Entry__c>(
            [SELECT Id, Name, Event_Type__c, Original_Data__c, Source_Object_Name__c, Endpoint_System__c,
             Source_Record_ID__c, Sync_Entry_Retry__c, Sync_User__c, Retry_Count__c
             FROM CSM_Sync_Entry__c
             WHERE Endpoint_System__c = 'LIVE'
             OR Endpoint_System__c = null
             ORDER BY CreatedDate, Name
             LIMIT 5000]);
        
        List<CSM_Sync_Entry__c> syncEntriesToProcess = new List<CSM_Sync_Entry__c>();
        
        if (!syncEntries.isEmpty()) {
            // Attempt to delete the records right away before processing.  This way, if another 
            // process happens to pick up the same sync entries, the first one to successfully delete
            // the record will get to process it.  If the delete fails, that means another process
            // has already picked it up.  Note that we're allowing partial success (allOrNone = false).  
            // We will only process successful deletes.
            Database.DeleteResult[] delResults = Database.delete(syncEntries.values(), false);
            
            // Gather sync entries that were successfully deleted.
            for(Database.DeleteResult dr: delResults) {
                if (dr.isSuccess()) {
                    syncEntriesToProcess.add(syncEntries.get(dr.getId()));
                }
            }
            
        }
        
        return syncEntriesToProcess;
    }
    
    
    // main entry point for processing any type of sync entry
    public static void processSyncEntry(CSM_Sync_Entry__c syncEntry) {
        
        // initialize sync entry history
        CSM_Sync_Entry_History__c syncEntryHist = new CSM_Sync_Entry_History__c(
            Name = syncEntry.Name,
            Endpoint_System__c = 'LIVE',
            Event_Type__c = syncEntry.Event_Type__c,
            Source_Object_Name__c = syncEntry.Source_Object_Name__c,
            Source_Record_ID__c = syncEntry.Source_Record_ID__c,
            Sync_User__c = syncEntry.Sync_User__c,
            Original_Data__c = syncEntry.Original_Data__c,
            Sync_Entry_Retry__c = syncEntry.Sync_Entry_Retry__c,
            Retry_Count__c = syncEntry.Retry_Count__c
        );
        
        try {
            if (syncEntry.Event_Type__c == CSMConstants.SYNC_EVENT_TYPE_INBOUND) processSyncEntryInbound(syncEntry, syncEntryHist);
            else if (syncEntry.Event_Type__c == CSMConstants.SYNC_EVENT_TYPE_OUTBOUND) processSyncEntryOutbound(syncEntry, syncEntryHist);
        } catch(Exception e) {
            syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_ERROR;
            syncEntryHist.Status_Details__c = e.getMessage() + '\r\n' + e.getStackTraceString();
        }
        
        if (syncEntryHist.Status__c == CSMConstants.SYNC_STATUS_AUTO_RETRIED &&
            syncEntryHist.Retry_Count__c > CSMAPIWrapper.csmMaxAutoRetries) {
                syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_ERROR;
                syncEntryHist.Status_Details__c = 'The maximum allowed retries has been reached for this sync entry.  Please contact a sys admin for assistance.';
            }
        
        // Save a truncated copy of Status Details so that we can filter on it, since we
        // can't filter on text area fields.
        if (syncEntryHist.Status_Details__c == null) {
            syncEntryHist.Status_Details_Truncated__c = null;
        } else {
            syncEntryHist.Status_Details_Truncated__c = syncEntryHist.Status_Details__c.left(255);
        }
        
        insert syncEntryHist;
        
        // If it's an auto retry, enqueue right away.  
        if (syncEntryHist.Status__c == CSMConstants.SYNC_STATUS_AUTO_RETRIED) {
            enqueueSyncEntry(syncEntryHist.Id);
        }
        
        // Mark the end of api calls so that CSMAPIWrapper can safely do housekeeping stuff.
        CSMAPIWrapper.endOfAPICalls();
    }
    
    // processes inbound sync entry and sets result fields on history record
    private static void processSyncEntryInbound(CSM_Sync_Entry__c syncEntry, CSM_Sync_Entry_History__c syncEntryHist) {
        
        
        CSMBusinessObjectRecordWrapper borw = CSMAPIWrapper.getBusinessObjectRecordByRecordId(syncEntry.Source_Object_Name__c, syncEntry.Source_Record_ID__c);
        SObject obj = borw.transformToSObject();
        
        System.debug('obj after transform');
        System.debug(obj);
        
        syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_SYNCHRONIZED;
        syncEntryHist.Status_Details__c = null;
        syncEntryHist.Endpoint_System__c = 'LIVE';
        syncEntryHist.Actual_Data__c = JSON.serialize(borw);
        
        obj.put('CSM_Sync_Status__c', syncEntryHist.Status__c);
        obj.put('CSM_Sync_Message__c', syncEntryHist.Status_Details__c);
        obj.put('CSM_Last_Synced_Date__c', Datetime.now());
        
        String objectTypeName = String.valueOf(obj.getSObjectType());
        if (obj.Id == null) {
            // Let outgoing sync run to let the Salesforce Id be synced to CSM
            insert obj;
            syncEntryHist.Status_Details__c = 'Created new ' + objectTypeName + ' with Id = ' + obj.Id;
            
        } else {
            // compare against existing obj and make sure there's at least 1 changed field
            // before performing the update.
            Id objId = obj.Id;
            System.debug('CSMSyncEntryHelper objId ' + objId);
            Set<String> fieldNames = CSMFieldMappingHelper.getMappedSalesforceFields(objectTypeName);
            fieldNames.addAll(CSMConstants.CSM_SYNC_FIELDS);
            String query = String.format('SELECT {0} FROM {1} WHERE Id = :objId',
                                         new String[]{ String.join(new List<String>(fieldNames), ', '), objectTypeName });
            
            List<SObject> existingObjs = Database.query(query);
            
            if (existingObjs.isEmpty()) {
                syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_ERROR;
                syncEntryHist.Status_Details__c = objectTypeName + ' with Id = ' + String.valueOf(objId) + ' does not exist. ' +
                    'The record may have been deleted.';
            } else {
                SObject existingObj = existingObjs[0];
                
                Map<String, Map<String, String>> mappedFieldChanges = 
                    CSMFieldMappingHelper.getMappedFieldChanges(CSMConstants.SYNC_EVENT_TYPE_INBOUND, existingObj, obj);
                
                if (!mappedFieldChanges.isEmpty()) {
                    // suppress outgoing sync
                    TriggerHelper.TemporaryOveride(objectTypeName + '.CSMSync', false);
                    update obj;
                    syncEntryHist.Status_Details__c = 'Updated ' + objectTypeName + ' with Id = ' + obj.Id;
                } else {
                    syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_SKIPPED;
                    syncEntryHist.Status_Details__c = 'Update not applied since there\'s no change to a mapped field';
                }
            }
        }
        
    }
    
    // processes outbound sync entry and sets result fields on history record
    private static void processSyncEntryOutbound(CSM_Sync_Entry__c syncEntry, CSM_Sync_Entry_History__c syncEntryHist) {
        String csmRecordId;
        try {
            // Retrieve sobject
            Id objId = syncEntry.Source_Record_ID__c;
            System.debug('DEBUG: CSMSyncEntryHelper syncEntry     ' + syncEntry);
            System.debug('DEBUG: CSMSyncEntryHelper syncEntry.SourceObjName     ' + syncEntry.Source_Object_Name__c);
            Set<String> fieldNames = CSMFieldMappingHelper.getMappedSalesforceFields(syncEntry.Source_Object_Name__c);
            
            fieldNames.addAll(CSMConstants.CSM_SYNC_FIELDS);
            
            String query = String.format('SELECT {0} FROM {1} WHERE Id = :objId',
                                         new String[]{ String.join(new List<String>(fieldNames), ', '), syncEntry.Source_Object_Name__c });
            
            List<SObject> objs = Database.query(query);
            
            if (objs.isEmpty()) {
                syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_ERROR;
                syncEntryHist.Status_Details__c = 'Record not found';
                return;
            } 
            
            syncEntryHist.Actual_Data__c = JSON.serialize(objs[0]);
            
            CSMBusinessObjectRecordResponseWrapper borrw = CSMAPIWrapper.saveBusinessObjectRecord(objs[0]);
            
            if (borrw.hasError) {
                syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_ERROR;
                syncEntryHist.Status_Details__c = JSON.serialize(borrw);
            } else {
                syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_SYNCHRONIZED;
                csmRecordId = borrw.busObRecId;
                syncEntryHist.Status_Details__c = 'Synced with CSM Record ID = ' + csmRecordId;
            }
        } catch(CSMFieldMappingHelper.CSMRequiresAutoRetryException ae) {
            syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_AUTO_RETRIED;
            syncEntryHist.Status_Details__c = ae.getMessage();
        } catch(Exception e) {
            System.debug('DEBUG: CSMSyncEntryHelper e' + e.getMessage());
            syncEntryHist.Status__c = CSMConstants.SYNC_STATUS_ERROR;
            syncEntryHist.Status_Details__c = e.getMessage();
        }
        
        
        // Update sync status fields on sobject
        SObject objToUpdate = CSMFieldMappingHelper.getSObjectWithName(syncEntry.Source_Object_Name__c);
        objToUpdate.Id = syncEntry.Source_Record_ID__c;
        objToUpdate.put('CSM_Sync_Status__c', syncEntryHist.Status__c);
        objToUpdate.put('CSM_Sync_Message__c', syncEntryHist.Status_Details__c);
        if (syncEntryHist.Status__c == CSMConstants.SYNC_STATUS_SYNCHRONIZED) {
            objToUpdate.put('CSM_Last_Synced_Date__c', Datetime.now());
            objToUpdate.put('CSM_Record_ID__c', csmRecordId);
            objToUpdate.put('CSM_Sync_Message__c', null);
        }
        
        TriggerHelper.TemporaryOveride(syncEntry.Source_Object_Name__c + '.CSMSync', false);
        update objToUpdate;
    }
    
    // queues up a new Sync Entry Processor if there isn't one running already
    public static void enqueueSyncEntryProcessor() {
        // We won't schedule the job if there's already one enqueued or processing.
        // If there's one processing, new entries will be picked when the finish method
        // spins up another job if it sees that there are more entries in the queueue.
        List<AsyncApexJob> jobs = [SELECT Id 
                                   FROM AsyncApexJob 
                                   WHERE ApexClass.Name = 'CSMSyncEntryProcessorBatch'
                                   AND Status IN ('Holding', 'Queued', 'Preparing', 'Processing')];
        
        if (jobs.isEmpty()) {
            CSMSyncEntryProcessorBatch b = new CSMSyncEntryProcessorBatch();
            Database.executeBatch(b, 1);
        }
    }
}