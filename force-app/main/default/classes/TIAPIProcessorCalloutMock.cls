@isTest
global class TIAPIProcessorCalloutMock implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest request){
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"id": "e3a230a4-06b1-47c5-aa46-fe5f9e8bbab3","name": "Test Client","sku": "test-client-sku","licenses": '+
                         '[{"id": "2a779a49-50eb-48de-a997-96d6aa6252ff","name": "Main","sku": "main","createdAt": "2018-02-15T14:06:03.084Z","updatedAt": '+
                         '"2018-02-15T14:06:03.084Z","accessDays": null,"seatsLimit": null,"learningPathIds": ["7c117319-60d1-45fd-bf1d-3d92bb824a84"],"courseIds": '+
                         '["13b808e9-4064-4a0b-b36d-8e8a4e242260", "8ae5bdab-e318-5ff1-b630-bfe01a1a09be"],"courseTags": [{ "id": "591a2429-e3b0-5a79-b18a-d5e35cb444ba", "label": "Test Tag" }],'+
                         '"parentLicenseId": "1234-5678"}]}');
        response.setStatusCode(200);
        return response;
    }

}