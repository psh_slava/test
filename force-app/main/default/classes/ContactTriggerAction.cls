public class ContactTriggerAction {
    
    public Map<Id, Lifecycle__c> contactToInactiveLifeCycleMap = new Map<Id, Lifecycle__c>();
    public Map<Id, Lifecycle__c> contactToActiveLifeCycleMap = new Map<Id, Lifecycle__c>();
    
    Public void contactRecordOnbeforeupdate(List<Contact> newList, Map<Id, Contact> oldMap)
    {      
        for(Contact c : newList){
            List<String> existingGroups = new List<String>();
            Set<String> newGroupSet = new Set<String>();
            String newGroup = 'Forums';
            String thisMembersGroupsConcat; 
            
            If(c.Community_Member__c == True && oldMap.get(c.Id).Community_Member__c == false){
                if(c.Community_Groups__c != null){
                    existingGroups.addAll(c.Community_Groups__c.split(';'));
                    newGroupSet.addAll(existingGroups);
                    
                    newGroupSet.add(newGroup);
                    
                    thisMembersGroupsConcat = String.join((Iterable<String>)newGroupSet, ';');
                    
                    c.Community_Groups__c = thisMembersGroupsConcat;
                }
                else {
                    c.Community_Groups__c = newGroup;
                }  
            }      
        }
                
                
                    
    }
    
    public void newLifecycleRecordOnAfterInsert(List<Contact> newList){
        System.debug('newLifecycleRecordOnAfterInsert Invoked!');
        if(newList <> null && newList.size() > 0){
            
            Set<Id> conIds = new Set<Id>();
            List<Lifecycle__c> newLifecycle = new List<Lifecycle__c>();
            
            //get custom setting data
            Map<String, ContactLifeCycleMapping__c> lyfCycleMapping = ContactLifeCycleMapping__c.getAll();
            
            for(Contact conObj : newList){
                conIds.add(conObj.id);
            }
            
            if(conIds <> null && conIds.size() > 0){
                Map<Id, Contact> additionalFields = new Map<Id, Contact>([Select id, name, createdby.ProfileId from Contact where id in :conIds]);
                
                for(Contact conObj : newList){
                    Lifecycle__c lyf = new Lifecycle__c();
                    // Condition1 ---- conObj.IsLead_Generated__c = FALSE
                    if(!conObj.IsLead_Generated__c){
                        //map contact fields to new Life cycle object
                        for(ContactLifeCycleMapping__c mapping : lyfCycleMapping.values()){
                            System.debug('___mapping___'+mapping);
                            if(!mapping.isDefault__c && mapping.Contact_API_Name__c <> null && mapping.Contact_API_Name__c <> '')
                                lyf.put(mapping.Name, conObj.get(mapping.Contact_API_Name__c));
                            else if(mapping.isDefault__c && mapping.Default_Value__c <> null && mapping.Default_Value__c <> '')
                                lyf.put(mapping.Name, mapping.Default_Value__c);
                        }
                        if(conObj.Behavior_Score__c >= 10 && conObj.Behavior_Score__c < 100 && (conObj.Mkto71_Lead_score__c < 100 || conObj.Mkto71_Lead_score__c == null) && conObj.Contact_Status__c  == 'New' && conObj.False_AQL__c == false)
                        {
                        
                            lyf.Lifecycle_Status__c = 'Active' ;
                            if(lyf.Start_Date__c == null) lyf.Start_Date__c = system.today();
                            if(lyf.Inquiry_Entry_Date__c == null) lyf.Inquiry_Entry_Date__c = system.today();
                            lyf.Inquiry_Lead_Source_Most_Recent__c = conObj.Inquiry_Lead_Source__c ;
                            lyf.Inquiry_Lead_Source_Detail_Most_Recent__c = conObj.Inquiry_Lead_Source_Detail__c ;
                            lyf.Inquiry_Last_Interesting_Moment__c = conObj.Inquiry_Last_Interesting_Moment__c ;
                            lyf.Lifecycle_Stage__c = 'Inquiry';
                                                                    
                        }
                        //Condition 2 ---------- Contact Created By User Role = BDR and Lead Generated = FALSE and Contact Status = NEW
                        String BDRIds = Label.BDR_Profile_IDs;
                        Id prflId = additionalFields.get(conObj.id).createdby.ProfileId;
                        if(BDRIds.contains(prflId) && !conObj.IsLead_Generated__c && conObj.Contact_Status__c == 'New')
                        {
                            lyf.Lifecycle_Status__c = 'Active';
                            lyf.Start_Date__c = Date.today();
                            lyf.TAL_Entry_Date__c = Date.today();
                            lyf.Lifecycle_Stage__c = 'TAL';
                        }
                        //Condition 3 ---------- Contact Marketo Lead Score > 99 and Contact Status = 'New' OR Contact Status = Marketing Qualified
                        else if(conObj.mkto71_Lead_Score__c > 99 && (conObj.Contact_Status__c == 'New' || conObj.Contact_Status__c == 'Marketing Qualified') && conObj.False_AQL__c == false){
                     
                                lyf.Lifecycle_Status__c = 'Active';
                                if(lyf.Start_Date__c == null) lyf.Start_Date__c = system.today();
                                if(lyf.Inquiry_Entry_Date__c == null) lyf.Inquiry_Entry_Date__c = system.today();
                                lyf.Inquiry_Lead_Source_Most_Recent__c = conObj.Inquiry_Lead_Source__c;
                                lyf.Inquiry_Lead_Source_Detail_Most_Recent__c= conObj.Inquiry_Lead_Source_Detail__c;
                                lyf.Inquiry_Last_Interesting_Moment__c= conObj.Inquiry_Last_Interesting_Moment__c;
                        
                                lyf.Inquiry_Exit_Date__c = system.today();

                                lyf.AQL_Entry_Date__c =System.today();
                                lyf.Lifecycle_Stage__c = 'AQL';
                        }
                        //Condition 4 ---------- Contact Status = Working 
                        else if(conObj.Contact_Status__c == 'Working'){
                            lyf.Lifecycle_Status__c = 'Active';
                            lyf.Start_Date__c = Date.today();
                            lyf.Lifecycle_Stage__c = 'TAL';
                            lyf.TAL_Entry_Date__c = Date.today();
                            lyf.Score_at_TAL__c = conObj.mkto71_Lead_Score__c;
                        }
                        //Condition 5 ---------- Contact Status = Engaged 
                        else if(conObj.Contact_Status__c == 'Engaged'){
                            lyf.Lifecycle_Status__c = 'Active';
                            lyf.Start_Date__c = Date.today();
                            lyf.Lifecycle_Stage__c = 'TQL';
                            lyf.TQL_Entry_Date__c = Date.today();
                            lyf.Score_at_TQL__c = conObj.mkto71_Lead_Score__c;
                        }
                        //Condition 6 ---------- Contact Status = Invalid Request    
                        else if(conObj.Contact_Status__c == 'Invalid Request'){
                            lyf.Lifecycle_Status__c = 'Completed';
                            lyf.Completed_Date__c = date.today();
                            lyf.Lifecycle_Stage__c = 'Disqualified';
                            lyf.Disqualified_Reason__c = conObj.Invalid_Request_Reason__c;
                            lyf.Disqualified_Date__c = Date.today();
                        }
                        //Condition 7 ---------- Contact Status = Back To Nurture      
                        else if(conObj.Contact_Status__c == 'Back To Nurture'){
                            lyf.Lifecycle_Status__c = 'Completed';
                            lyf.Completed_Date__c = date.today();
                            lyf.Lifecycle_Stage__c = 'Recycled';
                            lyf.Recycle_Reason__c = conObj.Back_To_Nurture_Reason__c;
                            lyf.Recycled_Date__c  = Date.today();
                        }
                        newLifecycle.add(lyf);
                    }
                }//loop ends
                
            }
            
            if(newLifecycle <> null && newLifecycle.size() > 0){
                System.debug('newLifecycle >> ' + newLifecycle);
                insert newLifecycle;
            }
        }//main if ends
    }
    
    public void existingLifecycleRecordOnAfterUpdate(List<Contact> newList, Map<Id, Contact> oldMap){
        System.debug('existingLifecycleRecordOnAfterUpdate Invoked!');
        if(newList <> null && newList.size() > 0){
            Set<Id> conIds = new Set<Id>();
            Map<Id, Contact> filteredRecs = new Map<Id, Contact>();
            List<Lifecycle__c> newLifecycle = new List<Lifecycle__c>();
            
            for(Contact conObj  :newList){
                filteredRecs.put(conObj.id, conObj);
                //filter those contacts whose related records need to be queried
                if(!conObj.IsLead_Generated__c && conObj.Contact_Status__c == 'New'){
                    conIds.add(conObj.id);
                }
            }
            System.debug('conIds >> ' + conIds);
            if(filteredRecs <> null && filteredRecs.size() > 0){
                
                Map<Id, Contact> additionalContactFields;
                if(conIds <> null && conIds.size() > 0){
                    additionalContactFields = new Map<Id, Contact>([Select id, name, createdby.ProfileId from Contact where id in :conIds]);
                }
                
                //get lifecycle record
                this.mostRecentLifecycleRecord(filteredRecs.keySet());
                
                if(filteredRecs <> null && filteredRecs.size() > 0){
                    for(Contact conObj : filteredRecs.values()){
                        Lifecycle__c RelatedlyfRec;
                        
                        //get most recrntly created Lifecycle with Status = Inactive
                        
                        if(this.contactToActiveLifeCycleMap.containsKey(conObj.id)){
                            RelatedlyfRec = this.contactToActiveLifeCycleMap.get(conObj.id);
                        }
                        else if(this.contactToInactiveLifeCycleMap.containsKey(conObj.id)){
                            RelatedlyfRec = this.contactToInactiveLifeCycleMap.get(conObj.id);
                        }
                        
                        System.debug('RelatedlyfRec >> ' + RelatedlyfRec);
                        if(RelatedlyfRec <> null){
                            Lifecycle__c lyf = new Lifecycle__c(id = RelatedlyfRec.id);
                            Boolean doUpdate = false;
                            
                            System.debug('conObj.Contact_Contact_Status__c  >> ' + conObj.Contact_Status__c );
                            System.debug('this.contactToActiveLifeCycleMap >> ' + this.contactToActiveLifeCycleMap );
                            System.debug('this.contactToInactiveLifeCycleMap >> ' + this.contactToInactiveLifeCycleMap );
                            
                            if(this.contactToInactiveLifeCycleMap <> null || this.contactToActiveLifeCycleMap <> null){
                                if((conObj.Behavior_Score__c <> oldMap.get(conObj.id).Behavior_Score__c )|| (conObj.Mkto71_Lead_score__c <> oldMap.get(conObj.id).Mkto71_Lead_score__c)||(conObj.Contact_Status__c <> oldMap.get(conObj.id).Contact_Status__c))
                                {
                                    if(conObj.Behavior_Score__c >= 10 && conObj.Behavior_Score__c < 100 && conObj.Mkto71_Lead_score__c > oldMap.get(conObj.id).Mkto71_Lead_score__c &&
                                       (conObj.Mkto71_Lead_score__c < 100 || conObj.Mkto71_Lead_score__c == null) && conObj.Contact_Status__c  == 'New' && conObj.False_AQL__c == false)
                                    {
                                        system.debug('-testing Behaviour score less thn 100-');
                                        //if(this.contactToInactiveLifeCycleMap.containsKey(conObj.id))
                                       // {
                                            system.debug('-testing Behaviour score less thn 100-');
                                            lyf.Lifecycle_Status__c = 'Active' ;
                                            if(lyf.Start_Date__c == null) lyf.Start_Date__c = system.today();
                                            if(lyf.Inquiry_Entry_Date__c == null) lyf.Inquiry_Entry_Date__c = system.today();
                                            lyf.Inquiry_Lead_Source_Most_Recent__c = conObj.Inquiry_Lead_Source__c ;
                                            lyf.Inquiry_Lead_Source_Detail_Most_Recent__c = conObj.Inquiry_Lead_Source_Detail__c ;
                                            lyf.Inquiry_Last_Interesting_Moment__c = conObj.Inquiry_Last_Interesting_Moment__c ;
                                            lyf.Lifecycle_Stage__c = 'Inquiry';
                                            system.debug('lyf---Behaviour score---'+lyf);
                                            if(!doUpdate)
                                                doUpdate  = true;
                                            
                                        //}                                           
                                    }
                                }
                                
                                if((conObj.contact_status__c <> oldMap.get(conObj.id).contact_status__c || conObj.mkto71_Lead_Score__c <> oldMap.get(conObj.id).mkto71_Lead_Score__c) && 
                                    conObj.mkto71_Lead_Score__c > 99 && (oldmap.get(conObj.id).mkto71_Lead_Score__c < 100 || oldmap.get(conObj.id).mkto71_Lead_Score__c == null) && (conObj.Contact_Status__c  == 'New' || conObj.Contact_Status__c  == 'Marketing Qualified') && conObj.False_AQL__c == false){                                    
                                    System.debug('Condition 2 ---------- Contact Marketo Lead Score is updated to > 99 and Contact Status = New OR Contact Status = Marketing Qualified');
                                    //if(this.contactToInactiveLifeCycleMap.containsKey(conObj.id))
                                   // {
                                        if(!doUpdate)
                                            doUpdate  = true;
                                        
                                        System.debug('C2');
                                      //  lyf.id = this.contactToInactiveLifeCycleMap.get(conObj.id).Id; 
                                        if(RelatedlyfRec.Lifecycle_Status__c == 'Inactive')
                                        {
                                            lyf.Lifecycle_Status__c = 'Active';
                                            if(lyf.Start_Date__c == null) lyf.Start_Date__c = system.today();
                                            if(lyf.Inquiry_Entry_Date__c == null) lyf.Inquiry_Entry_Date__c = system.today();
                                            lyf.Inquiry_Lead_Source_Most_Recent__c = conObj.Inquiry_Lead_Source__c;
                                            lyf.Inquiry_Lead_Source_Detail_Most_Recent__c= conObj.Inquiry_Lead_Source_Detail__c;
                                            lyf.Inquiry_Last_Interesting_Moment__c= conObj.Inquiry_Last_Interesting_Moment__c;
                                        }
                                        if(RelatedlyfRec.Lifecycle_Stage__c == 'Inquiry'){
                                            lyf.Inquiry_Exit_Date__c = system.today();
                                        }
                                        
                                        lyf.AQL_Entry_Date__c = System.today();
                                        lyf.Lifecycle_Stage__c = 'AQL';
                                        
                                    //}
                                        
                                }
                                
                                if(conObj.Contact_Status__c <> oldMap.get(conObj.id).Contact_Status__c){
                                    //Condition 1 ---------- Contact Created By User Role = BDR and Lead Generated = FALSE and Contact Status = NEW
                                    if(additionalContactFields <> null && additionalContactFields.containsKey(conObj.id)){
                                        String BDRIds = Label.BDR_Profile_IDs;
                                        Id ProfileId = additionalContactFields.get(conObj.id).createdby.ProfileId;
                                        if(BDRIds.contains(ProfileId) && !conObj.IsLead_Generated__c && conObj.Contact_Status__c  == 'New'){
                                            
                                            if(this.contactToInactiveLifeCycleMap.containsKey(conObj.id)){
                                                lyf.id = this.contactToInactiveLifeCycleMap.get(conObj.id).Id;
                                                
                                                if(!doUpdate)
                                                    doUpdate  = true;
                                                
                                                System.debug('C1');
                                                lyf.Lifecycle_Status__c  = 'Active';
                                                lyf.Start_Date__c = Date.today();
                                                lyf.TAL_Entry_Date__c = Date.today();
                                                lyf.Lifecycle_Stage__c = 'TAL';
                                            }
                                        }
                                    }
                                    
                                    //Condition 3 ---------- Contact Status = Working 
                                    if(conObj.Contact_Status__c == 'Working'){
                                        if(!doUpdate)
                                            doUpdate  = true;
                                        
                                        System.debug('C3');
                                        if(RelatedlyfRec.Lifecycle_Status__c == 'Inactive'){
                                            lyf.Lifecycle_Status__c = 'Active';
                                            lyf.Start_Date__c = Date.today();
                                        }
                                        if(RelatedlyfRec.Lifecycle_Stage__c == 'AQL')
                                            lyf.AQL_Exit_Date__c = Date.today();
                                        
                                        lyf.Lifecycle_Stage__c = 'TAL';
                                        lyf.TAL_Entry_Date__c = Date.today();
                                        lyf.Score_at_TAL__c = conObj.mkto71_Lead_Score__c;
                                    }
                                    //Condition 4 ---------- Contact Status = Engaged 
                                    else if(conObj.Contact_Status__c == 'Engaged'){ 
                                        if(!doUpdate)
                                            doUpdate  = true;
                                        System.debug('C4');  
                                        
                                        if(RelatedlyfRec.Lifecycle_Status__c == 'Inactive'){
                                            lyf.Lifecycle_Status__c = 'Active';
                                            lyf.Start_Date__c = Date.today();
                                        }
                                        
                                        if(RelatedlyfRec.Lifecycle_Stage__c == 'TAL')
                                            lyf.TAL_Exit_Date__c = Date.today();
                                        else if(RelatedlyfRec.Lifecycle_Stage__c == 'AQL')
                                            lyf.AQL_Exit_Date__c = Date.today();
                                        
                                        lyf.Lifecycle_Stage__c = 'TQL';
                                        lyf.TQL_Entry_Date__c = Date.today();
                                        lyf.Score_at_TQL__c = conObj.mkto71_Lead_Score__c;
                                    }
                                    //Condition 5 ---------- Contact Status = Invalid Request 
                                    else if(conObj.Contact_Status__c == 'Invalid Request'){
                                        if(!doUpdate)
                                            doUpdate  = true;
                                        System.debug('C5');    
                                        if(RelatedlyfRec.Lifecycle_Status__c == 'Inactive'){
                                            lyf.Lifecycle_Status__c = 'Active';
                                            lyf.Start_Date__c = Date.today();
                                        }
                                        
                                        if(RelatedlyfRec.Lifecycle_Stage__c == 'TQL')
                                            lyf.TQL_Exit_Date__c = Date.today();
                                        else if(RelatedlyfRec.Lifecycle_Stage__c == 'TAL')
                                            lyf.TAL_Exit_Date__c = Date.today();
                                        else if(RelatedlyfRec.Lifecycle_Stage__c == 'AQL')
                                            lyf.AQL_Exit_Date__c = Date.today();
                                        
                                        lyf.Lifecycle_Stage__c = 'Disqualified';
                                        lyf.Disqualified_Reason__c = conObj.Invalid_Request_Reason__c;
                                        lyf.Disqualified_Date__c = Date.today();
                                        lyf.Lifecycle_Status__c = 'Completed';
                                        lyf.Completed_Date__c = date.today();
                                    }
                                    //Condition 6 ---------- Contact Status = Back To Nurture      
                                    else if(conObj.Contact_Status__c == 'Back To Nurture'){
                                        if(!doUpdate)
                                            doUpdate  = true;
                                        System.debug('C6');
                                        lyf.Lifecycle_Status__c = 'Completed';
                                        lyf.Completed_Date__c = date.today();
                                        
                                        if(RelatedlyfRec.Lifecycle_Stage__c == 'TQL')
                                            lyf.TQL_Exit_Date__c = Date.today();
                                        else if(RelatedlyfRec.Lifecycle_Stage__c == 'TAL')
                                            lyf.TAL_Exit_Date__c = Date.today();
                                        else if(RelatedlyfRec.Lifecycle_Stage__c == 'AQL')
                                            lyf.AQL_Exit_Date__c = Date.today();
                                        
                                        lyf.Lifecycle_Stage__c = 'Recycled';
                                        lyf.Recycle_Reason__c = conObj.Back_To_Nurture_Reason__c;
                                        lyf.Recycled_Date__c  = Date.today();
                                    }
                                    
                                    //Every time a Contact Status is Updated
                                    if(RelatedlyfRec.Person_Status__c <> conObj.Contact_Status__c){
                                        System.debug('Update Person_Status__c');
                                        if(!doUpdate)
                                            doUpdate  = true;
                                        lyf.Prior_Person_Status__c = RelatedlyfRec.Person_Status__c;
                                        lyf.Person_Status__c = conObj.Contact_Status__c;
                                    }
                                }
                                
                                
                                //Condition 2 ---------- Contact Marketo Lead Score is updated to > 99 and Contact Status = 'New' OR Contact Status = Marketing Qualified
                                System.debug('conObj.contact_status__c >> '  +conObj.contact_status__c + ' and oldMap.get(conObj.id).contact_status__c >> '  +oldMap.get(conObj.id).contact_status__c);
                                System.debug('conObj.mkto71_Lead_Score__c >> '  + conObj.mkto71_Lead_Score__c + ' and oldMap.get(conObj.id).mkto71_Lead_Score__c >> '  + oldMap.get(conObj.id).mkto71_Lead_Score__c);
                                
                                
                                
                                //Every time a Contact Owner is Updated
                                if(conObj.ownerId <> oldMap.get(conObj.id).ownerId && RelatedlyfRec.ownerId <> conObj.ownerId){
                                    System.debug('Update Owner');
                                    if(!doUpdate)
                                        doUpdate  = true;
                                    lyf.ownerId = conObj.ownerId;
                                }
                                
                                //Every time a Contact Priority is Updated
                                if(conObj.Priority__c <> oldMap.get(conObj.id).Priority__c && RelatedlyfRec.Priority__c <> conObj.Priority__c){
                                    System.debug('Update Priority__c');
                                    if(!doUpdate)
                                        doUpdate  = true;
                                    lyf.Priority__c = conObj.Priority__c;
                                }
                                
                                if(doUpdate)
                                    newLifecycle.add(lyf);
                                System.debug('newLifecycle >> ' + newLifecycle);
                            }
                        }
                    }
                }
                if(newLifecycle <> null && newLifecycle.size() > 0){
                    System.debug('newLifecycle >> ' + newLifecycle);
                    update newLifecycle;
                }
            }
        }
    }
    
    public void mostRecentLifecycleRecord(Set<Id> conIds){
        if(conIds <> null && conIds.size() > 0){
            
            for(Contact obj : [Select id, (Select id, name, Lifecycle_Status__c, Lifecycle_Stage__c, Priority__c, ownerId, Person_Status__c from Lifecycles__r where Lifecycle_Status__c = 'Inactive' or Lifecycle_Status__c = 'Active' order by lastModifiedDate) from Contact where id in :conIds]){
                
                for(Lifecycle__c lyfObj : obj.Lifecycles__r){
                    if(lyfObj.Lifecycle_Status__c == 'Active' && !this.contactToActiveLifeCycleMap.containsKey(obj.id)){
                        this.contactToActiveLifeCycleMap.put(obj.Id, lyfObj);
                        //if(this.contactToInactiveLifeCycleMap.size() > 0)
                        //this.contactToInactiveLifeCycleMap.clear();
                    }
                    else if(lyfObj.Lifecycle_Status__c == 'Inactive' && !this.contactToInactiveLifeCycleMap.containsKey(obj.id) && this.contactToActiveLifeCycleMap.size() <= 0 && this.contactToInactiveLifeCycleMap.size() <= 0){
                        this.contactToInactiveLifeCycleMap.put(obj.Id, lyfObj);
                    }
                    
                }
                
            }
        }
    }
    
    //this part of code is implemented in MergeUtil and hence commented here
    /*public void deleteOrphanLifecycleRecords(Map<Id, Contact> oldMap){
        System.debug('oldMap >> ' + oldMap); 
        if(oldMap <> null && oldMap.size() > 0){
            List<Lifecycle__c > recordsToDelete = new List<Lifecycle__c >();
            
            Set<Id> nonMergeDelete = new Set<Id>();
            for(Contact conObj : oldMap.values()){
                if(conObj.masterRecordId == null)
                    nonMergeDelete.add(conObj.Id);
            }
            
            for(LifeCycle__c lf : [Select Id, Name, Related_Lead__c from LifeCycle__c where Related_Lead__c=null AND Related_Contact__c IN :nonMergeDelete]){
                if(lf.Related_Lead__c == null){
                    recordsToDelete.add(lf);
                }
            }
            
            System.debug('recordsToDelete >> ' + recordsToDelete);
            
            if(recordsToDelete <> null && recordsToDelete.size() > 0){
                delete recordsToDelete;
            }
        }
    }*/
    
}