trigger LeadTrigger on Lead (before insert, after insert, before Delete, after delete, before update, After update) {
    
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        if(trigger.isInsert){ ValidateCountryStateNames.runValidationCheck(null, null, trigger.new); }
        if(trigger.isUpdate){
            List<Lead> leadsToValidate = new List<Lead>();
            for(Lead l : trigger.new){
                if((l.Country_External_Source__c != null && l.Country_External_Source__c != trigger.oldMap.get(l.Id).Country_External_Source__c) ||
                   (l.State_External_Source__c != null && l.State_External_Source__c != trigger.oldMap.get(l.Id).State_External_Source__c)){
                       leadsToValidate.add(l);
                   }
            }
            if(leadsToValidate.size()>0){
                ValidateCountryStateNames.runValidationCheck(null, null, leadsToValidate);
            }
        }
        
        //IndustryValueMapping.updateIndustry(null, trigger.new);  
    }
    
    leadTriggerHandler handler = new leadTriggerHandler();
    
    if(trigger.isInsert && trigger.isAfter){
        handler.onAfterInsert(trigger.new);
    }
    
    if(trigger.isBefore && trigger.isDelete){
        handler.onBeforeDelete(trigger.oldMap);
    }
    
    //lead after delete
    if(trigger.isAfter && trigger.isDelete){
        handler.onAfterDelete(trigger.oldMap);
    }
    
    if(trigger.isAfter && trigger.isUpdate){
        system.debug('____event__fired___after___update');
        handler.onAfterUpdate(trigger.oldMap,trigger.newMap);
        
    }    
}