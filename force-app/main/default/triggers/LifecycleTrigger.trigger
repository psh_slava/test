trigger LifecycleTrigger on Lifecycle__c (After Update) 
{
   Set<id> Contactids = New Set<id>();
   Set<id> Leadids = New Set<id>();
   
   IF(Trigger.IsUpdate && Trigger.IsAfter)
   {
   
     For(Lifecycle__c L:Trigger.new)
     {
    
       IF(L.Related_Contact__c !=Null && L.AQL_Entry_Date__c != Null && L.AQL_Entry_Date__c != Trigger.oldMap.get(L.Id).AQL_Entry_Date__c)
       {
        Contactids.add(L.Related_Contact__c);
       }
       IF(L.Related_Lead__c!=Null && L.AQL_Entry_Date__c != Null && L.AQL_Entry_Date__c != Trigger.oldMap.get(L.Id).AQL_Entry_Date__c)
       {
        Leadids.add(L.Related_Lead__c);
       }
           
     } 
     If(Contactids.Size()>0 )
     {
     Map<id,Contact> UpdateMapcontacts = New Map<id,Contact>();
     List<contact> lstcontacts = [Select id,AQL_Date__c from Contact where id IN :Contactids]; 
     For(Lifecycle__c L:Trigger.new)
     {
        For(contact C :lstcontacts)
        {
          IF(c.id == L.Related_Contact__c)
          {
            c.AQL_Date__c = L.AQL_Entry_Date__c;
            UpdateMapcontacts.Put(C.id,C);
          }
        }
     }
     
      Update UpdateMapcontacts.Values();
     
     }
     If(Leadids.Size()>0 )
     {
     Map<id,Lead> UpdateMapLeads = New Map<id,Lead>();
     List<Lead> LstLeads = [Select id,AQL_Date__c from Lead where id IN :Leadids];
     
     For(Lifecycle__c L:Trigger.new)
     {
        For(Lead C :LstLeads)
        {
          IF(c.id == L.Related_Lead__c)
          {
            c.AQL_Date__c = L.AQL_Entry_Date__c;
            UpdateMapLeads.Put(C.id,C);
          }
        }
     }
     
      Update UpdateMapLeads.Values();
     
     }
   }
}