/* TEST CLASS = TaskTriggerTest */

public class TaskTriggerHandler {
    
    TaskTriggerAction taskAction = new TaskTriggerAction();
    
    /*After Insert*/
    public void afterInsert(List<Task> newTaskList){

        /*CHECK IF IT IS A FOLLOWUP TASK OF DISCOVERY TASK 
            THEN RE-ASSOCIATED THE OPPORTUNITY AND NOT CREATE DUPLICATE 
            OTHERWISE CREATE NEW OPPORTUNITY
        */
        TaskAction.followUpTaskOrNewTask(newTaskList);
        
    }
    
}