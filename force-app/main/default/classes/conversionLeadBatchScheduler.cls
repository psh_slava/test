global class conversionLeadBatchScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        conversionLeadBatch b = new conversionLeadBatch ();
            
        database.executebatch(b,5);
    }
   
}