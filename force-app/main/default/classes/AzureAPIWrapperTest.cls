@isTest
public class AzureAPIWrapperTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    public static testMethod void testTIAPI() {
        Azure_API_Settings__c settings = new Azure_API_Settings__c();
        settings.API_Key__c = 'testaccesskey';
        settings.Base_URL__c = 'testurl.test.test/';
        settings.Client_Id__c = '12345-6789';
        settings.Tennant_Id__c = '12345';
        settings.Name = 'Test';
        settings.Group_Id_All_Users_SSO__c = 'testgroup123';
        settings.Token_Endpoint__c = 'testurl2.test.test/';
        
        Id employeeRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        
        Account a = new Account();
        a.Name = 'Cherwell Test Account';
        insert a;
        
        settings.Cherwell_Account_Id__c = a.Id;
        insert settings;
        
        Contact c = new Contact();
        c.LastName = 'Wilburson';
        c.AccountId = a.Id;
        c.Email = 'test@test.com';
        c.Status__c = 'Active';
        c.RecordTypeId = employeeRTId;
        c.Azure_Id__c = 'testid-f4f4-4af3-a76e-25e3bab0d896';
        insert c;
        
                
      /*  TIAPIWrapper.licenseWrapper lw = new TIAPIWrapper.licenseWrapper();
        lw.accountName = 'test company';
        lw.companyLicenseId = '1234';
        lw.companyParentLicenseId = '5678';
        lw.employeeLicenseId = '9999';
        lw.productLicenseId = '5555';
        
        String jsonString = '{"id": "e3a230a4-06b1-47c5-aa46-fe5f9e8bbab3","name": "Test Client","sku": "test-client-sku","licenses": '+
                         '[{"id": "2a779a49-50eb-48de-a997-96d6aa6252ff","name": "Main","sku": "main","createdAt": "2018-02-15T14:06:03.084Z","updatedAt": '+
                         '"2018-02-15T14:06:03.084Z","accessDays": null,"seatsLimit": null,"learningPathIds": ["7c117319-60d1-45fd-bf1d-3d92bb824a84"],"courseIds": '+
                         '["13b808e9-4064-4a0b-b36d-8e8a4e242260", "8ae5bdab-e318-5ff1-b630-bfe01a1a09be"],"courseTags": [{ "id": "591a2429-e3b0-5a79-b18a-d5e35cb444ba", "label": "Test Tag" }],'+
                         '"parentLicenseId": "1234-5678"}]}';
        ResponseResult.parse(jsonString);
        
        TIRecordWrapper.getBodyLicenseCreate('clientId', 'sku', 'parentLicenseId', 'accountName', '72d0e1cb-edf3-4118-95c0-fadb3e1ed5fa');*/
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AzureAPICalloutMock());
        String jobId = System.schedule('ScheduleAzurePITest', CRON_EXP, new AzureAPIProcessorScheduler());
        
        AzureAPIProcessorBatch tp = new AzureAPIProcessorBatch();
        Database.executebatch(tp);
        
        Test.stopTest();//will sync back up with async operations if future/batch called before this
    }

}