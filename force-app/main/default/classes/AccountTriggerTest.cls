@isTest
public class AccountTriggerTest {
    
    public static testmethod void updateCommunityMembers(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account a = new Account(Name = 'Test Account', Account_Status__c = 'Prospect', RecordTypeId = recordTypeId);
        insert a;
        
        List<Contact> contacts = new List<Contact>();
        Contact c1 = new Contact(LastName = 'Tester1', AccountId = a.Id, Status__c = 'Active', Community_Member__c = true, Email = 'testUCM1@test.test' );
        Contact c2 = new Contact(LastName = 'Tester2', AccountId = a.Id, Status__c = 'Inactive', Community_Member__c = true, Email = 'testUCM2@test.test' );
        contacts.add(c1);
        contacts.add(c2);
        insert contacts;
        
        a.Account_Status__c = 'Active';
        update a;
        
        Contact c1v = [Select Community_Member_Type_Code__c From Contact Where LastName = 'Tester1'];
        System.assert(c1v.Community_Member_Type_Code__c == 'Customer');
        
        Contact c2v = [Select Community_Member_Type_Code__c From Contact Where LastName = 'Tester2'];
        //System.assert(c2v.Community_Member_Type_Code__c == 'Non-member');
    }
     public static testmethod void createChecklistItems(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='testerchecklisttriggerhandler@test.test', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testerchecklisttriggerhandler@test.test');
        insert u;
        
        String partnerRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        
        String partnerChecklistRTId = Schema.SObjectType.Checklist__c.getRecordTypeInfosByName().get('Partner Onboarding Checklist').getRecordTypeId();
        partnerChecklistRTId = String.valueOf(partnerChecklistRTId).subString(0,15);
        System.debug(partnerChecklistRTId);
        List<Account> lstAccount = New List<Account>();
        
         
        
        List<Checklist_Activities__c> customSettingList = new List<Checklist_Activities__c>();
        Checklist_Activities__c ca = new Checklist_Activities__c(Checklist_Record_Type_Id__c = partnerChecklistRTId,
                                                                Due_Date_Days_from_Today__c = 7,
                                                                Item_Order_Number__c = 1, Name = 'test1', Priority__c = 'Normal',
                                                                Stage_Section__c = 'Initial',Comments__c = 'Test',
                                                                Subject_Label__c = 'Do This');
        customSettingList.add(ca);
        Checklist_Activities__c ca2 = new Checklist_Activities__c(Checklist_Record_Type_Id__c = partnerChecklistRTId,
                                                                Due_Date_Days_from_Today__c = 14,
                                                                Item_Order_Number__c = 2, Name = 'test2', Priority__c = 'Normal',
                                                                Stage_Section__c = 'Approved',
                                                                Subject_Label__c = 'Do That');
        customSettingList.add(ca2);
        
        insert customSettingList;
        
        Account a = new Account(name='Test Account', RecordTypeId = partnerRTId, OwnerId = u.Id);
        lstAccount.Add(a);
        insert lstAccount;
        
        lstAccount[0].RecruitmentStage__c = 'Initial';
        Update lstAccount;
        
                
        Checklist__c ckl = new Checklist__c(Account__c = a.Id, RecordTypeId = partnerChecklistRTId);
        insert ckl;
        
        AccountTriggerHandler.createChecklistItems(lstAccount);
    }

}