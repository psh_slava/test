public class SignaturePackageFlowController {
    
    public List<Signature_Routing_Rule__c> rules {get; set;}
    public SBQQ__Quote__c quote;
    public SignaturePackageFlowController(ApexPages.StandardController sc) {
        this.quote = (SBQQ__Quote__c)sc.getRecord();
    }
    
    public PageReference createSignPkg() { 
        
        String theId = ApexPages.currentPage().getParameters().get('id');
        //In any of the buttons, specify parameter that is equal name of custom setting entry for Ironclad Workflow Paths
        String icFlow = ApexPages.currentPage().getParameters().get('flow');
        
        if(theId == null){
            return null;
        }
        
        String withDocs = ApexPages.currentPage().getParameters().get('withDocs');
        System.debug('withDocs=== ' + withDocs);
        
        //Declared variables
        String redirectURL;
        String ocSignerName;
        String ocSignerEmail;
        String ocSignerTitle;
        String psSignerName;
        String psSignerEmail;
        String psSignerTitle;
        String redirectURLwDocs;
        String SOWincluded;
        String cherwellGo;
        String spRecordId;
        Boolean addendumA = false;
        Boolean addendumB = false;
        Boolean addendumC = false;
        Boolean addendumD = false;
        Boolean addendumE = false;
        Boolean addendumF = false;
        Boolean addendumG = false;
        ContentDocument psSOW;
        ContentDocument lsSoW;
        String trainingSOW;
        String lsSignerEmail;
        String lsSignerName;
        String lsSignerTitle;
        String customerType;
        ContentVersion lsSOWBody;
        ContentVersion psSOWBody;
        
        //if(theId != null && icFlow != null && theObject != null){ 
        if(theId != null && icFlow != null){
            
            //Get the correct ironclad workflow url
            Ironclad_Workflow_Paths__c iwp = Database.query('Select Name, Workflow_URL__c From Ironclad_Workflow_Paths__c Where Name = \'' + icFlow + '\' Limit 1');             
            redirectURL = iwp.Workflow_URL__c;
            
            //Get all related Signature Routing Rules and Conditions that are Active and that apply to this particular Object
            rules = [Select Id, Name, Advanced_Condition__c, Conditions_Met__c, Document__c, Dynamic_Signer_Id_Field_API_Name__c,
                     Object_API_Name__c, Routing_Type__c, Signer_Email__c, Signer_Name__c, Signer_Title__c,
                     (Select Id, Name, Index__c, Tested_Field_API_Name__c, Condition__c, Value__c
                      From Signature_Routing_Conditions__r)
                     From Signature_Routing_Rule__c
                     Where Active__c = true];
            //And Object_API_Name__c = :theObject];
            System.debug('Routing rules to apply ---- ' + rules);
            
            String baseFields = 'Id,SBQQ__Account__c,SBQQ__Opportunity2__r.PSO_Total_Amount__c,SBQQ__Account__r.Name,SBQQ__Opportunity2__c,' +
                'SBQQ__PrimaryContact__c,SBQQ__SalesRep__c,Convert_to_Order_Confirmation_Timestamp__c,' + 
                'Convert_to_Order_Confirm__c,SBQQ__Primary__c,Billing_Contact__c,No_Send_for_Signature__c';
            List<String> baseFieldsList = baseFields.split(',');
            Set<String> testedFieldsSet = new Set<String>();
            testedFieldsSet.addall(baseFieldsList);
            for(Signature_Routing_Rule__c rule : rules){
                if(rule.Dynamic_Signer_Id_Field_API_Name__c != null){ testedFieldsSet.add(rule.Dynamic_Signer_Id_Field_API_Name__c); }
                for(Signature_Routing_Condition__c con : rule.Signature_Routing_Conditions__r){
                    testedFieldsSet.add(con.Tested_Field_API_Name__c);
                }
            }
            List<String> testedFieldsList = new List<String>(testedFieldsSet);
            String testedFields = String.join(testedFieldsList,','); 
            
            //Get the Quote information (and Order Confirmation routing)
            System.debug('Adding fields to query ---- ' + testedFields);
            String query = 'Select ' + testedFields + ' From SBQQ__Quote__c Where Id = :theId';
            System.debug('QUERY ---- ' + query);
            quote = Database.query(query);
            
            //Safeguard against OCs being sent for deal scenarios where they're not valid (i.e. Amazon Marketplace private offers)
            if (quote.No_Send_for_Signature__c == true){
                ApexPages.Message noSend = new ApexPages.Message(ApexPages.Severity.ERROR, 'This deal cannot be sent for electronic signature via Ironclad. <br>' +
                                                                 'Please contact your manager or Sales Ops team for questions. ');
                ApexPages.addMessage(noSend);
                return null;
            }
            
            //Safeguard against quotes being sent - should only be Order Confirmations
            if (quote.Convert_to_Order_Confirm__c == false){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'This quote has not been converted to an Order Confirmation. <br> '+
                                                                'Please return to your Quote, check the Convert to Order Confirmation box and fill out the required fields. '+
                                                                'Then, Generate and Save a new Quote Document. <br>'+
                                                                'If you have any issues, please contact your Sales Ops team. ');
                ApexPages.addMessage(myMsg); 
                return null;
            }
            
            //Safeguard against non-primary OCs being sent - should only be from Quote records that are Primary
            if (quote.SBQQ__Primary__c == false){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Order Confirmations can only be sent from Quotes marked Primary. Please visit the Primary Quote record or Go Back and mark this Quote as Primary before sending.');
                ApexPages.addMessage(myMsg); 
                return null;
            }
            
            //Get the Contact information
            Contact c;
            if ( quote.SBQQ__PrimaryContact__c != null){
                c = [Select Id, Email
                     From Contact
                     Where Id = :quote.SBQQ__PrimaryContact__c];
            } else {
                c = [Select Id, Email
                     From Contact
                     Where Id = :quote.Billing_Contact__c];
            }
            
            //Get the User information
            User u = [Select Id, ManagerId, Manager.Email, Manager.Name, Manager.Title
                      From User 
                      Where Id = :quote.SBQQ__SalesRep__c];              
            
            //Get Quote Document
            SBQQ__QuoteDocument__c qd; 
            Datetime convertDate = quote.Convert_to_Order_Confirmation_Timestamp__c;
            System.debug('Convert date ==== ' + convertDate);
            
            //Get OC signer info
            Map<String,String> ocSignerInfo = getRoutingRules('Order Confirmation');
            ocSignerEmail = ocSignerInfo.get('signerEmail');
            ocSignerName = ocSignerInfo.get('signerName');
            ocSignerTitle = ocSignerInfo.get('signerTitle');
            
            //Safeguards: make sure there's a document, and make sure it's an Order Confirmation by checking it against the timestamp
            try {
                if(convertDate == null){ throw new QueryException(); }
                qd = [Select SBQQ__DocumentId__c, CreatedDate
                      From SBQQ__QuoteDocument__c
                      Where SBQQ__Opportunity__c = :quote.SBQQ__Opportunity2__c
                      And CreatedDate > :convertDate
                      And SBQQ__Quote__c = :quote.Id
                      Order By CreatedDate DESC
                      Limit 1];
                System.debug('OC created date === ' + qd.CreatedDate);
            } catch (QueryException e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please go back and generate a new Order Confirmation document before trying to send.');
                ApexPages.addMessage(myMsg); 
                return null;
            }
            
            Document d = [Select Id, Body, Name
                          From Document
                          Where Id = :qd.SBQQ__DocumentId__c];
            
            //If wanting to send full package...
            if(withDocs == 'true'){
                
                //Get SOW signer info
                Map<String,String> sowSignerInfo = getRoutingRules('Professional Services SOW');
                psSignerEmail = sowSignerInfo.get('signerEmail');
                psSignerName = sowSignerInfo.get('signerName');
                psSignerTitle = sowSignerInfo.get('signerTitle');
                
                //Get the Quote Line information
                List<SBQQ__QuoteLine__c> psLines = [Select Id, SBQQ__Product__r.Name
                                                    From SBQQ__QuoteLine__c
                                                    Where SBQQ__Quote__c = :theId
                                                    And Quote_Group_Name__c = 'Professional Services'];
                Map<String, Id> psLinesMap = new Map<String, Id>();
                for(SBQQ__QuoteLine__c psl : psLines){
                    psLinesMap.put(psl.SBQQ__Product__r.Name, psl.Id);
                }
                if(psLinesMap.containsKey('Cherwell GO!')){
                    cherwellGo = 'Generate a CherwellGO! SOW';
                    SOWincluded = 'Yes';
                    if(psLinesMap.containsKey('Cherwell GO! Addendum A')){ addendumA = true; }
                    if(psLinesMap.containsKey('Cherwell GO! Addendum B')){ addendumB = true; }
                    if(psLinesMap.containsKey('Cherwell GO! Addendum C')){ addendumC = true; }
                    if(psLinesMap.containsKey('Cherwell GO! Addendum D')){ addendumD = true; }
                    if(psLinesMap.containsKey('Cherwell GO! Addendum E')){ addendumE = true; }
                    if(psLinesMap.containsKey('Cherwell GO! Addendum F')){ addendumF = true; }
                    if(psLinesMap.containsKey('Cherwell GO! Addendum G')){ addendumG = true; }
                }
                
                if(!psLinesMap.containsKey('Cherwell GO') && quote.SBQQ__Opportunity2__r.PSO_Total_Amount__c > 0){
                    cherwellGo = 'Upload an SOW';
                    SOWincluded = 'Yes';
                    
                    //Get PSO SOW
                    ContentDocumentLink psSOWLink;
                    try{
                        psSOWLink = [Select ContentDocumentId, ContentDocument.Title
                                     From ContentDocumentLink
                                     Where ContentDocument.Title LIKE '%SOW%'
                                     And (NOT ContentDocument.Title LIKE '%Training%')
                                     And ContentDocument.Title LIKE '%Final%'
                                     And LinkedEntityId = :quote.SBQQ__Opportunity2__r.Id];
                    } catch (NullPointerException e) {
                        //catch error
                        System.debug('No PS SOW found.');
                    } catch (Exception ex){
                        system.debug('Exception caught---'+ ex.getMessage());
                    }
                    if(psSOWLink != null){
                        psSOW = [Select Id, Title
                                 From ContentDocument
                                 Where Id = :psSOWLink.ContentDocumentId];
                        psSOWBody = [Select VersionData From ContentVersion Where ContentDocumentId = :psSOWLink.ContentDocumentId AND IsLatest = true];
                    }  
                }
                
                //Look for custom training SOW on Opp
                ContentDocumentLink lsSOWLink;
                try{
                    lsSOWLink = [Select ContentDocumentId, ContentDocument.Title
                                 From ContentDocumentLink
                                 Where ContentDocument.Title LIKE '%Training%'
                                 And ContentDocument.Title LIKE '%SOW%'
                                 And ContentDocument.Title LIKE '%Final%'
                                 And LinkedEntityId = :quote.SBQQ__Opportunity2__r.Id];
                } catch (NullPointerException e){
                    //catch error
                    System.debug('No Training SOQ found.');
                } catch (Exception ex){
                    System.debug('Exception caught---'+ ex.getMessage());
                }       
                
                if(lsSOWLink != null){
                    trainingSOW = 'Yes';          
                    Map<String,String> lsSignerInfo = getRoutingRules('Learning Services SOW');
                    lsSignerEmail = lsSignerInfo.get('signerEmail');
                    lsSignerName = lsSignerInfo.get('signerName');
                    lsSignerTitle = lsSignerInfo.get('signerTitle');
                    
                    lsSOW = [Select Id, Title
                             From ContentDocument
                             Where Id = :lsSOWLink.ContentDocumentId];
                    lsSOWBody = [Select VersionData From ContentVersion Where ContentDocumentId = :lsSOWLink.ContentDocumentId AND IsLatest = true];
                }     
            }
            
            //Create new Signature Package
            Signature_Package__c sp = new Signature_Package__c();
            sp.Cherwell_OC_Signer_Email__c = ocSignerEmail;
            sp.Cherwell_OC_Signer_Name__c = ocSignerName;
            sp.Contact__c = c.Id;
            sp.Customer_OC_Signer_Email__c = c.Email;
            sp.Opportunity__c = quote.SBQQ__Opportunity2__r.Id;
            sp.Quote2__c = quote.Id;
            sp.Statement_of_Work_included__c = SOWincluded;
            sp.Desired_SOW_Action__c = cherwellGo;
            sp.Password_Reset_Cherwell_Hosted__c = addendumA;
            sp.Password_Reset_On_Premise__c = addendumB;
            sp.SAML__c = addendumC;
            sp.Go_Live_Support__c = addendumD;
            sp.On_Site_Consulting__c = addendumE;
            sp.Upgrade_to_Basic_Training_Package__c = addendumF;
            sp.Change_Management_Process__c = addendumG;
            sp.Separate_Training_SOW__c = trainingSOW;
            sp.Cherwell_PSO_Signer_Email__c = psSignerEmail;
            sp.Cherwell_PSO_Signer_Name__c = psSignerName;
            sp.Cherwell_Training_Signer_Email__c = lsSignerEmail;
            sp.Cherwell_Training_Signer_Name__c = lsSignerName;
            sp.Customer_Type2__c = customerType;
            insert sp;
            
            //Attach Quote Document
            sp = [Select Id From Signature_Package__c Where Id = :sp.Id];
            spRecordId = sp.Id;
            
            Attachment a = new Attachment();
            a.ParentId = sp.Id;
            a.Body = d.Body;
            a.Name = d.Name;
            a.Description = 'OC';
            insert a;
            
            //Add Professional Services and/or Learning Services SOWs
            if(psSOW != null) {
                Attachment pss = new Attachment();
                pss.ParentId = sp.Id;
                pss.Body = psSOWBody.VersionData;
                pss.Name = psSow.Title + '.pdf';
                pss.ContentType = 'application/pdf';
                pss.Description = 'SOW';
                insert pss;
                
            }
            if(lsSOW != null) {
                Attachment lss = new Attachment();
                lss.ParentId = sp.Id;
                lss.Body = lsSOWBody.VersionData;
                lss.Name = lsSOW.Title + '.pdf';
                lss.Description = 'Training SOW';
                insert lss;
            }
        }
        
        //Redirect to Ironclad
        PageReference ironcladPage = new PageReference(redirectUrl + spRecordId);
        //if(withDocs != 'true'){ ironcladPage = new PageReference(redirectUrl + spRecordId); }
        //if(withDocs == 'true'){ ironcladPage = new PageReference(redirectURLwDocs + spRecordId); }
        System.debug('Redirect page to------'+ironcladPage);
        ironcladPage.setRedirect(true);
        return ironcladPage;
    }
    
    public PageReference goBack() {
        String theId = ApexPages.currentPage().getParameters().get('id');
        PageReference quotePage = new PageReference('/'+theId);
        System.debug('Redirect page to------'+quotePage);
        quotePage.setRedirect(true);
        return quotePage;
        //**********************
        
    }
    
    //Method gets signer information from routing rules and based on document
    public Map<String,String> getRoutingRules(String documentName){
        Signature_Routing_Rule__c thisRule;
        Map<String,String> signerInfoMap = new Map<String,String>();
        
        for(Signature_Routing_Rule__c rule : rules){
            Map<Integer,Boolean> indexResult = new Map<Integer,Boolean>();
            System.debug('Evaluating rule: ' + rule.Name + '... ');
            if(rule.Document__c == documentName){
                for(Signature_Routing_Condition__c con : rule.Signature_Routing_Conditions__r){
                    Boolean result = false;
                    Integer index = Integer.valueOf(con.Index__c);
                    String testedField;
                    if(con.Tested_Field_API_Name__c.contains('__r.')){
                        String [] concatFields = con.Tested_Field_API_Name__c.split('\\.');
                        testedField = String.valueOf(quote.getSObject(concatFields[0]).get(concatFields[1]));
                    } else if (!con.Tested_Field_API_Name__c.contains('__r.')){
                        testedField = String.valueOf(quote.get(con.Tested_Field_API_Name__c));
                    }
                    //String valueField = (String)quote.get(con.Value__c);
                    String valueField = con.Value__c;
                    System.debug('Testing if ' + testedField + ' ' + con.Condition__c + ' ' + valueField);
                    if(con.Condition__c == 'equals'){ if(testedField == valueField){ result = true;  } }
                    if(con.Condition__c == 'not equals'){ if(testedField != valueField){ result = true;  } }
                    if(con.Condition__c == 'contains'){ if(testedField.contains(valueField)){ result = true;  } }
                    if(con.Condition__c == 'does not contain'){ if(testedField.contains(valueField)){ result = true;  } }
                    if(con.Condition__c == 'greater than'){ if(Decimal.valueOf(testedField) > Decimal.valueOf(valueField)){ result = true;  } }
                    if(con.Condition__c == 'greater or equal to'){ if(Decimal.valueOf(testedField) >= Decimal.valueOf(valueField)){ result = true;  } }
                    if(con.Condition__c == 'less than'){ if(Decimal.valueOf(testedField) < Decimal.valueOf(valueField)){ result = true;  } }
                    if(con.Condition__c == 'less or equal to'){ if(Decimal.valueOf(testedField) <= Decimal.valueOf(valueField)){ result = true;  } }
                    indexResult.put(index, result);
                    System.debug('Result = ' + result);
                }
                Boolean conditionsMet;
                if(rule.Conditions_Met__c == 'All'){
                    conditionsMet = true;
                    for(Integer i : indexResult.keySet()){
                        if(indexResult.get(i) == false){
                            conditionsMet = false;
                        }
                    }  
                }
                if(rule.Conditions_Met__c == 'Any'){
                    conditionsMet = false;
                    for(Integer i : indexResult.keySet()){
                        if(indexResult.get(i) == true){
                            conditionsMet = true;
                        }
                    }
                }
                if(rule.Conditions_Met__c == 'Custom'){
                    String booExp = rule.Conditions_Met__c;
                    for(Signature_Routing_Condition__c con : rule.Signature_Routing_Conditions__r){
                        Boolean boo = indexResult.get(Integer.valueOf(con.Index__c));
                        String booString;
                        if(boo == true){ booString = 'TRUE'; }
                        if(boo == false){ booString = 'FALSE'; }
                        booExp = booExp.replace(String.valueOf(con.Index__c), booString);
                    }
                    if(BooleanExpression.eval(booExp) == true){
                        conditionsMet = true;
                    }
                }
                //If conditions are met for the routing rule..... 
                System.debug('Conditions met?: ' + conditionsMet);
                if(conditionsMet == true){
                    thisRule = rule;
                }
            }
        }  
        if(thisRule.Routing_Type__c == 'Static'){
            signerInfoMap.put('signerEmail', thisRule.Signer_Email__c);
            signerInfoMap.put('signerName', thisRule.Signer_Name__c);
            signerInfoMap.put('signerTitle', thisRule.Signer_Title__c);
        }
        if(thisRule.Routing_Type__c == 'Dynamic'){
            String userId;
            if(thisRule.Dynamic_Signer_Id_Field_API_Name__c.contains('.')){
                String [] concatFields = thisRule.Dynamic_Signer_Id_Field_API_Name__c.split('\\.');
                if(concatFields.size() == 2){
                    userId = String.valueOf(quote.getSObject(concatFields[0]).get(concatFields[1]));
                }
                if(concatFields.size() == 3){
                    userId = String.valueOf(quote.getSObject(concatFields[0]).getSObject(concatFields[1]).get(concatFields[2]));
                }
            }
            else if(!thisRule.Dynamic_Signer_Id_Field_API_Name__c.contains('.')){
                userId = (String)quote.get(thisRule.Dynamic_Signer_Id_Field_API_Name__c);
            }
            System.debug('User Id = ' + userId);
            User signer = [Select Id, FirstName, LastName, Email, Title
                           From User
                           Where Id = :userId
                           Limit 1];
            if(signer == null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No dynamic signer found for this record. Trying to pull field: ' + thisRule.Dynamic_Signer_Id_Field_API_Name__c);
                ApexPages.addMessage(myMsg); 
                return null;
            }
            if(signer != null){
                signerInfoMap.put('signerEmail', signer.Email);
                signerInfoMap.put('signerName', signer.FirstName + ' ' + signer.LastName);
                signerInfoMap.put('signerTitle', signer.Title);
            }
        }
        return signerInfoMap;
    }  
}