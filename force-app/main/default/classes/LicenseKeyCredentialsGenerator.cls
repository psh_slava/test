public class LicenseKeyCredentialsGenerator {
    
    public static String generateUsername(Id csmLKId, String accountName){
        
        String username;
        
        Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumeric.matcher(accountName);
        String usernameLong = matcher.replaceAll('');
        System.debug(usernameLong);
        
        if(usernameLong.length()>=10){ 
            username = usernameLong.substring(0,10);
        }
        if(usernameLong.length()<10){
            username = usernameLong;
        }
        System.debug(username);
        
        return username;
        
    }
    
    public static String generatePassword(Id csmLKId){
        Integer len = 32;
        Blob blobPw = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobPw);
        String password = key.substring(0,len);
        
        System.debug('**********' + password);
        
        return password;
    }
    
}