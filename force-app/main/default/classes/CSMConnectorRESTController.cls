@RestResource(urlMapping='/csmconnector')
global with sharing class CSMConnectorRESTController
{
	@HttpPost
	global static void processPostData() {
        RestRequest request = RestContext.request;
        String postDataText = request.requestBody.toString();

		System.debug(postDataText);

        if (TriggerHelper.DoExecute('Inbound.CSMSync')) {
			CSMSyncEntryHelper.enqueueSyncEntry(postDataText);
            CentralSyncEntryHelper.enqueueSyncEntry(postDataText);
		}
	}
}