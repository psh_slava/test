@isTest
public class CommunityActivityTriggerTest {
    
    public static testmethod void testtrigger(){
        Account a = new Account(Name = 'Test Account');
        insert a;
        
        Contact c = new Contact(AccountId = a.Id, LastName = 'Tester', Email = 'test@test.test');
        insert c;
        
        HL_Community_Activity__c hca = new HL_Community_Activity__c(Contact_Id__c = c.Id, Activity_Type__c = 'Test');
        insert hca;
        
    }
}