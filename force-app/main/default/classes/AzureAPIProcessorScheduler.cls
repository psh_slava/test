public class AzureAPIProcessorScheduler implements Schedulable {
    
     public void execute(SchedulableContext sc) {
		AzureAPIProcessorBatch azBatch = new AzureAPIProcessorBatch();
        Database.executebatch(azBatch,200);
	}

}