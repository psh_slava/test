/** 
* Hosted_Instance_AfterInsert  Trigger Handler
*
* @author Naomi Harmon 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Hosted_Instance_AfterInsert  extends TriggerHandlerBase {

    public override void mainEntry(TriggerParameters tp) {
       	if (TriggerHelper.DoExecute('Hosted_Instance__c.CSMSync')) {
            CSMSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.newList);
        }
        if (TriggerHelper.DoExecute('Hosted_Instance__c.CentralSync')) {
            CentralSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.newList);
        }
    }
}