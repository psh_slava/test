public class LicenseKeyAutoGenerator {
    
    public static List<CSM_License_Record__c> generateKeyRows(String contractId){
        
        Contract con;
        String quoteId;
        Account a;
        String accountId;
        String lkContactId;
        String lkContactEmail;
        String oppId;
        String companyName;
        Date maintenanceEndDate;
        Boolean isHosted = false;
        Boolean hasAddOns = false;
        String licenseType;
        String financeModel;
        String licenseModel;
        Decimal licenseQuantity;
        String instanceNumber;
        String instanceName;
        String dealType;
        Id resellerId;
        //String purchaseScenario; 
        List<CSM_License_Record__c> currentKeys;
        String oppRT;
        Id formerKey;
        //Decimal newQuantity;
        Boolean hasCurrentKey;
        String keyIdentifier;
        String keyIdentifier2;
        String errorMessage;
        Id matchedKey;
        Id exactKey;
        Date oppCloseDate;
        Set<String> addOnProds = new Set<String>();
        
        
        List<CSM_License_Record__c> autoGenKeys = new List<CSM_License_Record__c>();
        
        //Get source record values
        accountId = [Select AccountId From Contract Where Id = :contractId].AccountId;
        con = [Select Id, AccountId, Account.License_Key_Override__c, Account.Customer_Success_Advocate__c, Hosting_Model__c, EndDate, Account.Name, 
               SBQQ__Opportunity__c, SBQQ__Quote__c, SBQQ__Quote__r.Opportunity_Record_Type__c, StartDate 
               From Contract 
               Where AccountId =:accountId
               And Status = 'Activated'
               Order By ActivatedDate DESC
               Limit 1];
        System.debug('Contract---------'+con);
        
        //If License Key Override = true, don't run any automation
        if(con.Account.License_Key_Override__c != true){
            
            //Get most recently closed won Opportunity
            List<Opportunity> opps = new List<Opportunity>();
            SBQQ__Quote__c q;
            Contract cc;
            
            companyName = con.Account.name;
            //maintenanceEndDate = con.EndDate;
            //3/27/2019 - expiration date will default to 1 year after Opportunity close won date,
            //except if customer pre-pays for entire term***
            maintenanceEndDate = con.StartDate.addYears(1).addDays(-1); 
            
            try{
                opps = [Select Id, RecordType.Name, CloseDate, SBQQ__PrimaryQuote__c, ContractId, Sales_Assist_Partner_Percentage__c, Sales_Assist_PartnerId__c, Reseller_Partner__c   
                        From Opportunity 
                        Where AccountId = :con.AccountId 
                        And IsWon = TRUE
                        Order By CloseDate DESC];
                oppCloseDate = opps[0].CloseDate;
                System.debug('OppCloseDate ====' + oppCloseDate);
                //Legacy fields of Sales Assist Partner Percentage and Sales Assist Partner Id need to be looked at for a time being
                //New Reseller Partner field will be the correct indicator moving forward (after 4/19/19)
                if(opps[0].Sales_Assist_Partner_Percentage__c == 'Limited Reseller' && opps[0].Sales_Assist_PartnerId__c != null){
                    dealType = 'Reseller Partner';
                    resellerId = opps[0].Sales_Assist_PartnerId__c;
                }
                if(opps[0].Reseller_Partner__c != null){
                    dealType = 'Reseller Partner';
                    resellerId = opps[0].Reseller_Partner__c;
                }
                else if(opps[0].Sales_Assist_Partner_Percentage__c == 'Fulfillment Partner'){
                    dealType = 'Fulfillment Partner';
                }
                else if(opps[0].Reseller_Partner__c == null && opps[0].Sales_Assist_Partner_Percentage__c != 'Limited Reseller' && opps[0].Sales_Assist_Partner_Percentage__c != 'Fulfillment Partner'){
                    dealType = 'Direct';
                }
                
                oppId = opps[0].Id;
                oppRT = opps[0].RecordType.Name;
                
                q = [Select Id, Prepaid_Fees__c, Prorate_To_Date__c
                     From SBQQ__Quote__c
                     Where SBQQ__Opportunity2__c = :oppId
                     And SBQQ__Primary__c = true];
                quoteId = q.Id;
                
                //3/27/2019 - expiration date will default to 1 year after Opportunity close won date,
                //except if customer pre-pays for entire term***
                if(q.Prepaid_Fees__c == 'Customer to prepay entire Term' || q.Prepaid_Fees__c == 'Both'){
                    maintenanceEndDate = con.EndDate;
                //} else { maintenanceEndDate = con.StartDate.addYears(1); 
                } else if (oppRT == 'Upsell') { 
                    maintenanceEndDate = q.Prorate_To_Date__c;
                } //else {
                    //opps[0].CloseDate.addYears(1); }
                                                 
            } catch (Exception e){
                System.debug('Error caught while querying for Opportunities: ' + e.getMessage());
            }        
            
            System.debug('Opportunity Id ==== ' + oppId);
            System.debug('Opportunity Record Type ==== ' + oppRT);
            System.debug('Quote Id ==== ' + quoteId);
            
            if(con.Hosting_Model__c == 'Cherwell Hosted'){ isHosted = true; }
            
            Map<String,Decimal> mapAddOns = new Map<String,Decimal>();
            Map<String,String> mapAddOnProducts = new Map<String,String>();
            Map<String,Id> mapKeys = new Map<String,Id>();
            Map<String,Id> mapKeys2 = new Map<String,Id>();
            Map<String,Id> mapFormer = new Map<String,Id>();
            Map<String,Decimal> mapKeyQuantity = new Map<String,Decimal>();
            Map<String,Datetime> mapKeyQuantity2 = new Map<String,Datetime>();
            
            AggregateResult[] summedLines;
            AggregateResult[] addOnLines; 
            
            //If no Primary Quote on the most recently close won Opportunity, then License Key cannot be auto-generated
            if(quoteId != null){
                
                SBQQ__Quote__c lkContact = [Select License_Key_Contact__c, License_Key_Contact__r.Email, SBQQ__StartDate__c From SBQQ__Quote__c Where Id = :quoteId Limit 1];
                lkContactId = lkContact.License_Key_Contact__c;
                lkContactEmail = lkContact.License_Key_Contact__r.Email;
                
            } 
            
            //Get all Subscriptions related to Contract
            summedLines = [Select SBQQ__Product__c, Instance_Number__c, Instance_Name__c, SUM(SBQQ__Quantity__c) quant
                           From SBQQ__Subscription__c
                           Where SBQQ__Contract__c = :contractId
                           Group By SBQQ__Product__c, Instance_Number__c, Instance_Name__c];
            System.debug('Summed Subscription lines = ' + summedLines);
            
            //Get all Add-On Subscriptions related to Contract
            addOnLines = [Select Instance_Number__c, SUM(SBQQ__Quantity__c) 
                          From SBQQ__Subscription__c
                          Where SBQQ__Product__r.Name LIKE '%(Add-On)%'
                          And SBQQ__Contract__c = :contractId
                          Group By Instance_Number__c];
            System.debug('Add-on Subscription lines = ' + addOnLines); 
            
            //If there are Add-On lines, map them by Instance and Quantity
            if(addOnLines.size()>0){
                for(AggregateResult aol : addOnLines){
                    System.debug('Add-On Line----' + aol);
                    mapAddOns.put((String)aol.get('Instance_Number__c'),(Decimal)aol.get('expr0'));
                }  
            } 
            
            //Search for existing License Key records
            List<CSM_License_Record__c> existingLKRs = [Select Id, CreatedDate, Current_Key__c, License_Type__c, Licensed_Items__c, Expires__c, Hosted__c, Maintenance__c, 
                                                        License_Name__c, Primary_Contact__c, Renewal_Start_Date_Manual__c
                                                        From CSM_License_Record__c
                                                        Where Account__c = :accountId
                                                        //And (Type__c = 'Permanent'
                                                        //     OR Type__c = 'Subscription'
                                                        //     OR Type__c = 'Temporary')
                                                        And License_Type__c = 'CSM'
                                                        //And CreatedDate < :opps[0].CloseDate
                                                        //And (Current_Key__c = 'Yes'
                                                             //OR Previous_Key__c = true 
                                                             //OR Future_Key__c = true)
                                                        ORDER BY CreatedDate ASC];
            System.debug('Existing LKRs '+ existingLKRs);
            for(CSM_License_Record__c eLKR : existingLKRs){
                mapKeys.put(eLKR.License_Type__c + eLKR.Maintenance__c, eLKR.Id);
                if(eLKR.Current_Key__c == 'Yes'){ formerKey = eLKR.Id; }
                mapKeyQuantity.put(eLKR.License_Type__c + eLKR.Maintenance__c, eLKR.Licensed_Items__c);
                Date createDate = Date.newInstance(eLKR.CreatedDate.year(), eLKR.CreatedDate.month(), eLKR.CreatedDate.day());
                mapKeyQuantity2.put(eLKR.License_Type__c + eLKR.Maintenance__c + eLKR.Licensed_Items__c, createDate);
                mapKeys2.put(eLKR.License_Type__c + eLKR.Maintenance__c + eLKR.Licensed_Items__c, eLKR.Id);
            }
            
            for(AggregateResult line : summedLines){
                
                String productId = (String)line.get('SBQQ__Product__c'); 
                String productName = [Select Name From Product2 Where Id = :productId Limit 1].Name; 
                System.debug('Product Name ==== ' + productName);
                
                if(productName.contains('CSM') && (productName.contains('License') || productName.contains('Maintenance & Support'))){
                    
                    if(productName.contains('Subscription')) { financeModel = 'Subscription'; } 
                    if(productName.contains('Maintenance'))    { financeModel = 'Perpetual';    } 
                    if(financeModel == 'Perpetual')          { financeModel = 'Permanent';    }
                    
                    licenseType = 'CSM';
                    licenseQuantity = (Decimal)line.get('quant');
                    System.debug('Quantity = ' + licenseQuantity);
                    
                    instanceNumber = (String)line.get('Instance_Number__c');
                    instanceName = (String)line.get('Instance_Name__c');
                    
                    keyIdentifier = licenseType + instanceNumber; 
                    //keyIdentifier2 = licenseType + instanceNumber + licenseQuantity;
                    
                    //Look for existing keys that match license type and instance...
                    Boolean formerKeyExists = false;
                    Boolean exactKeyExists = false;
                    
                    System.debug('Get mapped key value------' + mapKeys.get(keyIdentifier));
                    matchedKey = mapKeys.get(keyIdentifier);
                    if(matchedKey != null){ formerKey = [Select Id From CSM_License_Record__c Where Id = :matchedKey].Id; }
                    
                    if(mapKeys.get(keyIdentifier) != null){
                        //If there's an existing key that matches license type and instance, then former key exists
                        formerKeyExists = true;
                        //If there's an existing key that matches license type, instance, AND quantity, then exact key already exists
                        
                        //if(mapKeyQuantity.get(keyIdentifier) == licenseQuantity){ 
                        keyIdentifier2 = keyIdentifier + (mapKeyQuantity.get(keyIdentifier));
                        System.debug('keyIdentifier2 ====' + keyIdentifier2);
                        System.debug('Looking for exact match -----' + mapKeyQuantity2.get(keyIdentifier2));
                        if(mapKeyQuantity2.get(keyIdentifier2) >= oppCloseDate){
                            exactKeyExists = true; 
                            exactKey = mapKeys2.get(keyIdentifier2); 
                            System.debug('Exact key ==== ' + exactKey);
                        }
                    }
                    System.debug('Former Key exists == '+formerKeyExists);
                    System.debug('Exact Key exists == '+exactKeyExists);
                    if(mapAddOns.get(instanceNumber) != null) { hasAddOns = true; }
                    
                    
                    //get purchase scenario
                    if(oppRT == 'Upsell' && hasAddOns == true && exactKeyExists){
                        oppRT = 'Upsell: Add-on Only';
                        System.debug('This deal is an Add-On Only Upsell');
                    }
                    
                    if(oppRT == 'Renewal' && financeModel != 'Permanent' && exactKeyExists){
                        CSM_License_Record__c renewalKey;
                        try{
                            renewalKey = [Select Id
                                          From CSM_License_Record__c
                                          Where Id = :exactKey
                                          //And Opportunity__c = :oppId
                                          ];
                        } catch(Exception e){
                            System.debug('Exception caught while looking for existing Renewal Key ====' + e.getMessage());
                        }
                        if(renewalKey == null){ exactKeyExists = false; }             
                    }
                    
                    if((!exactKeyExists && oppRT != 'Renewal') || (oppRT == 'Renewal' && financeModel != 'Permanent' && exactKeyExists)){
                        CSM_License_Record__c newKey = new CSM_License_Record__c();
                        newKey.Licensed_Items__c = licenseQuantity;
                        newKey.Account__c = accountId;
                        newKey.Opportunity__c = oppId;
                        newKey.License_Type__c = licenseType;
                        if(financeModel != 'Permanent'){ newKey.Expires__c = maintenanceEndDate; }
                        newKey.Hosted__c = isHosted;
                        if(oppRT != 'Renewal'){ newKey.Current_Key__c = 'Yes'; }
                        if(oppRT == 'Renewal'){ newKey.Current_Key__c = 'No'; newKey.Future_Key__c = true; }
                        newKey.Type__c = financeModel;
                        newKey.Primary_Contact__c = lkContactId;
                        newKey.Primary_Contact_Email__c = lkContactEmail;
                        newKey.Maintenance__c = instanceNumber;
                        newKey.License_Name__c = instanceName;
                        newKey.Generated_Date__c = Date.today();
                        newKey.Former_Key__c = formerKey;
                        newKey.CSA__c = con.Account.Customer_Success_Advocate__c;
                        newKey.Purchase_Scenario2__c = oppRT;
                        newKey.Deal_Type_Manual__c = dealType;
                        newKey.Reseller_Partner_Account__c = resellerId;
                        autoGenKeys.add(newKey);
                    }
                    if(exactKeyExists && oppRT == 'Upsell: Add-on Only'){
                        //populate row, but just update existing key*
                        try{
                            CSM_License_Record__c upsoldKey = [Select Id, Licensed_Items__c, Account__c, License_Type__c, Hosted__c, Expires__c, Deal_Type_Manual__c, Renewal_Start_Date_Manual__c,
                                                               Current_Key__c, Type__c, Maintenance__c, License_Name__c, Generated_Date__c, Former_Key__c, CSA__c, Purchase_Scenario2__c,
                                                               Reseller_Partner_Account__c
                                                               From CSM_License_Record__c
                                                               Where Id = :matchedKey
                                                               And Opportunity__c != :oppId];
                            if(upsoldKey != null){
                                upsoldKey.Purchase_Scenario2__c = oppRT;
                                upsoldKey.Opportunity__c = oppId;
                                upsoldKey.Primary_Contact__c = lkContactId;
                                upsoldKey.Primary_Contact_Email__c = lkContactEmail;
                                autoGenKeys.add(upsoldKey);
                            }
                        } catch (Exception e){
                            System.debug('Exception caught when looking for Upsold Key ====' + e.getMessage());
                        }
                    }
                }
            }     
        }
        System.debug(autoGenKeys);
        return autoGenKeys;
    }
    
    
    public static List<CSM_License_Record__c> getCurrentKeys(String accountId){
        System.debug('Account Id passed in = ' + accountId);
        
        String query = 'Select Id ';
        for(Schema.FieldSetMember f : Schema.SObjectType.CSM_License_Record__c.FieldSets.getMap().get('Key_Generator_Window_Current').getFields()){                         
                    query += ',' + f.getFieldPath();
                }
        
        query += ' From CSM_License_Record__c Where Account__c = :accountId And Current_Key__c = \''+ String.escapeSingleQuotes('Yes')+'\' ORDER BY Expires__c DESC';
        
        List<CSM_License_Record__c> currentKeys = Database.query(query);
       
        System.debug(currentKeys);
        return currentKeys;
    }
    
    public static List<CSM_License_Record__c> getOldKeys(String accountId){
        System.debug('Account Id passed in = ' + accountId);
        
        String query = 'Select Id ';
        for(Schema.FieldSetMember f : Schema.SObjectType.CSM_License_Record__c.FieldSets.getMap().get('Key_Generator_Window_Inactive').getFields()){                            
                    query += ',' + f.getFieldPath();
                }
        
        query += ' From CSM_License_Record__c Where Account__c = :accountId And Current_Key__c != \''+ String.escapeSingleQuotes('Yes')+'\' ORDER BY Expires__c DESC';
        
        List<CSM_License_Record__c> oldKeys = Database.query(query);
        
        System.debug(oldKeys);
        return oldKeys;
    }
}