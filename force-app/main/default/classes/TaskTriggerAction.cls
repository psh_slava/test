/* Test Class : TaskTriggerTest  */

public class TaskTriggerAction {
    
    /*
CHECK IF IT IS A FOLLOWUP TASK OF DISCOVERY TASK THEN RE-ASSOCIATED THE OPPORTUNITY AND NOT CREATE DUPLICATE
OTHERWISE CREATE NEW OPPORTUNITY
*/
    public void followUpTaskOrNewTask(List<Task> newTaskList){
        
        List<Task> filteredTaskToCreateNewOpp = new List<Task>();
        Set<ID> parentTaskIDToClose = new Set<ID>();
        Boolean isError = false;
        
        for(Task tsk : newTaskList){
            
            //IF IT IS A FOLLOWUP TASK AND SUBJECT IS DISCOVERY CALL AND TYPE IS MEETING
                if(tsk.Parent_Task_ID__c != null ){       //&& tsk.type.tolowercase() == 'meeting'
                    parentTaskIDToClose.add(tsk.Parent_Task_ID__c);
                }
                //IF IT IS NOT A FOLLOWUP TASK
                else {
                    filteredTaskToCreateNewOpp.add(tsk);
                }
            
        }
        
        
            //CREATE NEW OPPORTUNTY
            if(filteredTaskToCreateNewOpp !=null && filteredTaskToCreateNewOpp.size() > 0 && !isError){
                createNewOpportunity(filteredTaskToCreateNewOpp);
            }
            //CLOSE PARENT TASK
            if(parentTaskIDToClose!= null && parentTaskIDToClose.size() > 0 && !isError){
                closeParentTask(parentTaskIDToClose);
            }
       
    }
    
    //CLOSE PARENT TASK
    public void closeParentTask(Set<ID> tasksToBeClosed){
        
        List<Task> closeTaskList = new List<Task>();
        for(ID taskId : tasksToBeClosed){
            closeTaskList.add(new Task(id=taskId,Status='Completed'));
        }
        
        if(closeTaskList != null && closeTaskList.size() > 0)
            update closeTaskList ;
    }
    
    //CREATE NEW OPPORTUNITY EACH TIME IF NO OPPORTUNTIY IS ASSOCIATED WITH THE TASK
    public void createNewOpportunity(List<Task> newTaskList){
        
        
        //Create Contact-to-TaskId and Contact-To-UserId Map
        Set<Id> ContactIdSet = new Set<Id>();
        Set<Id> UserIdSet = new Set<Id>();
        List<Task> FilteredTasks = new List<Task>();
        List<String> userExclusionList = System.label.Automatic_Opportunity_Creation_User_Exclusion != '' ? System.label.Automatic_Opportunity_Creation_User_Exclusion.split(';') : null;
        List<String> profileInclusionList = System.label.Automatic_Opportunity_Creation_Profiles_Allowed != '' ? System.label.Automatic_Opportunity_Creation_Profiles_Allowed.split(';') : null;
        Set<String> userExclusionSet = new Set<String>();
        
        if(userExclusionList != null)
            userExclusionSet.addAll(userExclusionList);
        
        //Initial Workaround
        for(Task taskObj : newTaskList) {
            
            String ContactId = String.valueOf(taskObj.WhoId);
            
            if(taskObj.subject.tolowercase() == 'discovery call' && taskObj.Type == 'Meeting' && ContactId != null && ContactId.startsWith('003') && (userExclusionSet == null || !userExclusionSet.contains(taskObj.ownerid)) ){
                
                ContactIdSet.add(taskObj.WhoId);
                FilteredTasks.add(taskObj);
                UserIdSet.add(taskObj.OwnerId);
            }
        }
        
        //Fetch Contacts
        Map<Id, Contact> ContactList = new Map<Id, Contact>( [ Select id, name, account.name, LeadSource, accountid, generated_by__c 
                                                              from Contact 
                                                              where id in :ContactIdSet]);
        
        // Implementation Start Here
        // Create New Opportuntiy
        Map<Id, Opportunity> newOpportunitiesCreatedMap = new Map<Id, Opportunity>();
        
        for( Task takObj : FilteredTasks ){ 
            
            Contact ContactRelatedToTask = ContactList.get( takObj.whoid );
            
            //Opportunity naming convention - Opportunity Name = Contact.Account_Name
            if( ContactRelatedToTask != null && !newOpportunitiesCreatedMap.containsKey( takObj.id ) ){
                
                system.debug('___ContactRelatedToTask___'+ContactRelatedToTask.generated_by__c);
                
                Opportunity newOpp = new Opportunity( Name = ContactRelatedToTask.account.name + ' - Discovery Call' );
                newOpp.StageName = 'Pre-Qualification';
                newOpp.CloseDate = System.Date.today().addDays(99);
                newOpp.AccountId = ContactRelatedToTask.accountId;
                newOpp.OwnerId = takObj.ownerId;
                newOpp.LeadSource = '3rd Party';
                newOpp.Opportunity_Source__c = takObj.Opportunity_Source__c;
                //if(ContactRelatedToTask.Generated_By__c != null && ContactRelatedToTask.Generated_By__c == 'Channel Generated')
                //    newOpp.Opportunity_Source__c = 'Channel';
                //else if(ContactRelatedToTask.LeadSource == 'BDR Created' || ContactRelatedToTask.Generated_By__c == 'Sales Generated')
                //    newOpp.Opportunity_Source__c = 'BDR Outbound';
                //else if(ContactRelatedToTask.Generated_By__c == 'Marketing Generated')
                //    newOpp.Opportunity_Source__c = 'Marketing (BDR Inbound)';
                //else if(ContactRelatedToTask.Generated_By__c != null && ( ContactRelatedToTask.Generated_By__c == 'Sales Generated' || ContactRelatedToTask.Generated_By__c == 'Marketing Generated' ) )
                //    newOpp.Opportunity_Source__c = 'Cherwell';  
                          
                newOpp.LeadSource = ContactRelatedToTask.LeadSource;
                
                system.debug('___newOpp___'+newOpp);
                
                newOpportunitiesCreatedMap.put(takObj.id,  newOpp);
            }
            
            
            
        }
        //Insert New Opportunities
        if(newOpportunitiesCreatedMap != null || !newOpportunitiesCreatedMap.isEmpty()){
            insert newOpportunitiesCreatedMap.values();
            System.debug('Opportunity Created > ' + newOpportunitiesCreatedMap);
        }
            
        
        //Initialise lists
        List<OpportunityContactRole> NewOpportunityContactRole = new List<OpportunityContactRole>();
        List<OpportunityTeamMember> NewOpportunityTeamMember = new List<OpportunityTeamMember>();
        List<Task> UpdateTaskWithRelatedOpportunity = new List<Task>();
        
        
        for(Task taskObj : FilteredTasks ) {
            
            //Contact associated to the task should be the primary contact on the opportunity
            String ContactId = String.valueOf(taskObj.WhoId);
            Id CurrentUserProfileId = UserInfo.getProfileId();
            
            if(ContactId.startsWith('003')){
                
                OpportunityContactRole NewOcr = New OpportunityContactRole();
                NewOcr.OpportunityId = newOpportunitiesCreatedMap.get(taskObj.id).id;
                NewOcr.ContactId = taskObj.WhoId;
                NewOcr.IsPrimary = true;
                NewOcr.Role = 'Evaluator';
                NewOpportunityContactRole.add(NewOcr);
                
                System.debug('#test > ' + profileInclusionList + ' > ' + CurrentUserProfileId );
                
                //Contact Role will only get created if user of inside sales profile is creating discovery call task
                if( profileInclusionList != null && profileInclusionList.contains( CurrentUserProfileId ) ){
                    
                    //Add User who created the task to Opportunity Team with Role = business development rep 
                    OpportunityTeamMember member = new OpportunityTeamMember();  
                    member.OpportunityId = newOpportunitiesCreatedMap.get(taskObj.id).id; 
                    member.UserId = UserInfo.getUserId();  //User who created the task
                    member.TeamMemberRole = 'Business Development Rep';  
                    member.OpportunityAccessLevel = 'Edit'; 
                    
                    NewOpportunityTeamMember.add(member);
                    
                    }
                
                    //Associate Meeting Task to New Created Opportuntiy
                    Task UpdateTask = new Task(id = taskObj.id, whatid = newOpportunitiesCreatedMap.get(taskObj.id).id );
                    UpdateTaskWithRelatedOpportunity.add(UpdateTask);
                    
            }
        }
        
        //New Opportunity Contact Role Insert
        if(NewOpportunityContactRole != null || NewOpportunityContactRole.isEmpty())
            insert NewOpportunityContactRole;
        
        //New Opportunity Team Member Insert
        if(NewOpportunityTeamMember != null || NewOpportunityTeamMember.isEmpty())
            insert NewOpportunityTeamMember;
        
        //Associate the meeting task to the opportunity Update
        if(UpdateTaskWithRelatedOpportunity != null || UpdateTaskWithRelatedOpportunity.isEmpty())
            update UpdateTaskWithRelatedOpportunity;
        
        
    }//End of Method
    
    
    
}