/** 
* @author CRMCulture
* @version 1.00
* @description Test functionality
*/
@isTest
public class TaskHelperTest {
    /**
    * @author CRMCulture
    * @version 1.00
    * @description Test Happy Path of code 
    */

    public static Id testUserId;
    
    //public static testMethod void testInsert() {  

    //  TestingUtility.TestData td = new TestingUtility.TestData(); 

        
    //  //InsertFutureUser.insertUser() testUser = new InsertFutureUser.insertUser();
    //  //System.debug('test User Id: '+InsertFutureUser.testUserId);

    //  Profile p = [SELECT Id FROM Profile WHERE Name='*Inside Sales']; 
    //  UserRole r = [SELECT Id FROM UserRole WHERE Name='Inside Sales'];
    //  User adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

    //  System.runAs(adminUser) {
 //         User testUser = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
 //           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
 //           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
 //           TimeZoneSidKey='America/Los_Angeles', UserName='testUser' + UserInfo.getOrganizationId() +  '@testorg.com');
 //     insert testUser;
 //     testUserId = testUser.Id;
    //  }

    //  User testUser = [SELECT Id FROM User WHERE Id = :testUserId];

    //  td.Leads[0].OwnerId = testUserId;
    //  update td.Leads[0];

    //  System.runAs(testUser) {
    //      Test.startTest();
                        
    //          List<Lead> leadsFromTD = [SELECT OwnerId FROM Lead WHERE Id IN :td.Leads];

    //          List<Task> tasksCreated = createTasks(200,50,td.Leads[0].Id,testUserId);
    //          List<Event> eventsCreated = createEvents(200,50,td.Leads[0].Id,testUserId);
    //          insert tasksCreated;
    //          insert eventsCreated;
    //      Test.stopTest();//will sync back up with async operations if future/batch called before this
    //  }
    //  List<Lead> leadActivityCount = [SELECT Activity_Count__c FROM Lead WHERE Id = :td.Leads[0].Id LIMIT 1];
    //  System.assertEquals(300,leadActivityCount[0].Activity_Count__c);


    //}
    /**
    * @author CRMCulture
    * @version 1.00
    * @description Test Exceptions To code 
    */
    //public static testMethod void testDeletion() {  
    //  //Construct most objects and DML in TestingUtility.TestData class
    //  TestingUtility.TestData td = new TestingUtility.TestData(); 
        
    //  Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    //  UserRole r = [SELECT Id FROM UserRole WHERE Name='Inside Sales'];
    //  User adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

    //  System.runAs(adminUser) {
 //         User testUser = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
 //           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
 //           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
 //           TimeZoneSidKey='America/Los_Angeles', UserName='testUser' + UserInfo.getOrganizationId() +  '@testorg.com');
 //     insert testUser;
 //     testUserId = testUser.Id;
    //  }

    //  td.Leads[0].OwnerId = testUserId;
    //  update td.Leads[0];

    //  User testUser = [SELECT Id FROM User WHERE Id = :testUserId];

    //  System.runAs(testUser) {
    //      Test.startTest();

    //          List<Lead> leadsFromTD = [SELECT OwnerId FROM Lead WHERE Id IN :td.Leads];

    //          List<Task> tasksCreated = createTasks(200,0,td.Leads[0].Id,testUserId);
    //          List<Event> eventsCreated = createEvents(200,0,td.Leads[0].Id,testUserId);
    //          insert tasksCreated;
    //          insert eventsCreated;

    //          List<Task> tasksToDelete = [SELECT Id FROM Task WHERE Id IN :tasksCreated LIMIT 50];
    //          List<Event> eventsToDelete = [SELECT Id FROM Event WHERE Id IN :eventsCreated LIMIT 50];
    //          delete tasksToDelete;
    //          delete eventsToDelete;
    //      Test.stopTest();//will sync back up with async operations if future/batch called before this
    //  }
    //  List<Lead> leadActivityCount = [SELECT Activity_Count__c FROM Lead WHERE Id = :td.Leads[0].Id LIMIT 1];
    //  System.assertEquals(300,leadActivityCount[0].Activity_Count__c);
    //}




    private static List<Task> createTasks(Integer numToCreate, Integer numNotCounted, Id leadId, Id ownerId) {

        List<Task> tasksToReturn = new List<Task>();
        
        for(Integer i = 0; i < (numToCreate-numNotCounted); i++) {
            Task t = new Task();
            t.Subject = 'Testing record '+i;
            t.OwnerId = ownerId;
            t.Priority = 'Normal';
            t.Status = 'Completed';
            t.WhoId = leadId;
            t.Type = 'Meeting';
            tasksToReturn.add(t);
        }

        for(Integer i = 0; i < numNotCounted; i++) {
            Task t = new Task();
            t.Subject = 'Testing record '+i;
            t.OwnerId = ownerId;
            t.Priority = 'Normal';
            t.Status = 'Completed';
            t.WhoId = leadId;
            t.Type = 'HubSpot Task';
            tasksToReturn.add(t);
        }

        return tasksToReturn;
    }




    private static List<Event> createEvents(Integer numToCreate, Integer numNotCounted, Id leadId, Id ownerId) {

        List<Event> eventsToReturn = new List<Event>();
        
        Date myDate = Date.newInstance(2011, 11, 18);
        Time myTime = Time.newInstance(3, 3, 3, 0);


        for(Integer i = 0; i < (numToCreate-numNotCounted); i++) {
            Event e = new Event();
            e.Subject = 'Testing record '+i;
            e.OwnerId = ownerId;
            e.WhoId = leadId;
            e.Type = 'Meeting';
            e.DurationInMinutes = 60;
            e.StartDateTime = DateTime.newInstance(myDate, myTime);
            eventsToReturn.add(e);
        }

        for(Integer i = 0; i < numNotCounted; i++) {
            Event e = new Event();
            e.Subject = 'Testing record '+i;
            e.OwnerId = ownerId;
            e.WhoId = leadId;
            e.Type = 'HubSpot Task';
            e.DurationInMinutes = 60;
            e.StartDateTime = DateTime.newInstance(myDate, myTime);
            eventsToReturn.add(e);
        }

        return eventsToReturn;
    }






    //public static testMethod void testOutreachDate() {

    //  TestingUtility.TestData td = new TestingUtility.TestData(); 

    //  Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    //  UserRole r = [SELECT Id FROM UserRole WHERE Name='Inside Sales'];
    //  User adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

    //  System.runAs(adminUser) {
 //         User testUser = new User(Alias = 'standt', Email='salesforce@crmculture.com', 
 //           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
 //           LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id,
 //           TimeZoneSidKey='America/Los_Angeles', UserName='testUser' + UserInfo.getOrganizationId() +  '@testorg.com');
 //     insert testUser;
 //     testUserId = testUser.Id;
    //  }

    //  td.Leads[0].OwnerId = testUserId;
    //  update td.Leads[0];


    //  User testUser = [SELECT Id FROM User WHERE Id = :testUserId];

    //  System.runAs(testUser) {

    //      Test.startTest();

    //          List<Task> tasksToInsert = new List<Task>();

    //          Task t = new Task();
    //              t.ActivityDate = Date.newInstance(2016, 8, 31);
    //              t.OwnerId = testUserId;
    //              t.Subject = 'Test Subject';
    //              t.Status = 'Completed';
    //              t.Priority = 'Normal';
    //              t.WhoId = td.Leads[0].Id;
    //          tasksToInsert.add(t);

    //          Task t2 = new Task();
    //              t2.ActivityDate = Date.newInstance(2016, 8, 2);
    //              t2.OwnerId = testUserId;
    //              t2.Subject = 'Test Subject';
    //              t2.Status = 'Completed';
    //              t2.Priority = 'Normal';
    //              t2.WhoId = td.Leads[0].Id;
    //          tasksToInsert.add(t2);

    //          Task t3 = new Task();
    //              t3.ActivityDate = Date.newInstance(2016, 7, 31);
    //              t3.OwnerId = testUserId;
    //              t3.Subject = 'Test Subject';
    //              t3.Status = 'Completed';
    //              t3.Priority = 'Normal';
    //              t3.Type = 'HubSpot Task';
    //              t3.WhoId = td.Leads[0].Id;
    //          tasksToInsert.add(t3);

    //          insert tasksToInsert;

    //          update tasksToInsert;

    //          List<Lead> updatedLeads = [SELECT First_Outreach_Date__c FROM Lead WHERE Id = :td.Leads[0].Id];

    //          System.assertEquals(Date.newInstance(2016, 8, 2), updatedLeads[0].First_Outreach_Date__c);

    //          Test.stopTest();
    //  }
    //}







    public static testMethod void testSetScopingCall() {

        //TestingUtility.TestData td = new TestingUtility.TestData();
        List<Opportunity> opps = new List<Opportunity>();
        for(Integer i = 0; i < 3; i++) {
            Opportunity o = new Opportunity();
            o.Name = 'test '+i;
            o.Amount = 100.00;
            o.StageName = ' Discovery';
            o.Forecast_Category__c = 'Upside';
            o.CloseDate = Date.newInstance(2030, 12, 10);
            opps.add(o);
        }

        insert opps;

        List<Task> tasksToInsert = new List<Task>();    

        Test.startTest();
        for(Integer i = 0; i < 200; i++) {
            Task t = new Task();
                t.Type = 'Scoping Call';
                t.Subject = 'Test Subject';
                t.WhatId = opps[0].Id;
                t.Status = 'Completed';
                t.Priority = 'Normal';
            tasksToInsert.add(t);
        }
        insert tasksToInsert;
        Test.stopTest();

        List<Opportunity> updatedOpp = [SELECT IsScoping_Call_Complete__c FROM Opportunity WHERE Id = :opps[0].Id];
        System.assertEquals(true, updatedOpp[0].IsScoping_Call_Complete__c);
    }

}