@isTest
public class EmailtoChangeRequestTest {
    
    static testMethod void testEmailRequestWithUser(){
        //Create user record for test requestor
        User u = new User();
        u.LastName = 'Test';
        u.Email = 'test@cherwell.com';
        u.IsActive = true;
        u.Alias = 'ttest';
        u.Username = 'test@cherwell.com';
        u.CommunityNickname = 'tester';
        u.ProfileId = [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id;
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        insert u;
        
        //Test with user record exists for email sender
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.binaryAttachment inAtt = new Messaging.InboundEmail.binaryAttachment();
        
        email.subject = 'test';
        email.plainTextBody = 'Testing email with more than two hundred and fifty five characters in the email body. '+
            'It should truncate the email’s first sentence, and then put the complete email in HTML format into the additional requirements field. '+
            'Let’s see if this works the way that it is intended.';
        env.fromAddress = 'test@cherwell.com';
        
        inAtt.body = blob.valueOf('test');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'plain/txt';
        
        email.binaryAttachments = new Messaging.InboundEmail.binaryAttachment[] {inAtt };
            
            EmailtoChangeRequest ccr = new EmailtoChangeRequest();
        ccr.handleInboundEmail(email, env);
        
        //Assert that change request was created with correct values
        SLX__Change_Control__c req = [Select Id, Submitted_via__c, SLX__Requestor__c
                                      From SLX__Change_Control__c
                                      Where Name = 'test'];
        List<SLX__Change_Control__c> reqs = [Select Id
                                             From SLX__Change_Control__c];
        User requestor = [Select Id, Name
                          From User
                          Where Alias = 'ttest'];
        System.assertEquals('Email', req.Submitted_via__c);
        System.assertEquals(req.SLX__Requestor__c, requestor.Id);
        System.debug(reqs.size());
    }
    
    static testMethod void testEmailRequestNoUser(){
        //Test with email sender does not exist as user in Salesforce
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        
        email2.subject = 'testing';
        email2.plainTextBody = 'Here is the email body.';
        env2.fromAddress = 'tester.noexist@cherwell.com';
        email2.fromName = 'Test Person';
        
        EmailtoChangeRequest ccr = new EmailtoChangeRequest();
        ccr.handleInboundEmail(email2, env2);
        
        //Assert that change request was created with correct values
        SLX__Change_Control__c req = [Select Id, Submitted_via__c, SLX__Requestor__c, Administrator_Notes__c
                                      From SLX__Change_Control__c
                                      Where Name = 'testing'];
        List<SLX__Change_Control__c> reqs = [Select Id
                                             From SLX__Change_Control__c];
        System.debug(req.Administrator_Notes__c);
        System.assertEquals('Email', req.Submitted_via__c);
        System.debug(reqs.size());
    }
    
    static testMethod void testLongSentenceandtextAttach(){
        Messaging.InboundEmail email3 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.textAttachment inAtt = new Messaging.InboundEmail.textAttachment();
        
        email3.subject = 'Test Number 3';
        email3.plainTextBody = 'Testing email with more than two hundred and fifty five characters in the email body and'+
            'as a run on sentence that does not have a period in it what it should do is truncate at two hundred characters'+
            'and then put the complete email in HTML format into the additional requirements field. '+
            'Let’s see if this works the way that it is intended.';
        env3.fromAddress = 'tester.noexist@cherwell.com';
        email3.fromName = 'Tester Testerson';
        
        inAtt.body = String.valueOf('test');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'plain/txt';
        
        email3.textAttachments = new Messaging.InboundEmail.textAttachment[] {inAtt };
        
        EmailtoChangeRequest ccr = new EmailtoChangeRequest();
        ccr.handleInboundEmail(email3, env3);
        
        //Assert that change request was created with correct values
        SLX__Change_Control__c req = [Select SLX__Description__c, Priority__c
                                      From SLX__Change_Control__c
                                      Where Name = 'Test Number 3'];
        System.debug(req.SLX__Description__c);
        //System.assertEquals('Undefined',req.Priority__c);
    }
}