trigger AccountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    TriggerFactory.createTriggerDispatcher(Account.sObjectType);
    
    if(trigger.IsBefore && trigger.isUpdate){
        //*****
        //Push Community Member Type Code to Contacts when applicable 
        //(for Customer Community user management)
        List<Account> theseAccounts = new List<Account>();
        for(Account a : trigger.new){
            if(trigger.oldMap.get(a.Id).Account_Status__c != trigger.newMap.get(a.Id).Account_Status__c ||
               trigger.oldMap.get(a.Id).RecordTypeId != trigger.newMap.get(a.Id).RecordTypeId){
                   theseAccounts.add(a);
               }
        }
        if(theseAccounts.size()>0){
            AccountTriggerHandler.updateCommunityMemberCode(theseAccounts);
        }
        
        //*****
        //When an Account is inactivated, send a notice to the LMS
        List<Id> updatedAccounts = new List<Id>(); 
        for(Account a : trigger.new){
            if(trigger.oldMap.get(a.Id).Account_Status__c != trigger.newMap.get(a.Id).Account_Status__c){
                updatedAccounts.add(a.Id);        
            }
        }
        if(updatedAccounts.size()>0){
            GnosisAPIController.passAccountStatusUpdates(updatedAccounts);
        }
        
        //*****
        //When an Account's relevant information is updated, send a notice to the LMS to update Learners
        List<Id> changedAccounts = new List<Id>(); 
        for(Account a : trigger.new){
            if(trigger.oldMap.get(a.Id).RecordTypeId != trigger.newMap.get(a.Id).RecordTypeId ||
               trigger.oldMap.get(a.Id).Partner_Types__c != trigger.newMap.get(a.Id).Partner_Types__c ||
               trigger.oldMap.get(a.Id).Name != trigger.newMap.get(a.Id).Name
              ){
                changedAccounts.add(a.Id);
            }
        }
        if(changedAccounts.size()>0){
            GnosisAPIController.passContactUpdates(changedAccounts, null);
        }
        
        //*****
        //For partner Accounts, check Account recruitment stage updates for creation of onboarding tasks
        //AccountTriggerHandler.BeforeUpdate(trigger.new);    
    }
    
    if(trigger.isBefore){
        If(trigger.isInsert)
        {
            //AccountTriggerHandler.BeforeInsert(Trigger.new);
            ValidateCountryStateNames.runValidationCheck(null, trigger.new, null);  
        }
        if(trigger.isUpdate){
            //Push updated industry/vertical values if appropriate
            //IndustryValueMapping.updateIndustry(trigger.new, null);
            
            List<Account> accountsToValidate = new List<Account>();
            for(Account c : trigger.new){
                if((c.Country_External_Source__c != null && c.Country_External_Source__c != trigger.oldMap.get(c.Id).Country_External_Source__c) ||
                   (c.State_External_Source__c != null && c.State_External_Source__c != trigger.oldMap.get(c.Id).State_External_Source__c)){
                       accountsToValidate.add(c);
                   }
            }
            if(accountsToValidate.size()>0){
                ValidateCountryStateNames.runValidationCheck(null, accountsToValidate, null);
            }
            
        }            
    }
    /*  if(trigger.isAfter){
if(trigger.isUpdate)
{
List<Account> updateAccounts = new List<Account>();
for(Account a : trigger.new){
if(trigger.oldMap.get(a.Id).RecruitmentStage__c != trigger.newMap.get(a.Id).RecruitmentStage__c){
updateAccounts.add(a);
}
}
if(updateAccounts.size()>0){
AccountTriggerHandler.createChecklistItems(updateAccounts);
}        
}
}   */
}