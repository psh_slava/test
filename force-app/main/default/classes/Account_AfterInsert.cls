/** 
* Account_AfterInsert  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Account_AfterInsert  extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @date 20140701
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    * 
    * @last modified: Naomi Harmon 11/2019 to include Central Sync
    */
    public override void mainEntry(TriggerParameters tp) {
        if (TriggerHelper.DoExecute('Account.CSMSync') && TriggerHelper.DoExecute('Outbound.CSMSync')) {
            CSMSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.newList);
        }
        if (TriggerHelper.DoExecute('Account.CentralSync') && TriggerHelper.DoExecute('Outbound.CSMSync')) {
            CentralSyncEntryHelper.enqueueSyncEntry(tp.oldMap, tp.newList);
        }
    }
}