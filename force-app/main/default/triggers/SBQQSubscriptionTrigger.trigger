/*
 * SBQQSubscriptionTrigger
 * @author: Naomi Harmon
 * @last modified: 11/2019
 * @related classes: SBQQSubscriptionTriggerHandler
 * @test class: SBQQSubscriptionTriggerTest
*/

trigger SBQQSubscriptionTrigger on SBQQ__Subscription__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {
        
        //Initiate sync with Central
        if(!system.isFuture()){ TriggerFactory.createTriggerDispatcher(SBQQ__Subscription__c.sObjectType); }
        
        SBQQSubscriptionTriggerHandler handler = new SBQQSubscriptionTriggerHandler();
        
        if(trigger.isBefore){
            
            if(trigger.isInsert){
                handler.onBeforeInsert(trigger.new);
            }
            
            if(trigger.isUpdate){
                handler.onBeforeUpdate(trigger.oldMap, trigger.newMap);
            }   
        }
    }