/*
	Excel File export class for Concur Integration - Output, Empoyees, Accounts, and Project files
	john.mccarvill@iconatg.com - 5/8/2018

 */
public with sharing class ICON_ConcurExportExcel_Controller {

	
	//string containing formatted IIF file contents 
	public String FileOutput {get; set;}
	public string FileName {get; set;}
	
	public string sType {get; set;}
	
	public string ExcelFileName {get; set;}
	public list<OutputWrapper> listWrapper {get; set;}
	public string sXMLHeader {get; set;}
	
	public ICON_ConcurExportExcel_Controller(){		

		//get type to process (Employees, Projects, Accounts)
		sType = ApexPages.currentPage().getParameters().get('Type');
		
	}
	
	public PageReference GenerateExport() {

		//get the file contents
		String sOutput = GenerateFileContents();
		
		
		//set the contents to the property
		this.FileOutput = sOutput;


		//remain on page - content-type should initiate download of text file 
		return(null);
	}
	
	
	
	public String GenerateFileContents() {
		
		//generate the text file records
		string sOutput = ''; 

		//init wrapper
		this.listWrapper = new list<OutputWrapper>();

		//set header
		sXMLHeader='<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
		
		//maps to organize output
		map<string,string> mapResources = new map<string,string>();
		map<string,string> mapProjects = new map<string,string>();
		map<string,string> mapAssignments = new map<string,string>();

		map<string,OutputWrapper> mapResourceWrapper = new map<string,OutputWrapper>();
		map<string,OutputWrapper> mapProjectWrapper = new map<string,OutputWrapper>();
		map<string,OutputWrapper> mapAssignmentWrapper = new map<string,OutputWrapper>();


		list<string> listResources = new list<string>(); 
		set<string> setProjectIds = new set<string>(); 
		set<string> setAssignmnetIds = new set<string>();

		//----------------------------------------------------------------------------------------------------------------
		if (sType=='All')
		{

			//header row
			string sLine = 'DELETE,NAME,LEVEL_01_CODE,LEVEL_02_CODE,LEVEL_03_CODE\r\n';
			sOutput +=  sLine; 
			
			
			//********************************************************* Employees
			this.FileName='Cherwell-PSAData.csv';
			this.ExcelFileName='Cherwell-PSAData.xls';
			
			list<Contact> listResourcesSource = [SELECT Id, Name, Email, Concur_Employee_ID__c 
											FROM Contact 
											WHERE pse__Is_Resource__c=true 
											AND pse__Is_Resource_Active__c=true
											AND Concur_Employee_ID__c!=null];
			
			for(Contact pContact:listResourcesSource)
			{
				//text line
				sLine = ',"' + pContact.Name + '", ' +  pContact.Concur_Employee_ID__c  +  ',,\r\n';
				mapResources.put(pContact.Id, sLine);
				
				//create object wrapper
				OutputWrapper pWrapper = new OutputWrapper();
				pWrapper.sNAME=pContact.Name;
				pWrapper.sLEVEL_01_CODE=pContact.Concur_Employee_ID__c;
				mapResourceWrapper.put(pContact.Id, pWrapper);
				
				//record for sorting				
				string sSortKey = pContact.Name + '%' + pContact.Id;
				listResources.add(sSortKey);
			}
			
			//sort by name
			listResources.sort();
			
			
			
			//********************************************************* Projects/Assignments
			list<pse__Assignment__c> listAssignmentsProj = [Select p.Name, p.pse__Assignment_Number__c, p.pse__Resource__r.Email, p.pse__Resource__r.Concur_Employee_ID__c, p.pse__Resource__r.Id, 
											p.pse__Resource__c, p.pse__Project__r.pse__Project_ID__c, p.pse__Project__r.pse__Is_Active__c, 
											p.pse__Project__r.pse__Account__c, p.pse__Project__c, p.Id, 
											p.pse__Project__r.pse__Account__r.Name, p.pse__Project__r.Name , p.pse__Closed_for_Expense_Entry__c
											From pse__Assignment__c p
											WHERE p.pse__Project__r.pse__Is_Active__c=true
											AND p.pse__Closed_for_Expense_Entry__c=false
											AND p.pse__Resource__r.Email!=null];
			
			for(pse__Assignment__c pAssignment:listAssignmentsProj)
			{
				
				//resource-project				
				sLine = ',"' + pAssignment.pse__Project__r.Name + '", ' + pAssignment.pse__Resource__r.Concur_Employee_ID__c + ',' + pAssignment.pse__Project__r.pse__Project_ID__c + ',\r\n';
				string sKey = pAssignment.pse__Resource__r.Id + '-' + pAssignment.pse__Project__r.Id;
				
				//record project id
				setProjectIds.add(pAssignment.pse__Project__r.Id);
				
				//put in map				
				mapProjects.put(sKey, sLine);
				
				//create object wrapper
				OutputWrapper pWrapper = new OutputWrapper();
				pWrapper.sNAME=pAssignment.pse__Project__r.Name;
				pWrapper.sLEVEL_01_CODE=pAssignment.pse__Resource__r.Concur_Employee_ID__c;
				pWrapper.sLEVEL_02_CODE=pAssignment.pse__Project__r.pse__Project_ID__c;
				mapProjectWrapper.put(sKey, pWrapper);
				
				
				//resource-project-assignment
				if (pAssignment.pse__Closed_for_Expense_Entry__c==false) 
				{
					sLine = ',"' + pAssignment.Name + '", ' + pAssignment.pse__Resource__r.Concur_Employee_ID__c + ',' + pAssignment.pse__Project__r.pse__Project_ID__c + ',' + pAssignment.pse__Assignment_Number__c + '\r\n';
					sKey = pAssignment.pse__Resource__r.Id + '-' + pAssignment.pse__Project__r.Id + '-' + pAssignment.Id;
					
					//record assignment id
					setAssignmnetIds.add(pAssignment.Id);
					
					//put in map
					mapAssignments.put(sKey, sLine);
					
					//create object wrapper
					pWrapper = new OutputWrapper();
					pWrapper.sNAME=pAssignment.Name;
					pWrapper.sLEVEL_01_CODE=pAssignment.pse__Resource__r.Concur_Employee_ID__c;
					pWrapper.sLEVEL_02_CODE=pAssignment.pse__Project__r.pse__Project_ID__c;
					pWrapper.sLEVEL_03_CODE=pAssignment.pse__Assignment_Number__c;
					mapAssignmentWrapper.put(sKey, pWrapper);
					
					
				}
			}

			//now iterate and output text to file
			
			for(string sResourceKey:listResources)
			{
				//get Id portion of sort key
				string sResourceId = sResourceKey.split('%')[1]; 
				
				//output resource
				sLine = mapResources.get(sResourceId);
				sOutput += sLine;
				
				//get wrapper object
				OutputWrapper pWrapper = mapResourceWrapper.get(sResourceId);
				listWrapper.add(pWrapper);
				
				//output each project for this resource
				for(string sProjectId:setProjectIds)
				{
					string sProjectKey = sResourceId + '-' + sProjectId;
					if(mapProjects.containsKey(sProjectKey))
					{
						sLine = mapProjects.get(sProjectKey);
						sOutput += sLine;

						//get wrapper object
						pWrapper = mapProjectWrapper.get(sProjectKey);
						listWrapper.add(pWrapper);


						//now output all assignments for resource-project
						for(string sAssignmentId:setAssignmnetIds)
						{
							string sAssignmentKey = sResourceId + '-' + sProjectId + '-' + sAssignmentId;
							if(mapAssignments.containsKey(sAssignmentKey))
							{
								sLine = mapAssignments.get(sAssignmentKey);
								sOutput += sLine;
								
								//get wrapper object
								pWrapper = mapAssignmentWrapper.get(sAssignmentKey);
								listWrapper.add(pWrapper);
								
							}
						}						
							
					} 
				}
				
			}
			
			
		}

		//return output
		return(sOutput);		
	}

	public class OutputWrapper
	{
		public string sDELETE {get; set;}
		public string sNAME {get; set;}
		public string sLEVEL_01_CODE {get; set;}
		public string sLEVEL_02_CODE {get; set;}
		public string sLEVEL_03_CODE {get; set;}
	}


}