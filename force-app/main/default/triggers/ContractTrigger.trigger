/*
 * ContractTrigger
 * @author: Naomi Harmon
 * @last modified: 10/2019
 * @related classes: ContractTriggerHandler
 * @test class: ContractTriggerTest
*/

trigger ContractTrigger on Contract (before insert, before update, after insert, after update) {
    
    ContractTriggerHandler handler = new ContractTriggerHandler();
    
    if(trigger.isAfter){
        if(Trigger.IsInsert){
            system.debug('Contract After Insert');
            handler.onAfterInsert(trigger.new);
        }
        if(Trigger.IsUpdate){
            system.debug('Contract After Update');
            handler.onAfterUpdate(trigger.oldMap, trigger.newMap);
        }
    }
    
    if(trigger.isBefore){
        if(Trigger.IsInsert){
            system.debug('Contract Before Insert');
            handler.onBeforeInsert(trigger.new);
        }
        if(Trigger.IsUpdate){
            system.debug('Contract Before Update');
            handler.onBeforeUpdate(trigger.oldMap, trigger.newMap);
        }
    }
}