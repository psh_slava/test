@isTest
public class ValidateCountryStateNamesTest {
    
    public static testmethod void testValidation(){
        
        Country_Conversion_Mappings__c ccm = new Country_Conversion_Mappings__c(Name = 'United States of America', 
                                                                                Alternate_Country_Name__c = 'United States of America',
                                                                                Standard_Country_Name__c = 'United States',
                                                                                Active__c = true);
        insert ccm;
        
        Account a = new Account(Name = 'VCSNT, LLC.', Country_External_Source__c = 'United States of America', State_External_Source__c = 'TX');
        insert a;
        
        Account a2 = new Account(Name = 'VCSNT Corporation', Country_External_Source__c = 'United States', State_External_Source__c = 'Texas');
        insert a2;
        
        Contact c = new Contact(AccountId = a.Id, LastName = 'Testerson', Email = 'test@vcsnt.test', Country_External_Source__c = 'United States', 
                               State_External_Source__c = 'Alaska');
        insert c;
        
        Contact c2 = new Contact(AccountId = a.Id, LastName = 'Testerson', Email = 'test3@vcsnt.test', Country_External_Source__c = 'United States of America', 
                               State_External_Source__c = 'AK');
        insert c2;
        
        Lead l = new Lead(LastName = 'Testing', Company = 'VCSNT LLC', Email = 'test2@vcsnt.test', Country_External_Source__c = 'United States',
                         State_External_Source__c = 'TN');
        insert l;
        
        Lead l2 = new Lead(LastName = 'Testing', Company = 'VCSNT LLC', Email = 'test4@vcsnt.test', Country_External_Source__c = 'United States of America',
                         State_External_Source__c = 'Connecticut');
        insert l2;
        
        List<Account> aList = [Select Id, Country_External_Source__c, BillingCountry, BillingState, State_External_Source__c From Account];
        List<Contact> cList = [Select Id, Country_External_Source__c, MailingCountry, MailingState, State_External_Source__c From Contact];
        List<Lead> lList = [Select Id, Country_External_Source__c, Country, State, State_External_Source__c From Lead]; 
        
        ValidateCountryStateNames.runValidationCheck(cList, aList, lList);
        
    }

}