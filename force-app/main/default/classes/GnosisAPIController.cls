/*
* @class name:   GnosisAPIController
* @created:      By Naomi Harmon in Oct 2020
* @test class:   GnosisAPIControllerTest.apxc
* @initiated by: AssetBulkTrigger.apxt
* @description: 
*       Client side stub for API with Gnosis LMS
* @modifcation log:
*                
*/

public class GnosisAPIController {
    
    @future(callout=true)
    public static void getPurchaseDetails(List<Id> assetIds){
        
        System.debug('Querying for Learning Services assets...');
        List<Asset> learningPurchases = [SELECT Id, CreatedDate, PurchaseDate, Expiry_Date__c, StockKeepingUnit, Product2Id, Price_USD__c,
                                         ContactId, Contact.FirstName, Contact.LastName, Contact.Email, Account.Customer_Success_Advocate__r.Email,
                                         AccountId, Account.Type, Account.Name, Account_Record_Type__c, Number_of_Credits__c, Account.Partner_Types__c,
                                         Source__c, Transaction_Id__c
                                         From Asset
                                         Where StockKeepingUnit != null
                                         And ProductFamily = 'Learning Services'
                                         And Id IN :assetIds];
        
        if(learningPurchases.size() > 0){
            List<Asset> processedAssets = new List<Asset>();
            for(Asset lp : learningPurchases){
                datetime myDateTime = datetime.now();   
                string mydtstring = mydatetime.format();         
                System.debug('Formatting parameters...'+ lp);
                String scope = GnosisAPIWrapper.formatPurchaseBody(lp);
                System.debug('Sending purchase details to API wrapper...');
                String result = GnosisAPIWrapper.sendPurchaseDetails(scope, lp);
                lp.lmsSyncStatus__c = mydtstring + ': ' + result.left(255);
                processedAssets.add(lp);
            }
            if(processedAssets.size()>0){
                update processedAssets;
            }
        }  
    }//End getPurchaseDetails
    
    @future(callout=true)
    public static void passStatusUpdates(List<Id> accountIds, List<Id> contactIds){
        if(contactIds != null){
            List<Contact> theseContacts = [Select Id, Status__c
                                           From Contact
                                           Where Id IN :contactIds];
            if(theseContacts != null){
                List<Contact> processedContacts = new List<Contact>();
                for(Contact c : theseContacts){
                    datetime myDateTime = datetime.now();   
                    string mydtstring = mydatetime.format();                       
                    System.debug('Formatting parameters...'+ c.Id);
                    String scope = GnosisAPIWrapper.formatStatusBody(c, c.Status__c);
                    System.debug('Sending Contact Inactive status to API Wrapper...');
                    String result = GnosisAPIWrapper.sendContactStatus(scope, c);
                    c.lmsSyncStatus__c = mydtstring + ': ' + result;
                    processedContacts.add(c);
                }
                if(processedContacts.size()>0){
                    update processedContacts;
                } 
            }
        }
    }//End passStatusUpdates
    
    @future(callout=true)
    public static void passAccountStatusUpdates(List<Id> accountIds){
        if(accountIds != null){
            List<Account> theseAccounts = [Select Id, Account_Status__c
                                           From Account
                                           Where Id IN :accountIds];
            if(theseAccounts != null){
                List<Account> processedAccounts = new List<Account>();
                for(Account a : theseAccounts){
                    datetime myDateTime = datetime.now();   
                    string mydtstring = mydatetime.format();                   
                    System.debug('Formatting parameters...'+ a.Id);
                    String scope = GnosisAPIWrapper.formatAccountStatusBody(a, a.Account_Status__c);
                    System.debug('Sending Account Inactive status to API Wrapper...');
                    String result  = GnosisAPIWrapper.sendAccountStatus(scope, a);
                    //a.lmsSyncStatus__c = mydtstring + ': ' + result;
                    processedAccounts.add(a);
                }
                if(processedAccounts.size()>0){
                    update processedAccounts;
                } 
            }
        }
    }//End passAccountStatusUpdate
    
    @future(callout=true)
    public static void passContactUpdates(List<Id> accountIds, List<Id> contactIds){       
        if(contactIds != null){
            List<Contact> theseContacts = [Select Id, Learner__c, Status__c, FirstName, LastName, Email, Account.Partner_Types__c, AccountId,
                                           Account_Record_Type__c, Account_Name__c
                                           From Contact
                                           Where Id IN :contactIds];
            if(theseContacts != null){
                List<Contact> processedContacts = new List<Contact>();
                for(Contact c : theseContacts){
                    datetime myDateTime = datetime.now();   
                    string mydtstring = mydatetime.format();
                    
                    System.debug('Formatting parameters...'+ c.Id);
                    String scope = GnosisAPIWrapper.formatContactDetailsBody(c);
                    System.debug('Sending Contact record changes to API Wrapper...');
                    String result = GnosisAPIWrapper.sendContactDetails(scope, c);
                    c.lmsSyncStatus__c = mydtstring + ': ' + result;
                    processedContacts.add(c);
                }
                if(processedContacts.size()>0){
                    update processedContacts;
                } 
            }
        }
    }//End passContactUpdates
}