public class ValidateCountryStateNames {
    
    public static void runValidationCheck(List<Contact> contacts, List<Account> accounts, List<Lead> leads){
        
        //Get all Standard country names
        Schema.DescribeFieldResult cfieldResult = User.Countrycode.getDescribe();
        List<Schema.PicklistEntry> cple = cfieldResult.getPicklistValues();
        List<String> countrynames = new List<String>();
        Map<String,String> countryCodeMap = new Map<String,String>();
        System.debug('Picklist::'+cple);
        for( Schema.PicklistEntry f : cple){
            System.debug(f.getLabel() +'::'+ f.getValue());
            countrynames.add(f.getLabel());
            countryCodeMap.put(f.getLabel(), f.getValue());
        }
        
        //Get all Standard state names
        Schema.DescribeFieldResult sfieldResult = User.statecode.getDescribe();
        List<Schema.PicklistEntry> sple = sfieldResult.getPicklistValues();
        List<String> statenames = new List<String>();
        List<String> statecodes = new List<String>();
        System.debug('Picklist::'+sple);
        for( Schema.PicklistEntry f : sple){
            System.debug(f.getLabel() +'::'+ f.getValue());
            statenames.add(f.getLabel());
            statecodes.add(f.getValue());
        }
        
        //Get all custom mappings
        List<Country_Conversion_Mappings__c> convList = [Select Alternate_Country_Name__c, Standard_Country_Name__c
                                                         From Country_Conversion_Mappings__c
                                                         Where Active__c = true];
        Map<String,String> convMap = new Map<String,String>();
        
        for(Country_Conversion_Mappings__c conv : convList){
            convMap.put(conv.Alternate_Country_Name__c, conv.Standard_Country_Name__c);
        }
        
        
        //Evaluate and update
        Boolean countryFound = false;
        if(contacts != null && contacts.size()> 0){
            for(Contact c : contacts){
                if(c.Country_External_Source__c != null){
                    System.debug('Country from external source :: ' + c.Country_External_Source__c);
                    if(countrynames.contains(c.Country_External_Source__c)){
                        System.debug('Country found.');
                        c.MailingCountry = c.Country_External_Source__c;
                        c.MailingCountryCode = countryCodeMap.get(c.Country_External_Source__c);
                        countryFound = true;
                    } else {
                        System.debug('Standard Country not found. Checking custom mappings...');
                        if(convMap.keySet().contains(c.Country_External_Source__c)){
                            System.debug('Custom mapping for Country found.');
                            c.MailingCountry = convMap.get(c.Country_External_Source__c);
                            c.MailingCountryCode = countryCodeMap.get(convMap.get(c.Country_External_Source__c));
                            countryFound = true;
                        } else {
                            System.debug('Custom Country mapping not found. No action being taken.');
                        }}}
                if(countryFound == true){
                    if(statenames.contains(c.State_External_Source__c)){
                        c.MailingState = c.State_External_Source__c;
                    } else if (statecodes.contains(c.State_External_Source__c)){
                        c.MailingStateCode = c.State_External_Source__c;
                    } else {
                        System.debug('Standard state not found.');
                    }}}
        }
        if(accounts != null && accounts.size()> 0){
            for(Account a : accounts){
                if(a.Country_External_Source__c != null){
                    System.debug('Country from external source :: ' + a.Country_External_Source__c);
                    if(countrynames.contains(a.Country_External_Source__c)){
                        System.debug('Country found.');
                        a.BillingCountry = a.Country_External_Source__c;
                        a.BillingCountryCode = countryCodeMap.get(a.Country_External_Source__c);
                        countryFound = true;
                    } else {
                        System.debug('Standard Country not found. Checking custom mappings...');
                        if(convMap.keySet().contains(a.Country_External_Source__c)){
                            System.debug('Custom mapping for Country found.');
                            a.BillingCountry = convMap.get(a.Country_External_Source__c);
                            a.BillingCountryCode = countryCodeMap.get(convMap.get(a.Country_External_Source__c));
                            countryFound = true;
                        } else {
                            System.debug('Custom Country mapping not found. No action being taken.');
                        }}}
                if(countryFound == true){
                    if(statenames.contains(a.State_External_Source__c)){
                        a.BillingState = a.State_External_Source__c;
                    } else if (statecodes.contains(a.State_External_Source__c)){
                        a.BillingStateCode = a.State_External_Source__c;
                    } else {
                        System.debug('Standard state not found.');
                    }}}
        }
        if(leads != null && leads.size()> 0){
            for(Lead l : leads){
                if(l.Country_External_Source__c != null){
                    System.debug('Country from external source :: ' + l.Country_External_Source__c);
                    if(countrynames.contains(l.Country_External_Source__c)){
                        System.debug('Country found.');
                        l.Country = l.Country_External_Source__c;
                        l.CountryCode = countryCodeMap.get(l.Country_External_Source__c);
                        countryFound = true;
                    } else {
                        System.debug('Standard Country not found. Checking custom mappings...');
                        if(convMap.keySet().contains(l.Country_External_Source__c)){
                            l.Country = convMap.get(l.Country_External_Source__c);
                            l.CountryCode = countryCodeMap.get(convMap.get(l.Country_External_Source__c));
                            countryFound = true;
                        } else {
                            System.debug('Custom Country mapping not found. No action being taken.');
                        }}}
                if(countryFound == true){
                    if(statenames.contains(l.State_External_Source__c)){
                        l.State = l.State_External_Source__c;
                    } else if (statecodes.contains(l.State_External_Source__c)){
                        l.StateCode = l.State_External_Source__c;
                    } else {
                        System.debug('Standard state not found.');
                    }}}
        }
    }
}