/**
* @author CRMCulture
* @version 1.00
* @description Interface for doing CSM custom transformations on top of what the generic mapping does.
*				The integration code will call these methods after the generic mapping logic
*				has been applied, but before the target object is committed. 
* NOTE: Although the objects are 'passed by reference', that is, any changes to their properties would
* stick and be visible to the caller, if the objects are reinstantiated, then the caller will not
* see the changes after reinstantiation, thus, the need for a return object to ensure that the caller
* will have access to all changes post transformation.
*
* IMPT: Do not commit the objects here.  The caller will handle saving.
*/
public interface ICSMCustomTransformationHandler {
	// Method to perform additional transformation from a Salesforce object to CSM.  
	CSMBusinessObjectRecordWrapper doAdditionalTransformFromSFtoCSM(SObject obj, CSMBusinessObjectRecordWrapper borw);
	
	// Method to perform additional transformation from a CSM object to Salesforce.
	SObject doAdditionalTransformFromCSMtoSF(CSMBusinessObjectRecordWrapper borw, SObject obj);
}