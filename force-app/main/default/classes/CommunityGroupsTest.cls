@isTest
public class CommunityGroupsTest {
    
    public static testmethod void testNewCGM(){
        Account a = new Account(Name = 'Test Account');
        insert a;
        
        Id employeeRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();

        Contact c = new Contact(LastName = 'Tester', AccountId = a.Id, Email = 'tester@test.test');
        Contact c2 = new Contact(LastName = 'TestManager', AccountId = a.Id, Email = 'testmanager@test.test', RecordTypeId = employeeRTId, Status__c = 'Active');
        List<Contact> cs = new List<Contact>();
        cs.add(c);
        cs.add(c2);
        insert cs;
        
        Community_Group__c cGroup = new Community_Group__c(Name = 'CUG: Brisbane');
        insert cGroup;
        
        Community_Group_Manager__c cGroupMgr = new Community_Group_Manager__c(Name = 'TestManager', Community_Group__c = cGroup.Id,
                                                                              Employee_Contact__c = c2.Id);
        insert cGroupMgr;
        
        Id groupRequestRTId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Community Group Application').getRecordTypeId();
        Request__c cgr = new Request__c(Community_Group__c = cGroup.Id, Email__c = 'tester@test.test', 
                                        RecordTypeId = groupRequestRTId, Contact__c = c.Id, Status__c = 'New');
        insert cgr;
        
        c = [Select Community_Groups__c From Contact Where LastName = 'Tester' Limit 1];
        System.assertEquals(c.Community_Groups__c, null);
        
        cgr.Status__c = 'Accepted';
        update cgr;
        
        c = [Select Community_Groups__c From Contact Where LastName = 'Tester' Limit 1];
        System.assertEquals(c.Community_Groups__c, 'CUG: Brisbane');
        
        c.Community_Groups__c = 'CUG: Brisbane;Beta';
        update c;
        
        Request__c cgr2 = new Request__c(Community_Group__c = cGroup.Id, RecordTypeId = groupRequestRTId, Contact__c = c.Id, Status__c = 'Accepted');
        insert cgr2;
        
        c = [Select Community_Groups__c From Contact Where LastName = 'Tester' Limit 1];
        System.assertEquals(c.Community_Groups__c, 'CUG: Brisbane;Beta');
        
    }

}