@isTest (SeeAllData=true)
public class PartnerDealApprovalFlowControllerTest {
    
    public static testmethod void ApprovalFlowControllerTest(){
        
        Lead l = new Lead(LastName = 'TestLead', Email = 'testemail!@test.test', Company = 'TestCompany');
        insert l;

        Request__c r = new Request__c();
        r.RecordTypeId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Partner Opportunity Registration').getRecordTypeId();
        r.Nature_of_Partner_Involvement_Here__c = 'Referral';
        r.Contact_Email__c = 'testemail!@test.test';
        insert r;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(r); 
        PartnerDealApprovalFlowController  obj = new PartnerDealApprovalFlowController(sc); 
        obj.testString = 'test1';
        obj.getNewLeadRecord();
        
        PageReference pageRef = Page.PartnerDealApprovalFlowPage;
        pageRef.getParameters().put('id', String.valueOf(r.Id));
        pageRef.getParameters().put('testString', 'test1');
        Test.setCurrentPage(pageRef);
    }
    
    public static testmethod void ApprovalFlowControllerTest2(){
        
        Lead l = new Lead(LastName = 'TestLead', Email = 'testemail!@test.test', Company = 'TestCompany');
        insert l;

        Request__c r = new Request__c();
        r.RecordTypeId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Partner Opportunity Registration').getRecordTypeId();
        r.Nature_of_Partner_Involvement_Here__c = 'Referral';
        r.Contact_Email__c = 'testemail!@test.test';
        insert r;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(r); 
        PartnerDealApprovalFlowController  obj = new PartnerDealApprovalFlowController(sc); 
        obj.testString = 'test3';
        obj.getNewLeadRecord();        
        
        PageReference pageRef = Page.PartnerDealApprovalFlowPage;
        pageRef.getParameters().put('id', String.valueOf(r.Id));
        Test.setCurrentPage(pageRef);       
    }
}