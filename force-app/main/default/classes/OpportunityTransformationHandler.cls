/**
* @author CRMCulture
* @version 1.00
* @description This Class 
*/
public with sharing class OpportunityTransformationHandler implements ICSMCustomTransformationHandler{
	
	public CSMBusinessObjectRecordWrapper doAdditionalTransformFromSFtoCSM(SObject obj, CSMBusinessObjectRecordWrapper borw) {
    	// put transform logic here
    	System.debug('DEBUG: OpportunityTransformationHandler obj' + obj);
 		System.debug('DEBUG: OpportunityTransformationHandler borw' + borw);
 		Opportunity o = (Opportunity)obj;
 		Map<String, Boolean> stageMap = new Map<String, Boolean>();
 		for(OpportunityStage os : [Select Id, MasterLabel, IsWon From OpportunityStage Where IsActive = true]){
 			stageMap.put(os.MasterLabel, os.IsWon);
 		}

 		if(String.isNotEmpty(o.StageName) && stageMap.containsKey(o.StageName)){
 			Integer ix = 0;
 			Integer ixFieldToRemove = -1;
			for(CSMBusinessObjectRecordWrapper.FieldWrapper fw : borw.fields){
				if(fw.name == 'ActualCloseDate') {
					if (stageMap.get(o.StageName)){
						fw.value = CSMFieldMappingHelper.setCSMFieldValue(obj, 'CloseDate', false);
					} else {
						fw.value = '';
					}
				} else if(fw.name == 'ProjectedCloseDate') {
					if (stageMap.get(o.StageName)){
						// If closed, we don't want to clear the value in CSM.  We just don't want
						// to update it so we'll remove the field from the fields list.
						ixFieldToRemove = ix;
					} else {
						fw.value = CSMFieldMappingHelper.setCSMFieldValue(obj, 'CloseDate', false);
					}
				}
				ix++;
 			}

 			if (ixFieldToRemove <> -1) {
 				borw.fields.remove(ixFieldToRemove);
 			}
 		}

    	return borw;
 	}
 
 	public SObject doAdditionalTransformFromCSMtoSF(CSMBusinessObjectRecordWrapper borw, SObject obj) {
   		List<CSMBusinessObjectRecordWrapper.FieldWrapper> fwList = borw.fields;
 		Opportunity o = (Opportunity)obj;
 		String actualValue = '';
 		String projectedValue = '';
 		for(CSMBusinessObjectRecordWrapper.FieldWrapper fw : fwList){
			if(fw.Name == 'ActualCloseDate'){
				actualValue = fw.value;
			} else if (fw.Name == 'ProjectedCloseDate') {
				projectedValue = fw.value;
			}
 		}

 		String value = String.isNotBlank(actualValue) ? actualValue : projectedValue;
		CSMFieldMappingHelper.setSFFieldValue(o, 'CloseDate', value, false);

   		return o;
 	}
}