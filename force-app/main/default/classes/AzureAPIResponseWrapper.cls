// Structure to hold the response data from a get group members request.  
public class AzureAPIResponseWrapper
{
        public cls_value[] value;
        public Boolean hasNextLink;
    
    public class cls_value {
        public String id;
        public String displayName;
        public Boolean accountEnabled;
        public String onPremisesDistinguishedName;
        public String givenName;
        public String jobTitle;
        public String mail;
        public String mobilePhone;
        public String department;
        public String surname;
        public List<String> businessPhones;
    }
    
    public static AzureAPIResponseWrapper parseJSON(string json){
        return (AzureAPIResponseWrapper)System.JSON.deserialize(json, AzureAPIResponseWrapper.class);
    } 
}