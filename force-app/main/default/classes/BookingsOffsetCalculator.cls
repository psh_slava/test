/*
* @class name:   BookingsOffsetCalculator
* @created:      By Naomi Harmon in Oct 2020
* @test class:   BookingsProcessorTest.apxc
* @initiated by: BookingsProcessor.apxc
* @description: 
*    Calculates offset amounts for Bookings (Invoicing and Renewal Opportunities) when there was an Upsell within a year of the bookings
* @modifcation log:
*
*/

public class BookingsOffsetCalculator {
    
    public static Decimal calculateOffset(Date startDate, Date endDate, Decimal pricePerYear){
        
        /*
* OFFSET Calculation =
* 1. Get the date difference in months
* 2. Start date plus difference in months
* 3. Date difference in days of date above and end date +1 (Salesforce calculates Today)
* 4. Value of day difference over days in a month as defined as (365/12)
* 5. Total month and day proration. If end date is at the end of the month and day difference equals that day, then add a full month.
* 6. Subtract 12 (months) from the total month and day proration. This is the offset proration.
* 7. Multiple the offset proration by Quantity and Net Unit Price
*/        
        Decimal offsetAmount;
        Decimal daysTillInvoice = startDate.daysBetween(endDate);  
        
        /* 1. Get the date difference in months */     
        //Decimal monthsDifference = subStart.monthsBetween(opp.CloseDate);
        Decimal totalDaysBetween = startDate.daysBetween(endDate.addDays(1))/(365.0/12.0);
        Long monthsDifference = totalDaysBetween.round(System.RoundingMode.DOWN);
        System.debug('Get the date difference in months :: ' + monthsDifference);
        
        /* 2. Start date plus difference in months */
        Date startPlusMonths = startDate.addMonths(Integer.valueOf(monthsDifference));
        System.debug('Start date plus difference in months :: ' + startPlusMonths);
        
        /* 3. Date difference in days of date above and end date (Salesforce calculates Today) */
        Decimal dayDifference = startPlusMonths.daysBetween(endDate.addDays(1));
        System.debug('Date difference in days of date above and end date :: ' + dayDifference);
        
        /* 4. Value of day difference over days in a month as defined as (365/12) */
        Decimal daysInaMonth = 365.0/12.0;
        System.debug('Days in a Month :: ' + daysInaMonth);
        Decimal dayDifferenceInMonths = dayDifference/daysInaMonth;
        System.debug('Value of day difference over days in a month as defined as (365/12) :: ' + dayDifferenceInMonths);
        
        /* 5. Total month and day proration. If end date is at the end of the month and day difference equals that day, then add a full month. */
        // IF (dayDifference = number of day in month of End Date) OR (day of Start Date - the day of End Date = 1), 
		// then add one full month to monthDifference, 
		// else add dayDifferenceInMonths to monthDifference
        Integer daysInEndMonth = Date.daysInMonth(endDate.year(), endDate.month());
        Decimal upsellProration;
        If(dayDifference == daysInEndMonth || startDate.day()-endDate.day() == 1){
            upsellProration = monthsDifference + 1;
        }
        Else {
            upsellProration = monthsDifference + dayDifferenceInMonths;
        }
        System.debug('Get total month and day proration. This is the upsell proration. :: ' + upsellProration);
        
        /* 6. Subtract 12 (months) from the total month and day proration. This is the offset proration. */
        Decimal offsetProration = upsellProration - 12;
        System.debug('Subtract 12 (months) from the total month and day proration. This is the offset proration. :: ' + offsetProration);
        
        /* 7. Multiple the offset proration by Quantity and Net Unit Price */
        offsetAmount = pricePerYear/12 * offsetProration;
        System.debug('Multiple the offset proration by Quantity and Net Unit Price. This is the offset amount. :: ' + offsetAmount);
        
        return offsetAmount;
    }
    
}