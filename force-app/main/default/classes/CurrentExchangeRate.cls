/*
* @class name:   CurrentExchangeRate
* @created:      By Naomi Harmon in September 2019
* @test class:   CurrentExchangeRateTest.apxc
* @initiated by: OpportunityTrigger.apxt
* @description: 
*    Updates Opportunities with their applicable DatedConversionRate into a custom Dated Exchange Rate field 
*    so that custom converted fields can be created and stored. Initially implemented so that a USD booking value could be passed to Anaplan via our API
* @modifcation log:
*    Naomi Harmon - May 2020 - Updated to make sure that the rate being applied to each Opportunity is based on that Opportunity's Close Date, 
*							   and added method return for batch job
*/

public class CurrentExchangeRate {
    
    public static void captureOnOppty(List<Opportunity> opps){
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        Date today = Date.today();
        
        Set<Date> rateDates = new Set<Date>();
        Date earliestDate = Date.today();
        for(Opportunity opp : opps){
            rateDates.add(opp.CloseDate);
            if( opp.CloseDate < earliestDate ){
                earliestDate = opp.CloseDate;
            }
        }
        
        System.debug(earliestDate);
        List<DatedConversionRate> currentRates = [Select Id, IsoCode, ConversionRate, StartDate, NextStartDate
                                                  From DatedConversionRate];
        
        Map<String, Decimal> currentRateMap = new Map<String, Decimal>();
        for(DatedConversionRate rate : currentRates){
            currentRateMap.put(rate.IsoCode, rate.ConversionRate);
        }
        System.debug('Current rate map ---- ' + currentRateMap);
        
        for(Opportunity opp: opps){
            Decimal thisCurrentRate;
            for(DatedConversionRate rate : currentRates){
                if(rate.IsoCode == opp.CurrencyIsoCode && (rate.StartDate <= opp.CloseDate && rate.NextStartDate > opp.CloseDate)){
                    thisCurrentRate = rate.ConversionRate;
                }
            }
            if(thisCurrentRate != null && opp.Dated_Exchange_Rate__c != thisCurrentRate) { 
                System.debug('Updating Opp ' + opp.Id + ' from ' + opp.Dated_Exchange_Rate__c + ' to ' + thisCurrentRate);
                opp.Dated_Exchange_Rate__c = thisCurrentRate; 
                oppsToUpdate.add(opp);
            }
        } 
    }
    
    public static List<Opportunity> processBatch(List<Opportunity> opps){
        
        captureOnOppty(opps);    
        return opps;      
    
    }
}