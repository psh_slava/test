@isTest
public class AccountRangesTest {
    
    public static testmethod void testSetRanges(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        CSM_License_Record__c csmKey = new CSM_License_Record__c();
        csmKey.Current_Key__c = 'Yes';
        csmKey.License_Type__c = 'CSM';
        csmKey.Account__c = a.Id;
        csmKey.Licensed_Items__c = 50;
        csmKey.Expires__c = Date.today().addDays(100);
        insert csmKey;
        
        a.CSM_License_Ct_from_Subscription__c = 50;
        a.CAM_License_Ct_from_Subscription__c = 1200;
        a.NumberOfEmployees = 502;
        a.CSMe_License_Ct_from_Subscription__c = 0;
        a.PPM_License_Ct_from_Subscription__c = 50;
        a.HRSM_License_Ct_from_Subscription__c = 50;
        update a;
        
        
    }

}