/*
	Test for Concur Integration
	
	Created By:	John McCarvill - john.mccarvill@iconatg.com - 4/23/2018
	

*/
@isTest
public with sharing class ICON_Concur_Tests {

	static testMethod void testExportController(){
	
		//create region
		pse__Region__c pRegion = new pse__Region__c(
			Name = 'Corporate Region', 		//text(80)
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Hierarchy_Depth__c = 0.0 		//number(2,0)
		);
		insert(pRegion);		

        
		//create group
		pse__Grp__c pGroup = new pse__Grp__c(
			Name = 'Test Group', 		//text(80)
			CurrencyIsoCode = 'USD' 		//picklist(3)
		);
		insert(pGroup);
        
		//create practice        
        pse__Practice__c pPractice = new pse__Practice__c(Name='Test');
        insert(pPractice); 

		//create permission control
		pse__Permission_Control__c pPermissionControl = new pse__Permission_Control__c(
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Billing__c = true, 		//boolean
			pse__Cascading_Permission__c = true, 		//boolean
			pse__Compare_Project_Version__c = true, 		//boolean
			pse__Create_Project_Version__c = true, 		//boolean
			pse__Delete_Project_Version__c = true, 		//boolean
			pse__Edit_Task_Manager__c = true, 		//boolean
			pse__Expense_Entry__c = true, 		//boolean
			pse__Expense_Ops_Edit__c = true, 		//boolean
			pse__Forecast_Edit__c = true, 		//boolean
			pse__Forecast_View__c = true, 		//boolean
			pse__Invoicing__c = true, 		//boolean
			pse__Region__c = pRegion.Id, 		//reference(pse__Region__c)
			pse__Resource_Request_Entry__c = true, 		//boolean
			pse__Skills_And_Certifications_Entry__c = true, 		//boolean
			pse__Skills_And_Certifications_View__c = true, 		//boolean
			pse__Staffing__c = true, 		//boolean
			pse__Team_Create__c = true, 		//boolean
			pse__Team_Edit__c = true, 		//boolean
			pse__Team_View__c = true, 		//boolean
			pse__Timecard_Entry__c = true, 		//boolean
			pse__Timecard_Ops_Edit__c = true, 		//boolean
			pse__User__c = UserInfo.getUserId(), 		//reference(User)
			pse__View_Task_Manager__c = true 		//boolean
		);
		insert(pPermissionControl);


    	//create test resource
    	Contact pContact = new Contact(
	    	Firstname = 'John',
	    	Lastname = 'Doe',
	    	Phone = '+1.416.588.9002',
	    	Email = 'xxyz@softchoice.com',
	    	pse__Is_Resource__c = true,
	    	pse__Is_Resource_Active__c = true,
	    	pse__Salesforce_User__c = UserInfo.getUserId(),
	    	pse__Region__c = pRegion.Id,
	    	Concur_Employee_ID__c='xxx'
	    );
    	insert(pContact);
    	

        //create account
	    Account pAccount = new Account(
	        Name = 'Test Account',
	        BillingCity = 'Concord',
	        BillingStreet = '1313 Mockingbird Lane',
	        BillingState = 'New Hampshire',
	        BillingCountry = 'United States'
	        
	    );
	    insert pAccount;
	
		//create a project
		pse__Proj__c pProject = new pse__Proj__c(
			Name = 'Timecard Test Project', 		//text(80)
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Account__c = pAccount.Id, 		//reference(Account)
			pse__Action_Count_Billing_Eligible_Records__c = false, 		//boolean
			pse__Action_Update_Current_Time_Period__c = false, 		//boolean
			pse__Allow_Expenses_Without_Assignment__c = false, 		//boolean
			pse__Allow_Self_Staffing__c = false, 		//boolean
			pse__Allow_Timecards_Without_Assignment__c = false, 		//boolean
			pse__Closed_for_Expense_Entry__c = false, 		//boolean
			pse__Closed_for_Time_Entry__c = false, 		//boolean
			pse__Copy_Child_Records_from_Template_Async__c = false, 		//boolean
			pse__Credited_Non_Billable_Internal_Hours__c = 0.0, 		//number(18,2)
			pse__Current_Time_Period_End_Date__c = system.today(), 		//date
			pse__Daily_Timecard_Notes_Required__c = false, 		//boolean
			pse__End_Date__c = system.today(), 		//date
			pse__Exclude_From_Billing__c = false, 		//boolean
			pse__Exclude_from_Backlog__c = false, 		//boolean
			pse__Exclude_from_Project_Planner__c = false, 		//boolean
			pse__Excluded_Hours__c = 0.0, 		//number(18,2)
			pse__Expense_Budget__c = 0.0, 		//currency(18,2)
			pse__Expense_Costs__c = 0.0, 		//currency(18,2)
			pse__External_Costs__c = 0.0, 		//currency(18,2)
			pse__External_Time_Cost__c = 0.0, 		//currency(18,2)
			pse__Group__c = pGroup.Id, 		//reference(pse__Grp__c)
			pse__Hierarchy_Depth__c = 0.0, 		//number(2,0)
			pse__Inactive_Project_Backlog__c = 0.0, 		//currency(18,2)
			pse__Include_In_Forecasting__c = false, 		//boolean
			pse__Internal_Budget__c = 0.0, 		//currency(18,2)
			pse__Internal_Costs__c = 0.0, 		//currency(18,2)
			pse__Internal_Time_Cost__c = 0.0, 		//currency(18,2)
			pse__Invoiced__c = 0.0, 		//currency(18,2)
			pse__Is_Active__c = true, 		//boolean
			pse__Is_Billable__c = true, 		//boolean
			pse__Is_Template__c = false, 		//boolean
			pse__Milestone_Cost__c = 0.0, 		//currency(18,2)
			pse__Non_Billable_External_Hours__c = 0.0, 		//number(18,2)
			pse__Non_Billable_Internal_Hours__c = 0.0, 		//number(18,2)
			pse__Notes__c = 'TEST PROJECT FOR INTERNAL TESTING ONLY', 		//textarea(255)
			pse__Other_Costs__c = 1000.0, 		//currency(18,2)
			pse__Pass_Through_Billings__c = 0.0, 		//currency(18,2)
			pse__Practice__c = pPractice.Id, 		//reference(pse__Practice__c)
			pse__Pre_Bill_Type__c = 'None', 		//picklist(255)
			pse__Pre_Billed__c = 1000.0, 		//currency(18,2)
			pse__Project_Manager__c = pContact.Id, 		//reference(Contact)
			pse__Project_Name_Chain__c = 'Hello World', 		//text(255)
			pse__Project_Type__c = 'Internal Project', 		//picklist(255)
			pse__Region__c = pRegion.Id, 		//reference(pse__Region__c)
			pse__Requires_Dependency_Sync__c = false, 		//boolean
			pse__Revenue__c = 0.0, 		//currency(18,2)
			pse__Scheduled_Milestone__c = 0.0, 		//currency(18,2)
			pse__Scheduled_Time__c = 0.0, 		//currency(18,2)
			pse__Share_with_Project_Manager__c = false, 		//boolean
			pse__Share_with_Project_Resources__c = false, 		//boolean
			pse__Stage__c = '02.Project Execution', 		//picklist(255)
			pse__Start_Date__c = system.today(), 		//date
			pse__Tasks_Total_Points_Complete__c = 0.0, 		//number(11,2)
			pse__Tasks_Total_Points__c = 0.0, 		//number(11,2)
			pse__Time_Credited__c = false, 		//boolean
			pse__Time_Excluded__c = false 		//boolean
		);
		insert(pProject);        
    	
	    	
    	//create schedule
		pse__Schedule__c pSchedule = new pse__Schedule__c(
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Action_Force_Schedule_Refresh__c = false, 		//boolean
			pse__Action_Update_Future_Scheduled_Hours__c = false, 		//boolean
			pse__End_Date__c = system.today().addDays(10), 		//date
			pse__Friday_Hours__c = 8.0, 		//number(4,2)
			pse__Monday_Hours__c = 8.0, 		//number(4,2)
			pse__Saturday_Hours__c = 0.0, 		//number(4,2)
			pse__Scheduled_Days__c = 17.0, 		//number(18,6)
			pse__Start_Date__c = system.today(), 		//date
			pse__Sunday_Hours__c = 0.0, 		//number(4,2)
			pse__Thursday_Hours__c = 8.0, 		//number(4,2)
			pse__Tuesday_Hours__c = 8.0, 		//number(4,2)
			pse__Wednesday_Hours__c = 8.0 		//number(4,2)
		);
		insert(pSchedule);


		pse__Schedule_Exception__c pScheduleException = new pse__Schedule_Exception__c(
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Schedule__c = pSchedule.Id, 		//reference(pse__Schedule__c)
			pse__Date__c = system.today(), 		//date
			pse__End_Date__c = system.today().addDays(6), 		//date
			pse__Exception_Hours__c = 0.0, 		//number(6,2)
			pse__Friday_Hours__c = 8.0, 		//number(4,2)
			pse__Monday_Hours__c = 8.0, 		//number(4,2)
			pse__Saturday_Hours__c = 0.0, 		//number(4,2)
			pse__Sunday_Hours__c = 0.0, 		//number(4,2)
			pse__Thursday_Hours__c = 8.0, 		//number(4,2)
			pse__Tuesday_Hours__c = 8.0, 		//number(4,2)
			pse__Wednesday_Hours__c = 8.0 		//number(4,2)
		);
		insert(pScheduleException);


    	//create assignment
		pse__Assignment__c pAssignment = new pse__Assignment__c(
			Name = 'Test Assignment', 		//text(80)
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Bill_Rate__c = 20.0, 		//currency(18,4)
			pse__Closed_for_Expense_Entry__c = false, 		//boolean
			pse__Closed_for_Time_Entry__c = false, 		//boolean
			pse__Cost_Rate_Amount__c = 10.0, 		//number(18,2)
			pse__Cost_Rate_Currency_Code__c = 'USD', 		//text(3)
			pse__Daily_Bill_Rate__c = false, 		//boolean
			pse__Daily_Cost_Rate__c = false, 		//boolean
			pse__Daily_Timecard_Notes_Required__c = false, 		//boolean
			pse__Eligible_for_Schedule_Recalculation__c = false, 		//boolean
			pse__Exclude_from_Billing__c = false, 		//boolean
			pse__Exclude_from_Planners__c = false, 		//boolean
			pse__Is_Billable__c = true, 		//boolean
			pse__Nick_Name__c = 'Test Task', 		//text(255)
			pse__Project__c = pProject.Id, 		//reference(pse__Proj__c)
			pse__Resource__c = pContact.Id, 		//reference(Contact)
			pse__Role__c = 'Compliance Delivery - VP', 		//picklist(255)
			pse__Schedule_Updated__c = false, 		//boolean
			pse__Schedule__c = pSchedule.Id, 		//reference(pse__Schedule__c)
			pse__Status__c = 'Tentative', 		//picklist(255)
			pse__Planned_Bill_Rate__c = 20.0
		);
		insert(pAssignment);
	    	
		test.startTest();
				
		
		//run test on expense cover sheet pdf - set parameters
        PageReference pageRef = Page.ICON_ConcurExport;
        Test.setCurrentPage(pageRef);

        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('Type', 'All');

		//get controller		
		ICON_ConcurExport_Controller pExport = new ICON_ConcurExport_Controller();
		pExport.GenerateExport();
		
		test.stopTest();
		
	}


	static testMethod void testExportExcelController(){
	
		//create region
		pse__Region__c pRegion = new pse__Region__c(
			Name = 'Corporate Region', 		//text(80)
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Hierarchy_Depth__c = 0.0 		//number(2,0)
		);
		insert(pRegion);		

        
		//create group
		pse__Grp__c pGroup = new pse__Grp__c(
			Name = 'Test Group', 		//text(80)
			CurrencyIsoCode = 'USD' 		//picklist(3)
		);
		insert(pGroup);
        
		//create practice        
        pse__Practice__c pPractice = new pse__Practice__c(Name='Test');
        insert(pPractice); 

		//create permission control
		pse__Permission_Control__c pPermissionControl = new pse__Permission_Control__c(
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Billing__c = true, 		//boolean
			pse__Cascading_Permission__c = true, 		//boolean
			pse__Compare_Project_Version__c = true, 		//boolean
			pse__Create_Project_Version__c = true, 		//boolean
			pse__Delete_Project_Version__c = true, 		//boolean
			pse__Edit_Task_Manager__c = true, 		//boolean
			pse__Expense_Entry__c = true, 		//boolean
			pse__Expense_Ops_Edit__c = true, 		//boolean
			pse__Forecast_Edit__c = true, 		//boolean
			pse__Forecast_View__c = true, 		//boolean
			pse__Invoicing__c = true, 		//boolean
			pse__Region__c = pRegion.Id, 		//reference(pse__Region__c)
			pse__Resource_Request_Entry__c = true, 		//boolean
			pse__Skills_And_Certifications_Entry__c = true, 		//boolean
			pse__Skills_And_Certifications_View__c = true, 		//boolean
			pse__Staffing__c = true, 		//boolean
			pse__Team_Create__c = true, 		//boolean
			pse__Team_Edit__c = true, 		//boolean
			pse__Team_View__c = true, 		//boolean
			pse__Timecard_Entry__c = true, 		//boolean
			pse__Timecard_Ops_Edit__c = true, 		//boolean
			pse__User__c = UserInfo.getUserId(), 		//reference(User)
			pse__View_Task_Manager__c = true 		//boolean
		);
		insert(pPermissionControl);


    	//create test resource
    	Contact pContact = new Contact(
	    	Firstname = 'John',
	    	Lastname = 'Doe',
	    	Phone = '+1.416.588.9002',
	    	Email = 'xxyz@softchoice.com',
	    	pse__Is_Resource__c = true,
	    	pse__Is_Resource_Active__c = true,
	    	pse__Salesforce_User__c = UserInfo.getUserId(),
	    	pse__Region__c = pRegion.Id,
	    	Concur_Employee_ID__c='xxx'
	    );
    	insert(pContact);
    	

        //create account
	    Account pAccount = new Account(
	        Name = 'Test Account',
	        BillingCity = 'Concord',
	        BillingStreet = '1313 Mockingbird Lane',
	        BillingState = 'New Hampshire',
	        BillingCountry = 'United States'
	        
	    );
	    insert pAccount;
	
		//create a project
		pse__Proj__c pProject = new pse__Proj__c(
			Name = 'Timecard Test Project', 		//text(80)
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Account__c = pAccount.Id, 		//reference(Account)
			pse__Action_Count_Billing_Eligible_Records__c = false, 		//boolean
			pse__Action_Update_Current_Time_Period__c = false, 		//boolean
			pse__Allow_Expenses_Without_Assignment__c = false, 		//boolean
			pse__Allow_Self_Staffing__c = false, 		//boolean
			pse__Allow_Timecards_Without_Assignment__c = false, 		//boolean
			pse__Closed_for_Expense_Entry__c = false, 		//boolean
			pse__Closed_for_Time_Entry__c = false, 		//boolean
			pse__Copy_Child_Records_from_Template_Async__c = false, 		//boolean
			pse__Credited_Non_Billable_Internal_Hours__c = 0.0, 		//number(18,2)
			pse__Current_Time_Period_End_Date__c = system.today(), 		//date
			pse__Daily_Timecard_Notes_Required__c = false, 		//boolean
			pse__End_Date__c = system.today(), 		//date
			pse__Exclude_From_Billing__c = false, 		//boolean
			pse__Exclude_from_Backlog__c = false, 		//boolean
			pse__Exclude_from_Project_Planner__c = false, 		//boolean
			pse__Excluded_Hours__c = 0.0, 		//number(18,2)
			pse__Expense_Budget__c = 0.0, 		//currency(18,2)
			pse__Expense_Costs__c = 0.0, 		//currency(18,2)
			pse__External_Costs__c = 0.0, 		//currency(18,2)
			pse__External_Time_Cost__c = 0.0, 		//currency(18,2)
			pse__Group__c = pGroup.Id, 		//reference(pse__Grp__c)
			pse__Hierarchy_Depth__c = 0.0, 		//number(2,0)
			pse__Inactive_Project_Backlog__c = 0.0, 		//currency(18,2)
			pse__Include_In_Forecasting__c = false, 		//boolean
			pse__Internal_Budget__c = 0.0, 		//currency(18,2)
			pse__Internal_Costs__c = 0.0, 		//currency(18,2)
			pse__Internal_Time_Cost__c = 0.0, 		//currency(18,2)
			pse__Invoiced__c = 0.0, 		//currency(18,2)
			pse__Is_Active__c = true, 		//boolean
			pse__Is_Billable__c = true, 		//boolean
			pse__Is_Template__c = false, 		//boolean
			pse__Milestone_Cost__c = 0.0, 		//currency(18,2)
			pse__Non_Billable_External_Hours__c = 0.0, 		//number(18,2)
			pse__Non_Billable_Internal_Hours__c = 0.0, 		//number(18,2)
			pse__Notes__c = 'TEST PROJECT FOR INTERNAL TESTING ONLY', 		//textarea(255)
			pse__Other_Costs__c = 1000.0, 		//currency(18,2)
			pse__Pass_Through_Billings__c = 0.0, 		//currency(18,2)
			pse__Practice__c = pPractice.Id, 		//reference(pse__Practice__c)
			pse__Pre_Bill_Type__c = 'None', 		//picklist(255)
			pse__Pre_Billed__c = 1000.0, 		//currency(18,2)
			pse__Project_Manager__c = pContact.Id, 		//reference(Contact)
			pse__Project_Name_Chain__c = 'Hello World', 		//text(255)
			pse__Project_Type__c = 'Internal Project', 		//picklist(255)
			pse__Region__c = pRegion.Id, 		//reference(pse__Region__c)
			pse__Requires_Dependency_Sync__c = false, 		//boolean
			pse__Revenue__c = 0.0, 		//currency(18,2)
			pse__Scheduled_Milestone__c = 0.0, 		//currency(18,2)
			pse__Scheduled_Time__c = 0.0, 		//currency(18,2)
			pse__Share_with_Project_Manager__c = false, 		//boolean
			pse__Share_with_Project_Resources__c = false, 		//boolean
			pse__Stage__c = '02.Project Execution', 		//picklist(255)
			pse__Start_Date__c = system.today(), 		//date
			pse__Tasks_Total_Points_Complete__c = 0.0, 		//number(11,2)
			pse__Tasks_Total_Points__c = 0.0, 		//number(11,2)
			pse__Time_Credited__c = false, 		//boolean
			pse__Time_Excluded__c = false 		//boolean
		);
		insert(pProject);        
    	
	    	
    	//create schedule
		pse__Schedule__c pSchedule = new pse__Schedule__c(
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Action_Force_Schedule_Refresh__c = false, 		//boolean
			pse__Action_Update_Future_Scheduled_Hours__c = false, 		//boolean
			pse__End_Date__c = system.today().addDays(10), 		//date
			pse__Friday_Hours__c = 8.0, 		//number(4,2)
			pse__Monday_Hours__c = 8.0, 		//number(4,2)
			pse__Saturday_Hours__c = 0.0, 		//number(4,2)
			pse__Scheduled_Days__c = 17.0, 		//number(18,6)
			pse__Start_Date__c = system.today(), 		//date
			pse__Sunday_Hours__c = 0.0, 		//number(4,2)
			pse__Thursday_Hours__c = 8.0, 		//number(4,2)
			pse__Tuesday_Hours__c = 8.0, 		//number(4,2)
			pse__Wednesday_Hours__c = 8.0 		//number(4,2)
		);
		insert(pSchedule);


		pse__Schedule_Exception__c pScheduleException = new pse__Schedule_Exception__c(
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Schedule__c = pSchedule.Id, 		//reference(pse__Schedule__c)
			pse__Date__c = system.today(), 		//date
			pse__End_Date__c = system.today().addDays(6), 		//date
			pse__Exception_Hours__c = 0.0, 		//number(6,2)
			pse__Friday_Hours__c = 8.0, 		//number(4,2)
			pse__Monday_Hours__c = 8.0, 		//number(4,2)
			pse__Saturday_Hours__c = 0.0, 		//number(4,2)
			pse__Sunday_Hours__c = 0.0, 		//number(4,2)
			pse__Thursday_Hours__c = 8.0, 		//number(4,2)
			pse__Tuesday_Hours__c = 8.0, 		//number(4,2)
			pse__Wednesday_Hours__c = 8.0 		//number(4,2)
		);
		insert(pScheduleException);


    	//create assignment
		pse__Assignment__c pAssignment = new pse__Assignment__c(
			Name = 'Test Assignment', 		//text(80)
			CurrencyIsoCode = 'USD', 		//picklist(3)
			pse__Bill_Rate__c = 20.0, 		//currency(18,4)
			pse__Closed_for_Expense_Entry__c = false, 		//boolean
			pse__Closed_for_Time_Entry__c = false, 		//boolean
			pse__Cost_Rate_Amount__c = 10.0, 		//number(18,2)
			pse__Cost_Rate_Currency_Code__c = 'USD', 		//text(3)
			pse__Daily_Bill_Rate__c = false, 		//boolean
			pse__Daily_Cost_Rate__c = false, 		//boolean
			pse__Daily_Timecard_Notes_Required__c = false, 		//boolean
			pse__Eligible_for_Schedule_Recalculation__c = false, 		//boolean
			pse__Exclude_from_Billing__c = false, 		//boolean
			pse__Exclude_from_Planners__c = false, 		//boolean
			pse__Is_Billable__c = true, 		//boolean
			pse__Nick_Name__c = 'Test Task', 		//text(255)
			pse__Project__c = pProject.Id, 		//reference(pse__Proj__c)
			pse__Resource__c = pContact.Id, 		//reference(Contact)
			pse__Role__c = 'Compliance Delivery - VP', 		//picklist(255)
			pse__Schedule_Updated__c = false, 		//boolean
			pse__Schedule__c = pSchedule.Id, 		//reference(pse__Schedule__c)
			pse__Status__c = 'Tentative', 		//picklist(255)
			pse__Planned_Bill_Rate__c = 20.0
			
		);
		insert(pAssignment);
	    	
		test.startTest();
				
		
		//run test on expense cover sheet pdf - set parameters
        PageReference pageRef = Page.ICON_ConcurExportExcel;
        Test.setCurrentPage(pageRef);

        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('Type', 'All');

		//get controller		
		ICON_ConcurExportExcel_Controller pExport = new ICON_ConcurExportExcel_Controller();
		pExport.GenerateExport();
		
		test.stopTest();
		
	}
	
}