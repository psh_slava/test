@isTest

public class LimitDiscoverOrgContactsTest {
    
    public testMethod static void testContactImport(){
        //create the custom setting
        DiscoverOrg_Contacts_Limit__c batchSettings = new DiscoverOrg_Contacts_Limit__c(
            Name = 'Default',
            Daily_Limit__c = 100);
        insert batchSettings;
        
        Account acct = new Account(name = 'test account');
        insert acct;
        
        User u = [Select Id From User Where Name = 'Naomi Harmon'];
        
        List<Contact> cImport = new List<Contact>();
        for(Integer i = 0; i<100; i++){
            Contact c = new Contact();
            c.AccountId = acct.Id;
            c.lastName = 'Tester'+ i;
            c.DSCORGPKG__DiscoverOrg_Created_On__c = DateTime.NOW();
            c.Email = 'tester'+i+'@cherwell.test';
            c.OwnerId = u.Id;
            cImport.add(c);
        }
        insert cImport;
        
        List<Contact> inserts = [Select Id
                                  From Contact];
        System.debug('Contacts successfully inserted - - '+inserts.size());
        
        Contact c101 = new Contact(AccountId = acct.Id, lastName = 'test', 
                                  DSCORGPKG__DiscoverOrg_Created_On__c = DateTime.NOW(),
                                  email = 'lasttest@cherwell.com');
        try{
            insert c101;
        } catch (DMLException e){
            System.debug('The Contact could not be inserted: ' + e.getMessage());
        }

    }
}