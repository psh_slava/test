global class CSMSyncEntryProcessorBatch implements Database.Batchable<CSM_Sync_Entry__c>, Database.AllowsCallouts {
	
	global Iterable<CSM_Sync_Entry__c> start(Database.BatchableContext BC) {
		return CSMSyncEntryHelper.dequeueSyncEntry();
	}

   	global void execute(Database.BatchableContext BC, List<CSM_Sync_Entry__c> scope) {
   		// processSyncEntry makes a callout which causes an 'Uncommitted data...' exception
   		// when running in test mode.  
   		if (!Test.isRunningTest()) {
			CSMSyncEntryHelper.processSyncEntry(scope[0]);
   		}
	}
	
	global void finish(Database.BatchableContext BC) {
		List<CSM_Sync_Entry__c> syncEntries = [SELECT Id FROM CSM_Sync_Entry__c LIMIT 1];
		if (!syncEntries.isEmpty()) {
    		CSMSyncEntryHelper.enqueueSyncEntryProcessor();
		}
	}
	
}