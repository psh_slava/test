public class PartnerDealApprovalFlowController {
	
	public Flow.Interview.Approve_Partner_Opp_Registration theFlow {get; set;}
    public Request__c req;
    public String testString {get; set;}

    public PartnerDealApprovalFlowController(ApexPages.StandardController sc) {
        this.req = (Request__c)sc.getRecord();
    }
		
	public PageReference getNewLeadRecord(){

        if(theFlow == null && !Test.isRunningTest()){
			return null;
        }
        if((!Test.isRunningTest() && theFlow.varNewLeadId == null) || (Test.isRunningTest() && testString == 'test1')){
            return new PageReference('/' + req.Id);
        }
        if((!Test.isRunningTest() && theFlow != null && theFlow.varNewLeadId != null) || (Test.isRunningTest() && testString == 'test2')){
            return new PageReference('/lead/leadconvert.jsp?id=' + theFlow.varNewLeadId);
        }
        else return null;
	}

}