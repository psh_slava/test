public class QuoteLineTriggerHandler {
    
    public static void identifySaleType(SBQQ__QuoteLine__c[] lines){
        
        List<Id> accountIds = new List<Id>();
        for(SBQQ__QuoteLine__c ql : lines){
            accountIds.add(ql.Account_Id__c);
        }
        
        Map<Id, List<String>> subMap = new Map<Id,List<String>>();
        
        List<SBQQ__Subscription__c> subs = [Select SBQQ__Account__c, SBQQ__Product__c
                                            From SBQQ__Subscription__c
                                            Where SBQQ__Account__c IN :accountIds];
        //And Active__c = true];
        
        List<Asset> assetList = [Select AccountId, Product2Id
                                 From Asset
                                 Where AccountId IN :accountIds];
        
        for(SBQQ__Subscription__c sub : subs){ 
            //check if map contains account Id or not. If not then put into map
            if(!subMap.containsKey(sub.SBQQ__Account__c)){
                subMap.put(sub.SBQQ__Account__c, new List<String>());
            }
            //now add product names
            subMap.get(sub.SBQQ__Account__c).add(sub.SBQQ__Product__c);
        }
        
        for(Asset a : assetList){
            if(!subMap.containsKey(a.AccountId)){
                subMap.put(a.AccountId, new List<String>());
            }
            subMap.get(a.AccountId).add(a.Product2Id);
        }
        
        System.debug(submap);
        
        for(SBQQ__QuoteLine__c ql : lines){
            System.debug('Quote line Product = ' + ql.SBQQ__Product__c);
            //  if(ql.Sale_Type__c == null){              
            if(ql.Opportunity_Record_Type__c == 'New Logo'){
                ql.Sale_Type__c = 'New Logo';
            }
            
            else {
                //line is upsell if the line is linked to an original, upgraded subscription line
                if(ql.SBQQ__UpgradedSubscription__c != null){
                    ql.Sale_Type__c = 'Upsell';
                }
                else if(ql.SBQQ__RenewedSubscription__c != null){
                    ql.Sale_Type__c = 'Renewal';
                }
                
                //in case the upsell is not done via Amend on the Contract, or a new line of same product is added at a different price,
                //then upsell if a subscription already exists of that product on the Account
                else if(subMap.get(ql.Account_Id__c) != null){
                    if(subMap.get(ql.Account_Id__c).contains(ql.SBQQ__Product__c)){
                        System.debug('Product found in map.');
                        // For perpetual licenses (assets)...
                        if(ql.Opportunity_Record_Type__c == 'Renewal' && ql.License_Model__c == 'Perpetual'){
                            ql.Sale_Type__c = 'Renewal';
                        } else { 
                            ql.Sale_Type__c = 'Upsell';
                        }
                    }
                    if(!subMap.get(ql.Account_Id__c).contains(ql.SBQQ__Product__c)){
                        System.debug('Product not found in map.');
                        ql.Sale_Type__c = 'Cross-sell';
                    }
                }
                //cross-sell if there is no existing subscription of that product on the Account
                else if(subMap.get(ql.Account_Id__c) == null){
                    System.debug('Product not found in map.');
                    ql.Sale_Type__c = 'Cross-sell';
                }    
            }         
        }               
    }
}