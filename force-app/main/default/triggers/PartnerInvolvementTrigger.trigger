trigger PartnerInvolvementTrigger on Partner_Involvement__c (before insert, before update, before delete) {
    
    PartnerInvolvementTriggerAction action = new PartnerInvolvementTriggerAction();
    
    if(trigger.isDelete){
        action.removeBillingPartner(trigger.old);
        action.deletionNotification(trigger.old);
        action.removeSupportPartner(trigger.old);
    }

}