/*
 * ContractTriggerHandler
 * @author: Naomi Harmon
 * @last modified: 10/2019
 * @related classes: ContractTriggerHandler
 * @test class(es): ContractTriggerTest, ContractTermsUpdateTest
*/

public class ContractTriggerAction {
    
    //-------------------------------------------------------------------------------------------
    //Method to populate Contract Terms on new Contract from originating Quote and/or Opportunity
    //Method is future method to prevent hitting CPU errors due to CPQ automation
    //@author: Naomi Harmon
    //@created date: 
    //@last modified: 10/2019
    //@last modified notes: Moved to future method and logic updated to support future method. 
    
    @future
    public static void populateContractTerms(Id contractId, Id quoteId){
        
        List<Contract_Term_Mappings__c> fieldMappings = [Select Mirror_Field_API_Name__c, Update__c, From_Object_API_Name__c, From_Field_API_Name__c
                                                         From Contract_Term_Mappings__c
                                                         Where Active__c = true];
        
        //Get all fields from Quote that are mapped in custom setting "Contract Term Mappings"
        String query = 'SELECT Id, ';
        for(Contract_Term_Mappings__c mapping : fieldMappings)
        {
            if(mapping.From_Object_API_Name__c == 'SBQQ__Quote__c'){
                String theName = mapping.From_Field_API_Name__c;
            query += theName + ',';
            }
            if(mapping.From_Object_API_Name__c != 'SBQQ__Quote__c'){
                String theName = mapping.From_Object_API_Name__c + '.' + mapping.From_Field_API_Name__c;
                query += theName + ',';
            }
        }
        // Trim last comma
        query = query.subString(0, query.length() - 1);
        // Finalize query string
        query += ' FROM SBQQ__Quote__c Where Id = \'' + quoteId + '\' Limit 1';
        //End get all fields
        
        SBQQ__Quote__c q;
        try{
            q = Database.query(query);
        } catch (Exception e){
            System.debug('Exception caught while querying for Quote - ' + e.getMessage());
        }
        
         //Get all fields from Contract that are mapped in custom setting "Contract Term Mappings"
        String cQuery = 'SELECT Id, Account.OwnerId, Account.Customer_Success_Advocate__c, ';
        
        for(Contract_Term_Mappings__c mapping : fieldMappings)
        {
            String theName = mapping.Mirror_Field_API_Name__c;
            cQuery += theName + ',';
        }
        cQuery = cQuery.subString(0, cQuery.length() - 1);
        cQuery += ' FROM Contract Where Id = \'' + contractId + '\' Limit 1';
        
        Contract c;
        try{
            c = Database.query(cQuery);
        } catch (Exception e){
            System.debug('Exception caught while querying for Contract - ' + e.getMessage());
        }
        
        //Get values for all mapped fields
        for(Contract_Term_Mappings__c ctm : fieldMappings){
            if(ctm.From_Object_API_Name__c == 'SBQQ__Quote__c'){
                if(c.get(ctm.Mirror_Field_API_Name__c) == null && q.get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                }
                if(c.get(ctm.Mirror_Field_API_Name__c) != null && ctm.Update__c == true && q.get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                } 
            }
            if(ctm.From_Object_API_Name__c != 'SBQQ__Quote__c'){
                if(c.get(ctm.Mirror_Field_API_Name__c) == null && q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                }
                if(c.get(ctm.Mirror_Field_API_Name__c) != null && ctm.Update__c == true && q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c) != null){
                    try{
                        c.put(ctm.Mirror_Field_API_Name__c, q.getSObject(ctm.From_Object_API_Name__c).get(ctm.From_Field_API_Name__c));
                    } catch (Exception e){
                        System.debug('Error caught while trying to update Contract Id: ' + c.Id + ' - ' + e.getMessage());
                    }
                } 
            }
        }
        if(c != null){
            if(c.Account.Customer_Success_Advocate__c != null){
              c.OwnerId = c.Account.Customer_Success_Advocate__c;  
            } else { c.OwnerId = c.Account.OwnerId; }
        c.Status = 'Activated';
        //c.SBQQ__RenewalForecast__c = true;
        //c.Process_SaaS__c = true;
        c.Create_Booking__c = true;
        update c;  
        }
    }
    //-------------------------------------------------------------------------------------------
    
    //-------------------------------------------------------------------------------------------
    //Method to carry over Contract Terms from preview Contract to new Contract, when a user prompts it to happen
    //(custom button is pushed which checks a checkbox)
    //@author: Naomi Harmon
    //@created date: 
    //@last modified: 10/2019
    //@last modified notes: Updated to accept list of Contracts rather than one at a time
    
    public static void carryOverContractTerms(Map<Id, Contract> oldMap, Map<Id, Contract> newMap, List<Contract> newList){
        
        Map<Contract, Id> contractToOriginalIdMap = new Map<Contract, Id>();
        Map<Id, Contract> originalContractsMap = new Map<Id, Contract>();
        Map<Contract, Contract> newToOriginalContractMap = new Map<Contract, Contract>();
        Set<Id> originalContractIds = new Set<Id>();
        
        //Automatically carry over maintenance dates on new Contracts from renewals
        if(newList != null){
            for(Contract c : newList){
                String renewedContractId = c.Renewed_Contract_Id__c;
                if(renewedContractId != null){ originalContractIds.add(renewedContractId); }
                contractToOriginalIdMap.put(c, renewedContractId);
            }
            
            List<Contract> originalContracts = new List<Contract>();
            if(originalContractIds.size()>0){
                originalContracts = [Select Id, Maintenance_Start_Date__c, Maintenance_End_Date__c
                                                    From Contract
                                                    Where Id IN :originalContractIds];
            }
            
            for(Contract oc : originalContracts){
                originalContractsMap.put(oc.Id, oc);
            }
            
            for(Contract nc : contractToOriginalIdMap.keySet()){
                //The contractToOriginalIdMap = New Contract, Renewed Contract Id
                //The originalContractsMap = Renewed Contract Id, Renewed Contract
                //Getting... newToOriginalContractMap = New Contract, Renewed Contract
                newToOriginalContractMap.put(nc, originalContractsMap.get(contractToOriginalIdMap.get(nc)));
            }
            
            for(Contract nc : newToOriginalContractMap.keySet()){
                //For each Contract, loop through and set maintenance dates
                Contract oc = newToOriginalContractMap.get(nc);
                if(oc != null){ 
                    if((Date)oc.get('Maintenance_Start_Date__c') != null){ nc.Maintenance_Start_Date__c = (Date)oc.get('Maintenance_Start_Date__c'); }
                    if((Date)oc.get('Maintenance_End_Date__c') != null  ){ nc.Maintenance_End_Date__c = (Date)oc.get('Maintenance_End_Date__c'); }
                }
            }  
        }
        
        //Carry over additional terms from renewed Contracts when prompted
        if(newMap != null){
            for(Id key : newMap.keySet()){
            if(oldMap.get(key).Carry_Over_Original_Terms__c == false && newMap.get(key).Carry_Over_Original_Terms__c == true){
                try{
                    String renewedContractId = newMap.get(key).Renewed_Contract_Id__c;                
                    if(renewedContractId != null){ originalContractIds.add(renewedContractId); }
                    contractToOriginalIdMap.put(newMap.get(key), renewedContractId);
                } catch (Exception e){
                    System.debug('Exception caught while looking for Renewed Contract');
                }
            }
        }
 
        
        //Get all mapped fields from original Contracts  
        System.debug('Original Contract Ids = ' + originalContractIds);  
        if(originalContractIds.size()>0){
            List<Contract> originalContracts;
            String query = 'SELECT Id ';
            
            for(Schema.FieldSetMember f : Schema.SObjectType.Contract.fieldSets.getMap().get('Contract_Term_Fields_To_Carry_to_Renewed').getFields()){                          
                query += ',' + f.getFieldPath();
            }
            query += ' FROM Contract WHERE Id IN :originalContractIds';
            originalContracts = Database.query(query);
            
            for(Contract oc : originalContracts){
                originalContractsMap.put(oc.Id, oc);
            }
            
            for(Contract nc : contractToOriginalIdMap.keySet()){
                //The contractToOriginalIdMap = New Contract, Renewed Contract Id
                //The originalContractsMap = Renewed Contract Id, Renewed Contract
                //Getting... newToOriginalContractMap = New Contract, Renewed Contract
                newToOriginalContractMap.put(nc, originalContractsMap.get(contractToOriginalIdMap.get(nc)));
            }
            
            //Now loop through Contract map
            for(Contract nc : newToOriginalContractMap.keySet()){
                //For each Contract, loop through all Contract Term fields to evaluate and set
                Contract oc = newToOriginalContractMap.get(nc);
                for(Schema.FieldSetMember f : Schema.SObjectType.Contract.fieldSets.getMap().get('Contract_Term_Fields_To_Carry_to_Renewed').getFields()){
                    if(nc.get(f.getFieldPath()) == null && oc.get(f.getFieldPath()) != null){ 
                        nc.put(f.getFieldPath(), oc.get(f.getFieldPath())); 
                    }
                }  
            }  
        }
    }
    }
    //-------------------------------------------------------------------------------------------

}