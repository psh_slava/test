@isTest

public class SignatureConfirmationtoOpportunityTest {
    
    static testMethod void testEmailHandler(){
        
        //Create new Opportunity (and all auxillary records) for test
        Account a = new Account();
        a.Name = 'test account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Total_Contract_Value__c = 100000;
        o.PSO_Total_Amount__c = 0;
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Discovery';
        o.Primary_Competitor__c = 'Other';
        o.Forecast_Category__c = 'Pipeline';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        //Test inbound email with correct Id
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'Contract Signed and Fully Executed for "Order Confirmation with CPQ Testing Becky Account"';
        email.plainTextBody = 
            'Cherwell Software '+
            'Hi Becky Brown,' +
            'We are writing to confirm that an Order Confirmation with Very Good Building Co. has been completed. '+
            '(SFDC Opportunity: '+ o.Id + ') The executed OC is available in the Ironclad Workflow, here: '+
            'https://preview.ironcladapp.com/workflow/5c355ec66e3a233171507d44.'+
            'Thanks, '+
            'The Ironclad Team'; 
        env.fromAddress = 'preview@ironcladapp.com';
        
        SignatureConfirmationtoOpportunity scto = new SignatureConfirmationtoOpportunity();
        scto.handleInboundEmail(email, env);
        
        //Assert that the Opportunity was updated with the correct values
        Opportunity opp = [Select Id, Ironclad_Workflow_URL__c, Ironclad_Workflow_Status__c From Opportunity Where Id = :o.Id];
        System.assertEquals('Signed', opp.Ironclad_Workflow_Status__c);
        System.debug('Ironclad Workflow Status----' + opp.Ironclad_Workflow_Status__c);
        System.assertEquals('https://preview.ironcladapp.com/workflow/5c355ec66e3a233171507d44', opp.Ironclad_Workflow_URL__c);
        System.debug('Ironclad Workflow URL----' + opp.Ironclad_Workflow_URL__c);
    }

}