/*
* @class name:   InitiateOCRedirectControllerTest
* @created:      By Naomi Harmon in Sep 2020
*
*/

@isTest
public class InitiateOCRedirectControllerTest {
    
    @testSetup static void setup(){
        Account a = new Account();
        a.Name = 'test account';
        a.BillingCity = 'Testville';
        a.BillingStreet = 'street';
        a.BillingCountry = 'United Kingdom';
        a.BillingPostalCode = '11111';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        c.Email = 'test@test.test';
        insert c;
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Total_Contract_Value__c = 100000;
        o.PSO_Total_Amount__c = 0;
        o.Name = 'testopp';
        o.CloseDate = System.today();
        o.StageName = 'Finalize';
        o.Forecast_Category__c = 'Commit';
        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole();
        cr.ContactId = c.Id;
        cr.OpportunityId = o.Id;
        cr.IsPrimary = true;
        insert cr;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__PrimaryContact__c = c.Id;
        q.SBQQ__Account__c = a.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.Billing_Contact__c = c.Id;
        q.License_Key_Contact__c = c.Id;
        q.PO_Required__c = 'No';
        q.Tax_Exempt__c = 'No';
        q.Term__c = '3';
        q.SBQQ__BillingCity__c = 'Colorado Springs';
        q.SBQQ__BillingCountry__c = 'United States';
        q.SBQQ__BillingStreet__c = '123 Test Street';
        q.SBQQ__BillingPostalCode__c = '12345';
        q.SBQQ__BillingState__c = 'Colorado';
        q.SBQQ__BillingName__c = 'Test Account';
        q.Regional_Quote_Terms__c = 'North America';
        insert q;      
    }
    
     public static testmethod void testSuccess() {
        
        SBQQ__Quote__c q = [Select Id, Name, Convert_to_Order_Confirm__c, SBQQ__Primary__c, CreatedDate From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = true;
        q.Convert_to_Order_Confirm__c = true;
        q.Convert_to_Order_Confirmation_Timestamp__c = System.now().addDays(-1);
        System.debug('Quote created ----'+ q.CreatedDate + '----- timestamp ----' + q.Convert_to_Order_Confirmation_Timestamp__c);
        update q;
        
        Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        InitiateOCRedirectController spfc = new InitiateOCRedirectController(sc);
        
        PageReference pageRef = Page.InitiateOCRedirect;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        Test.setCurrentPage(pageRef);
       
        spfc.evaluateOC();
        Test.stopTest();
    }

    public static testmethod void testNotOC() {
        
        SBQQ__Quote__c q = [Select Id, Name, Convert_to_Order_Confirm__c, SBQQ__Primary__c From SBQQ__Quote__c Limit 1];
        q.Convert_to_Order_Confirm__c = false;
        update q;
        
        Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        InitiateOCRedirectController spfc = new InitiateOCRedirectController(sc);
        
        PageReference pageRef = Page.InitiateOCRedirect;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        Test.setCurrentPage(pageRef);
       
        spfc.evaluateOC();
        Test.stopTest();   
    }
    
    public static testmethod void testNotPrimary() {
        
        SBQQ__Quote__c q = [Select Name, Convert_to_Order_Confirm__c, SBQQ__Primary__c, Id From SBQQ__Quote__c Limit 1];
        q.SBQQ__Primary__c = false;
        q.Convert_to_Order_Confirm__c = true;
        update q;
        
        Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
        InitiateOCRedirectController spfc = new InitiateOCRedirectController(sc);
        
        PageReference pageRef = Page.InitiateOCRedirect;
        pageRef.getParameters().put('id', String.valueOf(q.Id));
        Test.setCurrentPage(pageRef);
       
        spfc.evaluateOC();
        Test.stopTest();   
    }
}