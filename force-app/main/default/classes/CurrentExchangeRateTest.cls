@isTest
public class CurrentExchangeRateTest {
    
    public static testmethod void testRates(){
        
        Account a = new Account(Name = 'Test Account');
        insert a;
        
        Opportunity o = new Opportunity(Name = 'Test Opp', StageName = 'Pre-Qualification', AccountId = a.Id, CloseDate = Date.today());
        insert o;
    }
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    public static testmethod void testBatch(){
        
        Account a = new Account();
        a.Name = 'Cherwell Test Account';
        a.CurrencyIsoCode = 'CAD';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Wilburson';
        c.AccountId = a.Id;
        c.Email = 'test@test.com';
        insert c;
        
        Opportunity o = new Opportunity();
        o.Name = 'test opp';
        o.CloseDate = Date.today().addDays(15);
        o.StageName = 'Discover';
        o.Forecast_Category__c = 'Omit';
        o.AccountId = a.Id;
        o.Primary_Contact__c = c.Id;
        o.CurrencyIsoCode = 'CAD';
        insert o;
        
        Test.startTest();
        String jobId = System.schedule('ScheduleCurrentExchangeRateBatchTest', CRON_EXP, new CurrentExchangeRateBatchScheduler());
        
        CurrentExchangeRateBatch tb = new CurrentExchangeRateBatch();
        Database.executebatch(tb);
        
        Test.stopTest();
        
    }

}