@isTest
private class LicenseKeyGeneratorTests {
    
    @testSetup static void setup(){
        Account a = new Account();
        a.Name = 'Test Account';
        a.Type = 'Customer';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'testerson';
        c.AccountId = a.Id;
        c.Email = 'tester@test.com';
        insert c;
        
        Contact c2 = new Contact();
        c2.LastName = 'testerson';
        c2.AccountId = a.Id;
        c2.Email = 'secondtester@test.com';
        insert c2;
        
        AccountContactRole acr = new AccountContactRole();
        acr.AccountId = a.Id;
        acr.ContactId = c.Id;
        acr.Role = 'License Key Contact';
        insert acr;
        
        AccountContactRole acr2 = new AccountContactRole();
        acr2.AccountId = a.Id;
        acr2.ContactId = c2.Id;
        acr2.Role = 'License Key Contact';
        insert acr2;
        
        CSM_License_Record__c testKey = new CSM_License_Record__c();
        testKey.Current_Key__c = 'Yes';
        testKey.Maintenance__c = 'Instance 1';
        testKey.License_Name__c = 'TestCompany';
        testKey.Type__c = 'Subscription';
        testKey.Deal_Type_Manual__c = 'Direct';
        testKey.Purchase_Scenario2__c = 'New Logo';
        testKey.Primary_Contact__c = c.Id;
        testKey.Expires__c = Date.newInstance(2018,1,31);
        testKey.Account__c = a.Id;
        testKey.Hosted__c = true;
        insert testKey;
        
        Opportunity o = new Opportunity();
        o.CloseDate = Date.Today();
        o.StageName = 'Qualified';
        o.AccountId = a.Id;
        o.Name = 'Test Opp';
        o.Forecast_Category__c = 'Pipeline';
        o.Primary_Competitor__c = 'IBM';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__StartDate__c = Date.today();
        q.Prepaid_Fees__c = 'Both';
        q.SBQQ__Account__c = a.Id; 
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__Primary__c = true;
        q.Term__c = '3';
        q.License_Key_Contact__c = c.Id;
        insert q;
        
        o.StageName = 'Closed - Won';
        o.Close_Reason__c = 'Cost/Price';
        o.Forecast_Category__c = 'Won';
        o.SBQQ__PrimaryQuote__c = q.Id;
        update o;
        
        Contract cnt = new Contract();
        cnt.StartDate = Date.today().addDays(365);
    	cnt.ContractTerm = 36;
        cnt.AccountId = a.Id;
        cnt.Status = 'Draft';
        //cnt.SBQQ__Opportunity__c = o.Id;
        insert cnt;
        
        cnt.SBQQ__Opportunity__c = o.Id;        
        cnt.Status = 'Activated';
        update cnt;
        
        Product2 p = new Product2();
        p.Name = 'CSM Subscription License';
        insert p;
        
        SBQQ__Subscription__c s = new SBQQ__Subscription__c();
        s.SBQQ__Quantity__c = 10;
        s.SBQQ__Product__c = p.Id;
        s.SBQQ__Account__c = a.Id;
        s.SBQQ__Contract__c = cnt.Id;
        s.Instance_Number__c = 'Instance 1';
        s.Instance_Name__c = 'TestInst';
        s.SBQQ__Bundle__c = true;
        insert s;   
        
        SBQQ__Subscription__c s2 = new SBQQ__Subscription__c();
        s2.SBQQ__Quantity__c = 15;
        s2.SBQQ__Product__c = p.Id;
        s2.SBQQ__Account__c = a.Id;
        s2.SBQQ__Contract__c = cnt.Id;
        s2.Instance_Number__c = 'Instance 1';
        s2.Instance_Name__c = 'TestInst';
        s2.SBQQ__Bundle__c = true;
        insert s2; 
    }
    
    //LicenseKeyExpirationBatch test
    @isTest static void testExpiredKeysandTrigger(){ 
        Test.startTest();
        String chron = '0 0 23 * * ?';
        LicenseKeyExpirationBatch lkeb = new LicenseKeyExpirationBatch();
        System.schedule('LicenseKeyExpirationBatch'+system.now(), chron, lkeb);
        Database.executeBatch(lkeb);
        Test.stopTest();
        
        CSM_License_Record__c testSuccess = [Select Current_Key__c From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        System.assertEquals('No', testSuccess.Current_Key__c);
    }
    
    //CSMLicenseRecordTrigger & LicenseKeyTriggerHandler tests
    //burstKeyEffect test
    
    @isTest static void testBurstKey(){
        CSM_License_Record__c originalKey = [Select Id, Account__c, Primary_Contact__c From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        
        CSM_License_Record__c burstKey = new CSM_License_Record__c();
        burstKey.Current_Key__c = 'Yes';
        burstKey.Maintenance__c = 'Instance 1';
        burstKey.License_Name__c = 'TestBurst';
        burstKey.Type__c = 'Burst!';
        burstKey.Deal_Type_Manual__c = 'Direct';
        burstKey.Purchase_Scenario2__c = 'Upsell';
        burstKey.Primary_Contact__c = originalKey.Primary_Contact__c;
        burstKey.Expires__c = Date.newInstance(2018,2,5);
        burstKey.Account__c = originalKey.Account__c;
        burstKey.Hosted__c = true;
        insert burstKey;
        
        CSM_License_Record__c testFlip = [Select Current_Key__c FROM CSM_License_Record__c Where Id = :originalKey.Id];
        SYstem.assertEquals('No', testFlip.Current_Key__c);  
    }
    
    @isTest static void testmakeLKGCallout() {
        Contract ct = [Select Id, AccountId, Account.Name From Contract Limit 1];
        
        Test.startTest();
        PageReference pageRef = Page.LicenseKeyGenerator;
        PageRef.getParameters().put('id', String.valueOf(ct.Id));
        PageRef.getParameters().put('accountid', String.valueOf(ct.AccountId));
        Test.setCurrentPage(pageRef);
        
        LicenseKeyGeneratorController lkgc = new LicenseKeyGeneratorController();
        Test.setMock(HttpCalloutMock.class, new LicenseKeyGenCalloutMock());
        //lkgc.addLicenseKey();
        lkgc.saveKeys();
        CSM_License_Record__c lkr = [Select Id From CSM_License_Record__c Limit 1];
        
        Test.stopTest();
    }
    
    //issueRenewal test
    @isTest static void issueRenewalTest(){
        CSM_License_Record__c originalKey = [Select Id, Current_Key__c, Primary_Contact__c, Account__c 
                                             From CSM_License_Record__c Where License_Name__c = 'TestCompany'];

        CSM_License_Record__c renewalKey = new CSM_License_Record__c();
        renewalKey.Current_Key__c = 'Yes';
        renewalKey.Maintenance__c = 'Instance 1';
        renewalKey.License_Name__c = 'TestRenewal';
        renewalKey.Type__c = 'Subscription';
        renewalKey.Deal_Type_Manual__c = 'Direct';
        renewalKey.Purchase_Scenario2__c = 'Renewal';
        renewalKey.Renewal_Start_Date_Manual__c = Date.today();
        renewalKey.Former_Key__c = originalKey.Id;
        renewalKey.Primary_Contact__c = originalKey.Primary_Contact__c;
        renewalKey.Expires__c = Date.newInstance(2019,2,5);
        renewalKey.Account__c = originalKey.Account__c;
        renewalKey.Hosted__c = true;
        insert renewalKey;
        
        CSM_License_Record__c testIssue = [Select Current_Key__c, Renewal_Key__c 
                                             From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        System.assertEquals(testIssue.Renewal_Key__c, renewalKey.Id);
    }
    
    //makeFormerKey test
       @isTest static void issueNewTest(){
        CSM_License_Record__c originalKey = [Select Id, Current_Key__c, Primary_Contact__c, Account__c 
                                             From CSM_License_Record__c Where License_Name__c = 'TestCompany'];

        CSM_License_Record__c upsellKey = new CSM_License_Record__c();
        upsellKey.Current_Key__c = 'Yes';
        upsellKey.Maintenance__c = 'Instance 1';
        upsellKey.License_Name__c = 'TestUpsell';
        upsellKey.Type__c = 'Subscription';
        upsellKey.Deal_Type_Manual__c = 'Direct';
        upsellKey.Purchase_Scenario2__c = 'Upsell';
        upsellKey.Former_Key__c = originalKey.Id;
        upsellKey.Primary_Contact__c = originalKey.Primary_Contact__c;
        upsellKey.Expires__c = Date.newInstance(2019,2,5);
        upsellKey.Account__c = originalKey.Account__c;
        upsellKey.Hosted__c = true;
        insert upsellKey;
        
        CSM_License_Record__c testIssue = [Select Current_Key__c 
                                             From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        System.assertEquals(testIssue.Current_Key__c, 'No');
    }
    
    //LicenseKeyContacts test
    @isTest static void testLKCs(){
        CSM_License_Record__c originalKey = [Select Id, Current_Key__c, Primary_Contact__c, Account__c 
                                             From CSM_License_Record__c Where License_Name__c = 'TestCompany'];
        Account pa = new Account();
        pa.Name = 'Partner Account';
        pa.Type = 'Partner';
        pa.Authorized_Partner__c = true;
        pa.Reseller_Partner__c = true;
        pa.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        pa.BillingCountry = 'United States';
        insert pa;
        
        Contact pc = new Contact();
        pc.LastName = 'partnerson';
        pc.Email = 'tester2@test.com';
        pc.AccountId = pa.Id;
        insert pc;
        
        Contact pc2 = new Contact();
        pc2.LastName = 'partnerson';
        pc2.Email = 'tester3@test.com';
        pc2.AccountId = pa.Id;
        insert pc2;
        
        AccountContactRole pacr = new AccountContactRole();
        pacr.AccountId = pa.Id;
        pacr.ContactId = pc.Id;
        pacr.Role = 'License Key Contact';
        insert pacr;
        
        AccountContactRole pacr2 = new AccountContactRole();
        pacr2.AccountId = pa.Id;
        pacr2.ContactId = pc2.Id;
        pacr2.Role = 'License Key Contact';
        insert pacr2;
        
        CSM_License_Record__c partnerKey = new CSM_License_Record__c();
        partnerKey.Current_Key__c = 'Yes';
        partnerKey.Maintenance__c = 'Instance 1';
        partnerKey.License_Name__c = 'TestPartner';
        partnerKey.Type__c = 'Subscription';
        partnerKey.Deal_Type_Manual__c = 'Reseller Partner';
        partnerKey.Reseller_Partner_Account__c = pa.Id;
        partnerKey.Partner_Contact__c = pc.Id;
        partnerKey.Purchase_Scenario2__c = 'Upsell';
        partnerKey.Primary_Contact__c = originalKey.Primary_Contact__c;
        partnerKey.Expires__c = Date.newInstance(2019,2,5);
        partnerKey.Account__c = originalKey.Account__c;
        partnerKey.Hosted__c = true;
        insert partnerKey;    
        
        CSM_License_Record__c testEmails = [Select License_Key_Contact_Email_2__c, Partner_Contact_Email_2__c FROM CSM_License_Record__c
                                            Where Id = :partnerKey.Id];
        System.assertEquals(testEmails.License_Key_Contact_Email_2__c,'secondtester@test.com');
        System.assertEquals(testEmails.Partner_Contact_Email_2__c,'tester3@test.com');
    }
    
    //addKeys test
    @isTest static void addKeys(){
        Contract ct = [Select Id, AccountId, Account.Name From Contract Limit 1];
        
        Test.startTest();
        PageReference pageRef = Page.LicenseKeyGenerator;
        PageRef.getParameters().put('id', String.valueOf(ct.Id));
        PageRef.getParameters().put('accountid', String.valueOf(ct.AccountId));
        Test.setCurrentPage(pageRef);
        
        LicenseKeyGeneratorController lkgc = new LicenseKeyGeneratorController();
        Test.setMock(HttpCalloutMock.class, new LicenseKeyGenCalloutMock());
        lkgc.addLicenseKey();
        PageRef.getParameters().put('index', String.valueOf(2));
        lkgc.deleteRow();
        lkgc.goSuccess();
       // lkgc.saveKeys();
       // CSM_License_Record__c lkr = [Select Id From CSM_License_Record__c Limit 1];
        
        Test.stopTest();
    }
}