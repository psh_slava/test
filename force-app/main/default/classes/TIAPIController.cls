public class TIAPIController {
    @future(callout=true)
    public static void TIAPIController(Id contactId, String productSKU, String accountName, String userType, String accountId){
        
        String freeAccessLicenseId;
        
        Account a = [Select Id, TI_Manager_SKU__c, TI_Employee_SKU__c
                     From Account
                     Where Id = :accountId];
        
        TI_API_Settings__c settings = [Select Customer_ClientId__c, Partner_ClientId__c, Employee_ClientId__c, Default_Customer_License_Id__c
                                       From TI_API_Settings__c];
        String clientId;
        if(userType == 'customer'){
            clientId = settings.Customer_ClientId__c;
            freeAccessLicenseId = settings.Default_Customer_License_Id__c;
        }
        if(userType == 'employee'){
            clientId = settings.Employee_ClientId__c;
        }
        if(userType == 'partner'){
            clientId = settings.Partner_ClientId__c;
        }
        
        //Look for existing SKU
        System.debug('Look for existing SKU.....');
        TIAPIWrapper.licenseWrapper lw = new TIAPIWrapper.licenseWrapper();
        lw = TIAPIWrapper.getClientLicense(clientId, accountName, productSKU);
        
        TIAPIWrapper.licenseWrapper lw2 = new TIAPIWrapper.licenseWrapper();
        if(lw.employeeLicenseId == null){
            //If doesn't exist, create SKU hierachy for Company and Employee under Product SKU
            System.debug('Initiating create SKUs...');
            lw2 = TIAPIWrapper.createSKUs(clientId, lw);
            System.debug(lw2);
        }
        if(lw.employeeLicenseId != null && lw.companyParentLicenseId == freeAccessLicenseId){
            //If does exist under Free Access, re-parent Company sublicense to new Training Product SKU
            System.debug('Initiating re-parent SKU...');
            lw2 = TIAPIWrapper.reparentSKUs(accountName, productSKU, lw);
            System.debug(lw2);
        }
        
        //Push new SKU ids to Account in Salesforce
        if(lw2 != null){
            if((a.TI_Employee_SKU__c != lw2.employeeLicenseId || a.TI_Manager_SKU__c != lw2.companyLicenseId) && lw2.employeeLicenseId != null && lw2.companyLicenseId != null){
                System.debug('Pushing new SKU ids to Account....');
                a.TI_Employee_SKU__c = lw2.employeeLicenseId;
                a.TI_Manager_SKU__c = lw2.companyLicenseId;
                update a;
            } else { System.debug('No new SKU ids needed on Account....'); }  
        }
    }
    @future(callout=true)
    public static void TICallout(Map<Id,String> courseMap, Map<Id,String> bundleMap, Map<Id,String> lpathMap){
        System.debug('courseMap >>>>' + courseMap);
        System.debug('bundleMap >>>>' + bundleMap);
        System.debug('lpathMap >>>>' + lpathMap);
        TIAPIWrapper.updateTIUsers(courseMap, bundleMap, lpathMap, null); 
    }
}