public with sharing class budgetSubmitToCloseController {

    public pse__Budget__c Budget{get; set;}
    public pse__Budget__c Budgetrecord{get; set;}
    ApexPages.StandardController standardController;
    
    public budgetSubmitToCloseController(ApexPages.StandardController controller) 
    {
      This.Budget = (pse__Budget__c)controller.getRecord();
      Budgetrecord = [Select id,TimecardOpen__c from pse__Budget__c where id =:Budget.id];
    }

     public PageReference doupdate(){
     
     IF(Budgetrecord.TimecardOpen__c != 0 && Budgetrecord.TimecardOpen__c != Null)
     {
        ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,'Budget contains Open Timecards. All Timecards must be Closed before Project can be closed.' );
        ApexPages.addmessage(msg);
       //Budgetrecord.Adderror('Budget contains Open Timecards. All Timecards must be Closed before Project can be closed.');
        PageReference pr = new PageReference('/'+Budgetrecord.id);
       // pr.setRedirect(false);
       // return pr; 
     }
     else
     {
        Budgetrecord.pse__Status__c='Submitted for Close';
        System.debug(' Budgetrecord.pse__Status__c'+ Budgetrecord.pse__Status__c);
        Update Budgetrecord;
        System.debug(' Budgetrecord.pse__Status__c'+ Budgetrecord.pse__Status__c);
        PageReference pr = new PageReference('/'+Budgetrecord.id);
        pr.setRedirect(False);
        return pr;
     }
        return Null; 
     
     }
}