public class TestDataFactory {
    //New Methods
    public static Account createAccount(Boolean doInsert, String aName){
        Account aNew = New Account(name = aName);      
        if(doInsert)
            insert aNew;
        return aNew;
    }
    public static Opportunity createOpportunity(Boolean doInsert, String oName, Account aNew){
        Opportunity oNew = New Opportunity(name = oName, CloseDate = System.Date.today(), AccountId = aNew.id, Amount = 1);  
        oNew.StageName = '  Pre-Qualification';
        oNew.Type = 'New Business';
        if(doInsert)
            insert oNew;
        return oNew;
    }
    public static Contact createContact(Boolean doInsert, String cFirstName, String cLastName, Account aNew){
        Contact cNew = New Contact(FirstName = cFirstName, LastName = cLastName, Accountid = aNew.id);      
        if(doInsert)
            insert cNew;
        return cNew;
    }
}