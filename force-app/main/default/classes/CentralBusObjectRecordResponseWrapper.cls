// Structure to hold the response data from a business object record save request.  
// Also encapsulates specific parsing logic.
public class CentralBusObjectRecordResponseWrapper
{
	public class FieldValidationErrorWrapper {
		public String error;
		public String errorCode;
		public String fieldId;
	}

	public String busObPublicId;
	public String busObRecId;
	public List<FieldValidationErrorWrapper> fieldValidationErrors;
	public String errorCode;
	public String errorMessage;
	public Boolean hasError;

	public static CentralBusObjectRecordResponseWrapper parseJSON(String jsonStr) {
		CentralBusObjectRecordResponseWrapper borw = (CentralBusObjectRecordResponseWrapper)JSON.deserialize(jsonStr, CentralBusObjectRecordResponseWrapper.class);
		return borw;
	}
}