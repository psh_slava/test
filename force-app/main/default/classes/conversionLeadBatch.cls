global class conversionLeadBatch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'Select Id, Matched_Account_Id__c From Lead where Matched_Account_Id__c != null And IsConverted = false';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<lead> scope)

    {
        System.debug('Test'+scope.size()); 
        System.debug('Test id =='+scope);
         LeadStatus convertStatus = [Select Id, MasterLabel 
                           From LeadStatus
                           Where IsConverted = true
                           Limit 1];
         List<Database.LeadConvert> myConversions = new List<Database.LeadConvert>();
 
        For(Lead l : scope){
         System.debug('Test id =='+ l.id);
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setAccountId(l.Matched_Account_Id__c);
            lc.setLeadId(l.Id);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(true);
            myConversions.add(lc);
        }

         Database.LeadConvertResult[] lcrs = Database.convertLead(myConversions, false);
         System.debug(lcrs);
         Set<id> contactidlst = New Set<id>();
         For(Database.LeadConvertResult lcr : lcrs)
         {
           Id cId = lcr.getContactId();
           Boolean success = lcr.isSuccess();
           IF(success = True && cId != Null)
           {
             contactidlst.Add(cId);
           }
         }
         
         If(contactidlst.Size()>0)
         {
            List<contact> lstcontact = [Select id,Do_Not_Sync_with_CSM_Manual__c,Reference__c,Name from contact where id In : contactidlst];
            Map<Id,contact> updatelstcontact = New Map<Id,contact>();
            Set<string> ContactName = New Set<String>();
            For(contact c : lstcontact)
            {
              c.Do_Not_Sync_with_CSM_Manual__c = True;
              c.Reference__c = True;
              ContactName.Add(C.name);
              updatelstcontact.Put(C.id,C);
            }
            List<contact> oldcontacts = [Select Id,Reference__c,Name from contact where Name In :ContactName And Id NoT IN :contactidlst];
            For(contact c:oldcontacts)
            {
              c.Reference__c = True;
              updatelstcontact.Put(C.id,C);
            }
            If(updatelstcontact.Size()>0)
            {
              Update updatelstcontact.Values();
            }
         }
         
    }  
    global void finish(Database.BatchableContext BC)
    {
      
    }
}