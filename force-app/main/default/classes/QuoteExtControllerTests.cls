@isTest
private class QuoteExtControllerTests {
    
    testMethod static void testSubmit() {
        User m = new User();
        m.LastName = 'Manager';
        m.Alias = 'mtest';
        m.CommunityNickname = 'manager';
        m.Email = 'testmanager@cherwell.test';
        m.Username = 'testmanager@cherwell.test';
        m.TimeZoneSidKey = 'America/Los_Angeles';
    	m.EmailEncodingKey = 'UTF-8';
     	m.LanguageLocaleKey = 'en_US';
     	m.LocaleSidKey = 'en_US';
        m.ProfileId = UserInfo.getProfileId();
        insert m;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__SalesRep__c = m.id;
        insert quote;
        
        Test.startTest();
        QuoteExtController con = new QuoteExtController(new ApexPages.StandardController(quote));
        con.onSubmit();
        quote = [SELECT ApprovalStatus__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        Test.stopTest();
        
        System.assertEquals('Approved', quote.ApprovalStatus__c);
    }
    
    testMethod static void testRecall() {
        User m = new User();
        m.LastName = 'Manager';
        m.Alias = 'mtest';
        m.CommunityNickname = 'manager';
        m.Email = 'testmanager@cherwell.test';
        m.Username = 'testmanager@cherwell.test';
        m.TimeZoneSidKey = 'America/Los_Angeles';
    	m.EmailEncodingKey = 'UTF-8';
     	m.LanguageLocaleKey = 'en_US';
     	m.LocaleSidKey = 'en_US';
        m.ProfileId = UserInfo.getProfileId();
        insert m;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.SBQQ__SalesRep__c = m.id;
        insert quote;
        
        Test.startTest();
        QuoteExtController con = new QuoteExtController(new ApexPages.StandardController(quote));
        con.onRecall();
        quote = [SELECT ApprovalStatus__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        Test.stopTest();
        
        System.assertEquals('Recalled', quote.ApprovalStatus__c);
    }
}