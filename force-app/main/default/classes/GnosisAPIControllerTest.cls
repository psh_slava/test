@isTest
public class GnosisAPIControllerTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    public static testMethod void testLMSAPI() {
        Gnosis_API_Settings__c settings = new Gnosis_API_Settings__c();
        settings.API_Key__c = 'testaccesskey';
        settings.Endpoint_URL__c = 'testurl.test.test/';
        settings.Name = 'Sandbox'; 
        insert settings;
        
        Account a = new Account();
        a.Name = 'Test Account';
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        a.Partner_Types__c = 'Referral Partner; Reseller Partner';
        a.Account_Status__c = 'Active';
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Bob';
        c.LastName = 'Testerson';
        c.AccountId = a.Id;
        c.Email = 'testbob@test.com';
        c.Status__c = 'Active';
        c.Learner__c = true;
        insert c;
        
        Contact c2 = new Contact();
        c2.FirstName = 'Jane';
        c2.LastName = 'Testerson';
        c2.AccountId = a.Id;
        c2.Email = 'testjane@test.com';
        c2.Status__c = 'Active';
        insert c2;
        
        Product2 p = new Product2();
        p.Name = 'Training Package (Default)';
        p.StockKeepingUnit = 'trainingtest-package';
        p.Family = 'Learning Services';
        insert p;
        
        List<Asset> assets = new List<Asset>();
        
        Asset at = new Asset();
        at.Name = 'Test Training Package Purchase';
        at.Product2Id = p.Id;
        at.AccountId = a.Id;
        at.Quantity = 1;
        at.PurchaseDate = Date.today();
        at.Number_of_Credits__c = 8;
        at.Expiry_Date__c = Date.today().addDays(365);
        at.ContactId = c.Id;
        //insert at;
        assets.add(at);
                              
        Test.startTest();
        insert at;
        Test.setMock(HttpCalloutMock.class, new GnosisAPICalloutMock());
        //c.Status__c = 'Inactive';
        //c.FirstName = 'Bobby';
        //update c;
        //a.Account_Status__c = 'Inactive';
        //update a;
        Test.stopTest();
    }
    
      public static testMethod void testLMSAPI_2() {
        Gnosis_API_Settings__c settings = new Gnosis_API_Settings__c();
        settings.API_Key__c = 'testaccesskey';
        settings.Endpoint_URL__c = 'testurl.test.test/';
        settings.Name = 'Sandbox'; 
        insert settings;
        
        Account a = new Account();
        a.Name = 'Test Account';
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        a.Partner_Types__c = 'Referral Partner; Reseller Partner';
        a.Account_Status__c = 'Active';
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Bob';
        c.LastName = 'Testerson';
        c.AccountId = a.Id;
        c.Email = 'testbob@test.com';
        c.Status__c = 'Active';
        c.Learner__c = true;
        insert c;
        
        Contact c2 = new Contact();
        c2.FirstName = 'Jane';
        c2.LastName = 'Testerson';
        c2.AccountId = a.Id;
        c2.Email = 'testjane@test.com';
        c2.Status__c = 'Active';
        insert c2;
        
        Product2 p = new Product2();
        p.Name = 'Training Package (Default)';
        p.StockKeepingUnit = 'trainingtest-package';
        p.Family = 'Learning Services';
        insert p;
        
        List<Asset> assets = new List<Asset>();
        
        Asset at = new Asset();
        at.Name = 'Test Training Package Purchase';
        at.Product2Id = p.Id;
        at.AccountId = a.Id;
        at.Quantity = 1;
        at.PurchaseDate = Date.today();
        at.Number_of_Credits__c = 8;
        at.Expiry_Date__c = Date.today().addDays(365);
        at.ContactId = c.Id;
        //insert at;
        assets.add(at);
                              
        Test.startTest();
        //insert at;
        c.Status__c = 'Inactive';
        c.FirstName = 'Bobby';
        update c;
        Test.setMock(HttpCalloutMock.class, new GnosisAPICalloutMock());
        //a.Account_Status__c = 'Inactive';
        //update a;
        Test.stopTest();
    }
    
        public static testMethod void testLMSAPI_3() {
            
        String jsonString =  '{"status": "success","message": "all good"}'; 
		//String responseJSON = JSON.createGenerator(jsonString)
		//System.debug(responseJSON);
            
        GnosisAPIResponseWrapper.parse(jsonString);
    }
}