// Structure to hold the response data from a business object request.  Also encapsulates specific
// parsing logic.
public class CSMBusinessObjectWrapper
{
	/*
	public String busObId;
	public String displayName;
	public String name;
	public Boolean supporting;
	public Boolean lookup;
	public Boolean major;

	// The following properties have illegal names (have illegal characters or
	// use reserved words) in the JSON response so we're renaming with an _x to
	// distinguish from the original properties.
	public Boolean group_x;

	public static Map<String, CSMBusinessObjectWrapper> parseJSON(String jsonStr) {
		// For most fields, we'll do safe type parsing.  So as long as the types and field names
		// match, we get automatic parsing.
		List<CSMBusinessObjectWrapper> bows = (List<CSMBusinessObjectWrapper>)JSON.deserialize(jsonStr, List<CSMBusinessObjectWrapper>.class);

		// Put in a map, keyed on name
		Map<String, CSMBusinessObjectWrapper> bowMap = new Map<String, CSMBusinessObjectWrapper>();
		for(CSMBusinessObjectWrapper bow : bows) bowMap.put(bow.name, bow);
	
		// For properties with illegal names, we'll parse manually.
		List<Object> parsedValue = (List<Object>)JSON.deserializeUntyped(jsonStr);
		for(Object pv : parsedValue) {
			Map<String, Object> bow = (Map<String, Object>)pv;
			String bowName = String.valueOf(bow.get('name'));
			bowMap.get(bowName).group_x = Boolean.valueOf(bow.get('group'));
		}

		return bowMap;
	}
	*/
	
}