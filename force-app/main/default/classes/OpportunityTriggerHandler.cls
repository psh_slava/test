/*
TEST CLASS = OpportunityTriggerTest
*/

public class OpportunityTriggerHandler {   
    
   OpportunityTriggerAction oppAction = new OpportunityTriggerAction();
   
   /*
       BEFORE UPDATE
   */
   public void beforeUpdate(Map<Id,Opportunity> oldMap,Map<Id,Opportunity> newMap){
       
       if(!RecursionController.stopOppContactRoleValidation){
       
           //VERIFY THAT THERE IS ATLEAST ONE OPPORTUNITY CONTACT ROLE ON OPPORTUNITY
           oppAction.validateOpportunityContactRoles(newMap);
       }
   }
    
}